/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tc_frontend/pages/home/bloc/home_bloc.dart';
import 'package:tc_frontend/pages/home/model/module.dart';
import 'package:tc_frontend/pages/home/model/module_list.dart';

void main() {
  group("HomePageState tests", () {
    late List<Module> randomList;

    setUp(() {
      randomList = moduleList.toList()..shuffle();
      randomList = randomList
          .map((module) => module.id % 2 == 0 ? module : module.toToggled())
          .toList();
    });

    test("Base constructor does not allow modules to be empty", () {
      expect(
        () => HomeState([]),
        kDebugMode ? throwsAssertionError : throwsArgumentError,
      );
    });
    test("fromIdList constructor does not allow idList to be empty", () {
      expect(
        () => HomeState.fromIdList([]),
        kDebugMode ? throwsAssertionError : throwsArgumentError,
      );
    });
    test("fromIdList constructor parses idList correctly", () {
      expect(
        HomeState.fromIdList(
          randomList
              .map((module) => module.id * (module.enabled ? 1 : -1))
              .toList(),
        ),
        equals(HomeState(randomList)),
      );
    });
    test("asIdList parses modules correctly", () {
      expect(
        HomeState(randomList).asIdList,
        equals(
          randomList
              .map((module) => module.id * (module.enabled ? 1 : -1))
              .toList(),
        ),
      );
    });
  });
}
