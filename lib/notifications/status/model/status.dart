/*
 * Copyright 2020-2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'status_notification.dart';

part 'status.g.dart';

@CopyWith()
@JsonSerializable(explicitToJson: true)
class Status extends Equatable {
  final bool blockApp;

  final bool hideNotificationIfUpdated;

  final bool hideHeaderLogo;

  final String latestAndroidVersion;

  final String latestIosVersion;

  final String minimumVersionRequired;

  final List<StatusNotification> entries;

  Status({
    this.blockApp = false,
    this.hideNotificationIfUpdated = false,
    this.hideHeaderLogo = false,
    this.latestAndroidVersion = "0.0.0",
    this.latestIosVersion = "0.0.0",
    this.minimumVersionRequired = "0.0.0",
    this.entries = const [],
  });

  factory Status.fromJson(Map<String, dynamic> json) => _$StatusFromJson(json);

  Map<String, dynamic> toJson() => _$StatusToJson(this);

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [
        blockApp,
        hideNotificationIfUpdated,
        hideHeaderLogo,
        latestAndroidVersion,
        latestIosVersion,
        minimumVersionRequired,
        entries,
      ];
}
