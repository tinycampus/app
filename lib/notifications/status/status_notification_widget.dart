/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';
import 'dart:io' show Platform;
import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:version/version.dart';

import '../../common/constants/api_constants.dart';
import '../../common/tc_markdown_stylesheet.dart';
import '../../common/tc_theme.dart';
import '../../common/tc_utility_functions.dart';
import '../../common/widgets/tc_button.dart';
import '../../repositories/http/http_repository.dart';
import 'model/status.dart';

class StatusNotificationWidget extends StatefulWidget {
  const StatusNotificationWidget({Key? key}) : super(key: key);

  @override
  _StatusNotificationWidgetState createState() =>
      _StatusNotificationWidgetState();
}

class _StatusNotificationWidgetState extends State<StatusNotificationWidget>
    with
        // Ignored because:
        // "When used as a mixin, provides no-op method implementations."
        // ignore: prefer_mixin
        WidgetsBindingObserver {
  final showOverlay = ValueNotifier<bool>(false);
  final dismissed = ValueNotifier<bool>(false);
  final blockApp = ValueNotifier<bool>(false);
  final hideHeaderLogo = ValueNotifier<bool>(false);
  final appDoesNotFulfillUpdateRequirements = ValueNotifier<bool>(false);
  final appIsUpdatableForPlatform = ValueNotifier<bool>(false);
  final expandDebug = ValueNotifier<bool>(true);
  final currentVer = ValueNotifier<String>("");
  final minimumVer = ValueNotifier<String>("");
  final latestAndroidVer = ValueNotifier<String>("");
  final latestIosVer = ValueNotifier<String>("");
  final hideNotificationIfUpdated = ValueNotifier<bool>(false);

  Status? status;

  late final SharedPreferences sp;
  final String spKey = "status_notification_app";

  @override
  void initState() {
    super.initState();
    // In main.dart [WidgetsFlutterBinding.ensureInitialized] already is called!
    WidgetsBinding.instance!.addObserver(this);
    init();
  }

  Future<void> init() async {
    /// Do not remove. iOS needs an initial function call,
    /// probably other (android) devices too.
    reloadStatus();
    Connectivity().onConnectivityChanged.listen((result) {
      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        reloadStatus();
      }
    });

    sp = await SharedPreferences.getInstance();
  }

  @override
  void dispose() {
    // In main.dart [WidgetsFlutterBinding.ensureInitialized] already is called!
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      debugPrint("[STATUS][LIFECYCLE_STATE] $state");
      reloadStatus();
    }
  }

  Future<bool> checkIfStatusHasBeenDismissed(Status? status) async {
    try {
      final savedJson = sp.getString(spKey);
      if (savedJson != null) {
        final savedStatus = Status.fromJson(jsonDecode(savedJson));
        if (savedStatus == status) {
          debugPrint("[STATUS][ALREADY_DISMISSED]"
              " same as saved dismissed status notification!");
          return true;
        } else {
          debugPrint("[STATUS][NEW] Not the same, new status notification");
          return false;
        }
      }
    } on Exception catch (e) {
      debugPrint("[STATUS][DISMISSED][ERROR] ${e.toString()}.");
    }
    return false;
  }

  Future<void> saveTcStatusDismissed(Status? status) async {
    var json = "";
    try {
      json = jsonEncode(status);
    } on Exception catch (e) {
      debugPrint(
          "[STATUS][DISMISSED][ERROR] Could encode json: ${e.toString()}");
    }
    final success = await sp.setString(spKey, json);
    if (success) {
      debugPrint("[STATUS][DISMISSED] User dismissed, saved.");
    } else {
      debugPrint(
          "[STATUS][DISMISSED][ERROR] Could not safe user status dismissal");
    }
  }

  Future<void> reloadStatus() async {
    try {
      var res = await getServerString(
        "${localizationUri.toString()}"
        "status${(kReleaseMode ? "" : ".debug")}.json",
      );
      final remoteStatus = Status.fromJson(jsonDecode(res));
      if (remoteStatus == status) {
        debugPrint("[STATUS][FETCH_REMOTE] Data from server is not new");
        return;
      }
      status = remoteStatus;
    } on FormatException catch (e) {
      debugPrint("[STATUS][FETCH_REMOTE][PARSE] Could read remote status: "
          "${e.toString()}");

      /// If we mess up the [remote json]
      hideStatusNotification();
      return;
    } on OfflineException catch (e) {
      debugPrint("[STATUS][FETCH_REMOTE][OFFLINE] Client is offline: "
          "${e.toString()}");
    } on Exception catch (e) {
      debugPrint("[STATUS][FETCH_REMOTE] Could not load remote status: "
          "${e.toString()}");
    }
    if (status == null) {
      debugPrint("[STATUS][FETCH_REMOTE] No data from server found");
      return;
    }
    try {
      if (await checkIfStatusHasBeenDismissed(status)) {
        debugPrint("[STATUS] User already dismissed this status notification");
        hideStatusNotification();
        return;
      }
      blockApp.value = status?.blockApp ?? false;
      hideHeaderLogo.value = status?.hideHeaderLogo ?? false;

      await setVersionRequirements();

      dismissed.value = false;
      hideNotificationIfUpdated.value =
          status?.hideNotificationIfUpdated ?? false;
      if (blockApp.value) {
        showStatusNotification();
        return;
      }
      if (appIsUpdatableForPlatform.value ||
          appDoesNotFulfillUpdateRequirements.value) {
        showStatusNotification();
        return;
      } else {
        if (hideNotificationIfUpdated.value) {
          hideStatusNotification();
          return;
        } else {
          showStatusNotification();
          return;
        }
      }
    } on Exception catch (e) {
      debugPrint("[STATUS][GENERAL][ERROR] ${e.toString()}.");

      /// Something is probably wrong with the [remote status.json]
      /// Do not show overlay.
      showOverlay.value = false;
      dismissed.value = true;
      return;
    }
  }

  void showStatusNotification() {
    showOverlay.value = true;
    dismissed.value = false;
  }

  void hideStatusNotification() {
    showOverlay.value = false;
    dismissed.value = true;
  }

  /// Default is [0.0.0] if an error occurs
  Version getSafeVersion(String input) {
    try {
      final val = input != "" ? input : "0.0.0";
      return Version.parse(val);
    } on Exception catch (e) {
      debugPrint("[STATUS][GETVERSION][ERROR] ${e.toString()}");
      return Version(0, 0, 0);
    }
  }

  Future<void> setVersionRequirements() async {
    try {
      final packageInfo = await PackageInfo.fromPlatform();
      final currentVersion = getSafeVersion(packageInfo.version);
      final minimumVersion =
          getSafeVersion(status?.minimumVersionRequired ?? "0.0.0");
      final latestIosVersion =
          getSafeVersion(status?.latestIosVersion ?? "0.0.0");
      final latestAndroidVersion =
          getSafeVersion(status?.latestAndroidVersion ?? "0.0.0");

      currentVer.value = currentVersion.toString();
      minimumVer.value = minimumVersion.toString();
      latestAndroidVer.value = latestAndroidVersion.toString();
      latestIosVer.value = latestIosVersion.toString();

      debugPrint("[STATUS][VERSION] Current $currentVersion"
          " | Minimum $minimumVersion"
          " | Latest Android Version $latestAndroidVersion"
          " | Latest iOS Version $latestIosVersion");

      /// Careful, if [Version] has a [preRelease], major, minor and patch
      /// will be determined lower even if all 3 are the same.
      if (minimumVersion > currentVersion) {
        debugPrint("[STATUS][VERSION] Minimum version not installed");
        appDoesNotFulfillUpdateRequirements.value = true;
        blockApp.value = true;
      } else {
        appDoesNotFulfillUpdateRequirements.value = false;
      }

      if (Platform.isAndroid) {
        if (latestAndroidVersion > currentVersion) {
          debugPrint(
              "[STATUS][VERSION] Latest Android version in store available");
          appIsUpdatableForPlatform.value = true;
        } else {
          appIsUpdatableForPlatform.value = false;
        }
      }
      if (Platform.isIOS) {
        if (latestIosVersion > currentVersion) {
          debugPrint("[STATUS][VERSION] Latest iOS version in store available");
          appIsUpdatableForPlatform.value = true;
        } else {
          appIsUpdatableForPlatform.value = false;
        }
      }
    } on Exception catch (e) {
      debugPrint("[STATUS][APPHASLATESTUPDATE][ERROR] ${e.toString()}");

      /// [in doubt] we don't want users to falsely navigate
      /// to update page
      appDoesNotFulfillUpdateRequirements.value = false;
      appIsUpdatableForPlatform.value = false;
    }
  }

  Future<String> getServerString(String url) async =>
      utf8.decode((await HttpRepository().get(url, secured: false)).bodyBytes);

  @override
  Widget build(BuildContext context) => Positioned.fill(
          child: AnimatedBuilder(
        animation: Listenable.merge([
          showOverlay,
          dismissed,
          blockApp,
          appDoesNotFulfillUpdateRequirements,
          appIsUpdatableForPlatform,
          hideHeaderLogo,
          currentVer,
          minimumVer,
          latestAndroidVer,
          latestIosVer,
        ]),
        builder: (context, child) => !showOverlay.value
            ? Container()
            : dismissed.value
                ? Container()
                : Stack(
                    children: [
                      Positioned.fill(
                        child: ClipRect(
                          child: BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
                            child: Container(color: Colors.black54),
                          ),
                        ),
                      ),
                      Positioned.fill(
                        child: SafeArea(
                          bottom: true,
                          top: true,
                          child: Container(
                            margin: EdgeInsets.all(16.0),
                            child: Material(
                              borderRadius: BorderRadius.circular(16.0),
                              elevation: 12,
                              clipBehavior: Clip.antiAlias,
                              color: CurrentTheme().themeData.primaryColor,
                              child: Stack(
                                children: [
                                  buildNotificationContent(),
                                  if (!blockApp.value ||
                                      appIsUpdatableForPlatform.value)
                                    buildGradientPositioned(),
                                  Positioned(
                                      bottom: 12,
                                      left: 12,
                                      right: 12,
                                      child: Column(
                                        children: [
                                          if (appIsUpdatableForPlatform.value)
                                            buildUpdateButton(),
                                          if (!blockApp.value)
                                            buildDismissButton(),
                                        ],
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      if (!kReleaseMode) buildDebugPanel(),
                    ],
                  ),
      ));

  Widget buildNotificationContent() => SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (!hideHeaderLogo.value)
                Container(
                  margin: EdgeInsets.only(top: 92.0, bottom: 64.0),
                  height: 144,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      FlareActor(
                        "assets/flare/tc_logo_compound.flr",
                        animation: "status",
                      ),
                    ],
                  ),
                ),
              if (appDoesNotFulfillUpdateRequirements.value)
                Container(
                  margin:
                      EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
                  child: I18nText(
                    'status_notification.version_not_supported',
                    translationParams: <String, String>{
                      'version': currentVer.value,
                    },
                    child: Text(
                      '',
                      textAlign: TextAlign.center,
                      style: CurrentTheme()
                          .themeData
                          .textTheme
                          .headline4
                          ?.copyWith(color: CurrentTheme().textPassive),
                    ),
                  ),
                ),
              if (status != null)
                for (var status in status!.entries)
                  Container(
                    margin: EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          read(status.title, context),
                          style: CurrentTheme().themeData.textTheme.headline2,
                        ),
                        Container(height: 24),
                        Markdown(
                          padding: EdgeInsets.zero,
                          data: read(status.body, context),
                          onTapLink: (_, url, __) async {
                            if (url != null && await canLaunch(url)) {
                              await launch(url);
                            }
                          },
                          styleSheet: TinyCampusMarkdownStylesheet(context),
                          physics: BouncingScrollPhysics(),
                          shrinkWrap: true,
                        ),
                      ],
                    ),
                  ),
              Container(height: 180),
            ],
          ),
        ),
      );

  Positioned buildDebugPanel() => Positioned(
        top: 0,
        left: 0,
        right: 0,
        child: Material(
          color: Colors.white54,
          child: InkWell(
            onTap: () {
              reloadStatus();
              setState(() {});
            },
            child: ValueListenableBuilder<bool>(
              valueListenable: expandDebug,
              builder: (context, value, child) => Row(
                children: [
                  TextButton(
                    child: Text(value ? "hide" : "show"),
                    onPressed: () => expandDebug.value = !expandDebug.value,
                  ),
                  if (value)
                    Flexible(
                      child: Column(
                        children: [
                          Text("blockApp: ${blockApp.value}" * 4),
                          Text("appDoesNotFulfillUpdateRequirements:"
                              " ${appDoesNotFulfillUpdateRequirements.value}"),
                          Text("dismissed: ${dismissed.value}"),
                          Text("appIsUpdatableForPlatform:"
                              " ${appIsUpdatableForPlatform.value}"),
                          Text("currentVersion: ${currentVer.value}"),
                          Text("latestAndroidVer: ${latestAndroidVer.value}"),
                          Text("latestIosVer: ${latestIosVer.value}"),
                          Text("minimumVer: ${minimumVer.value}"),
                          Text("hideNotificationIfUpdated:"
                              " ${hideNotificationIfUpdated.value}"),
                        ],
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
      );

  Positioned buildGradientPositioned() => Positioned(
        bottom: 0,
        left: 0,
        right: 0,
        height: 92,
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  CurrentTheme().themeData.primaryColor.withOpacity(0.0),
                  CurrentTheme().themeData.primaryColor,
                  CurrentTheme().themeData.primaryColor,
                ]),
          ),
        ),
      );

  Widget buildDismissButton() => Container(
        margin: EdgeInsets.only(top: 12),
        child: TCButton(
          buttonLabel: FlutterI18n.translate(
              context, "status_notification.button_dismiss"),
          onPressedCallback: () {
            hideStatusNotification();
            saveTcStatusDismissed(status);
          },
          buttonStyle: TCButtonStyle.outline,
          elevation: 0,
        ),
      );

  Widget buildUpdateButton() => Container(
        margin: EdgeInsets.only(top: 12),
        child: TCButton(
          buttonLabel: FlutterI18n.translate(
              context, "status_notification.button_update"),
          onPressedCallback: () {
            if (Platform.isAndroid) {
              _launchUrl(storeAndroidUrl);
            }
            if (Platform.isIOS) {
              _launchUrl(storeIosUrl);
            }
          },
        ),
      );

  Future<void> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
