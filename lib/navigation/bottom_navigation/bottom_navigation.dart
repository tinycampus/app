/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../bloc/user/user_bloc.dart';
import '../../common/tc_theme.dart';
import 'tab_helper.dart';

/// A [BottomNavigationBar] displayed on top of our four main pages.
class BottomNavigation extends StatefulWidget {
  /// Tab indicating, which page currently is displayed.
  final TabItem tab;

  BottomNavigation({Key? key, this.tab = TabItem.home}) : super(key: key);

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  late TabItem _currentTab;

  _BottomNavigationState();

  @override
  void initState() {
    super.initState();
    _currentTab = widget.tab;
  }

  void _selectTab(TabItem tabItem) {
    setState(() => _currentTab = tabItem);
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () {
          // Avoid popping if not on HomeScreen
          var result = (_currentTab == TabItem.home);
          if (!result) _selectTab(TabItem.home);
          return Future.value(result);
        },
        child: Scaffold(
          body: TabHelper.route(_currentTab)(context),
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Theme.of(context).primaryColor,
            type: BottomNavigationBarType.fixed,
            currentIndex: _currentTab.index,
            onTap: (index) => _selectTab(TabItem.values[index]),
            iconSize: 26.0,
            selectedItemColor: CurrentTheme().tcBlue,
            unselectedItemColor: CorporateColors.tinyCampusIconGrey,
            items: [
              for (var item in TabItem.values)
                BottomNavigationBarItem(
                  icon: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                          child: Icon(
                        TabHelper.icon(item),
                      )),
                      if (item == TabItem.settings)
                        BlocBuilder<UserBloc, UserState>(
                            builder: (context, state) => state.user.verified
                                ? Positioned(child: Container())
                                : Positioned(
                                    right: 0,
                                    top: 0,
                                    child: AnimatedOpacity(
                                        opacity: _currentTab == TabItem.settings
                                            ? 0
                                            : 1,
                                        duration: Duration(milliseconds: 120),
                                        child: (state.user.emailSent)
                                            ? Container(
                                                child: Icon(
                                                  Icons.mail,
                                                  size: 8,
                                                  color: CurrentTheme()
                                                      .themeData
                                                      .colorScheme
                                                      .secondary,
                                                ),
                                                height: 12,
                                                width: 12,
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Theme.of(context)
                                                        .primaryColor),
                                              )
                                            : Container(
                                                height: 12,
                                                width: 12,
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                        width: 2,
                                                        color: Theme.of(context)
                                                            .primaryColor),
                                                    color: CorporateColors
                                                        .ppAnswerWrongRed),
                                              )))),
                    ],
                  ),
                  label: FlutterI18n.translate(
                      context, '${TabHelper.description(item)}.title'),
                ),
            ],
          ),
        ),
      );
}
