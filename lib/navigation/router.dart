/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../common/constants/routing_constants.dart';
import '../common/widgets/code_of_conduct/code_of_conduct.dart';
import '../modules/bulletin_board/bulletin_board_startscreen.dart';
import '../modules/business_cards/business_card_edit.dart';
import '../modules/business_cards/business_card_startscreen.dart';
import '../modules/business_cards/contacts/vcard/vcard.dart';
import '../modules/business_cards/contacts/vcard_contact_note.dart';
import '../modules/business_cards/contacts/vcard_contact_screen.dart';
import '../modules/business_cards/qr_code_screen.dart';
import '../modules/cafeteria/bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../modules/cafeteria/cafeteria_item_page_args.dart';
import '../modules/cafeteria/model/cafeteria_settings.dart';
import '../modules/cafeteria/ui/pages/cafeteria_detail_page.dart';
import '../modules/cafeteria/ui/pages/cafeteria_item_page.dart';
import '../modules/cafeteria/ui/pages/cafeteria_overview_page.dart';
import '../modules/cafeteria/ui/pages/cafeteria_quickstart_page.dart';
import '../modules/cafeteria/ui/pages/cafeteria_selection_page.dart';
import '../modules/cafeteria/ui/pages/cafeteria_settings_page.dart';
import '../modules/cafeteria/ui/pages/cafeteria_wizard_page.dart';
import '../modules/coming_soon/ui/coming_soon_page.dart';
import '../modules/events/events_startscreen.dart';
import '../modules/feedback/feedback_startscreen.dart';
import '../modules/important_links/ui/important_links_start_page.dart';
import '../modules/organizer/ui/configuration/organizer_configuration_page.dart';
import '../modules/organizer/ui/explanations/multi_modules_explanation_page.dart';
import '../modules/organizer/ui/organizer/organizer_start.dart';
import '../modules/organizer/ui/organizer/program_history/organizer_program_history_page.dart';
import '../modules/organizer/ui/pool_config/organizer_pool_config_page.dart';
import '../modules/organizer/ui/search_modules/o_search_modules_page.dart';
import '../modules/organizer/ui/select_program/organizer_select_program_page.dart';
import '../modules/organizer/ui/selected_modules/o_selected_modules_page.dart';
import '../modules/organizer/ui/subject_config/organizer_subject_config_args.dart';
import '../modules/organizer/ui/subject_config/organizer_subject_config_page.dart';
import '../modules/practice_phase/pages/pp_about/pp_about_page.dart';
import '../modules/practice_phase/pages/pp_entry/pp_entry_page.dart';
import '../modules/practice_phase/pages/pp_overview/pp_overview_page.dart';
import '../modules/practice_phase/pages/pp_quiz/pp_quiz_page.dart';
import '../modules/practice_phase/pages/pp_result/pp_result_page.dart';
import '../modules/practice_phase/pages/pp_selection/pp_selection_page.dart';
import '../modules/practice_phase/pages/pp_send_data/pp_send_data_page.dart';
import '../modules/practice_phase/pages/pp_send_data/pp_verify_data_page.dart';
import '../modules/practice_phase/pages/pp_unit/pp_unit_page.dart';
import '../modules/practice_phase/utility/pp_bloc_util.dart';
import '../modules/qanda/my_questions_page.dart';
import '../modules/qanda/qanda_main_category_screen.dart';
import '../modules/qanda/qanda_question_screen.dart';
import '../modules/qanda/qanda_startscreen.dart';
import '../modules/qanda/qanda_subcategory_screen.dart';
import '../modules/qanda/qanda_view_question_page.dart';
import '../modules/semester_fee/sf_start_page.dart';
import '../modules/sport/ui/sport_main_view.dart';
import '../modules/zs/ui/pages/zs_page.dart';
import '../pages/app_start/greeter_page.dart';
import '../pages/app_start/landing_page.dart';
import '../pages/app_start/splash_page.dart';
import '../pages/bouncy_ball/offline_game.dart';
import '../pages/home/home_settings_page.dart';
import '../pages/profile/profile_hearts_page.dart';
import '../pages/settings/brand_info_page.dart';
import '../pages/settings/credits_page.dart';
import '../pages/settings/data_deletion_page.dart';
import '../pages/settings/data_usage_liability_page.dart';
import '../pages/settings/settings_language_page.dart';
import '../pages/timeline/ui/timeline_settings_page.dart';
import 'bottom_navigation/bottom_navigation.dart';
import 'bottom_navigation/tab_helper.dart';
import 'undefined_page.dart';

/// RouteHandler which is called after every Navigator call.
Route<dynamic> generateRoute(RouteSettings settings) {
  final args = settings.arguments;

  switch (settings.name) {
    // Main route
    case mainRoute:
      return MaterialPageRoute(
        builder: (_) => SplashPage(),
      );

    // Login routes
    case loginRoute:
      return MaterialPageRoute(
        builder: (_) => (args is LandingPageArguments)
            ? LandingPage(tasks: args.tasks)
            : LandingPage(),
      );
    case loginGreeterRoute:
      return MaterialPageRoute(
        builder: (_) => GreeterPage(),
      );

    // Bouncy Ball route
    case offlineGame:
      return MaterialPageRoute(
        builder: (_) => OfflineGameWidget(),
      );

    // Home routes
    // Bottom navigation tabs are handled in [TabHelper]
    case homeTabRoute:
      return MaterialPageRoute(
        builder: (_) => BottomNavigation(
          tab: TabItem.home,
        ),
      );
    case homeSettingsRoute:
      return MaterialPageRoute(
        builder: (_) => HomeSettingsPage(),
      );

    // Timeline routes
    // Bottom navigation tabs are handled in [TabHelper]
    case timelineTabRoute:
      return MaterialPageRoute(
        builder: (_) => BottomNavigation(
          tab: TabItem.timeline,
        ),
      );
    case timelineSettingsRoute:
      if (args is! FaveCallback) break;
      return MaterialPageRoute(
        builder: (_) => TimelineSettingsPage(
          callback: args,
        ),
      );

    // Profile routes
    // Bottom navigation tabs are handled in [TabHelper]
    case profileTabRoute:
      return MaterialPageRoute(
        builder: (_) => BottomNavigation(
          tab: TabItem.profile,
        ),
      );
    case profileHeartsRoute:
      return MaterialPageRoute(
        builder: (_) => ProfileHeartsPage(),
      );

    // Settings routes
    // Bottom navigation tabs are handled in [TabHelper]
    case settingsTabRoute:
      return MaterialPageRoute(
        builder: (_) => BottomNavigation(
          tab: TabItem.settings,
        ),
      );
    case settingsLanguageRoute:
      return MaterialPageRoute(
        builder: (_) => SettingsLanguage(),
      );
    case settingsCreditsRoute:
      return MaterialPageRoute(
        builder: (_) => CreditsPage(),
      );
    case settingsBrandInfoRoute:
      return MaterialPageRoute(
        builder: (_) => BrandInfoPage(),
      );
    case settingsDataDeletionRoute:
      return MaterialPageRoute(
        builder: (_) => DataDeletionPage(),
      );
    // TODO This page is currently unused
    case settingsDataUsageLiabilityRoute:
      return MaterialPageRoute(
        builder: (_) => DataUsageLiabilityPage(),
      );

    // Coming Soon route
    case comingSoonRoute:
      return MaterialPageRoute(
        builder: (_) => ComingSoonPage(),
      );

    // vCard routes
    case vCardModuleRoute:
      return MaterialPageRoute(
        builder: (_) => BusinessCardStartScreen(),
      );
    case vCardEditRoute:
      return MaterialPageRoute<VCard>(
        builder: (_) => BusinessCardEdit(),
      );
    case vCardQRCodeRoute:
      if (args is! QRScreenArguments) break;
      return MaterialPageRoute(
        builder: (_) => QrCodeScreen(
          qrArguments: args,
        ),
      );
    case vCardContactScreenRoute:
      if (args is! VCardContactScreenArguments) break;
      return MaterialPageRoute(
        builder: (_) => VCardContactScreen(
          pos: args.pos,
        ),
      );
    case vCardContactNoteRoute:
      if (args is! VCardContactNoteArguments) break;
      return MaterialPageRoute(
        builder: (_) => VCardContactNote(
          pos: args.pos,
        ),
      );

    // Q & A routes
    case qaModuleRoute:
      return MaterialPageRoute(
        builder: (_) => QAndAStartScreen(),
      );
    case qaAllQuestions:
      return MaterialPageRoute(
        builder: (_) => (args is QAndAQuestionScreenArguments)
            ? QAndAQuestionScreen(
                showCategory: args.showCategory,
                openAskQuestion: args.openAskQuestion,
                initialQuestionText: args.initialQuestionText,
              )
            : QAndAQuestionScreen(),
      );
    case qaAddCategories:
      return MaterialPageRoute(
        builder: (_) => QAndAMainCategoryScreen(),
      );
    case qaAddSubcategories:
      if (args is! QAndASubcategoryScreenArguments) break;
      return MaterialPageRoute(
        builder: (_) => QAndASubcategoryScreen(
          category: args.category,
        ),
      );
    case qaViewQuestionRoute:
      if (args is! int) break;
      return MaterialPageRoute(
        builder: (_) => QAndAViewQuestionPage(args),
      );
    case qaMyQuestionsRoute:
      return MaterialPageRoute(
        builder: (_) => MyQuestionsPage(),
      );

    // Organizer routes
    case orgaModuleRoute:
      return MaterialPageRoute(
        builder: (_) => OrganizerStart(),
      );
    case orgaConfigurationRoute:
      return MaterialPageRoute(
        builder: (_) => OrganizerConfigurationPage(),
      );
    case orgaSelectProgramRoute:
      return MaterialPageRoute(
        builder: (_) => OrganizerSelectProgramPage(),
      );
    case orgaPoolConfigRoute:
      if (args is! OrganizerPoolConfigPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => OrganizerPoolConfigPage(
          pool: args.pool,
          program: args.program,
        ),
      );
    case orgaProgramHistoryRoute:
      if (args is! OrganizerProgramHistoryPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => OrganizerProgramHistoryPage(
          program: args.program,
        ),
      );
    case orgaSubjectConfigRoute:
      if (args is! OrganizerSubjectConfigPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => OrganizerSubjectConfigPage(
          pool: args.pool,
          subject: args.subject,
          program: args.program,
        ),
      );
    case orgaSearchModulesRoute:
      if (args is! OSearchModulesPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => OSearchModulesPage(
          program: args.program,
        ),
      );
    case orgaSelectedModulesRoute:
      if (args is! OSelectedModulesPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => OSelectedModulesPage(
          program: args.program,
        ),
      );
    case orgaMultipleModulesExplanation:
      return MaterialPageRoute(
        builder: (_) => OMultipleModulesExplanationPage(),
      );

    // Cafeteria routes
    case cafeteriaModuleRoute:
      return MaterialPageRoute(
        builder: (context) {
          if (BlocProvider.of<CafeteriaSettingsBloc>(context)
              .state
              .cafeteriaSelection
              .isEmpty) {
            BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                SettingsChangedEvent(
                    settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .state
                        .settings
                        .copyWith(hasDoneWizard: false)));
          }

          return (BlocProvider.of<CafeteriaSettingsBloc>(context)
                  .state
                  .settings
                  .hasDoneWizard)
              ? CafeteriaOverviewPage()
              : CafeteriaQuickstartPage();
        },
      );
    case cafeteriaSelectionRoute:
      if (args is! int) break;
      return MaterialPageRoute(
        builder: (_) => CafeteriaSelectionPage(
          initIndex: args,
        ),
      );
    case cafeteriaOverviewRoute:
      return MaterialPageRoute(
        builder: (_) => CafeteriaOverviewPage(),
      );
    case cafeteriaDetailRoute:
      if (args is! String) break;
      return MaterialPageRoute(
          builder: (_) => CafeteriaDetailPage(
                name: args,
              ));
    case cafeteriaSettingsRoute:
      return MaterialPageRoute(
        builder: (_) => CafeteriaSettingsPage(),
      );
    case cafeteriaWizardRoute:
      return MaterialPageRoute(
        builder: (_) => CafeteriaWizardPage(),
      );
    case cafeteriaItemRoute:
      if (args is! CafeteriaItemPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => CafeteriaItemPage(
          item: args.item,
          name: args.name,
        ),
      );

    // Bulletin Board route
    case bulletinBoardRoute:
      return MaterialPageRoute(
        builder: (_) => BulletinBoardStartScreen(),
      );

    // Semester Fee route
    case semesterFeeRoute:
      return MaterialPageRoute(
        builder: (_) => SemesterFeeStartScreen(),
      );

    // Events routes
    case eventsRoute:
      return MaterialPageRoute(
        builder: (_) => EventsStartScreen(),
      );

    // Feedback route
    case feedbackRoute:
      return MaterialPageRoute(
        builder: (_) => FeedbackStartScreen(),
      );

    // Code of Conduct route
    case cocRoute:
      return MaterialPageRoute(
        builder: (_) => CodeOfConductPage(),
      );

    // Important Links route
    case importantLinksRoute:
      return MaterialPageRoute(
        builder: (_) => ImportantLinksStartPage(),
      );

    // Practice Phase routes
    case practicePhaseRoute:
      return MaterialPageRoute(
        builder: (context) {
          PpBlocUtil.initBlocs(context);

          if (PpBlocUtil.userHasSelectedPhase()) {
            return PpOverviewPage();
          } else {
            return PpSelectionPage();
          }
        },
      );
    case ppAboutRoute:
      if (args is! PpAboutPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => PpAboutPage(
          phase: args.phase,
        ),
      );
    case ppEntryRoute:
      if (args is! PpEntryPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => PpEntryPage(
          unit: args.unit,
        ),
      );
    case ppOverviewRoute:
      return MaterialPageRoute(
        builder: (_) => PpOverviewPage(),
      );
    case ppQuizRoute:
      if (args is! PpQuizPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => PpQuizPage(
          originalUnit: args.originalUnit,
        ),
      );
    case ppResultRoute:
      if (args is! PpResultPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => PpResultPage(
          resultMap: args.resultMap,
          unit: args.unit,
        ),
      );
    case ppSelectionRoute:
      return MaterialPageRoute(
        builder: (_) => PpSelectionPage(),
      );
    case ppSendDataRoute:
      if (args is! PpSendDataPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => PpSendDataPage(
          selectedPhase: args.selectedPhase,
        ),
      );
    case ppVerifyDataRoute:
      if (args is! PpVerifyDataArguments) break;
      return MaterialPageRoute(
        builder: (_) => PpVerifyDataPage(
          selectedPhase: args.selectedPhase,
          matNumber: args.matNumber,
          lastName: args.lastName,
        ),
      );
    case ppUnitRoute:
      if (args is! PpUnitPageArguments) break;
      return MaterialPageRoute(
        builder: (_) => PpUnitPage(
          unit: args.unit,
        ),
      );

    // Zentrale Studienberatung route
    case zsRoute:
      return MaterialPageRoute(
        builder: (_) => ZsPage(),
      );

    // Hochschulsport route
    case sportRoute:
      return MaterialPageRoute(
        builder: (_) => SportMainView(),
      );

    // Error page for invalid routes
    default:
      return kDebugMode
          ? MaterialPageRoute(
              builder: (_) => UndefinedPage(
                name: settings.name,
              ),
            )
          : throw UndefinedPageException(settings.name);
  }

  // Is only reached on 'break' after missing arguments
  throw MissingPageArgumentsException(settings.name);
}

/// An [Exception] thrown when trying to open an undefined page
/// when not in [kDebugMode].
class UndefinedPageException implements Exception {
  final String message;

  UndefinedPageException([String? message]) : message = message ?? '';

  @override
  String toString() => "${runtimeType.toString()}: $message";
}

class MissingPageArgumentsException implements Exception {
  final String message;

  MissingPageArgumentsException([String? message])
      : message = message ?? 'Undefined route';

  @override
  String toString() =>
      "${runtimeType.toString()}: Missing arguments on route $message";
}
