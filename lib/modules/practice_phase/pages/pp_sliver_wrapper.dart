/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

class PpSliverWrapper extends StatelessWidget {
  final Widget child;
  final Widget appBarBackground;

  const PpSliverWrapper({
    Key? key,
    required this.child,
    required this.appBarBackground,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: <Widget>[
          SliverAppBar(
            stretch: true,
            expandedHeight: 180,
            floating: true,
            stretchTriggerOffset: 120,
            onStretchTrigger: () async =>
                // `WidgetsFlutterBinding.ensureInitialized()` is called in
                // main.dart so this is safe.
                WidgetsBinding.instance!
                    .addPostFrameCallback((_) => Navigator.pop(context)),
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: [StretchMode.zoomBackground],
              collapseMode: CollapseMode.parallax,
              background: InkWell(
                onTap: () => Navigator.pop(context),
                child: Material(
                  child: appBarBackground,
                ),
              ),
            ),
            leading: Hero(
              tag: "back",
              child: Material(
                color: Colors.transparent,
                child: BackButton(),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate.fixed([child]),
          )
        ],
      );
}
