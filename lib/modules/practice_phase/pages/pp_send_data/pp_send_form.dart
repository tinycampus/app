/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/tc_form_field/tc_form_field.dart';
import '../../../../common/widgets/tc_form_field/tc_form_field_model.dart';
import '../../model/pp_phase.dart';

class PpSendForm extends StatelessWidget {
  final TextEditingController matController;
  final TextEditingController nameController;
  final PpPhase selectedPhase;

  PpSendForm({
    Key? key,
    required this.matController,
    required this.nameController,
    required this.selectedPhase,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 20),
            child: Column(
              children: [
                Text(
                  FlutterI18n.translate(
                    context,
                    'modules.practice_phase.ready',
                  ),
                  style: Theme.of(context).textTheme.headline1,
                ),
                Container(height: 30),
                Row(
                  children: [
                    Spacer(
                      flex: 1,
                    ),
                    Expanded(
                      flex: 3,
                      child: Text(
                        FlutterI18n.translate(
                          context,
                          'modules.practice_phase.pp_finished_text',
                        ),
                        style: Theme.of(context).textTheme.bodyText1,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                  ],
                ),
                Container(height: 30),
                Text(
                  FlutterI18n.translate(
                    context,
                    'modules.practice_phase.pp_finished_info',
                  ),
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(fontWeight: FontWeight.w800),
                ),
              ],
            ),
          ),
          Container(
            height: 30,
          ),
          Column(
            children: [
              Material(
                shadowColor: CurrentTheme().textPassive.withOpacity(0.4),
                elevation: 4,
                color: CurrentTheme().themeData.primaryColor,
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                  child: Center(
                    child: Column(
                      children: [
                        TCFormField(
                          formFieldModel: TCFormFieldModel(
                            title: FlutterI18n.translate(
                              context,
                              'modules.practice_phase.matriculation_number',
                            ),
                            hintText: FlutterI18n.translate(
                              context,
                              'modules.practice_phase.matriculation_number',
                            ),
                            maxLength: 10,
                            controller: matController,
                            valid: [
                              (text) => text.trim().length >= 4
                                  ? null
                                  : FlutterI18n.translate(
                                      context,
                                      'modules.practice_phase'
                                      '.matriculation_number_error_length',
                                    ),
                            ],
                          ),
                        ),
                        Container(
                          height: 16,
                        ),
                        TCFormField(
                          formFieldModel: TCFormFieldModel(
                            title: FlutterI18n.translate(
                              context,
                              'modules.practice_phase.last_name',
                            ),
                            hintText: FlutterI18n.translate(
                              context,
                              'modules.practice_phase.last_name',
                            ),
                            maxLength: 256,
                            controller: nameController,
                            valid: [
                              (text) => text.trim().isEmpty
                                  ? FlutterI18n.translate(
                                      context,
                                      'modules.practice_phase'
                                      '.last_name_error_length',
                                    )
                                  : null,
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(height: 32),
        ],
      );
}
