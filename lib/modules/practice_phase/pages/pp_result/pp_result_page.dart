/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/tc_button.dart';
import '../../../../common/widgets/tc_cached_image.dart';
import '../../model/pp_unit.dart';
import '../pp_quiz/pp_quiz_page.dart';

class PpResultPageArguments {
  PpResultPageArguments(this.unit, this.resultMap);

  final PpUnit unit;
  final Map<int, int> resultMap;
}

class PpResultPage extends StatefulWidget {
  const PpResultPage({
    Key? key,
    required this.unit,
    required this.resultMap,
  }) : super(key: key);

  final PpUnit unit;
  final Map<int, int> resultMap;

  @override
  _PpResultPageState createState() => _PpResultPageState();
}

class _PpResultPageState extends State<PpResultPage>
    with TickerProviderStateMixin {
  int amountTicks = 0;
  late PpResult resultType;
  late AnimationController animIntroController;
  late ResultAnimation animationIntro;
  late AnimationController animRotationController;
  late RotateAnimation animationRotation;
  Map<int, AnimationController> animTickControllerList =
      <int, AnimationController>{};
  Map<int, ResultDotTickAnimation> animationTickList =
      <int, ResultDotTickAnimation>{};

  @override
  void initState() {
    super.initState();
    initAnimationMeta();
    initAnimations();
  }

  @override
  void dispose() {
    animIntroController.dispose();

    for (var c in animTickControllerList.entries) {
      c.value.dispose();
    }
    animRotationController.dispose();
    super.dispose();
  }

  void resetAnimations() {
    Navigator.pushReplacementNamed(context, ppResultRoute,
        arguments: PpResultPageArguments(widget.unit, widget.resultMap));
  }

  void initAnimations() {
    // controller #1 -> intro and minor details animation
    animIntroController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 3600),
    );

    animationIntro = ResultAnimation(controller: animIntroController);
    Future.delayed(Duration(milliseconds: 560), () {
      if (mounted) {
        animIntroController.animateTo(1.0);
      }
    });

    // controller #2 -> ticker animation for each dot/result-entry
    var i = 0;
    for (var e in widget.resultMap.keys) {
      final controller = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 900),
      );
      animTickControllerList[e] = controller;
      animationTickList[e] = ResultDotTickAnimation(controller: controller);
      Future.delayed(Duration(milliseconds: 960 + 120 * i++), () {
        if (mounted) {
          controller.animateTo(1.0);
        }
      });
    }

    // controller #3 -> rotation background
    animRotationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 2400),
    );

    animationRotation = RotateAnimation(controller: animRotationController);
    animRotationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animRotationController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animRotationController.forward();
      }
    });
    animRotationController.reset();
    animRotationController.forward();
  }

  void initAnimationMeta() {
    amountTicks = widget.resultMap.entries.length;
    resultType = resultPercentage() == 100 ? PpResult.win : PpResult.fail;
  }

  int resultPercentage() {
    final results = widget.resultMap.values;
    if (results.isEmpty) {
      return 0;
    }
    return ((results.reduce((v, e) => v + e) * 100) / results.length).floor();
  }

  String animatedPercentage(double animationValue) =>
      "${(animationValue * resultPercentage()).floor()} %";

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Container(
          margin: EdgeInsets.all(16.0),
          child: SafeArea(
            child: Column(
              children: [
                Expanded(
                  child: RepaintBoundary(
                    child: AnimatedBuilder(
                      animation: animIntroController,
                      builder: (context, child) => Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          if (!kReleaseMode)
                            IconButton(
                              icon: Icon(Icons.refresh),
                              onPressed: resetAnimations,
                            ),
                          Text(
                            FlutterI18n.translate(
                              context,
                              'modules.practice_phase.your_result',
                            ),
                            style: CurrentTheme()
                                .themeData
                                .textTheme
                                .headline1
                                ?.copyWith(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 36,
                                    letterSpacing: -1.2),
                          ),
                          Container(height: 46),
                          GestureDetector(
                            onTapDown: (details) {
                              animIntroController
                                  .animateTo(0.60,
                                      duration: Duration(milliseconds: 90))
                                  .then((_) {
                                animIntroController.forward();
                              });
                            },
                            child: Transform.scale(
                              scale: animationIntro.scaleImage.value,
                              child: TcCachedImage(
                                url: widget.unit.image,
                                isRounded: true,
                                borderRadius: 999,
                                height: 172,
                                width: 172,
                                imageMargin: EdgeInsets.all(8.0),
                                stackBackground: Stack(
                                  clipBehavior: Clip.antiAlias,
                                  children: [
                                    if (resultType == PpResult.win)
                                      AnimatedBuilder(
                                        animation: animRotationController,
                                        builder: (context, child) =>
                                            Positioned.fill(
                                          child: Transform.rotate(
                                            angle: animationRotation
                                                .rotation.value,
                                            child: Transform.scale(
                                              scale: animationIntro
                                                      .scaleWinBackground
                                                      .value *
                                                  2.0,
                                              child: Image.asset(
                                                'assets/images/praxis_phase'
                                                '/winBackground.png',
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    Positioned.fill(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: CurrentTheme()
                                              .themeData
                                              .primaryColor,
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Transform.translate(
                            offset: Offset(0, -46),
                            child: Transform.scale(
                              scale: animationIntro.scalePercent.value,
                              child: Material(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(24.0)),
                                color: CurrentTheme().themeData.primaryColor,
                                elevation: 4,
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 36,
                                    vertical: 8,
                                  ),
                                  child: Text(
                                    animatedPercentage(
                                      animationIntro.scalePercentText.value,
                                    ),
                                    style: CurrentTheme()
                                        .themeData
                                        .textTheme
                                        .headline3,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // TODO: Honestly this should be its own Widget
                              ...widget.resultMap.entries.map(
                                (e) {
                                  final animation = animationTickList[e.key]!;
                                  return AnimatedBuilder(
                                    animation: animTickControllerList[e.key]!,
                                    builder: (context, child) =>
                                        Transform.translate(
                                      offset: animation.translateDot.value,
                                      child: Transform.scale(
                                        scale: animation.scaleDot.value,
                                        child: Container(
                                          margin: EdgeInsets.all(4),
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                              width: 2,
                                              color: e.value == 0
                                                  ? CorporateColors
                                                      .ppAnswerWrongRed
                                                  : CorporateColors
                                                      .ppAnswerCorrectGreen,
                                              style: BorderStyle.solid,
                                            ),
                                            color: e.value == 0
                                                ? CorporateColors
                                                    .ppAnswerWrongBackgroundRed
                                                : CorporateColors
                                                    .ppAnswerCorrectGreen,
                                          ),
                                          height: 16,
                                          width: 16,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ).toList(),
                            ],
                          ),
                          Container(height: 46),
                        ],
                      ),
                    ),
                  ),
                ),
                if (resultType == PpResult.fail)
                  TCButton(
                      elevation: 0,
                      buttonLabel: FlutterI18n.translate(
                        context,
                        'modules.practice_phase.repeat_quiz',
                      ),
                      buttonStyle: TCButtonStyle.outline,
                      onPressedCallback: () {
                        Navigator.pushReplacementNamed(
                          context,
                          ppQuizRoute,
                          arguments: PpQuizPageArguments(widget.unit),
                        );
                      }),
                Container(height: 12),
                TCButton(
                  buttonLabel: FlutterI18n.translate(
                    context,
                    'modules.practice_phase.ready',
                  ),
                  onPressedCallback: () {
                    Navigator.pop(
                      context,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      );
}

class ResultAnimation {
  ResultAnimation({
    required this.controller,
  })  : scaleImage = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 1.2)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 5.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.2, end: 0.8)
                .chain(CurveTween(curve: Curves.elasticOut)),
            weight: 20.0,
          ),
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(0.8),
            weight: 35.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.8, end: 0.825)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 40,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        ),
        scalePercent = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 1.2)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.2, end: 1.0)
                .chain(CurveTween(curve: Curves.elasticOut)),
            weight: 100.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.375, 0.6, curve: Curves.linear),
          ),
        ),
        scaleWinBackground = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 1.2)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.2, end: 1.0)
                .chain(CurveTween(curve: Curves.elasticOut)),
            weight: 100.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.6, 0.8, curve: Curves.linear),
          ),
        ),
        scalePercentText = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 1.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.375, 0.6, curve: Curves.linear),
          ),
        ),
        questionBodyOpacity = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 1.0)
                .chain(CurveTween(curve: Curves.linear)),
            weight: 100.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.225, 0.25, curve: Curves.linear),
          ),
        );

  final AnimationController controller;

  final Animation<double> scaleImage;
  final Animation<double> scalePercent;
  final Animation<double> scalePercentText;
  final Animation<double> scaleWinBackground;
  final Animation<double> questionBodyOpacity;
}

class ResultDotTickAnimation {
  ResultDotTickAnimation({
    required this.controller,
  })  : scaleDot = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 1.6)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 25.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.6, end: 1.0)
                .chain(CurveTween(curve: Curves.elasticOut)),
            weight: 75.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 0.5, curve: Curves.linear),
          ),
        ),
        translateDot = TweenSequence(<TweenSequenceItem<Offset>>[
          TweenSequenceItem<Offset>(
            tween: Tween<Offset>(begin: Offset(0, 32), end: Offset(0, 0))
                .chain(CurveTween(curve: Curves.elasticOut)),
            weight: 100.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        );

  final AnimationController controller;

  final Animation<double> scaleDot;
  final Animation<Offset> translateDot;
}

class RotateAnimation {
  RotateAnimation({
    required this.controller,
  }) : rotation = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: pi / 4)
                .chain(CurveTween(curve: Curves.easeInOut)),
            weight: 100.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        );

  final AnimationController controller;

  final Animation<double> rotation;
}

enum PpResult {
  fail,
  win,
}
