/*
 * Copyright 2020-2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart' as tc_util;
import '../../../../common/widgets/tc_button.dart';
import '../../bloc/pp_model/pp_model_bloc.dart';
import '../../bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../../model/pp_phase.dart';
import '../../utility/pp_bloc_util.dart';

class PpSelectionPage extends StatefulWidget {
  const PpSelectionPage({Key? key}) : super(key: key);

  @override
  _PpSelectionPageState createState() => _PpSelectionPageState();
}

class _PpSelectionPageState extends State<PpSelectionPage> {
  ValueNotifier<bool> showDebug = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          actions: [
            if (!kReleaseMode)
              ElevatedButton(
                onPressed: () {
                  showDebug.value = !showDebug.value;
                },
                child: Text(
                  "Debug",
                  textAlign: TextAlign.center,
                ),
              ),
          ],
          title: Row(
            children: [
              Text(
                FlutterI18n.translate(context, 'modules.practice_phase.title'),
              ),
            ],
          ),
        ),
        backgroundColor: CurrentTheme().themeData.backgroundColor,
        body: Stack(
          children: [
            Positioned.fill(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                reverse: true,
                child: BlocBuilder<PpModelBloc, PpModelState>(
                  builder: (context, state) => Column(
                    children: [
                      Container(
                        height: 240,
                      ),
                      for (var phase in state.phases)
                        Column(
                          children: [
                            PhaseListTile(phase: phase),
                            ValueListenableBuilder<bool>(
                              valueListenable: showDebug,
                              builder: (context, value, child) => !value
                                  ? Container()
                                  : Row(
                                      children: [
                                        Flexible(
                                          child: TextButton(
                                            // Only used in Debug,
                                            // no need to localize
                                            child: Text(
                                              phase.title["de"] ?? '',
                                            ),
                                            onPressed: () {
                                              Clipboard.setData(
                                                ClipboardData(
                                                  text: jsonEncode(
                                                    phase.toJson(),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                            ),
                          ],
                        ),
                      if (state.phases.isEmpty)
                        Container(
                          margin:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 32),
                          child: Text(
                            FlutterI18n.translate(
                              context,
                              'modules.practice_phase.missing_phases',
                            ),
                            style: CurrentTheme().themeData.textTheme.headline3,
                          ),
                        ),
                      Container(height: 42),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Container(
                width: 150,
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            color: CurrentTheme().themeData.backgroundColor,
                            width: 220,
                            padding: EdgeInsets.symmetric(
                              vertical: 32,
                              horizontal: 64,
                            ),
                            child: Text(
                              FlutterI18n.translate(
                                  context, 'modules.practice_phase.choose_pp'),
                              textAlign: TextAlign.center,
                              style: CurrentTheme()
                                  .themeData
                                  .textTheme
                                  .headline1
                                  ?.copyWith(
                                    color: CurrentTheme().textSoftWhite,
                                    fontWeight: FontWeight.w800,
                                  ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 42,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            CurrentTheme().themeData.backgroundColor,
                            CurrentTheme().themeData.backgroundColor,
                            CurrentTheme()
                                .themeData
                                .backgroundColor
                                .withOpacity(0.0),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ValueListenableBuilder<bool>(
              valueListenable: showDebug,
              builder: (context, value, child) =>
                  value ? DebugButtons() : Container(),
            )
          ],
        ),
      );
}

class DebugButtons extends StatelessWidget {
  const DebugButtons({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Positioned(
        top: 0,
        left: 0,
        right: 0,
        child: Container(
          color: CurrentTheme().themeData.primaryColor,
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                      child: TextButton(
                          child: Text("ADD TESTDATA"),
                          onPressed: () {
                            PpBlocUtil.addModelEvent(PmbGenerateModelsEvent());
                          })),
                  Expanded(
                      child: TextButton(
                          child: Text("Clear Image Cache"),
                          onPressed: () {
                            // FIXME: if possible don't use the underlying
                            //  implementation details of used packages.
                            DefaultCacheManager().emptyCache();
                          })),
                  Expanded(
                      child: TextButton(
                          child: Text(
                            "FETCH AND OVERRIDE FROM SRV",
                            textAlign: TextAlign.center,
                          ),
                          onPressed: () {
                            PpBlocUtil.addModelEvent(
                                PpFetchAndReplaceModelFromRemoteEvent(
                                    forceReload: true));
                          })),
                  Expanded(
                      child: TextButton(
                          child: Text(
                            "FETCH AND OVERRIDE DEBUG FROM SRV",
                            textAlign: TextAlign.center,
                          ),
                          onPressed: () {
                            PpBlocUtil.addModelEvent(
                                PpFetchDebugModelFromRemoteEvent(
                                    forceReload: true));
                          })),
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: TextButton(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(child: Text("DEBUG ONLY")),
                              Switch(
                                value: false,
                                onChanged: (value) {
                                  debugPrint("i don't switch!");
                                },
                              ),
                            ],
                          ),
                          onPressed: () {})),
                  Expanded(
                      child: TextButton(
                          child: Text("RESET USER PROGRESSION"),
                          onPressed: () {
                            PpBlocUtil.addProgressEvent(UpbResetEvent());
                          })),
                  Expanded(
                      child: TextButton(
                          child: Text("RESET Model"),
                          onPressed: () {
                            PpBlocUtil.addModelEvent(PpModelResetEvent());
                          })),
                  Expanded(
                      child: TextButton(
                          child: Text("RESET Progress"),
                          onPressed: () {
                            PpBlocUtil.addProgressEvent(UpbResetEvent());
                          })),
                ],
              ),
            ],
          ),
        ),
      );
}

class PhaseListTile extends StatelessWidget {
  const PhaseListTile({
    Key? key,
    required this.phase,
  }) : super(key: key);

  final PpPhase phase;

  @override
  Widget build(BuildContext context) => TCPressableWrapper(
        widget: ListTile(
          contentPadding:
              EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
          onTap: () {
            PpBlocUtil.addProgressEvent(UpbSelectPhaseEvent(phase.phaseId));
            Navigator.pushReplacementNamed(context, ppOverviewRoute);
          },
          title: Text(
            tc_util.read(phase.title, context),
            style: CurrentTheme().themeData.textTheme.headline2,
          ),
        ),
      );
}
