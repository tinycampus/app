/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/styled_print.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart' as tc_util;
import '../../../../common/widgets/tc_popup_widgets.dart';
import '../../bloc/pp_model/pp_model_bloc.dart';
import '../../bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../../model/pp_group.dart';
import '../../model/pp_phase.dart';
import '../../utility/pp_bloc_util.dart';
import '../pp_send_data/pp_send_data_page.dart';
import 'pp_group_body.dart';
import 'pp_phase_not_found.dart';

enum OverviewPopUpSelection {
  navigatePhaseSelectionScreen,
  forceReload,
  forceReloadDebug,
  reset,
  cheat,
}

class PpOverviewPage extends StatefulWidget {
  const PpOverviewPage({Key? key}) : super(key: key);

  @override
  _PpOverviewPageState createState() => _PpOverviewPageState();
}

class _PpOverviewPageState extends State<PpOverviewPage> {
  ValueNotifier<bool> showDebug = ValueNotifier<bool>(false);
  ValueNotifier<bool> finished = ValueNotifier<bool>(false);
  ValueNotifier<int> highlightedUnitId = ValueNotifier<int>(-1);
  @override
  void initState() {
    super.initState();
    checkIfEverythingIsFinished();
  }

  void determineHighlightedUnit() {
    // go through all groups
    final selectedPhase = PpBlocUtil.getUserSelectedPhase();
    for (var group in selectedPhase.groups) {
      for (var unit in group.units) {
        final unitState = PpBlocUtil.getDynamicUnitState(unit).state;
        if (!(unitState == UnitEnum.level2Complete ||
            unitState == UnitEnum.level2Failed)) {
          highlightedUnitId.value = unit.unitId;
          sprint("Highlighted unit id: ${highlightedUnitId.value}",
              style: PrintStyle.praxisPhase, caller: PpOverviewPage);
          return;
        }
      }
    }
    // return the first that is not finished
  }

  void checkIfEverythingIsFinished() {
    determineHighlightedUnit();
    finished.value = _isFinished();
  }

  void removeFinishedForAnimation() {
    finished.value = false;
  }

  @override
  Widget build(BuildContext context) => BlocBuilder<PpModelBloc, PpModelState>(
        builder: (context, state) =>
            BlocConsumer<PpUserProgressBloc, PpUserProgressState>(
          listener: (context, state) {
            checkIfEverythingIsFinished();
            // PpBlocUtil.timeFunction(checkIfEverythingIsFinished);
          },
          builder: (context, state) {
            final selectedPhase = PpBlocUtil.getUserSelectedPhase();
            if (state.phaseSelection == -1 ||
                selectedPhase == const PpPhase()) {
              return PpPhaseNotFound();
            }
            return DefaultTabController(
              length: selectedPhase.groups.length,
              initialIndex: 0,
              child: BlocListener<PpModelBloc, PpModelState>(
                listener: (context, state) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      duration: Duration(seconds: 2),
                      content: Text(
                        FlutterI18n.translate(
                          context,
                          'modules.practice_phase.module_data',
                        ),
                      ),
                    ),
                  );
                },
                child: Scaffold(
                  appBar: buildAppBar(context, selectedPhase),
                  body: buildBody(context, selectedPhase),
                ),
              ),
            );
          },
        ),
      );

  Stack buildBody(BuildContext context, PpPhase selectedPhase) => Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: [
          TabBarView(
            children: [
              for (final group in selectedPhase.groups)
                PpGroupBody(
                  group: group,
                  state: PpBlocUtil.getDynamicGroupState(group, selectedPhase),
                  selectedPhase: selectedPhase,
                  showDebug: showDebug,
                  highlightedUnitId: highlightedUnitId,
                  checkIfEverythingIsFinished: checkIfEverythingIsFinished,
                  removeFinishedForAnimation: removeFinishedForAnimation,
                ),
            ],
          ),
          ValueListenableBuilder<bool>(
            valueListenable: finished,
            builder: (context, value, child) => AnimatedPositioned(
              duration: Duration(milliseconds: 560),
              bottom: value ? 0 : -260,
              left: 0,
              right: 0,
              curve: Curves.fastLinearToSlowEaseIn,
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.8),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Card(
                  margin: EdgeInsets.zero,
                  elevation: 8,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                          topLeft: Radius.circular(10))),
                  color: Theme.of(context).primaryColor,
                  child: SafeArea(
                    bottom: true,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                FlutterI18n.translate(
                                  context,
                                  'modules.practice_phase.ready',
                                ),
                                style: Theme.of(context).textTheme.headline2,
                              ),
                              margin: EdgeInsets.only(top: 30),
                            ),
                            Container(
                              child: Text(
                                FlutterI18n.translate(
                                  context,
                                  'modules.practice_phase.everything_done',
                                ),
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(fontSize: 15),
                              ),
                              margin: EdgeInsets.only(bottom: 30),
                            ),
                          ],
                        ),
                        RaisedButton(
                          onPressed: () {
                            removeFinishedForAnimation();
                            Navigator.pushNamed(
                              context,
                              ppSendDataRoute,
                              arguments: PpSendDataPageArguments(
                                PpBlocUtil.getUserSelectedPhase(),
                              ),
                            ).then((value) => checkIfEverythingIsFinished());
                          },
                          color: CorporateColors.tinyCampusOrange,
                          child: Text(
                            FlutterI18n.translate(
                              context,
                              'modules.practice_phase.submit',
                            ),
                            style: Theme.of(context).textTheme.button?.copyWith(
                                  color: Colors.white,
                                  fontSize: 20,
                                ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      );

  AppBar buildAppBar(BuildContext context, PpPhase selectedPhase) => AppBar(
        actions: [
          PopupMenuButton<OverviewPopUpSelection>(
            onSelected: (result) {
              switch (result) {
                case OverviewPopUpSelection.navigatePhaseSelectionScreen:
                  Navigator.pushReplacementNamed(context, ppSelectionRoute);
                  break;
                case OverviewPopUpSelection.forceReload:
                  PpBlocUtil.addModelEvent(
                    PpFetchAndReplaceModelFromRemoteEvent(forceReload: true),
                  );
                  break;
                case OverviewPopUpSelection.forceReloadDebug:
                  PpBlocUtil.addModelEvent(
                    PpFetchDebugModelFromRemoteEvent(forceReload: true),
                  );
                  break;
                case OverviewPopUpSelection.reset:
                  selectedPhase.groups
                      .expand((e) => e.units)
                      .expand((e) => [
                            PpRemoveQuestionResultsEvent(e),
                            PpUncheckAllUnitEntriesEvent(e),
                          ])
                      .forEach(PpBlocUtil.addProgressEvent);
                  break;
                case OverviewPopUpSelection.cheat:
                  selectedPhase.groups
                      .expand((e) => e.units)
                      .expand((e) => [
                            PpCheckAllUnitEntriesEvent(e),
                            PpSetAllQuestionsToCorrectEvent(e),
                          ])
                      .forEach(PpBlocUtil.addProgressEvent);
                  break;
              }
            },
            itemBuilder: (_) => [
              // TcPopupWidgets.buildCheckedPopupItem(pp_settingsFastAnimations,
              //     PopUpSelection.animationSpeed, "Schnelle Animationen"),
              // PopupMenuDivider(),
              // TcPopupWidgets.buildCheckedPopupItem(
              //     pp_settingsEnableVibrations,
              //     PopUpSelection.vibration,
              //     "Vibrationen"),
              // if (!kReleaseMode)
              TcPopupWidgets.buildTextPopupItem(
                value: OverviewPopUpSelection.navigatePhaseSelectionScreen,
                title: "Andere Phase auswählen",
                subtitle: tc_util.read(selectedPhase.title, context),
                subtitleMaxLines: 1,
              ),
              PopupMenuDivider(),
              TcPopupWidgets.buildTextPopupItem(
                value: OverviewPopUpSelection.forceReload,
                title: "Moduldaten aktualisieren",
                // subtitle: "Debug Only",
                subtitleMaxLines: 1,
              ),
              if (!kReleaseMode) PopupMenuDivider(),
              if (!kReleaseMode)
                TcPopupWidgets.buildTextPopupItem(
                  value: OverviewPopUpSelection.forceReloadDebug,
                  title: "Moduldaten aktualisieren",
                  subtitle: "(Debug-Webserver)",
                  subtitleMaxLines: 1,
                ),
              if (!kReleaseMode) PopupMenuDivider(),
              if (!kReleaseMode)
                TcPopupWidgets.buildTextPopupItem(
                  value: OverviewPopUpSelection.reset,
                  title: "Fortschritt zurücksetzen",
                  subtitle: "(Debug)",
                  subtitleMaxLines: 1,
                ),
              if (!kReleaseMode) PopupMenuDivider(),
              if (!kReleaseMode)
                TcPopupWidgets.buildTextPopupItem(
                  value: OverviewPopUpSelection.cheat,
                  title: "Praxisphase abschließen",
                  subtitle: "(Cheat)",
                  subtitleMaxLines: 1,
                ),
            ],
          )
        ],
        leading: Hero(
            tag: "back",
            child: Material(color: Colors.transparent, child: BackButton())),
        title: GestureDetector(
            onTap: () {
              if (!kReleaseMode) {
                showDebug.value = !showDebug.value;
              }
            },
            child: Text(tc_util.read(selectedPhase.title, context))),
        bottom: TabBar(
          indicatorWeight: 22,
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              width: 2.0,
              color: CurrentTheme().themeData.colorScheme.secondary,
            ),
            insets: EdgeInsets.symmetric(horizontal: 8.0),
          ),
          physics: BouncingScrollPhysics(),
          isScrollable: true,
          unselectedLabelColor: CurrentTheme().themeData.iconTheme.color,
          labelStyle: CurrentTheme().themeData.textTheme.headline4,
          labelColor: CurrentTheme().themeData.colorScheme.secondary,
          tabs: [
            for (var group in selectedPhase.groups)
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(tc_util.read(group.title, context)),
                  Container(height: 6),
                  buildGroupTabIndicator(
                    group,
                    PpBlocUtil.getDynamicGroupState(group, selectedPhase),
                  ),
                ],
              ),
          ],
        ),
      );

  bool _isFinished() {
    var phase = PpBlocUtil.getUserSelectedPhase();
    final result = phase.groups.every((element) =>
        PpBlocUtil.getDynamicGroupState(element, phase).state ==
        GroupEnum.complete);
    sprint("praxis phase has been completed by user: ${result.toString()}",
        caller: PpOverviewPage, style: PrintStyle.praxisPhase);

    return result && !PpBlocUtil.successFullSend(phase.phaseId);
  }

  Widget buildGroupTabIndicator(PpGroup group, GroupState groupState) =>
      Container(
        child: !groupState.isLocked
            ? Text(
                "${groupState.amountCompletedUnits} / ${group.units.length}",
                style: TextStyle(fontSize: 14.0, letterSpacing: -1.6),
              )
            : Icon(Icons.lock, size: 16),
      );
}
