/*
 * Copyright 2020-2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:percent_indicator/percent_indicator.dart';

import '../../../../../common/constants/routing_constants.dart';
import '../../../../../common/tc_theme.dart';
import '../../../model/pp_unit.dart';
import '../../../utility/pp_bloc_util.dart';
import '../../pp_entry/pp_entry_page.dart';
import '../../pp_quiz/pp_quiz_page.dart';

void constCallback() {}

class PPElemCards extends StatelessWidget {
  final String title;
  final String body;
  final int level;
  final PpUnit unit;
  final UnitState unitState;
  final VoidCallback navigationCallbackOnPop;

  const PPElemCards({
    Key? key,
    required this.title,
    required this.body,
    required this.level,
    required this.unit,
    required this.unitState,
    this.navigationCallbackOnPop = constCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        margin: EdgeInsets.zero,
        elevation: 4,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              buildElemCardHeader(context),
              buildElemCardBody(context),
            ],
          ),
        ),
      );

  Widget buildElemCardHeader(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(vertical: 16),
        child: Column(
          children: [
            Text(title,
                style: Theme.of(context)
                    .textTheme
                    .headline2
                    ?.copyWith(fontWeight: FontWeight.w700)),
            Text(
              "LEVEL $level",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  ?.copyWith(color: CurrentTheme().textPassive),
            ),
          ],
        ),
      );

  Column buildElemCardBody(BuildContext context) {
    final cState = identifyState();
    final progress = calculateProgress();
    return Column(
      children: [
        Container(
          height: 80,
          margin: EdgeInsets.symmetric(vertical: 12),
          child: progress == -1
              ? buildElemDescription(cState, context)
              : progressWidget(context, progress),
        ),
        Container(
            margin: EdgeInsets.only(top: 12, bottom: 12),
            child: buildElemButton(cState, context)),
      ],
    );
  }

  Widget progressWidget(BuildContext context, double progress) =>
      CircularPercentIndicator(
        radius: 72,
        lineWidth: 2,
        animation: true,
        curve: Curves.easeOut,
        animationDuration: 900,
        backgroundColor: progress == 1
            ? Theme.of(context).primaryColor
            : CurrentTheme().passiveIconColor,
        progressColor: progress == 1
            ? CurrentTheme().passiveIconColor
            : CorporateColors.tinyCampusOrange,
        center: progress == 1
            ? Icon(
                Icons.check,
                size: 30,
              )
            : Text(
                "${(progress * 100).floor()} %",
                style: Theme.of(context).textTheme.headline4?.copyWith(
                      color: Color.fromRGBO(252, 90, 73, 1.0),
                    ),
              ),
        percent: progress,
      );

  double calculateProgress() {
    if (level == 1) {
      return unitState.amountUncheckedEntries == 0 ? 1 : -1;
    }
    if (level == 2) {
      var amount =
          unitState.amountWrongAnswers + unitState.amountCorrectAnswers;
      if (amount == 0) {
        return -1;
      }
      return unitState.amountCorrectAnswers / amount;
    }
    return -1;
  }

  Widget buildElemDescription(ElemCardState cState, BuildContext context) =>
      cState == ElemCardState.locked
          ? Icon(
              Icons.lock,
              size: 28,
              color: CorporateColors.passiveFontColor,
            )
          : Center(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Text(
                  body,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            );

  Widget buildElemButton(ElemCardState cState, BuildContext context) {
    var bgColor = CurrentTheme().themeData.colorScheme.secondary;
    var fontColor = CurrentTheme().themeData.primaryColor;
    var buttonText = FlutterI18n.translate(
        context, 'modules.practice_phase.pp_unit.button_text_start');
    switch (cState) {
      case ElemCardState.highlight:
        bgColor = CorporateColors.tinyCampusOrange;
        fontColor = Colors.white;
        buttonText = FlutterI18n.translate(
            context, 'modules.practice_phase.pp_unit.button_text_start');
        break;
      case ElemCardState.repeat:
        bgColor = CurrentTheme().themeData.primaryColor;
        fontColor = CurrentTheme().themeData.colorScheme.secondary;
        buttonText = FlutterI18n.translate(
            context, 'modules.practice_phase.pp_unit.button_text_repeat');
        break;
      case ElemCardState.locked:
        bgColor = CorporateColors.passiveFontColor;
        fontColor = Colors.white;
        buttonText = FlutterI18n.translate(
            context, 'modules.practice_phase.pp_unit.button_text_locked');
        break;
      case ElemCardState.failed:
        bgColor = CurrentTheme().themeData.primaryColor;
        fontColor = CorporateColors.tinyCampusOrange;
        buttonText = FlutterI18n.translate(
            context, 'modules.practice_phase.pp_unit.button_text_repeat');
        break;
    }
    return RaisedButton(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
      elevation: 0.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
      ),
      clipBehavior: Clip.antiAlias,
      onPressed: cState == ElemCardState.locked
          ? null
          : () {
              if (level == 1) {
                Navigator.pushNamed(
                  context,
                  ppEntryRoute,
                  arguments: PpEntryPageArguments(unit),
                ).then((v) => navigationCallbackOnPop());
              } else if (level == 2) {
                Navigator.pushNamed(
                  context,
                  ppQuizRoute,
                  arguments: PpQuizPageArguments(unit),
                ).then((v) => navigationCallbackOnPop());
              }
            },
      child: Text(
        buttonText,
        style: Theme.of(context).textTheme.button?.copyWith(
              color: fontColor,
              fontSize: 16,
            ),
      ),
      color: bgColor,
    );
  }

  ElemCardState identifyState() {
    if (level == 1) {
      switch (unitState.state) {
        case UnitEnum.initial:
          return ElemCardState.highlight;
        case UnitEnum.level1Complete:
          return ElemCardState.repeat;
        case UnitEnum.level2Failed:
          return ElemCardState.repeat;
        case UnitEnum.level2Complete:
          return ElemCardState.repeat;
      }
    } else if (level == 2) {
      switch (unitState.state) {
        case UnitEnum.initial:
          return ElemCardState.locked;
        case UnitEnum.level1Complete:
          return ElemCardState.highlight;
        case UnitEnum.level2Failed:
          return ElemCardState.failed;
        case UnitEnum.level2Complete:
          return ElemCardState.repeat;
      }
    } else {
      return ElemCardState.highlight;
    }
  }
}

enum ElemCardState {
  highlight,
  repeat,
  locked,
  failed,
}
