// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_model_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpModelStateCopyWith on PpModelState {
  PpModelState copyWith({
    DateTime? lastUpdated,
    List<PpPhase>? phases,
  }) {
    return PpModelState(
      lastUpdated: lastUpdated ?? this.lastUpdated,
      phases: phases ?? this.phases,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpModelState _$PpModelStateFromJson(Map json) => PpModelState(
      phases: (json['phases'] as List<dynamic>?)
              ?.map(
                  (e) => PpPhase.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
      lastUpdated: json['lastUpdated'] == null
          ? null
          : DateTime.parse(json['lastUpdated'] as String),
    );

Map<String, dynamic> _$PpModelStateToJson(PpModelState instance) =>
    <String, dynamic>{
      'phases': instance.phases.map((e) => e.toJson()).toList(),
      'lastUpdated': instance.lastUpdated?.toIso8601String(),
    };
