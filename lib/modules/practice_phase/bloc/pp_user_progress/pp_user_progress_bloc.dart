/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../bloc/perm_hyd_bloc/perm_hyd_bloc.dart';
import '../../../../common/constants/api_constants.dart';
import '../../../../common/styled_print.dart';
import '../../../../repositories/http/http_repository.dart';
import '../../model/pp_unit.dart';

part 'pp_user_progress_bloc.g.dart';
part 'pp_user_progress_event.dart';
part 'pp_user_progress_state.dart';

class PpUserProgressBloc
    extends PermHydBloc<PpUserProgressEvent, PpUserProgressState> {
  PpUserProgressBloc() : super(PpUserProgressState.initial());

  @override
  Stream<PpUserProgressState> mapEventToState(
    PpUserProgressEvent event,
  ) async* {
    yield await event.executeAction(state);
  }

  @override
  PpUserProgressState fromJson(Map<String, dynamic> json) {
    try {
      return PpUserProgressState.fromJson(json);
    } on Exception catch (e) {
      debugPrint(e.toString());
      return PpUserProgressState.initial();
    }
  }

  @override
  Map<String, dynamic> toJson(PpUserProgressState state) => state.toJson();
}
