// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_success_data.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpSuccessDataCopyWith on PpSuccessData {
  PpSuccessData copyWith({
    String? lastName,
    int? matNumber,
  }) {
    return PpSuccessData(
      lastName: lastName ?? this.lastName,
      matNumber: matNumber ?? this.matNumber,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpSuccessData _$PpSuccessDataFromJson(Map json) => PpSuccessData(
      matNumber: json['matNumber'] as int? ?? -1,
      lastName: json['lastName'] as String? ?? "",
    );

Map<String, dynamic> _$PpSuccessDataToJson(PpSuccessData instance) =>
    <String, dynamic>{
      'matNumber': instance.matNumber,
      'lastName': instance.lastName,
    };
