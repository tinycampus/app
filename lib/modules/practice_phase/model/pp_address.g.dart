// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_address.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpAddressCopyWith on PpAddress {
  PpAddress copyWith({
    String? city,
    String? email,
    String? latitude,
    String? longitude,
    String? phone,
    String? postal,
    String? street,
  }) {
    return PpAddress(
      city: city ?? this.city,
      email: email ?? this.email,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      phone: phone ?? this.phone,
      postal: postal ?? this.postal,
      street: street ?? this.street,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpAddress _$PpAddressFromJson(Map json) => PpAddress(
      street: json['street'] as String? ?? "",
      city: json['city'] as String? ?? "",
      postal: json['postal'] as String? ?? "",
      phone: json['phone'] as String? ?? "",
      email: json['email'] as String? ?? "",
      latitude: json['latitude'] as String? ?? "",
      longitude: json['longitude'] as String? ?? "",
    );

Map<String, dynamic> _$PpAddressToJson(PpAddress instance) => <String, dynamic>{
      'street': instance.street,
      'city': instance.city,
      'postal': instance.postal,
      'phone': instance.phone,
      'email': instance.email,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
