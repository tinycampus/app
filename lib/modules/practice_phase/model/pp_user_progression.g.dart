// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_user_progression.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpUserProgressionCopyWith on PpUserProgression {
  PpUserProgression copyWith({
    Set<int>? entryCheckedMap,
    Set<int>? entryQuestionMap,
    int? phaseSelection,
    Set<int>? skippedGroups,
    Map<int, DateTime>? successfulSent,
  }) {
    return PpUserProgression(
      entryCheckedMap: entryCheckedMap ?? this.entryCheckedMap,
      entryQuestionMap: entryQuestionMap ?? this.entryQuestionMap,
      phaseSelection: phaseSelection ?? this.phaseSelection,
      skippedGroups: skippedGroups ?? this.skippedGroups,
      successfulSent: successfulSent ?? this.successfulSent,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpUserProgression _$PpUserProgressionFromJson(Map json) => PpUserProgression(
      phaseSelection: json['phaseSelection'] as int? ?? -1,
      skippedGroups: (json['skippedGroups'] as List<dynamic>?)
              ?.map((e) => e as int)
              .toSet() ??
          const {},
      entryCheckedMap: (json['entryCheckedMap'] as List<dynamic>?)
              ?.map((e) => e as int)
              .toSet() ??
          const {},
      entryQuestionMap: (json['entryQuestionMap'] as List<dynamic>?)
              ?.map((e) => e as int)
              .toSet() ??
          const {},
      successfulSent: (json['successfulSent'] as Map?)?.map(
            (k, e) =>
                MapEntry(int.parse(k as String), DateTime.parse(e as String)),
          ) ??
          const {},
    );

Map<String, dynamic> _$PpUserProgressionToJson(PpUserProgression instance) =>
    <String, dynamic>{
      'phaseSelection': instance.phaseSelection,
      'skippedGroups': instance.skippedGroups.toList(),
      'entryCheckedMap': instance.entryCheckedMap.toList(),
      'entryQuestionMap': instance.entryQuestionMap.toList(),
      'successfulSent': instance.successfulSent
          .map((k, e) => MapEntry(k.toString(), e.toIso8601String())),
    };
