/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

import 'pp_answer.dart';

part 'pp_question.g.dart';

/// [PpQuestion] is one specific question in the quiz of an [PpUnit]
///
/// It contains [questionId], [answers], [correctAnswerId], [body],
/// [bodyCorrect], [bodyWrong], [image], [imageAlt]
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpQuestion {
  /// [questionId] is a unique Id for [PpQuestion]
  final int questionId;

  /// [answers] contains all answers to the question
  final List<PpAnswer> answers;

  /// [correctAnswerId] is the id of the correct answer
  final int correctAnswerId;

  /// [body] contains the question text in different languages
  final Map<String, String> body;

  /// [bodyCorrect] is the text when clicked on
  /// the correct answer in different languages
  final Map<String, String> bodyCorrect;

  /// [bodyWrong] is the text when clicked on
  /// a wrong answer in different languages
  final Map<String, String> bodyWrong;

  /// [image] the url to the image displayed in the background
  final String image;

  /// [imageAlt] the description of the image
  final Map<String, String> imageAlt;

  /// The constructor of [PpQuestion]
  PpQuestion({
    this.questionId = -1,
    this.answers = const [],
    this.correctAnswerId = -1,
    this.body = const {},
    this.bodyCorrect = const {},
    this.bodyWrong = const {},
    this.image = "",
    this.imageAlt = const {},
  });

  factory PpQuestion.fromJson(Map<String, dynamic> json) =>
      _$PpQuestionFromJson(json);

  Map<String, dynamic> toJson() => _$PpQuestionToJson(this);
}
