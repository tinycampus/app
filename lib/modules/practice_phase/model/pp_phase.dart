/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../common/styled_print.dart';
import 'pp_group.dart';
import 'pp_person.dart';

part 'pp_phase.g.dart';

/// [PpPhase] is a specific practice phase inside the module
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpPhase extends Equatable {
  /// [phaseId] a unique id for [PpPhase]
  final int phaseId;

  /// [title] is a map with the title in different languages
  final Map<String, String> title;

  /// [groups] contains all [PpGroup]s of the phase
  final List<PpGroup> groups;

  /// [description] is the description in different languages
  @JsonKey(fromJson: getMap)
  final Map<String, String> description;

  /// [faqUrl] contains the url to the faq
  final String faqUrl;

  /// [categoryId] is a unique id that points to a
  /// [Category] in the Q&A module
  final int categoryId;

  /// [persons] the supervisors of the practice phase
  final List<PpPerson> persons;

  /// The constructor of [PpPhase]
  const PpPhase({
    this.phaseId = -1,
    this.title = const {},
    this.groups = const [],
    this.description = const {},
    this.faqUrl = "",
    this.categoryId = -1,
    this.persons = const [],
  });

  static Map<String, String> getMap(dynamic val) {
    sprint(val.runtimeType.toString());
    if (val is Map<String, String>) {
      return val;
    } else {
      try {
        final parsed = Map<String, String>.from(val);
        return parsed;
      } on Exception catch (e) {
        eprint("${e.toString()} error while loading persistence "
            "data in PpPhase: ${val.toString()}");
        return <String, String>{};
      }
    }
  }

  factory PpPhase.fromJson(Map<String, dynamic> json) =>
      _$PpPhaseFromJson(json);

  Map<String, dynamic> toJson() => _$PpPhaseToJson(this);

  @override
  List<Object?> get props => [
        phaseId,
        title,
        groups,
        description,
        faqUrl,
        categoryId,
        persons,
      ];
}
