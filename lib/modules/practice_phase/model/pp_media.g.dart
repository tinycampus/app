// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_media.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpMediaCopyWith on PpMedia {
  PpMedia copyWith({
    int? mediaId,
    int? order,
    Map<String, String>? title,
    int? type,
    String? url,
  }) {
    return PpMedia(
      mediaId: mediaId ?? this.mediaId,
      order: order ?? this.order,
      title: title ?? this.title,
      type: type ?? this.type,
      url: url ?? this.url,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpMedia _$PpMediaFromJson(Map json) => PpMedia(
      mediaId: json['mediaId'] as int? ?? -1,
      order: json['order'] as int? ?? -1,
      type: json['type'] as int? ?? -1,
      url: json['url'] as String? ?? "",
      title: (json['title'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
    );

Map<String, dynamic> _$PpMediaToJson(PpMedia instance) => <String, dynamic>{
      'mediaId': instance.mediaId,
      'order': instance.order,
      'type': instance.type,
      'url': instance.url,
      'title': instance.title,
    };
