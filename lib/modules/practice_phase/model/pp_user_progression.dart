/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pp_user_progression.g.dart';

/// [PpUserProgression] tracks the progression of an user in a practice phase
///
/// It contains [phaseSelection], [skippedGroups],
/// [entryCheckedMap], [entryQuestionMap], [successfulSent]
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpUserProgression {
  /// [phaseSelection] is the [phaseId] of a selected [PpPhase]
  final int phaseSelection;

  /// [skippedGroups] contains all skipped [PpGroup]s
  final Set<int> skippedGroups;

  /// [entryCheckedMap] contains all checked [PpEntry]s
  final Set<int> entryCheckedMap;

  /// [entryQuestionMap] contains all answered [PpQuestion]s
  final Set<int> entryQuestionMap;

  /// [successfulSent] contains all times a user sent it's
  /// data to the backend for a [PpPhase]
  final Map<int, DateTime> successfulSent;

  PpUserProgression({
    this.phaseSelection = -1,
    this.skippedGroups = const {},
    this.entryCheckedMap = const {},
    this.entryQuestionMap = const {},
    this.successfulSent = const {},
  });

  factory PpUserProgression.fromJson(Map<String, dynamic> json) =>
      _$PpUserProgressionFromJson(json);

  Map<String, dynamic> toJson() => _$PpUserProgressionToJson(this);
}
