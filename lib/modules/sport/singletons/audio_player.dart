/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter_sound_lite/flutter_sound.dart';

import '../../../common/styled_print.dart';

class AudioPlayer {
  static final AudioPlayer _singleton = AudioPlayer._internal();

  factory AudioPlayer() => _singleton;

  final FlutterSoundPlayer _mPlayer = FlutterSoundPlayer();

  AudioPlayer._internal() {
    _mPlayer.openAudioSession(
      focus: AudioFocus.requestFocusAndDuckOthers,
      mode: SessionMode.modeSpokenAudio,
    );
  }

  bool get isPlaying => _mPlayer.isPlaying;

  bool get isPaused => _mPlayer.isPaused;

  // String _exampleAudioFile = 'https://tinycampus.de/sport/deepchills.mp3';
  // 'https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3';

  String get randomSong {
    const base = "https://tinycampus.de/sport/";
    final songs = [
      "deepchills",
      "letitflow",
      "swimmingplaces",
      "thenights",
    ];
    return "$base${songs[Random().nextInt(songs.length)]}.mp3";
  }

  double _volume = 1.0;

  bool get muted => _volume == 0.0;

  Future<void> play({String? path}) async {
    // _mPlayer.setVolume(Random().nextDouble());
    _mPlayer.setVolume(0.8);
    // if (!_mPlayer.isPlaying) {
    // return _mPlayer.startPlayer(
    //   fromURI: path ?? _exampleAudioFile,
    //   codec: Codec.mp3,
    // );
    try {
      _mPlayer.setAudioFocus(focus: AudioFocus.requestFocusAndDuckOthers);
      await _mPlayer.startPlayer(
        fromURI: path ?? randomSong,
        whenFinished: play,
        codec: Codec.aacADTS,
      ); // Play a temporary file
    } on Exception catch (e) {
      eprint(e);
    }
    // final _playerSubscription = _mPlayer.onProgress.listen((e) {
    //   sprint("MUSIC EXITED");
    //   eprint(e);
    // });
  }

  void playOrResume() {
    if (_mPlayer.isPaused) {
      resume();
    } else {
      play();
    }
  }

  Future<void> pauseOrResume() async {
    try {
      _mPlayer.isPaused ? _mPlayer.resumePlayer() : _mPlayer.pausePlayer();
    } on Exception catch (e) {
      eprint(e);
    }
  }

  Future<void> stop() async {
    try {
      return _mPlayer.stopPlayer();
    } on Exception catch (e) {
      eprint(e, caller: AudioPlayer);
    }
  }

  Future<void> pause() async {
    try {
      return _mPlayer.isPlaying ? _mPlayer.pausePlayer() : null;
    } on Exception catch (e) {
      eprint(e, caller: AudioPlayer);
    }
  }

  Future<void> resume() async {
    try {
      return _mPlayer.resumePlayer();
    } on Exception catch (e) {
      eprint(e, caller: AudioPlayer);
    }
  }

  Future<bool> toggleMute() async {
    _volume = muted ? 1.0 : 0.0;
    await _mPlayer.setVolume(_volume);
    return muted;
  }

  void setVolume(double volume) {
    _mPlayer.setVolume(volume);
  }
}
