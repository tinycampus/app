/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'external_content.g.dart';

@immutable
@JsonSerializable()
class ExternalContent {
  final String imageUrl;
  final Map<String, String> text;

  // In case there are different links for different languages
  final Map<String, String> link;

  const ExternalContent(
      {required this.imageUrl, required this.text, required this.link});

  factory ExternalContent.fromJson(Map<String, dynamic> json) =>
      _$ExternalContentFromJson(json);

  Uri get imageUri => Uri.parse(imageUrl);

  Map<String, Uri> get linkUri =>
      link.map((key, value) => MapEntry(key, Uri.parse(value)));

  @override
  String toString() =>
      '{ imageURL: { $imageUrl }, text: { $text }, link: { $link } }';
}
