/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../common/styled_print.dart';
import '../../../common/tc_utility_functions.dart';
import 'instruction.dart';

part 'exercise.g.dart';

enum EPhase { preparation, execution, pause }
enum EMode { switching, alternating, none }
enum EInstruction {
  effect,
  startingPosition,
  motionSequence,
  repetitions,
  variant
}

@JsonSerializable()
@immutable
class Exercise extends Equatable implements Comparable<Exercise> {
  final Map<String, String> name;
  final int id;
  final String imageUrl;
  final Map<String, String> imgAltText;
  final Instruction instructions;
  final String exerciseMode;
  final int repetition;
  final bool showDuration;
  final int singleDuration;
  final int totalDuration;
  final int sets;

  // Unused as of now
  final List<String> tags;
  final String category;
  final Map<String, String> soundFile;

  Exercise({
    this.name = const {},
    required this.id,
    required this.imageUrl,
    this.imgAltText = const {},
    required this.instructions,
    this.exerciseMode = "",
    required this.repetition,
    this.showDuration = true,
    this.singleDuration = 3000,
    this.totalDuration = 60000,
    required this.sets,
    this.tags = const [],
    required this.category,
    this.soundFile = const {},
  });

  factory Exercise.fromJson(Map<String, dynamic> json) =>
      _$ExerciseFromJson(json);

  Map<String, dynamic> toJson() => _$ExerciseToJson(this);

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [
        name,
        id,
        imageUrl,
        imgAltText,
        instructions,
        exerciseMode,
        repetition,
        showDuration,
        singleDuration,
        totalDuration,
        sets,
        tags,
        category,
        soundFile,
      ];

  Duration get computeTotalDuration {
    final mode = getExerciseMode;
    final defaultDuration = Duration(seconds: 60);
    switch (mode) {

      /// Everything is done [TWICE] (because each side)
      case EMode.switching:
        if ((repetition == 0) || (singleDuration == 0) || (sets == 0)) {
          eprint(
              "[SPORT][MODEL] "
              "Error while computing EMode.switching, "
              "Some values are missing or 0",
              caller: Exercise);
          return defaultDuration;
        } else {
          final msResult = repetition * singleDuration * sets * 2;
          return Duration(milliseconds: msResult);
        }

      /// Everything is done [TWICE] (because each side alternating)
      case EMode.alternating:
        if (repetition == 0 || singleDuration == 0 || sets == 0) {
          eprint(
              "[SPORT][MODEL] "
              "Error while computing EMode.alternating, "
              "Some values are missing or 0",
              caller: Exercise);
          return defaultDuration;
        } else {
          final msResult = repetition * singleDuration * sets * 2;
          return Duration(milliseconds: msResult);
        }
      case EMode.none:
        if (totalDuration == 0) {
          eprint(
              "[SPORT][MODEL] "
              "Total duration was not found, returning default 60 seconds",
              caller: Exercise);
        }
        return Duration(milliseconds: totalDuration);
      default:
        Duration(milliseconds: totalDuration);
    }
    return defaultDuration;
  }

  @override
  int compareTo(Exercise other) => id - other.id;

  EMode get getExerciseMode {
    switch (exerciseMode) {
      case "switching":
        return EMode.switching;
      case "alternating":
        return EMode.alternating;
      case "none":
      default:
        return EMode.none;
    }
  }

  int calculateAmountInstructions() {
    var amount = 0;
    if (hasInstructionByExercise(EInstruction.effect)) {
      amount++;
    }
    if (hasInstructionByExercise(EInstruction.motionSequence)) {
      amount++;
    }
    if (hasInstructionByExercise(EInstruction.repetitions)) {
      amount++;
    }
    if (hasInstructionByExercise(EInstruction.startingPosition)) {
      amount++;
    }
    if (hasInstructionByExercise(EInstruction.variant)) {
      amount++;
    }
    return amount;
  }

  bool hasInstructionByExercise(EInstruction instruction) {
    switch (instruction) {
      case EInstruction.effect:
        return hasGermanContent(instructions.effect);
      case EInstruction.startingPosition:
        return hasGermanContent(instructions.startingPosition);
      case EInstruction.motionSequence:
        return hasGermanContent(instructions.motionSequence);
      case EInstruction.repetitions:
        return hasGermanContent(instructions.repetitions);
      case EInstruction.variant:
        return hasGermanContent(instructions.variant);
      default:
        return false;
    }
  }
}

class NoNextInstructionException implements Exception {
  NoNextInstructionException();
}
