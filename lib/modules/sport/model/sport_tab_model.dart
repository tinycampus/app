/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import 'external_content_list.dart';

part 'sport_tab_model.g.dart';

@JsonSerializable()
class SportTabModel {
  Map<String, String> description;
  List<ExternalContentList> segment;
  Map<String, String> headline;
  Map<String, String> tab;

  SportTabModel({
    required this.description,
    required this.segment,
    required this.headline,
    required this.tab,
  });

  /// Creates a [SportTabModel] from Json.
  factory SportTabModel.fromJson(Map<String, dynamic> json) =>
      _$SportTabModelFromJson(json);

  /// Creates a Json from [SportTabModel].
  Map<String, dynamic> toJson() => _$SportTabModelToJson(this);
}
