/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'instruction.g.dart';

@JsonSerializable()
@immutable
class Instruction extends Equatable {
  final Map<String, String> effect;
  final Map<String, String> startingPosition;
  final Map<String, String> motionSequence;
  final Map<String, String> repetitions;
  final Map<String, String> variant;

  Instruction({
    this.effect = const {},
    this.startingPosition = const {},
    this.motionSequence = const {},
    this.repetitions = const {},
    this.variant = const {},
  });

  factory Instruction.fromJson(Map<String, dynamic> json) =>
      _$InstructionFromJson(json);

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [
        effect,
        startingPosition,
        motionSequence,
        repetitions,
        variant,
      ];
}
