// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exercise.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Exercise _$ExerciseFromJson(Map<String, dynamic> json) => Exercise(
      name: (json['name'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry(k, e as String),
          ) ??
          const {},
      id: json['id'] as int,
      imageUrl: json['imageUrl'] as String,
      imgAltText: (json['imgAltText'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry(k, e as String),
          ) ??
          const {},
      instructions:
          Instruction.fromJson(json['instructions'] as Map<String, dynamic>),
      exerciseMode: json['exerciseMode'] as String? ?? "",
      repetition: json['repetition'] as int,
      showDuration: json['showDuration'] as bool? ?? true,
      singleDuration: json['singleDuration'] as int? ?? 3000,
      totalDuration: json['totalDuration'] as int? ?? 60000,
      sets: json['sets'] as int,
      tags:
          (json['tags'] as List<dynamic>?)?.map((e) => e as String).toList() ??
              const [],
      category: json['category'] as String,
      soundFile: (json['soundFile'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry(k, e as String),
          ) ??
          const {},
    );

Map<String, dynamic> _$ExerciseToJson(Exercise instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'imageUrl': instance.imageUrl,
      'imgAltText': instance.imgAltText,
      'instructions': instance.instructions,
      'exerciseMode': instance.exerciseMode,
      'repetition': instance.repetition,
      'showDuration': instance.showDuration,
      'singleDuration': instance.singleDuration,
      'totalDuration': instance.totalDuration,
      'sets': instance.sets,
      'tags': instance.tags,
      'category': instance.category,
      'soundFile': instance.soundFile,
    };
