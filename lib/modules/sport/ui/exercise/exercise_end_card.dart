/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart';
import '../../../../common/widgets/tc_button.dart';
import '../../i18n_keys.dart';
import '../../model/exercise.dart';

class ExerciseEndCard extends StatelessWidget {
  static const String _i18nKey = '$sportExercise.end_card';
  final List<Exercise> completedExercises;

  ExerciseEndCard({
    Key? key,
    required this.completedExercises,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 12.0),
            child: Stack(
              children: [
                Positioned.fill(
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: buildContent(context),
                  ),
                ),
                Positioned(
                  bottom: 16,
                  left: 0,
                  right: 0,
                  child: TCButton(
                    buttonLabel:
                        FlutterI18n.translate(context, '$_i18nKey.quit'),
                    onPressedCallback: () => Navigator.of(context).pop(),
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  double delayedProgress(double animationValue, int index) =>
      ((animationValue * completedExercises.length.toDouble()) - (index / 3))
          .clamp(0, 1)
          .toDouble();

  Column buildContent(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          header(context),
          for (var i = 0; i < completedExercises.length; i++)
            TweenAnimationBuilder<double>(
                duration: Duration(milliseconds: 560 + 260 * i),
                curve: Curves.linear,
                tween: Tween<double>(begin: 0.0, end: 1.0),
                builder: (context, value, child) => Transform.translate(
                    offset: Offset(0, ((1 - delayedProgress(value, i)) * 24)),
                    child: Opacity(
                        opacity: delayedProgress(value, i), child: child)),
                child: singleListTile(completedExercises[i], context)),
          Container(height: 84)
        ],
      );

  Container singleListTile(Exercise e, BuildContext context) => Container(
        decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(12))),
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.symmetric(vertical: 6.0),
        child: ListTile(
          leading: Image(image: NetworkImage(e.imageUrl)),
          title: Text(
            read(e.name, context),
            style: CurrentTheme()
                .themeData
                .textTheme
                .headline4
                ?.copyWith(fontWeight: FontWeight.w700),
          ),
          tileColor: CurrentTheme().themeData.primaryColor,
          contentPadding:
              EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
          // TODO: Replace with actual functionality
          // trailing: HeartWidget(
          //   likable: _LikePlaceholder(),
          //   toggleLike: ()
          // => Scaffold.of(context).showSnackBar(SnackBar(
          //     content: Text('Placeholder'),
          //   )),
          // ),
        ),
      );

  Container header(BuildContext context) => Container(
        margin: EdgeInsets.only(top: 52, bottom: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              FlutterI18n.translate(context, '$_i18nKey.done'),
              textAlign: TextAlign.left,
              style: CurrentTheme()
                  .themeData
                  .textTheme
                  .headline1
                  ?.copyWith(fontSize: 42, fontWeight: FontWeight.w900),
            ),
            Text(
              "${completedExercises.length} "
              "${FlutterI18n.plural(context, '$_i18nKey.exercises.count', 2)}",
              textAlign: TextAlign.left,
              style: CurrentTheme()
                  .themeData
                  .textTheme
                  .bodyText1
                  ?.copyWith(fontSize: 24),
            ),
          ],
        ),
      );
}
