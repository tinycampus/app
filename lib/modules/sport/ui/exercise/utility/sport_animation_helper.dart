/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

class SportAnimationHelper {
  Animation<double> gameLoop = AlwaysStoppedAnimation<double>(0);
  AnimationController ctrl;

  SportAnimationHelper(TickerProvider tickerProvider)
      : ctrl = AnimationController(
          duration: const Duration(seconds: 3),
          vsync: tickerProvider,
        );

  void replayWithDuration(Duration duration) {
    resetAnimationDuration(duration);
    ctrl.forward();
  }

  void resetAnimationDuration(Duration duration) => setTo(
        Tween<double>(begin: 0.0, end: duration.inMilliseconds.toDouble())
            //  .chain(CurveTween(curve: Cubic(.6, .03, .84, .33)
            .animate(ctrl),
        duration,
      );

  void resetWithValue(
    double value, {
    Duration customDuration = const Duration(milliseconds: 560),
  }) =>
      setTo(
        TweenSequence(
          <TweenSequenceItem<double>>[
            TweenSequenceItem<double>(
              tween: Tween<double>(begin: 0.0, end: -value)
                  .chain(CurveTween(curve: Cubic(.6, .03, .84, .33))),
              weight: 50.0,
            ),
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(value),
              weight: 50.0,
            ),
            TweenSequenceItem<double>(
              tween: Tween<double>(begin: value, end: 0.0)
                  .chain(CurveTween(curve: Cubic(0, .66, .01, 1))),
              // .chain(CurveTween(curve: Curves.easeOut)),
              weight: 50.0,
            ),
          ],
        ).animate(ctrl),
        customDuration,
      );

  void initWith(double begin, double end, {int millisecondsDuration = 1000}) =>
      setTo(
        Tween<double>(begin: begin, end: end).animate(
          CurvedAnimation(parent: ctrl, curve: Curves.easeInOut),
        ),
        Duration(milliseconds: millisecondsDuration),
      );

  void setTo(Animation<double> animation, Duration duration) {
    reset();
    gameLoop = animation;
    ctrl.duration = duration;
  }

  void stop() => ctrl.stop();

  void addListener(VoidCallback function) => ctrl.addListener(function);

  void addAfterEnd(VoidCallback callback) => ctrl.addStatusListener(
        (status) {
          switch (status) {
            case AnimationStatus.dismissed:
              break;
            case AnimationStatus.forward:
              break;
            case AnimationStatus.reverse:
              break;
            case AnimationStatus.completed:
              callback();
              break;
          }
        },
      );

  void forward() {
    if (ctrl.isCompleted) {
      ctrl.reset();
    }
    ctrl.forward();
  }

  void replay() => ctrl
    ..reset()
    ..forward();

  void reset() => ctrl
    ..stop()
    ..reset();

  void forwardWithDurationFactor(double factor) => ctrl
    ..stop()
    ..duration = ctrl.duration! * factor
    ..forward();

  void dispose() => ctrl
    ..stop()
    ..dispose();
}
