/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../common/tc_theme.dart';
import '../../../../../common/tc_utility_functions.dart';
import '../../../../../common/widgets/dialog/tc_dialog.dart';
import '../../../../../common/widgets/tc_cached_image.dart';
import '../../../model/external_content.dart';
import '../../common_ui.dart';

class ExternalEntry extends StatelessWidget {
  final ExternalContent externalContent;

  const ExternalEntry({Key? key, required this.externalContent})
      : super(key: key);

  //Debug only
  const ExternalEntry.placeholder({
    Key? key,
  })  : externalContent = const ExternalContent(
            imageUrl:
                'https://www.thm.de/sport/images/Homepage/Sportprogramm/Button-Sportprogramm.jpg',
            text: {
              'de': 'Platzhalter',
              'en': 'placeholder'
            },
            link: {
              'de': 'https://www.thm.de/sport/',
              'en': 'https://www.thm.de/sport/en/'
            }),
        super(key: key);

  // TODO: Add links, images
  @override
  Widget build(BuildContext context) => ListTile(
        contentPadding:
            const EdgeInsets.symmetric(vertical: 8.0, horizontal: 12),
        // contentPadding: EdgeInsets.zero,
        leading: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: 72, maxWidth: 72),
          child: TcCachedImage(
            url: externalContent.imageUrl,
            boxFit: BoxFit.cover,
            isRounded: true,
            width: 54,
            height: 54,
            borderRadius: 36,
          ),
        ),
        title: Text(
          read(externalContent.text, context),
          style: textStyle,
        ),
        // trailing: Icon(Icons.arrow_forward_ios_outlined),
        onTap: () {
          TCDialog.showCustomDialog(
            context: context,
            onConfirm: () => _launchUrl(read(externalContent.link, context)),
            functionActionText:
                FlutterI18n.translate(context, 'common.actions.continue'),
            headlineText: FlutterI18n.translate(
                context,
                'common.external_links.'
                'dialog_headline'),
            functionActionColor: CurrentTheme().tcBlue,
            bodyText: '${read(externalContent.link, context)}\n\n'
                '${FlutterI18n.translate(
              context,
              'common.external_links.dialog_body',
            )}',
          );
        },
      );
  Future<void> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
