/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/tc_button.dart';
import '../../bloc/settings/exercise_settings_bloc.dart';
import '../../common/settings_adapter.dart';
import '../../i18n_keys.dart';
import '../../model/exercise.dart';
import '../exercise/exercise_view.dart';
import 'card_styled_wrapper.dart';
import 'settings/program_selector.dart';
import 'settings/settings_widget.dart';
import 'settings/timing_widget.dart';

const int _totalDurationMinimumMinutes = 5;
const int _totalDurationMaximumMinutes = 30;
const int _pauseDurationMinimumSeconds = 5;
const int _pauseDurationMaximumSeconds = 60;

class ExercisesTab extends StatefulWidget {
  ExercisesTab(this.exerciseList, {Key? key}) : super(key: key);
  final List<Exercise> exerciseList;

  @override
  _ExercisesTabState createState() => _ExercisesTabState();
}

class _ExercisesTabState extends State<ExercisesTab>
    with AutomaticKeepAliveClientMixin<ExercisesTab> {
  late ExerciseSettingsEvent event;

  bool showMoreOptions = false;
  ValueNotifier<bool> exercisesLoaded = ValueNotifier<bool>(true);

  @override
  void initState() {
    super.initState();
    event = ExerciseSettingsEvent.fromState(
      BlocProvider.of<ExerciseSettingsBloc>(context).state,
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Stack(
      children: [
        buildOptionList(context),
        buildButtonBackground(),
        Positioned(bottom: 24, left: 16, right: 16, child: buildStartButton()),
      ],
    );
  }

  Positioned buildButtonBackground() => Positioned(
        bottom: 0,
        left: 0,
        right: 6,
        height: 128,
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                end: Alignment.bottomCenter,
                begin: Alignment.topCenter,
                stops: [
                  0.0,
                  0.2
                ],
                colors: [
                  CurrentTheme().themeData.backgroundColor.withOpacity(0.0),
                  CurrentTheme().themeData.backgroundColor,
                ]),
          ),
        ),
      );

  Widget buildOptionList(BuildContext context) => Scrollbar(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 32.0, horizontal: 8.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16.0,
                    bottom: 16.0,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      I18nText(
                        '$sportSport.program_selection',
                        child: Text(
                          '',
                          style: TextStyle(
                            fontSize: 24,
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ProgramSelector(
                  onPressed: (cat) {
                    setState(() {
                      event = event.copyWith(selectedExerciseCategory: cat);
                    });
                  },
                ),
                // total duration of training
                CardStyledWrapper(
                  children: [
                    TimingWidget.minutes(
                      divisions: 5,
                      initialValue: event.totalDuration?.inMinutes ?? 0,
                      min: _totalDurationMinimumMinutes,
                      max: _totalDurationMaximumMinutes,
                      durationCallback: (duration) {
                        debugPrint(duration.toString());
                        setState(() {
                          event = event.copyWith(totalDuration: duration);
                        });
                      },
                    ),
                  ],
                ),
                buildFurtherOptions(),
                Container(height: 120),
              ],
            ),
          ),
        ),
      );

  AnimatedCrossFade buildFurtherOptions() => AnimatedCrossFade(
        alignment: Alignment.topCenter,
        duration: Duration(milliseconds: 240),
        sizeCurve: Curves.easeOut,
        crossFadeState: showMoreOptions
            ? CrossFadeState.showSecond
            : CrossFadeState.showFirst,
        firstChild: InkWell(
          onTap: () => setState(() => showMoreOptions = true),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 24.0),
            child: Row(
              children: [
                Expanded(
                  child: I18nText(
                    '$sportSport.show_more_options',
                    child: Text("",
                        textAlign: TextAlign.center,
                        style: CurrentTheme()
                            .sportTextLabel
                            .copyWith(color: CurrentTheme().tcBlue)),
                  ),
                ),
              ],
            ),
          ),
        ),
        secondChild: Column(
          children: [
            CheckboxListSettings(
              i18nKey: '$sportSport.instructions',
              onToggle: (value) => setState(
                  () => event = event.copyWith(enableInstruction: value)),
              onSwitch: (value) => setState(
                  () => event = event.copyWith(automaticInstruction: value)),
              automatic: event.automaticInstruction ?? true,
              enabled: event.enableInstruction ?? true,
            ),
            // TODO: cut for initial release
            // CheckboxListSettings(
            //   i18nKey: '$sportSport.exercise',
            //   onToggle: (value) => setState(
            //       () => event = event.copyWith(enableExercises: value)),
            //   onSwitch: (value) => setState(
            //       () => event = event.copyWith(automaticExercises: value)),
            //   automatic: event.automaticExercises,
            //   enabled: event.enableExercises,
            // ),
            CheckboxListSettings(
              i18nKey: '$sportSport.pauses',
              onToggle: (value) =>
                  setState(() => event = event.copyWith(enablePauses: value)),
              onSwitch: (value) => setState(
                  () => event = event.copyWith(automaticPauses: value)),
              automatic: event.automaticPauses ?? true,
              enabled: event.enablePauses ?? true,
              // pause duration between exercises
              child: Container(),
              disappearableChild: TimingWidget.seconds(
                divisions: 11,
                initialValue: event.pauseDuration?.inSeconds ?? 0,
                min: _pauseDurationMinimumSeconds,
                max: _pauseDurationMaximumSeconds,
                durationCallback: (duration) {
                  debugPrint(duration.toString());
                  setState(() {
                    event = event.copyWith(pauseDuration: duration);
                  });
                },
              ),
            ),
            CardStyledWrapper(children: [
              buildTtsOption(),
            ]),
            CardStyledWrapper(children: [
              buildMusic(),
            ]),
            CardStyledWrapper(
              children: buildInstructionOptions,
            ),
          ],
        ),
      );

  List<Widget> get buildInstructionOptions => [
        SwitchListTile(
          title: I18nText(
            '$sportSport.show_effect',
            child: Text("", style: CurrentTheme().sportTextLabel),
          ),
          subtitle: I18nText(
            '$sportSport.show_effect_desc',
            child: Text("", style: CurrentTheme().sportTextLabelSubtitle),
          ),
          value: event.showEffectDesc ?? true,
          onChanged: (value) {
            setState(
              () {
                event = event.copyWith(showEffectDesc: value);
              },
            );
          },
        ),
        SwitchListTile(
          title: I18nText(
            '$sportSport.show_start_pos',
            child: Text("", style: CurrentTheme().sportTextLabel),
          ),
          subtitle: I18nText(
            '$sportSport.show_start_pos_desc',
            child: Text("", style: CurrentTheme().sportTextLabelSubtitle),
          ),
          value: event.showStartingPositionDesc ?? true,
          onChanged: (value) {
            setState(
              () {
                event = event.copyWith(showStartingPositionDesc: value);
              },
            );
          },
        ),
        SwitchListTile(
          title: I18nText(
            '$sportSport.show_motion',
            child: Text("", style: CurrentTheme().sportTextLabel),
          ),
          subtitle: I18nText(
            '$sportSport.show_motion_desc',
            child: Text("", style: CurrentTheme().sportTextLabelSubtitle),
          ),
          value: event.showMotionSequenceDesc ?? true,
          onChanged: (value) {
            setState(
              () {
                event = event.copyWith(showMotionSequenceDesc: value);
              },
            );
          },
        ),
        SwitchListTile(
          title: I18nText(
            '$sportSport.show_repetition',
            child: Text("", style: CurrentTheme().sportTextLabel),
          ),
          subtitle: I18nText(
            '$sportSport.show_repetition_desc',
            child: Text("", style: CurrentTheme().sportTextLabelSubtitle),
          ),
          value: event.showRepetitionDesc ?? true,
          onChanged: (value) {
            setState(
              () {
                event = event.copyWith(showRepetitionDesc: value);
              },
            );
          },
        ),
        SwitchListTile(
          title: I18nText(
            '$sportSport.show_variant',
            child: Text("", style: CurrentTheme().sportTextLabel),
          ),
          subtitle: I18nText(
            '$sportSport.show_variant_desc',
            child: Text("", style: CurrentTheme().sportTextLabelSubtitle),
          ),
          value: event.showVariantDesc ?? true,
          onChanged: (value) {
            setState(
              () {
                event = event.copyWith(showVariantDesc: value);
              },
            );
          },
        ),
      ];

  SwitchListTile buildTtsOption() => SwitchListTile(
        title: I18nText(
          '$sportSport.tts',
          child: Text("", style: CurrentTheme().sportTextLabel),
        ),
        subtitle: I18nText(
          '$sportSport.tts_desc',
          child: Text("", style: CurrentTheme().sportTextLabelSubtitle),
        ),
        value: event.enableTextToSpeech ?? true,
        onChanged: (value) {
          setState(
            () {
              event = event.copyWith(enableTextToSpeech: value);
            },
          );
        },
      );

  SwitchListTile buildMusic() => SwitchListTile(
        title: I18nText(
          '$sportSport.music',
          child: Text("", style: CurrentTheme().sportTextLabel),
        ),
        subtitle: I18nText(
          '$sportSport.music_desc',
          child: Text("", style: CurrentTheme().sportTextLabelSubtitle),
        ),
        value: event.showMusic ?? false,
        onChanged: (value) {
          setState(
            () {
              event = event.copyWith(showMusic: value);
            },
          );
        },
      );

  Widget buildStartButton() => SafeArea(
        bottom: true,
        child: Padding(
          padding: const EdgeInsets.only(top: 24.0),
          child: ValueListenableBuilder(
            valueListenable: exercisesLoaded,
            builder: (context, value, child) => TCButton(
              customHeight: 72,
              onPressedCallback: !exercisesLoaded.value
                  ? null
                  : () {
                      final s1 = event.showEffectDesc ?? false;
                      final s2 = event.showStartingPositionDesc ?? false;
                      final s3 = event.showMotionSequenceDesc ?? false;
                      final s4 = event.showRepetitionDesc ?? false;
                      final s5 = event.showVariantDesc ?? false;
                      final doesHaveSomeInstruction =
                          s1 || s2 || s3 || s4 || s5;
                      event = event.copyWith(
                          enableInstruction: doesHaveSomeInstruction &&
                              (event.enableInstruction ?? false));
                      BlocProvider.of<ExerciseSettingsBloc>(context).add(event);
                      Future.delayed(Duration(milliseconds: 12), () {
                        final computedList = SettingsAdapter(
                          state: BlocProvider.of<ExerciseSettingsBloc>(context)
                              .state,
                          // context.bloc<ExerciseSettingsBloc>().state,
                          exerciseList: <Exercise>[...widget.exerciseList],
                          locale: FlutterI18n.currentLocale(context)
                                  ?.languageCode ??
                              'de',
                        ).computeExerciseProgram();

                        /// TODO: Constant route
                        /// TODO: Route Parameter
                        // .then(
                        //   (program) =>
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ExerciseView(
                              exercises: computedList,
                            ),
                          ),
                        );
                      });
                    },
              buttonLabel: FlutterI18n.translate(context, '$sportSport.start'),
            ),
          ),
        ),
      );

  @override
  bool get wantKeepAlive => true;
}

// if (!kReleaseMode)
//   Row(
//     children: [
//       IconButton(
//         icon: Icon(Icons.play_circle_fill),
//         onPressed: () => AudioPlayer().play(),
//       ),
//       IconButton(
//         icon: Icon(Icons.pause),
//         onPressed: () => AudioPlayer().pauseOrResume(),
//       ),
//     ],
//   ),
// !kReleaseMode
//     ? TCButton(
//         buttonLabel: 'TTS Test',
//         onPressedCallback: () => Navigator.of(context).push(
//             MaterialPageRoute(builder: (context) => TTSTest())),
//       )
//     : SizedBox.shrink(),
// !kReleaseMode
//     ? Padding(
//         padding: const EdgeInsets.only(top: 24.0),
//         child: SizedBox(
//           width: double.infinity,
//           height: 70,
//           child: ValueListenableBuilder(
//             valueListenable: exercisesLoaded,
//             builder: (context, value, child) => TCButton(
//               onPressedCallback: !exercisesLoaded.value
//                   ? null
//                   : () {
//                       context
//                           .bloc<ExerciseSettingsBloc>()
//                           .add(event);
//                       Future.delayed(Duration(milliseconds: 12),
//                           () {
//                         sprint(exerciseList);
//                         final computedList = SettingsAdapter(
//                           state:
//                               BlocProvider.of<ExerciseSettingsBloc>(
//                                       context)
//                                   .state,
//                           exerciseList: <Exercise>[...exerciseList],
//                           locale: FlutterI18n.currentLocale(context)
//                               .languageCode,
//                         ).computeExerciseProgram();
//                         Navigator.of(context).push(
//                           MaterialPageRoute(
//                             builder: (context) => ExerciseEndCard(
//                               completedExercises: computedList,
//                             ),
//                           ),
//                         );
//                       });
//                     },
//               buttonLabel: 'Endcard test',
//             ),
//           ),
//         ),
//       )
//     : SizedBox.shrink(),
