/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/cupertino.dart' show CupertinoSegmentedControl;
import 'package:flutter/material.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';

import '../../../../../common/tc_theme.dart';
import '../../../i18n_keys.dart';
import '../card_styled_wrapper.dart';

typedef ToggleCallback = void Function(bool enabled);
typedef SwitchCallback = void Function(bool value);

class CheckboxListSettings extends StatefulWidget {
  final ToggleCallback onToggle;
  final SwitchCallback onSwitch;

  final bool enabled;
  final bool automatic;
  final String i18nKey;
  final Widget? child;
  final Widget? disappearableChild;

  const CheckboxListSettings({
    Key? key,
    required this.i18nKey,
    required this.onToggle,
    required this.onSwitch,
    required this.enabled,
    required this.automatic,
    this.disappearableChild,
    this.child,
  }) : super(key: key);

  @override
  _CheckboxListSettingsState createState() => _CheckboxListSettingsState();
}

class _CheckboxListSettingsState extends State<CheckboxListSettings> {
  late bool enabled;
  late bool automatic;

  @override
  void initState() {
    super.initState();
    enabled = widget.enabled;
    automatic = widget.automatic;
  }

  @override
  Widget build(BuildContext context) => CardStyledWrapper(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 8.0, left: 16.0, right: 8),
                child: Row(
                  children: [
                    Expanded(
                      child: I18nText(
                        widget.i18nKey,
                        child: Text("", style: CurrentTheme().sportTextLabel),
                      ),
                    ),
                    Checkbox(
                      value: enabled,
                      onChanged: (value) {
                        // kann nur nullsafe sein,
                        // da tristate nicht aktiviert ist
                        setState(() => enabled = value!);
                        widget.onToggle(value!);
                      },
                    ),
                  ],
                ),
              ),
              AnimatedCrossFade(
                firstChild: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(
                          top: 8.0, bottom: 8.0, left: 16.0, right: 8),
                      child: Row(
                        children: [
                          Expanded(
                              child: I18nText(
                            '$sportSport.procedure',
                            child:
                                Text("", style: CurrentTheme().sportTextLabel),
                          )),
                          CupertinoSegmentedControl<bool>(
                            groupValue: automatic,
                            onValueChanged: (value) {
                              widget.onSwitch(value);
                              setState(() {
                                automatic = value;
                              });
                            },
                            borderColor: CurrentTheme().textPassive,
                            selectedColor:
                                CurrentTheme().themeData.colorScheme.secondary,
                            unselectedColor:
                                CurrentTheme().themeData.backgroundColor,
                            children: {
                              false: I18nText('$sportSport.manual'),
                              true: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8),
                                child: I18nText('$sportSport.automatic'),
                              ),
                            },
                          ),
                        ],
                      ),
                    ),
                    if (widget.disappearableChild != null)
                      widget.disappearableChild!,
                  ],
                ),
                secondChild: Container(),
                crossFadeState: enabled
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: const Duration(milliseconds: 200),
              ),
              if (widget.child != null) widget.child!,
            ],
          )
        ],
      );
}
