/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../../common/constants/routing_constants.dart';
import '../../../common/tinycampus_icons.dart';
import '../contacts/vcard/vcard.dart';
import '../qr_code_screen.dart';

/// The upper half of the [BusinessCardStartScreen]
///
/// When "formattedName" is empty it only shows a hint to fill it,
/// when it's not empty then it shows information about the [VCard]
class VCardPreview extends StatelessWidget {
  /// The personal [VCard] of an user
  final VCard personal;

  /// The maximal character length to be shown from "formattedName"
  final int maxStringLengthOfPersonalPreviewName = 14;

  /// The slidableController of all [VCardContactPreview]s
  final SlidableController slidableController;

  /// Constructor of [VCardPreview]
  const VCardPreview({
    Key? key,
    required this.personal,
    required this.slidableController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0))),
        margin: EdgeInsets.all(8.0),
        elevation: 2.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
          child: personal.formattedName == "" // check if a name is put in.
              // if not, display incentive to put in data.
              ? InkWell(
                  onTap: () {
                    _closeSlidables();
                    Navigator.pushNamed(context, vCardEditRoute);
                  },
                  child: Container(
                    padding: EdgeInsets.all(20),
                    child: Center(
                      child: I18nText(
                        'modules.business_cards.vcard.preview.create_vcard',
                        child: Text(
                          '',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20.0),
                        ),
                      ),
                    ),
                    height: 150.0,
                  ),
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(
                          vertical: 16.0, horizontal: 16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        textBaseline: TextBaseline.alphabetic,
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        children: <Widget>[
                          Text(
                            personal.formattedName,
                            style: TextStyle(
                              fontSize: 26,
                              letterSpacing: -2,
                            ),
                          ),
                          Text(
                            _formatString(personal.title),
                            style: TextStyle(
                              fontSize: 15,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 8.0),
                          ),
                          I18nText(
                            'modules.business_cards.vcard.preview.interests',
                            child: Text(
                              '',
                              style: TextStyle(
                                fontSize: 14,
                                color: Color.fromARGB(255, 1, 68, 87),
                              ),
                            ),
                          ),
                          Text(
                            personal.categories,
                            style: TextStyle(
                              fontSize: 20,
                              color: Color.fromARGB(255, 1, 68, 87),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(16.0),
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 1, 68, 87),
                          ),
                          child: GestureDetector(
                            onTap: () {
                              _closeSlidables();
                              Navigator.pushNamed(context, vCardEditRoute);
                            },
                            child: Icon(
                              TinyCampusIcons.edit,
                              color: Colors.white,
                              size: 42,
                            ),
                          ),
                        ),
                        Divider(
                          color: Colors.white,
                          thickness: 1,
                          height: 1,
                        ),
                        Container(
                          padding: EdgeInsets.all(16.0),
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 1, 68, 87),
                          ),
                          child: GestureDetector(
                            onTap: () {
                              _closeSlidables();
                              Navigator.pushNamed(
                                context,
                                vCardQRCodeRoute,
                                arguments: QRScreenArguments(
                                    isQrGenerationScreen: true),
                              );
                            },
                            child: Icon(
                              TinyCampusIcons.qr,
                              color: Colors.white,
                              size: 42,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
        ),
      );

  String _formatString(String value) {
    if (value.isEmpty) {
      return "...";
    } else {
      return value.length > maxStringLengthOfPersonalPreviewName
          ? ("${value.substring(0, maxStringLengthOfPersonalPreviewName)}...")
          : value;
    }
  }

  void _closeSlidables() {
    if (slidableController.activeState != null) {
      slidableController.activeState!.close();
    }
  }
}
