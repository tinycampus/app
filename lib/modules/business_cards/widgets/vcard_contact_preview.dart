/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../../../common/tinycampus_icons.dart';
import '../contacts/vcard/vcard.dart';
import '../contacts/vcard_contact_screen.dart';
import '../contacts/vcard_sharing.dart';
import '../v_card/v_card_bloc.dart';

/// This class is the lower half of [BusinessCardStartScreen]
/// It is the design of one list element in the contact list
class VCardContactPreview extends StatelessWidget {
  /// The contact which data is needed
  final VCard contact;

  /// The position of this contact in the list
  final int pos;

  /// The slidableController of the ListTile
  final SlidableController slidableController;

  /// Constructor of [VCardContactPreview]
  const VCardContactPreview({
    Key? key,
    required this.contact,
    required this.pos,
    required this.slidableController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Slidable(
        controller: slidableController,
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.25,
        child: ListTile(
            contentPadding:
                EdgeInsets.symmetric(vertical: 6.0, horizontal: 10.0),
            onTap: () {
              if (slidableController.activeState != null) {
                slidableController.activeState!.close();
              }
              Navigator.pushNamed(context, vCardContactScreenRoute,
                  arguments: VCardContactScreenArguments(pos));
            },
            leading: CircleAvatar(
              // Ein bisschen Farbe ins Spiel bringen ;)
              backgroundColor: shadeColor(Color(contact.hashCode)),
              radius: 30.0,
              child: Text(
                contact.formattedName.isEmpty
                    ? "?"
                    : contact.formattedName
                        .substring(0, _getMaxLength(contact.formattedName))
                        .toUpperCase(),
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
            ),
            title: Text(
              contact.formattedName.isEmpty ? "..." : contact.formattedName,
              style: TextStyle(fontSize: 20.0),
            ),
            subtitle: Container(
              padding: EdgeInsets.all(0.0),
              margin: EdgeInsets.all(0.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    contact.title.isEmpty
                        ? FlutterI18n.translate(
                            context,
                            'modules.business_cards.'
                            'vcard.contacts.no_information')
                        : contact.title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.grey),
                  ),
                  Text(
                    contact.note.isEmpty
                        ? FlutterI18n.translate(
                            context,
                            'modules.business_cards.'
                            'vcard.contacts.no_information')
                        : contact.note,
                    style: TextStyle(
                      color: Color.fromARGB(255, 1, 68, 87),
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            )),
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: FlutterI18n.translate(
                context,
                'modules.business_cards.'
                'vcard.contacts.share'),
            color: Color.fromARGB(255, 1, 68, 87),
            iconWidget: Container(
              child: Icon(
                Icons.share,
                color: Colors.white,
                size: 30.0,
              ),
              margin: EdgeInsets.only(bottom: 12.0),
            ),
            onTap: () => VCardSharing().share(contact),
          ),
          IconSlideAction(
            caption: FlutterI18n.translate(
                context,
                'modules.business_cards.'
                'vcard.contacts.delete'),
            color: Colors.red[500],
            iconWidget: Container(
              child: Icon(
                TinyCampusIcons.trash,
                color: Colors.white,
                size: 30.0,
              ),
              margin: EdgeInsets.only(bottom: 12.0),
            ),
            onTap: () {
              BlocProvider.of<VCardBloc>(context)
                  .add(ContactListDeleteEvent(contact: contact));
            },
          ),
        ],
      );

  int _getMaxLength(String value) => value.length == 1 ? 1 : 2;
}
