/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';

import '../../common/tc_theme.dart';
import 'contacts/vcard_sharing.dart';
import 'v_card/v_card_bloc.dart';
import 'widgets/v_card_form_field.dart';

/// The screen is used when the user wants to edit his personal [VCard]
class BusinessCardEdit extends StatelessWidget {
  final GlobalKey _scaffold = GlobalKey();

  BusinessCardEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocBuilder<VCardBloc, VCardState>(
        builder: (context, state) => Scaffold(
          key: _scaffold,
          appBar: AppBar(
            title: I18nText('modules.business_cards.edit_screen.title'),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.share,
                  color: CorporateColors.tinyCampusDarkBlue,
                ),
                onPressed: () => VCardSharing().share(state.personal),
              ),
            ],
          ),
          body: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: ListView(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10, bottom: 5, left: 10),
                          alignment: Alignment.centerLeft,
                          child: I18nText(
                            'modules.business_cards.edit_screen.hint',
                            child: Text(
                              '',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(
                            bottom: 10,
                            left: 10,
                          ),
                          child: I18nText(
                            'modules.business_cards.edit_screen.hint_text',
                            child: Text(
                              '',
                              style: TextStyle(
                                fontStyle: FontStyle.italic,
                                fontSize: 16.0,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                        VCardFormField.getName(state, context),
                        VCardFormField.getCategories(state, context),
                        VCardFormField.getTitle(state, context),
                        VCardFormField.getRole(state, context),
                        VCardFormField.getOrganisation(state, context),
                        VCardFormField.getEmail(state, context),
                        VCardFormField.getUrl(state, context),
                        VCardFormField.getPhone(state, context),
                        Container(
                          height: 40,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
