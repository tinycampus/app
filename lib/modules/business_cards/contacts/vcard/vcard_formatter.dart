/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:intl/intl.dart';

import 'vcard.dart';

/// forked from https://github.com/valerycolong/vcard

class VCardFormatter {
  /// Encode string
  /// @param  {String}     value to encode
  /// @return {String}     encoded string
  String e(String value) {
    if (value.isEmpty) return '';

    return value
        .replaceAll(RegExp(r'/\n/g'), '\\n')
        .replaceAll(RegExp(r'/,/g'), '\\,')
        .replaceAll(RegExp(r'/;/g'), '\\;');
  }

  /// Return new line characters
  /// @return {String} new line characters
  String nl() => '\r\n';

  /// Get formatted address
  /// @param  {Map<String, String>}         address
  /// @param  {String}         type address type
  /// @param {String}         Encoding prefix encodingPrefix
  /// @return {String}         Formatted address

  /// Convert date to YYYYMMDD format
  /// @param  {Date}       date to encode
  /// @return {String}     encoded date
  String formatVCardDate(DateTime date) => DateFormat("yyyyMMdd").format(date);

  String getFormattedString(VCard vCard) {
    var formattedVCardString = '';
    formattedVCardString += 'BEGIN:VCARD${nl()}';
    formattedVCardString += 'VERSION:2.1${nl()}';

    var formattedName = vCard.formattedName;

    formattedVCardString += 'N:${e(formattedName)};;;;${nl()}';
    formattedVCardString += 'FN:${e(formattedName)}${nl()}';

    formattedVCardString +=
        'UID:${e("31f566259c1a3f810256e3679e10faa457bb4a0b")}${nl()}';

    if (vCard.organization.isNotEmpty) {
      formattedVCardString += 'ORG:${e(vCard.organization)}${nl()}';
    }

    if (vCard.role.isNotEmpty) {
      formattedVCardString += 'ROLE:${e(vCard.role)}${nl()}';
    }

    if (vCard.title.isNotEmpty) {
      formattedVCardString += 'TITLE:${e(vCard.title)}${nl()}';
    }

    if (vCard.otherPhone.isNotEmpty) {
      formattedVCardString += 'TEL;OTHER;VOICE:${e(vCard.otherPhone)}${nl()}';
    }

    if (vCard.email.isNotEmpty) {
      formattedVCardString += 'EMAIL;HOME;INTERNET:${e(vCard.email)}${nl()}';
    }

    if (vCard.categories.isNotEmpty) {
      formattedVCardString += 'CATEGORIES:${e(vCard.categories)}${nl()}';
    }

    if (vCard.url.isNotEmpty) {
      formattedVCardString += 'URL:${e(vCard.url)}${nl()}';
    }

    if (vCard.note.isNotEmpty) {
      formattedVCardString += 'NOTE:${e(vCard.note)}${nl()}';
    }

    formattedVCardString += 'REV:${DateTime.now().toIso8601String()}${nl()}';

    formattedVCardString += 'END:VCARD${nl()}';
    return formattedVCardString;
  }
}
