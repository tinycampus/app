// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vcard.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VCard _$VCardFromJson(Map json) => VCard(
      email: json['email'] as String? ?? '',
      formattedName: json['formattedName'] as String? ?? '',
      categories: json['categories'] as String? ?? '',
      note: json['note'] as String? ?? '',
      organization: json['organization'] as String? ?? '',
      role: json['role'] as String? ?? '',
      title: json['title'] as String? ?? '',
      url: json['url'] as String? ?? '',
      otherPhone: json['otherPhone'] as String? ?? '',
      version: json['version'] as String? ?? '2.1',
      date: json['date'] as String? ?? '',
    );

Map<String, dynamic> _$VCardToJson(VCard instance) => <String, dynamic>{
      'email': instance.email,
      'formattedName': instance.formattedName,
      'categories': instance.categories,
      'note': instance.note,
      'organization': instance.organization,
      'role': instance.role,
      'title': instance.title,
      'url': instance.url,
      'otherPhone': instance.otherPhone,
      'version': instance.version,
      'date': instance.date,
    };
