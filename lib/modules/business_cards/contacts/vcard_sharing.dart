/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_extend/share_extend.dart';

import 'vcard/vcard.dart';

/// This class is used to share [VCard] via "WhatsApp", "Telegram" etc
class VCardSharing {
  /// This Method is called to share
  /// @param the [VCard] the user wants to share
  Future<void> share(VCard vCard) async {
    try {
      var _vcf = await _createFile(vCard.getFormattedString());
      await _readFile();
      _vcf = await _changeExtension(".vcf");
      ShareExtend.share(_vcf.path, "file");
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/contact.txt');
  }

  Future<String> get _localPath async =>
      (await getApplicationDocumentsDirectory()).path;

  Future<String> _readFile() async {
    try {
      final file = await _localFile;
      var contents = await file.readAsString();
      return contents;
    } on Exception catch (e) {
      debugPrint(e.toString());
      return "";
    }
  }

  Future<File> _createFile(String data) async {
    final file = await _localFile;
    return file.writeAsString(data);
  }

  Future<File> _changeExtension(String ext) async {
    final file = await _localFile;
    var _newFile = file.renameSync(file.path.replaceAll(".txt", ext));
    return _newFile;
  }
}
