/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'contacts/vcard/vcard.dart';
import 'v_card/v_card_bloc.dart';

/// The screen to scan QrCode from another device
class QrReadScreen extends StatefulWidget {
  const QrReadScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => QrReadScreenState();
}

/// The state of [QrReadScreen]
class QrReadScreenState extends State<QrReadScreen> {
  /// Needed, because the Scanner would read the same QrCode multiple times
  bool _check = true;
  List<VCard> _contacts = [];
  GlobalKey qrViewKey = GlobalKey(debugLabel: 'QR');
  late QRViewController controller;

  // In order to get hot reload to work we need to pause the camera if the
  // platform is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var bloc = BlocProvider.of<VCardBloc>(context);
    _contacts = bloc.state.contacts;
    return BlocBuilder<VCardBloc, VCardState>(
      builder: (context, state) => SizedBox(
        width: 300,
        height: 300,
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 5,
              left: 5,
              child: Container(
                color: Colors.black,
                height: 292,
                width: 292,
                child: Center(
                  child: QRView(
                    key: qrViewKey,
                    formatsAllowed: [BarcodeFormat.qrcode],
                    onQRViewCreated: (controller) {
                      setState(() => this.controller = controller);
                      controller.scannedDataStream.listen(
                        (code) => _handleQrCode(code.code),
                      );
                    },
                  ),
                ),
              ),
            ),
            Positioned(
              child: Container(
                height: 300,
                width: 300,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  border: Border.all(
                    width: 5,
                    color: Color.fromARGB(255, 1, 68, 78),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  VCard _codeToCard(String code) {
    final cleaned = code
      ..replaceAll("BEGIN:VCARD", "")
      ..replaceAll("END:VCARD", "")
      ..replaceAll(";CHARSET=UTF-8", "")
      ..replaceAll("VERSION:2.1", "")
      // Magic number to tell tinyCampus vCards apart from regular ones
      ..replaceAll("UID:31f566259c1a3f810256e3679e10faa457bb4a0b", "")
      ..replaceAll(";HOME;INTERNET", "")
      ..replaceAll(";VALUE=uri;TYPE=\"voice,other\":tel", "");

    final entries = cleaned
        .split("\r")
        .map((e) => e.trim().split(":"))
        .map((e) => MapEntry(e.first, e.last));

    final paramMap = Map.fromEntries(entries);

    return VCard(
      formattedName: paramMap["FN"] ?? "",
      email: paramMap["EMAIL"] ?? "",
      categories: paramMap["CATEGORIES"] ?? "",
      otherPhone: paramMap["TEL;OTHER;VOICE"] ?? "",
      title: paramMap["TITLE"] ?? "",
      role: paramMap["ROLE"] ?? "",
      organization: paramMap["ORG"] ?? "",
      url: paramMap["URL"] ?? "",
      date: DateTime.now().toString(),
    );
  }

  /// Handles the information read by the scanner
  void _handleQrCode(String? code) {
    if (code != null &&
        code.contains('BEGIN:VCARD') &&
        code.contains('END:VCARD') &&
        // Magic number to tell tinyCampus vCards apart from regular ones
        code.contains("31f566259c1a3f810256e3679e10faa457bb4a0b")) {
      if (_check) {
        setState(() => _check = false);
        BlocProvider.of<VCardBloc>(context).add(
          ContactListSortEvent(
            contacts: _sortDate(_migrateOldDate(_contacts)),
            sort: "newest",
          ),
        );
        BlocProvider.of<VCardBloc>(context).add(
          ContactListAddEvent(contact: _codeToCard(code)),
        );
        Navigator.of(context).pop();
      }
    }
  }

  /// This function is needed so the old DateFormat could be
  /// transformed into ISO8601 format
  List<VCard> _migrateOldDate(List<VCard> contacts) => contacts
      .where((element) => element.date.contains("Uhr"))
      .map(
        (element) => VCard(
          formattedName: element.formattedName,
          categories: element.categories,
          role: element.role,
          title: element.title,
          organization: element.organization,
          email: element.email,
          url: element.url,
          otherPhone: element.otherPhone,
          date: DateFormat(
            "dd.MM.yyyy, hh:mm",
          ).parse(element.date).toString(),
        ),
      )
      .toList();

  /// When a new user is added the list sorts itself back to newest
  List<VCard> _sortDate(List<VCard> contacts) => contacts
    ..sort((a, b) => DateTime.parse(b.date).compareTo(DateTime.parse(a.date)));
}
