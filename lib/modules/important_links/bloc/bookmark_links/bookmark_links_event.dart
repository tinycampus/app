/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../../model/item.dart';
import 'bookmark_links_state.dart';

@immutable
abstract class BookmarkLinksEvent {
  const BookmarkLinksEvent();

  BookmarkLinksState performAction(BookmarkLinksState state);
}

class AddBookmarkLinksEvent extends BookmarkLinksEvent {
  final Item item;
  AddBookmarkLinksEvent({required this.item});

  @override
  BookmarkLinksState performAction(BookmarkLinksState state) =>
      AddBookmarkLinksState(state.bookmarkedItems..add(item), state.startIndex);
}

class RemoveBookmarkLinksEvent extends BookmarkLinksEvent {
  final Item item;
  RemoveBookmarkLinksEvent({required this.item});

  @override
  BookmarkLinksState performAction(BookmarkLinksState state) =>
      RemoveBookmarkLinksState(
          (state.bookmarkedItems
            ..removeWhere((element) => element.url == item.url)),
          state.startIndex);
}

class SetStartIndexEvent extends BookmarkLinksEvent {
  final int startIndex;
  SetStartIndexEvent({required this.startIndex});

  @override
  BookmarkLinksState performAction(BookmarkLinksState state) =>
      SwapStartIndexState(state.bookmarkedItems, startIndex);
}

class BookmarkLinksResetEvent extends BookmarkLinksEvent {
  const BookmarkLinksResetEvent();

  @override
  BookmarkLinksState performAction(BookmarkLinksState state) =>
      InitialBookmarkLinksState();
}
