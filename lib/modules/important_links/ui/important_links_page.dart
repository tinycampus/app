/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../common/constants/api_constants.dart';
import '../../../common/tc_theme.dart';
import '../../../common/widgets/dialog/tc_dialog.dart';
import '../../../repositories/http/http_repository.dart';
import '../model/category.dart';
import '../widget/list_item_widget.dart';

class ImportantLinksPage extends StatefulWidget {
  const ImportantLinksPage({Key? key}) : super(key: key);

  @override
  _ImportantLinksPageState createState() => _ImportantLinksPageState();
}

class _ImportantLinksPageState extends State<ImportantLinksPage>
    with AutomaticKeepAliveClientMixin {
  List<ILCategory> _categories = <ILCategory>[];
  late SharedPreferences sp;
  bool errorLoadingModel = false;
  ILLoadingState loadingState = ILLoadingState.none;
  final ValueNotifier<String> errorMessage = ValueNotifier<String>("none");
  final ValueNotifier<String> updated = ValueNotifier<String>("");

  @override
  void initState() {
    super.initState();
    initialize(context);
  }

  @override
  void dispose() {
    errorMessage.dispose();
    updated.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return SingleChildScrollView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        child: _categories.isNotEmpty
            ? Column(
                children: [
                  if (!kReleaseMode) buildDebugOptions(context),
                  SizedBox(
                    height: 220,
                    child: LoadingProgressWidget(
                      errorMessage: errorMessage,
                    ),
                  ),
                  for (var category in _categories) _buildCategory(category),
                  Container(height: 140)
                ],
              )
            : Container());
  }

  Row buildDebugOptions(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FloatingActionButton(
            heroTag: "2",
            backgroundColor: Colors.red,
            child: Icon(Icons.delete_outline),
            onPressed: () async {
              await sp.remove('de.links.json');
              await sp.remove('en.links.json');
              await sp.remove('de.links.json.lastUpdated');
              await sp.remove('en.links.json.lastUpdated');
              errorMessage.value = "none";
              _categories = [];
              setState(() {});
            },
          ),
          FloatingActionButton(
            heroTag: "1",
            child: Icon(Icons.refresh),
            onPressed: () async {
              await initialize(context);
              setState(() {});
            },
          ),
        ],
      );

  Widget _buildCategory(ILCategory category) => Column(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 6),
            child: ClipRRect(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              child: Container(
                color: Theme.of(context).primaryColor,
                padding: const EdgeInsets.only(top: 6, bottom: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12.0),
                            child: Text(
                              category.title,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline2
                                  ?.copyWith(
                                    fontWeight: FontWeight.w700,
                                  ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                        ],
                      ),
                    ),
                    ListItemWidget(
                      listItems: category.items,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );

  Future<void> initialize(BuildContext context) async {
    // GET all possible locales (["de", "en"])
    // TODO: find dynamic way,
    // [WidgetsBinding.instance.window.locales] does not work;
    // var languages = ["de", "en"];
    // for (var language in languages) {}
    var language = FlutterI18n.currentLocale(context).toString();

    sp = await SharedPreferences.getInstance();

    var key = '$language.links.json';
    var remoteLoaded = true;
    String jsonString;
    errorMessage.value = "loading";
    switch (await checkState(key)) {
      case ILState.blankState:
        jsonString = await getServerString(key).then((value) async => value,
            onError: (e) async {
          {
            debugPrint(e);
            jsonString = await getLocalString(language);
            remoteLoaded = false;
            return jsonString;
          }
        });
        var parsedModel = await stringToCategories(jsonString);
        if (parsedModel.isNotEmpty) {
          await sp.setString(key, jsonString);
          if (remoteLoaded) await setLastUpdated(key);
        } else {
          errorMessage.value = "no_valid_model";
        }
        _categories = parsedModel;
        errorMessage.value = "initialized";
        break;
      case ILState.noUpdateRequired:
        jsonString = sp.getString(key) ?? '';
        var parsedModel = await stringToCategories(jsonString);
        _categories = parsedModel;
        errorMessage.value = "no_update_required";
        break;
      case ILState.shouldUpdate:
        jsonString = sp.getString(key) ?? '';
        var parsedModel = await stringToCategories(jsonString);
        if (mounted) {
          setState(() {
            _categories = parsedModel;
            errorMessage.value = "loading";
          });
        }
        await Future.delayed(Duration(milliseconds: 1200)).then((value) async {
          await getServerString(key).then((val) async {
            var parsedModel = await stringToCategories(val);
            if (parsedModel.isNotEmpty) {
              await sp.setString(key, val);
              await setLastUpdated(key);
              if (mounted) {
                setState(() {
                  _categories = parsedModel;
                  errorMessage.value = "update_success";
                });
              }
            } else {
              errorMessage.value = "no_valid_model";
            }
          }, onError: handleError);
        });
        break;
    }
    if (mounted) {
      setState(() {});
    }
  }

  void handleError(dynamic e) {
    debugPrint(e.toString());

    if (!mounted) return;

    setState(() {
      loadingState = ILLoadingState.error;
    });

    if (e is NotFoundException) {
      setState(() {
        errorMessage.value = "not_found_exception";
      });
    } else if (e is SocketException) {
      setState(() {
        errorMessage.value = "socket_exception";
      });
    } else if (e is InternalServerErrorException) {
      setState(() {
        errorMessage.value = "internal_server_error_exception";
      });
    } else if (e is ClientException) {
      setState(() {
        errorMessage.value = "client_exception";
      });
    } else {
      setState(() {
        errorMessage.value = "exception";
      });
    }
  }

  Future<ILState> checkState(String key) async {
    if (!sp.containsKey(key)) {
      return ILState.blankState;
    } else {
      final lastUpdated = sp.getString(getUpdateString(key));

      if (lastUpdated == null) {
        return ILState.shouldUpdate;
      }

      var date = DateTime.now();
      try {
        date = DateTime.parse(lastUpdated);
        updated.value = DateFormat.yMd(
                FlutterI18n.currentLocale(context)?.languageCode ?? 'de')
            .add_jm()
            .format(date);
      } on FormatException catch (e) {
        debugPrint(e.toString());
      }

      final ddd = date.difference(DateTime.now()).inSeconds.abs();

      if (ddd > 5) {
        return ILState.shouldUpdate;
      } else {
        return ILState.noUpdateRequired;
      }
    }
  }

  Future<void> setLastUpdated(String key) async {
    final keyLastUpdated = '$key.lastUpdated';
    if (mounted) {
      updated.value = DateFormat.yMd(
              FlutterI18n.currentLocale(context)?.languageCode ?? 'de')
          .add_jm()
          .format(DateTime.now());
    }
    await sp.setString(keyLastUpdated, DateTime.now().toString());
  }

  String getUpdateString(String key) => '$key.lastUpdated';

  Future<List<ILCategory>> getServerCategories(String key) async =>
      await stringToCategories(await getServerString(key));

  Future<List<ILCategory>> getLocalCategories(String key) async =>
      await stringToCategories(await getLocalString(key));

  static Future<String> getServerString(String key) async {
    var url = "$localizationUri$key";
    var _httpRepo = HttpRepository();
    var res = await _httpRepo
        .get(url, secured: false, headers: {'Accept': 'application/json'});
    var stringResult = utf8.decode(res.bodyBytes);
    return stringResult;
  }

  Future<String> getLocalString(String language) async {
    final localJson = await DefaultAssetBundle.of(context)
        .loadString("assets/flutter_i18n/$language.links.json");
    return localJson;
  }

  Future<List<ILCategory>> stringToCategories(String input) async {
    dynamic result;
    try {
      result = jsonDecode(input);
    } on Exception catch (e) {
      debugPrint("[IMPORTANT LINKS DECODE FAIL] $e");
      return [];
    }
    var categories = <ILCategory>[];
    try {
      for (var mapData in result) {
        categories.add(ILCategory.fromJson(mapData));
      }
    } on Exception catch (e) {
      debugPrint("IMPORTANT LINKS JSON PARSING ERROR] $e");
      return [];
    }
    return categories;
  }

  @override
  bool get wantKeepAlive => true;
}

class LoadingProgressWidget extends StatelessWidget {
  const LoadingProgressWidget({
    Key? key,
    required this.errorMessage,
  }) : super(key: key);

  final ValueNotifier<String> errorMessage;

  @override
  Widget build(BuildContext context) => AnimatedSwitcher(
        duration: Duration(milliseconds: 600),
        child: determineLoadingState(context),
      );

  Widget determineLoadingState(BuildContext context) {
    switch (errorMessage.value) {
      case "no_update_required":
      case "initialized":
      case "update_success":
        return ClipOval(
          clipBehavior: Clip.antiAlias,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                TCDialog.showCustomDialog(
                    context: context,
                    onConfirm: () {},
                    functionActionText:
                        FlutterI18n.translate(context, 'common.actions.ok'),
                    headlineText: FlutterI18n.translate(
                        context, 'modules.important_links.explain_headline'),
                    bodyText: FlutterI18n.translate(context,
                        'modules.important_links.explain_description'));
              },
              child: Padding(
                padding: EdgeInsets.all(24.0),
                child: Icon(
                  Icons.check_circle_rounded,
                  key: Key(errorMessage.value),
                  size: 80,
                  color: CorporateColors.tinyCampusIconGrey.withOpacity(0.1),
                ),
              ),
            ),
          ),
        );
      case "none":
      case "loading":
        return Container(
            key: Key(errorMessage.value),
            margin: EdgeInsets.all(24.0),
            width: 80,
            height: 80,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(
                  CorporateColors.tinyCampusIconGrey.withOpacity(0.1)),
              backgroundColor:
                  CorporateColors.tinyCampusIconGrey.withOpacity(0.05),
            ));
      case "error":
      case "socket_exception":
      case "not_found_exception":
      case "internal_server_error_exception":
      case "client_exception":
      case "no_valid_model":
        return ClipOval(
          clipBehavior: Clip.antiAlias,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                TCDialog.showCustomDialog(
                    context: context,
                    onConfirm: () {},
                    functionActionText:
                        FlutterI18n.translate(context, 'common.actions.ok'),
                    headlineText: FlutterI18n.translate(
                        context, 'modules.important_links.messages.error'),
                    bodyText:
                        determineErrorMessage(context, errorMessage.value));
              },
              child: Stack(
                  key: Key(errorMessage.value),
                  alignment: Alignment.center,
                  children: [
                    Icon(
                      Icons.error,
                      size: 128,
                      color: CorporateColors.cafeteriaCautionYellow
                          .withOpacity(0.3),
                    ),
                  ]),
            ),
          ),
        );
      default:
        return Text(
          errorMessage.value,
          key: Key(errorMessage.value),
        );
    }
  }

  String determineErrorMessage(BuildContext context, String errorKey) {
    switch (errorKey) {
      case "not_found_exception":
      case "internal_server_error_exception":
      case "no_valid_model":
        return "${FlutterI18n.translate(
          context,
          'modules.important_links.messages.$errorKey',
        )} ${FlutterI18n.translate(
          context,
          'modules.important_links.messages.help_us',
        )}";
      default:
        return FlutterI18n.translate(
            context, 'modules.important_links.messages.$errorKey');
    }
  }
}

enum ILState {
  blankState,
  noUpdateRequired,
  shouldUpdate,
}
enum ILLoadingState {
  none,
  error,
  loaded,
  fetching,
}
