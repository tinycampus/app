/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';

import '../../../common/tc_theme.dart';
import '../bloc/bookmark_links/bookmark_links_bloc.dart';
import '../model/item.dart';
import '../widget/list_item_widget.dart';

class BookmarkLinkPage extends StatefulWidget {
  const BookmarkLinkPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => BookmarkLinkState();
}

class BookmarkLinkState extends State<BookmarkLinkPage> {
  @override
  Widget build(BuildContext context) =>
      BlocBuilder<BookmarkLinksBloc, BookmarkLinksState>(
        builder: (context, state) => (state.bookmarkedItems.isNotEmpty)
            ? Stack(
                children: [
                  Positioned(
                    child: SizedBox(
                      height: 220,
                      child: Center(
                          child: Icon(
                        Icons.bookmark,
                        color:
                            CorporateColors.tinyCampusIconGrey.withOpacity(0.1),
                        size: 72,
                      )),
                    ),
                  ),
                  Positioned.fill(
                    child: SingleChildScrollView(
                      reverse: true,
                      physics: BouncingScrollPhysics(
                          parent: AlwaysScrollableScrollPhysics()),
                      child: Column(
                        children: [
                          Container(height: 220),
                          _buildBookmarkedLinks(context, state.bookmarkedItems)
                        ],
                      ),
                    ),
                  ),
                ],
              )
            : Center(
                child: I18nText(
                'modules.important_links.no_bookmark',
                child: Text(
                  "Keine Lesezeichen hinzugefügt",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      ?.copyWith(fontSize: 20),
                ),
              )),
      );
}

Widget _buildBookmarkedLinks(BuildContext context, List<Item> items) => Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 6),
          child: ClipRRect(
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
            child: Container(
              color: Theme.of(context).primaryColor,
              padding: const EdgeInsets.only(top: 6, bottom: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListItemWidget(
                    listItems: items,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
