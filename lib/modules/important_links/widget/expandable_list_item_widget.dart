/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../model/item.dart';
import 'list_item_widget.dart';

class EntryItem extends StatefulWidget {
  final Item item;

  const EntryItem({Key? key, required this.item}) : super(key: key);

  @override
  _EntryItemState createState() => _EntryItemState();
}

class _EntryItemState extends State<EntryItem> {
  final ValueNotifier<bool> isExpanded = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) => Material(
        color: Theme.of(context).primaryColor,
        child: InkWell(
          child: Theme(
            data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
            child: ExpansionTile(
              key: PageStorageKey<Item>(widget.item),
              title: Text(
                widget.item.title,
                style: Theme.of(context).textTheme.headline3,
              ),
              subtitle: Text('${widget.item.children.length} Einträge'),
              onExpansionChanged: (value) {
                isExpanded.value = value;
              },
              trailing: Chip(
                backgroundColor: Colors.transparent,
                label: ValueListenableBuilder(
                  valueListenable: isExpanded,
                  builder: (context, value, child) => Wrap(
                    alignment: WrapAlignment.center,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Text(
                        FlutterI18n.translate(
                            context,
                            isExpanded.value
                                ? 'modules.important_links.expandable_shrink'
                                : 'modules.important_links.expandable_expand'),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            ?.copyWith(color: CorporateColors.tinyCampusOrange),
                      ),
                      TweenAnimationBuilder<double>(
                        duration: Duration(milliseconds: 90),
                        tween: Tween<double>(
                            begin: 0,
                            end: isExpanded.value ? pi * 0.5 : pi * 1.5),
                        curve: Curves.easeOut,
                        builder: (context, value, child) => Transform.rotate(
                          angle: value,
                          child: Icon(
                            Icons.chevron_left,
                            color: CorporateColors.tinyCampusOrange,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              children: widget.item.children
                  .map((e) => Column(
                        children: [
                          LinkListTile(
                            item: e,
                          ),
                          Divider(
                            color: Theme.of(context).backgroundColor,
                            height: 16,
                            thickness: 1,
                            indent: 16,
                            endIndent: 16,
                          )
                        ],
                      ))
                  .toList(),
            ),
          ),
        ),
      );
}
