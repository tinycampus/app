/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../common/tc_theme.dart';
import '../bloc/bookmark_links/bookmark_links_bloc.dart';
import '../model/item.dart';
import '../widget/bookmark_widget.dart';
import 'expandable_list_item_widget.dart';

class ListItemWidget extends StatelessWidget {
  final List<Item> listItems;

  const ListItemWidget({
    Key? key,
    required this.listItems,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => ListView.separated(
        itemCount: listItems.length,
        shrinkWrap: true,
        itemBuilder: (context, index) => listItems[index].children.isNotEmpty
            ? EntryItem(item: listItems[index])
            : LinkListTile(item: listItems[index]),
        separatorBuilder: (context, index) => Divider(
          color: Theme.of(context).backgroundColor,
          height: 16,
          thickness: 1,
          indent: 16,
          endIndent: 16,
        ),
        physics: NeverScrollableScrollPhysics(),
      );
}

class LinkListTile extends StatefulWidget {
  const LinkListTile({
    Key? key,
    required this.item,
  }) : super(key: key);

  final Item item;

  @override
  _LinkListTileState createState() => _LinkListTileState();
}

class _LinkListTileState extends State<LinkListTile> {
  @override
  Widget build(BuildContext context) => Material(
        color: Theme.of(context).primaryColor,
        child: ListTile(
          key: Key(widget.item.toString()),
          leading: CircleAvatar(
            backgroundColor: validUrl(widget.item.imageUrl)
                ? Colors.transparent
                : Theme.of(context).backgroundColor,
            child: validUrl(widget.item.imageUrl)
                ? FutureBuilder(
                    future: Future.delayed(Duration(milliseconds: 120)),
                    builder: (context, snapshot) => ClipOval(
                      clipBehavior: Clip.antiAlias,
                      child: SizedBox(
                        width: 40,
                        height: 40,
                        child: CachedNetworkImage(
                          key: Key(widget.item.imageUrl),
                          imageUrl: widget.item.imageUrl,
                          errorWidget: (context, url, error) => Center(
                            child: Text(widget.item.title.length >= 2
                                ? widget.item.title.substring(0, 2)
                                : "?"),
                          ),
                        ),
                      ),
                    ),
                  )
                : Text(widget.item.title.length >= 2
                    ? widget.item.title.substring(0, 2).toUpperCase()
                    : "?"),
          ),
          onTap: () => _launchURL(widget.item.url),
          title: Text(
            widget.item.title,
            style: Theme.of(context)
                .textTheme
                .headline3
                ?.copyWith(color: CurrentTheme().textSoftWhite),
          ),
          trailing: BlocBuilder<BookmarkLinksBloc, BookmarkLinksState>(
            builder: (context, state) => BookmarkWidget(
              item: widget.item,
              bookmarked: state.bookmarkedItems
                  .any((element) => element.url == widget.item.url),
            ),
          ),
        ),
      );

  bool validUrl(String url) {
    if (url.isEmpty) return false;

    if (url.contains(RegExp(r'\.(gif|jpe?g|tiff?|png|webp|bmp)$'))) {
      if (Uri.parse(url).isAbsolute) {
        return true;
      }
    }
    return false;
  }
}

Future<void> _launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
