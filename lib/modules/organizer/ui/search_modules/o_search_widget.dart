/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';

const String _i18nKey = "modules.organizer.ui.search_modules";

class OSearchWidget extends StatefulWidget {
  OSearchWidget({
    Key? key,
    required this.myController,
    required this.onChanged,
    required this.resetFilter,
  }) : super(key: key);
  final TextEditingController myController;
  final void Function(String) onChanged;
  final VoidCallback resetFilter;

  @override
  _OSearchWidgetState createState() => _OSearchWidgetState();
}

class _OSearchWidgetState extends State<OSearchWidget> {
  late FocusNode focusNode;

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 4.0),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Stack(
          children: <Widget>[
            TextField(
              autofocus: true,
              focusNode: focusNode,
              style: TextStyle(
                  fontSize: 22.0,
                  fontStyle: FontStyle.normal,
                  color: CurrentTheme().tcBlueFont),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 24.0,
                    horizontal: 8.0,
                  ),
                  // contentPadding: EdgeInsets.only(
                  // left: 15, bottom: 11, top: 11, right: 15),
                  hintText: FlutterI18n.translate(context, '$_i18nKey.search'),
                  hintStyle:
                      TextStyle(color: CorporateColors.tinyCampusIconGrey)),
              controller: widget.myController,
              onChanged: widget.onChanged,
            ),
            Positioned(
                top: 0,
                bottom: 0,
                right: 8,
                child: AnimatedOpacity(
                  curve: Curves.easeIn,
                  duration: Duration(milliseconds: 160),
                  opacity: widget.myController.text.isEmpty ? 1 : 0,
                  child: Icon(Icons.search,
                      size: 32.0, color: CorporateColors.tinyCampusIconGrey),
                )),
            Positioned(
                top: 0,
                bottom: 0,
                right: 0,
                child: AnimatedSwitcher(
                  duration: Duration(
                    milliseconds: 210,
                  ),
                  child: widget.myController.text.isNotEmpty
                      ? IconButton(
                          onPressed: () {
                            setState(() {
                              widget.myController.clear();
                              widget.resetFilter();
                            });
                          },
                          icon: Icon(Icons.backspace,
                              size: 24.0,
                              color: widget.myController.text.isEmpty
                                  ? CorporateColors.tinyCampusIconGrey
                                  : CurrentTheme().tcBlueFont),
                        )
                      : IconButton(
                          onPressed: () => focusNode.requestFocus(),
                          icon: Icon(Icons.search,
                              size: 32.0,
                              color: CorporateColors.tinyCampusIconGrey),
                        ),
                )),
          ],
        ),
      );
}
