/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../../common/tc_theme.dart';

class ODismissibleHintWidget extends StatelessWidget {
  ODismissibleHintWidget({
    Key? key, // TODO NNBD MIGRATION
    required this.hintType, // TODO NNBD MIGRATION
    required this.hintText,
    required this.canBeDismissed, // TODO NNBD MIGRATION
    required this.isVisible, // TODO NNBD MIGRATION
  }) : super(key: key);

  final HintType hintType;
  final String hintText;
  final bool canBeDismissed;
  final bool isVisible;

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //icon
            _buildIcon(),
            Expanded(
              flex: 10,
              child: _buildText(),
            ),
            //text
          ],
        ),
      );

  Widget _buildText() => Text(
        hintText,
        style: TextStyle(
          color: getColor(),
          fontSize: 16.0,
        ),
      );

  Widget _buildIcon() {
    var icon = Icons.info;

    switch (hintType) {
      case HintType.notice:
        icon = icon;
        break;
      case HintType.friendly:
        icon = Icons.tag_faces;
        break;
      case HintType.caution:
        icon = Icons.error_outline;
        break;
      case HintType.danger:
        icon = Icons.error;
        break;
    }
    return Container(
        margin: EdgeInsets.only(right: 8.0),
        child: Icon(icon, size: 20.0, color: getColor()));
  }

  Color getColor() {
    var color = CorporateColors.tinyCampusIconGrey;
    switch (hintType) {
      case HintType.notice:
        color = color;
        break;
      case HintType.friendly:
        color = CorporateColors.cafeteriaVeganGreen;
        break;
      case HintType.caution:
        color = CorporateColors.cafeteriaCautionYellow;
        break;
      case HintType.danger:
        color = CorporateColors.cafeteriaCautionRed;
        break;
    }
    return color;
  }
}

enum HintType {
  notice,
  friendly,
  caution,
  danger,
}
