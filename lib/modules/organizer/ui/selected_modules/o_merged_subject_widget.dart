/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../model/o_lesson.dart';

const String _i18nKey = "modules.organizer.ui.selected_modules";

class OMergedSubjectWidget extends StatefulWidget {
  const OMergedSubjectWidget({
    Key? key,
    required this.subjectPools,
    required this.subjectNames,
    required this.subjectLessons,
    required this.lessonPoolName,
  }) : super(key: key);

  final List<String> subjectPools;
  final List<String> subjectNames;
  final List<OLesson> subjectLessons;
  final Map<int, List<String>> lessonPoolName;

  @override
  _OMergedSubjectWidgetState createState() => _OMergedSubjectWidgetState();
}

class _OMergedSubjectWidgetState extends State<OMergedSubjectWidget>
    with SingleTickerProviderStateMixin {
  bool isExpandedd = false;

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(vertical: 4.0),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Card(
              child: ExpansionPanelList(
                expandedHeaderPadding: EdgeInsets.all(0.0),
                expansionCallback: (index, isExpandedd) {
                  setState(() {
                    this.isExpandedd = !isExpandedd;
                  });
                },
                children: [
                  ExpansionPanel(
                    isExpanded: isExpandedd,
                    headerBuilder: (context, isExpandedd) => _buildHeader(),
                    body: _buildLessons(),
                  )
                ],

                // items.map((NewItem item) {
                //   return new ExpansionPanel(
                //     headerBuilder: (BuildContext context, bool isExpanded) {
                //       return new ListTile(
                //           leading: item.iconpic,
                //           title: new Text(
                //             item.header,
                //             textAlign: TextAlign.left,
                //             style: new TextStyle(
                //               fontSize: 20.0,
                //               fontWeight: FontWeight.w400,
                //             ),
                //           ));
                //     },
                //     isExpanded: item.isExpanded,
                //     body: item.body,
                //   );
                // }).toList()
              ),
            ),

            // _buildLessons(),
            // ExpansionPanelList(
            // duration: Duration(milliseconds: 1000),
            // // height: 550,
            // alignment: Alignment.topCenter,
            // child:

            // ),
            // Text(subjectLessons.length.toString()),
          ],
        ),
      );

  Widget _buildHeader() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.subjectNames.length,
            itemBuilder: (context, index) {
              final item = widget.subjectNames[index];
              return Text(
                item,
                style: TextStyle(fontSize: 16.0, letterSpacing: -0.6),
              );
            },
          ),
          I18nText(
            '$_i18nKey.merged_subject_categories',
            child: Text("Kategorien:"),
          ),
          Text(widget.subjectPools
              .reduce((value, element) => '$value, $element')),
        ],
      );

  Widget _buildLessons() =>
      BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
        builder: (context, state) =>
            // if (state is OLessonSelectionReadyState) {
            // return CircularProgressIndicator();
            ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: widget.subjectLessons.length,
          itemBuilder: (context, index) {
            var isSelected =
                state.isInSelection(lessonId: widget.subjectLessons[index].id);

            return Material(
              color: Colors.white,
              // child: Text(subjectLessons[index].name),
              child: CheckboxListTile(
                value: isSelected,
                onChanged: (value) {
                  // setState(() {
                  var lessonConfigBloc =
                      BlocProvider.of<OLessonSelectionBloc>(context);
                  var previousLessons = state.lessons;
                  if (state.isInSelection(
                          lessonId: widget.subjectLessons[index].id) ==
                      true) {
                    //remove program
                    lessonConfigBloc.add(
                      RemoveOLessonEvent(
                        lesson: widget.subjectLessons[index].id ?? -1,
                        previousLessonList: previousLessons,
                      ),
                    );
                  } else {
                    //add Program
                    lessonConfigBloc.add(
                      AddOLessonEvent(
                        lesson: widget.subjectLessons[index].id ?? -1,
                        previousLessonList: previousLessons,
                      ),
                    );
                  }
                },
                title: Text(widget.subjectLessons[index].name ?? ""),
                // checkColor: _estimateBrightness(foregroundColor).color,
                // activeColor: foregroundColor,
                subtitle: Text(widget.subjectLessons[index].comment ?? "",
                    style: TextStyle(
                      fontSize: 16,
                      letterSpacing: -0.8,
                      color: CorporateColors.tinyCampusDarkBlue,
                    )),
                secondary: CircleAvatar(
                  child: Text(
                    widget.subjectLessons[index].method ?? "",

                    // style: isSelected
                    //     ? _estimateBrightness(foregroundColor)
                    //     : TextStyle(color: Colors.black87),
                    // style: isSelected
                    //     ? _estimateBrightness(foregroundColor)
                    //     : TextStyle(color: Colors.black87),
                  ),
                  backgroundColor: isSelected
                      ? CorporateColors.tinyCampusBlue
                      : CorporateColors.passiveBackgroundLight,
                ),
              ),
            );
          },
        ),
      );
}
