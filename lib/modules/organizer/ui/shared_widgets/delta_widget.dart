/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../model/o_change_record.dart';
import '../../model/o_lesson.dart';
import '../../model/o_pool.dart';
import '../../model/o_program.dart';
import '../../model/o_subject.dart';
import '../organizer/o_utility_functions.dart';

class DeltaWidget extends StatefulWidget {
  const DeltaWidget({
    Key? key,
    this.program,
    this.pool,
    this.subject,
    this.lesson,
    this.fullContextString = "",
    this.showIndirectRemoved = false,
  }) : super(key: key);

  final OProgram? program;
  final OPool? pool;
  final OSubject? subject;
  final OLesson? lesson;
  final String fullContextString;
  final bool showIndirectRemoved;

  @override
  _DeltaWidgetState createState() => _DeltaWidgetState();
}

class _DeltaWidgetState extends State<DeltaWidget>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    var keyTemp = "";
    keyTemp = OUtil.concatPPSLtrimmed(
        widget.program, widget.pool, widget.subject, widget.lesson);
    if (widget.fullContextString.isNotEmpty) {
      keyTemp = widget.fullContextString;
    }
    // if (keyTemp == null && widget.lesson != null) {
    //   return Container();
    // }
    if (keyTemp == "") return Container();

    return BlocBuilder<OProgramHistoryBloc, OProgramHistoryState>(
        builder: (context, state) {
      final changePair = state.hasChanged(keyTemp,
          showIndirectRemoved: widget.showIndirectRemoved);
      final indirectChange = changePair[0];
      final directChange = changePair[1];
      final subtreeChecked = changePair[2];
      final directUserChecked = changePair[3];
      var list = <Widget>[];
      if (indirectChange || directChange) {
        // if (state == null) return Container();
        // if (state.changeRecordMap == null) return Container();
        // if (state.changeRecordMap[key] == null) return Container();
        const color = CorporateColors.cafeteriaCautionRed;
        const iconSize = 14.0;
        final ts = Theme.of(context)
            .textTheme
            .headline4
            ?.copyWith(color: color, fontSize: iconSize);

        final changeRecords = state.changeRecordMap[keyTemp];
        // if (state.changeRecordMap[key].delta == null) return Container();
        if (changeRecords != null) {
          for (var i = 0; i < changeRecords.delta.length; i++) {
            if (directChange && !directUserChecked) {
              switch (changeRecords.delta[i]) {
                case ODelta.added:
                  list.add(
                    Row(
                      children: [
                        // Icon(TinyCampusIcons.star_0p,
                        //     color: color, size: iconSize),
                        // Container(width: 8),
                        Expanded(
                          child: Text(
                              FlutterI18n.translate(
                                      context,
                                      'modules.organizer.ui.'
                                      'changes.delta_added')
                                  .toUpperCase(),
                              style: ts),
                        ),
                      ],
                    ),
                  );
                  break;

                case ODelta.nameChanged:
                  list.add(
                    Row(
                      children: [
                        // Icon(Icons.compare, color: color, size: iconSize),
                        // Container(width: 8),
                        Expanded(
                          child: Text(
                              FlutterI18n.translate(
                                      context,
                                      'modules.organizer.ui.'
                                      'changes.delta_name_changed')
                                  .toUpperCase(),
                              style: ts),
                        ),
                      ],
                    ),
                  );
                  break;

                case ODelta.amountSubtreeIncreased:
                  list.add(
                    Row(
                      children: [
                        // Icon(Icons.add_box,
                        //     color: color, size: iconSize),
                        // Icon(Icons.unfold_more,
                        //     color: color, size: iconSize),
                        // Container(width: 8),
                        Expanded(
                          child: Text(
                              FlutterI18n.translate(
                                context,
                                'modules.organizer.ui.'
                                'changes.delta_amount_subtree_increased',
                              ).toUpperCase(),
                              style: ts),
                        ),
                      ],
                    ),
                  );
                  break;

                case ODelta.amountSubtreeDecreased:
                  list.add(
                    Row(
                      children: [
                        // Icon(Icons.indeterminate_check_box,
                        //     color: color, size: iconSize),
                        // Icon(Icons.unfold_less,
                        //     color: color, size: iconSize),
                        // Container(width: 8),
                        Expanded(
                          child: Text(
                              FlutterI18n.translate(
                                context,
                                'modules.organizer.ui.'
                                'changes.delta_amount_subtree_decreased',
                              ).toUpperCase(),
                              style: ts),
                        ),
                      ],
                    ),
                  );
                  break;

                case ODelta.removed:
                  list.add(
                    Row(
                      children: [
                        // Icon(Icons.remove, color: color, size: iconSize),
                        // Container(width: 8),
                        Expanded(
                          child: Text(
                              FlutterI18n.translate(
                                      context,
                                      'modules.organizer.ui.'
                                      'changes.delta_removed')
                                  .toUpperCase(),
                              style: ts),
                        ),
                      ],
                    ),
                  );
                  break;

                case ODelta.descriptionChanged:
                  list.add(
                    Row(
                      children: [
                        // Icon(Icons.description,
                        //     color: color, size: iconSize),
                        // Container(width: 8),
                        Expanded(
                          child: Text(
                              FlutterI18n.translate(
                                      context,
                                      'modules.organizer.ui.'
                                      'changes.delta_description_changed')
                                  .toUpperCase(),
                              style: ts),
                        ),
                      ],
                    ),
                  );
                  break;

                default:
                  break;
              }
            }
          }
        }
        if (indirectChange && !subtreeChecked) {
          list.add(
            Row(
              children: [
                Icon(Icons.more_horiz, color: color, size: 12),
                Container(width: 8),
              ],
            ),
          );
        }

        // return AnimatedSize(
        //   duration: Duration(milliseconds: 260),
        //   vsync: this,
        //   curve: Curves.easeOut,
        //   child: AnimatedSwitcher(
        //     duration: Duration(milliseconds: 260),
        //     child:
        //         //  subtreeChecked && directUserChecked
        //         //     ? Container()
        //         //     :
        //         Column(
        //             crossAxisAlignment: CrossAxisAlignment.start,
        //             children: list),
        //   ),
        // );
      }
      return AnimatedSize(
        duration: Duration(milliseconds: 260),
        curve: Curves.easeOut,
        child: AnimatedSwitcher(
          duration: Duration(milliseconds: 260),
          child:
              //  subtreeChecked && directUserChecked
              //     ? Container()
              //     :
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start, children: list),
        ),
      );
    });
  }
}
