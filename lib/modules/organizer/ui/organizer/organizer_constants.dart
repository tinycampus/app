/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

export 'package:flutter_i18n/flutter_i18n.dart'
    show FlutterI18n, I18nText, I18nPlural;

const String i18nOKey = 'modules.organizer.ui.organizer';
const String i18nODKey = '$i18nOKey.drawer';
const String i18nOPKey = '$i18nOKey.page';
const String i18nOWKey = '$i18nOKey.week_widget';
const String i18nOCKey = 'modules.organizer.ui.config';
const String i18nOCHKey = 'modules.organizer.ui.changes';

/// Determines when data from organizer is considered old
/// datedThreshold will deliver absolute amount of seconds
const int datedThreshold = 20 * 60 * 1000; // 20 minutes
const int datedProgramUpdatingThreshold = 6 * 60 * 60 * 1000; // 6 hours

const int forcedDelayInMilliseconds = 550;
const int switcherDelayInMilliseconds = 400;

/// Organizer overview_week constants
const double maxHeight = 625.0;
const double weekBodyExtraHeight = 350;
const double extraSpace = 0.0;

/// Organizer bottom sheet drawer constants
const double oDrawerIconHeight = 42;
const double oDrawerInfoBlockWidth = 70.0;
const EdgeInsetsGeometry iconMargin = EdgeInsets.only(bottom: 12.0);

/// Organizer context separator for delta map
const String contextStringSeparator = "_";
