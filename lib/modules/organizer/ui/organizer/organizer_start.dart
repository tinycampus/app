/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/o_animation_bloc/o_animation_bloc.dart';
import '../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import '../../blocs/o_message_bloc/o_message_bloc.dart';
import '../../blocs/o_view_mode_bloc/o_view_mode_bloc.dart';
import 'organizer_page.dart';

class OrganizerStart extends StatelessWidget {
  const OrganizerStart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MultiBlocProvider(
        providers: [
          BlocProvider<ODrawerBloc>(
            create: (context) => ODrawerBloc(),
          ),
          BlocProvider<OMessageBloc>(
            create: (context) => OMessageBloc(),
          ),
          BlocProvider<OViewModeBloc>(
            create: (context) => OViewModeBloc(),
          ),
          BlocProvider<OAnimationBloc>(
            create: (context) => OAnimationBloc(),
          ),
        ],
        child: OrganizerPage(),
      );
}
