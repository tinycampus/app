/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/dialog/tc_dialog.dart' show TCDialog;
import '../../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../model/o_change_record.dart';
import '../../model/o_pool.dart';
import '../../model/o_program.dart';
import '../organizer/o_utility_functions.dart';
import '../organizer/program_history/o_change_list_tile.dart';
import 'o_subject_list_tile.dart';

const String _i18nKey = "modules.organizer.ui.pool_config";

class OrganizerPoolConfigPageArguments {
  OrganizerPoolConfigPageArguments({
    required this.program,
    required this.pool,
  });

  OPool pool;
  OProgram program;
}

class OrganizerPoolConfigPage extends StatefulWidget {
  const OrganizerPoolConfigPage({
    Key? key,
    required this.pool,
    required this.program,
  }) : super(key: key);
  final OPool pool;
  final OProgram program;

  @override
  _OrganizerPoolConfigPageState createState() =>
      _OrganizerPoolConfigPageState();
}

class _OrganizerPoolConfigPageState extends State<OrganizerPoolConfigPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => Navigator.of(context).pop(),
          label: Padding(
            padding: EdgeInsets.symmetric(horizontal: 32.0),
            child: I18nText(
              'modules.organizer.ui.config.done',
              child: Text(
                "Fertig",
                style: Theme.of(context).textTheme.headline4,
                textAlign: TextAlign.left,
              ),
            ),
          ),
          backgroundColor: Theme.of(context).primaryColor,
          foregroundColor: CorporateColors.tinyCampusBlue,
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: Stack(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(top: appBarHeight),
                child: _buildPoolModules(context)),
            _buildCustomAppBar(context),
          ],
        ),
      );

  final appBarHeight = 150.0;
  Widget _buildCustomAppBar(BuildContext context) => SizedBox(
        height: appBarHeight,
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  // borderRadius: BorderRadius.only(
                  //     bottomLeft: Radius.circular(12),
                  //     bottomRight: Radius.circular(12)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      spreadRadius: 0,
                      blurRadius: 3,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Hero(
                        tag: "BACKBUTTON",
                        child: Material(
                          color: Colors.transparent,
                          child: BackButton(
                            color: Colors.black,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Hero(
                          tag: widget.pool.id,
                          child: Material(
                            type: MaterialType.transparency,
                            child: Text(
                              widget.pool.name,
                              textAlign: TextAlign.left,
                              style: Theme.of(context).textTheme.headline3,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Container(
                          margin: EdgeInsets.all(0.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Expanded(
                                child: Card(
                                  elevation: 0.0,
                                  margin: EdgeInsets.all(0.0),
                                  child: FlatButton(
                                    padding: EdgeInsets.all(8.0),
                                    onPressed: () => TCDialog.showCustomDialog(
                                      context: context,
                                      onConfirm: () {
                                        removeAllSubjectLessons(widget.pool);
                                      },
                                      functionActionText: FlutterI18n.translate(
                                        context,
                                        '$_i18nKey.deselect_everything',
                                      ),
                                      headlineText: FlutterI18n.translate(
                                          context, '$_i18nKey.headline'),
                                      bodyText: FlutterI18n.translate(
                                          context, '$_i18nKey.body'),
                                      functionActionColor:
                                          CorporateColors.cafeteriaCautionRed,
                                    ),
                                    // onPressed: () =>
                                    //     removeAllSubjectLessons(widget.pool),
                                    child: Center(
                                        child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.remove,
                                        ),
                                        I18nText(
                                          '$_i18nKey.deselect_everything',
                                          child: Text("Alles abwählen",
                                              softWrap: true,
                                              maxLines: 2,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w300,
                                                  fontSize: 12.0)),
                                        ),
                                        // ),
                                      ],
                                    )),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Card(
                                  margin: EdgeInsets.all(0.0),
                                  elevation: 0.0,
                                  child: FlatButton(
                                    padding: EdgeInsets.all(8.0),
                                    onPressed: () =>
                                        addAllSubjectLessons(widget.pool),
                                    child: Center(
                                        child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.add,
                                        ),
                                        I18nText(
                                          '$_i18nKey.select_everything',
                                          child: Text(
                                            "Alles auswählen",
                                            softWrap: true,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w300,
                                                fontSize: 11.0),
                                          ),
                                        ),
                                      ],
                                    )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );

  Widget _notifyRemovalOfSubjectsWidget(BuildContext context) =>
      BlocBuilder<OProgramHistoryBloc, OProgramHistoryState>(
        builder: (context, state) {
          // if (state == null) return Container();
          try {
            var changes = state
                .getSubChangesMap(OUtil.concatPP(widget.program, widget.pool))
                .entries
                .where(
                    (element) => element.value.delta.contains(ODelta.removed));

            if (changes.isEmpty) {
              return Container();
            }
            return Container(
              margin: EdgeInsets.symmetric(vertical: 16.0),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24.0),
                    child: Text(
                        FlutterI18n.translate(context,
                                'modules.organizer.ui.changes.delta_removed')
                            .toUpperCase(),
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline3?.copyWith(
                              color: CorporateColors.cafeteriaCautionRed,
                            )),
                  ),
                  ...changes
                      .map((entry) => OChangeListTile(entry, ODelta.removed))
                      .toList()
                ],
              ),
            );
          } on Exception catch (e) {
            debugPrint(e.toString());
          }
          return Container();
        },
      );

  Widget _buildPoolModules(BuildContext context) => SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Container(
              margin:
                  EdgeInsets.only(bottom: 16.0, top: 16, left: 16, right: 16),
              child: ClipRRect(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                child: Container(
                  color: Theme.of(context).primaryColor,
                  child: ListView.builder(
                    shrinkWrap: true,
                    padding: EdgeInsets.zero,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: widget.pool.subjects.length,
                    itemBuilder: (context, index) => Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: _buildCustomListTile(
                            index: index,
                            context: context,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            _notifyRemovalOfSubjectsWidget(context),
            Container(height: 80),
          ],
        ),
      );

  void removeAllSubjectLessons(OPool pool) {
    setState(
      () {
        var lessonConfigBloc = BlocProvider.of<OLessonSelectionBloc>(context);
        var previousLessons = lessonConfigBloc.state.lessons;
        lessonConfigBloc.add(RemoveMultipleOLessonsEvent(
          lessons: getDistinctLessonsInPool(pool),
          previousLessonList: previousLessons,
        ));
      },
    );
  }

  void addAllSubjectLessons(OPool pool) {
    setState(
      () {
        var lessonConfigBloc = BlocProvider.of<OLessonSelectionBloc>(context);
        var previousLessons = lessonConfigBloc.state.lessons;
        lessonConfigBloc.add(
          AddMultipleOLessonsEvent(
            lessons: getDistinctLessonsInPool(pool),
            previousLessonList: previousLessons,
          ),
        );
      },
    );
  }

  Widget _buildCustomListTile({
    required BuildContext context,
    required int index,
  }) =>
      OSubjectListTile(
        pool: widget.pool,
        subject: widget.pool.subjects[index],
        program: widget.program,
      );

  List<int> getDistinctLessonsInPool(OPool pool) {
    var distinctLessonsInPool = <int>[];
    if (pool.subjects.isNotEmpty) {
      for (var subject in pool.subjects) {
        if (subject.lessons.isNotEmpty) {
          for (var subjectLesson in subject.lessons) {
            if (!distinctLessonsInPool.contains(subjectLesson.id)) {
              distinctLessonsInPool.add(subjectLesson.id ?? -1);
            }
          }
        }
      }
    }
    return distinctLessonsInPool;
  }
}
