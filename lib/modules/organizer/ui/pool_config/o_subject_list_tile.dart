/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../model/o_interferences.dart';
import '../../model/o_pool.dart';
import '../../model/o_program.dart';
import '../../model/o_subject.dart';
import '../organizer/o_utility_functions.dart';
import '../shared_widgets/delta_widget.dart';
import '../subject_config/organizer_subject_config_args.dart';

const String _i18nKey = "modules.organizer.ui.pool_config";

// enum OSubjectListTileType {
//   checkbox,
//   remove,
// }

class OSubjectListTile extends StatefulWidget {
  const OSubjectListTile({
    Key? key,
    // required this.context,
    // required this.state,
    this.onlyShowIfPreSelected = false,
    this.showDialogOnRemove = false,
    // required this.index,
    required this.subject,
    required this.program,
    required this.pool,
    // this.arguments,
  }) : super(key: key);
  // final BuildContext context;
  // final OLessonSelectionReadyState state;
  final bool onlyShowIfPreSelected;
  final bool showDialogOnRemove;
  // final int index;
  final OSubject subject;
  final OProgram program;
  final OPool pool;

  // final OSubjectListTileType type;
  // final OrganizerSubjectConfigPageArguments? arguments;

  @override
  _OSubjectListTileState createState() => _OSubjectListTileState();
}

class _OSubjectListTileState extends State<OSubjectListTile> {
  final interferences = OInterferences();

  @override
  void initState() {
    super.initState();
    // determine interferences
    // if (widget.program == null) {
    //   // Won't translate as this is just debug output
    //   debugPrint("no Program");
    //   return;
    // }
    // if (widget.pool == null) {
    //   debugPrint("no Pool");
    //   return;
    // }
    // if (widget.subject == null) {
    //   debugPrint("no Subject");
    //   return;
    // }
    interferences.calculateInterferences(
      originProgram: widget.program,
      originPool: widget.pool,
      originSubject: widget.subject,
    );
    debugPrint(interferences.toString());
  }

  // interferences

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
          builder: (context, state) {
        var hasSelectedLessons =
            state.subjectHasLessonsInSelection(subject: widget.subject);
        if (widget.onlyShowIfPreSelected && !hasSelectedLessons) {
          return Container();
        }
        if (state is OLessonSelectionReadyState) {
          //get interferences

          return Material(
            color: Theme.of(context).primaryColor,
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(
                horizontal: 8.0,
                vertical: 0,
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    // flex: 6,
                    child: GestureDetector(
                      onTap: () {
                        OUtil.addOHistoryBlocEvent(UserCheckedChangeEvent(
                            OUtil.concatPPS(
                                widget.program, widget.pool, widget.subject)));
                        Navigator.pushNamed(
                          context,
                          orgaSubjectConfigRoute,
                          arguments: OrganizerSubjectConfigPageArguments(
                            pool: widget.pool,
                            subject: widget.subject,
                            program: widget.program,
                          ),
                        );
                      },
                      child: Container(
                        color: Theme.of(context).primaryColor,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 4.0),
                                child: Container(
                                  margin: EdgeInsets.only(left: 8.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Material(
                                        type: MaterialType.transparency,
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text(widget.subject.name,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 2,
                                                  softWrap: true,
                                                  style: TextStyle(
                                                      fontSize: 16.0,
                                                      letterSpacing: -0.6)),
                                            ),
                                          ],
                                        ),
                                      ),
                                      DeltaWidget(
                                        program: widget.program,
                                        pool: widget.pool,
                                        subject: widget.subject,
                                      ),
                                      if (OUtil.concatPoolStrings(interferences)
                                          .isNotEmpty)
                                        Text(
                                          OUtil.concatPoolStrings(
                                              interferences),
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.w400,
                                            letterSpacing: -0.7,
                                            color: CorporateColors
                                                .tinyCampusIconGrey,
                                          ),
                                          // ),
                                        )
                                      else
                                        Container(),
                                      TweenAnimationBuilder(
                                        tween: IntTween(
                                          begin: 0,
                                          end: state
                                              .getAmountOfLessonsFromSubject(
                                            subject: widget.subject,
                                          ),
                                        ),
                                        curve: Curves.linear,
                                        duration: Duration(milliseconds: 260),
                                        builder: (context, i, child) =>
                                            I18nText(
                                          '$_i18nKey.x_of_y',
                                          translationParams: {
                                            'x': '$i',
                                            'y': widget.subject.lessons.length
                                                .toString()
                                          },
                                          child: Text(
                                            '',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              letterSpacing: -0.6,
                                              color: hasSelectedLessons
                                                  ? CurrentTheme().tcBlue
                                                  : CorporateColors
                                                      .tinyCampusIconGrey,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  // _buildTrailing(context, state),
                  SizedBox(
                    width: 80,
                    // margin: EdgeInsets.only(left: 15.0),
                    child: _addButton(context, state),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Container();
        }
      });

  Widget _addButton(BuildContext context, OLessonSelectionReadyState state) {
    var selectedLessons =
        state.getAmountOfLessonsFromSubject(subject: widget.subject);
    final amountLessons = widget.subject.lessons.length;
    var status = SubjectListTileStatus.noLessonsSelected;
    if (amountLessons == 0) {
      status = SubjectListTileStatus.noLessonsAvailable;
    } else if (selectedLessons == 0) {
      status = SubjectListTileStatus.noLessonsSelected;
    } else if (selectedLessons > 0 && selectedLessons < amountLessons) {
      status = SubjectListTileStatus.someLessonsSelected;
    } else if (selectedLessons == amountLessons) {
      status = SubjectListTileStatus.allLessonsSelected;
    }

    switch (status) {
      case SubjectListTileStatus.noLessonsSelected:
        return FlatButton(
          color: CorporateColors.cafeteriaVeganGreen,
          onPressed: () => _addAllLessonsFromSubject(context, state),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 4.0),
                child: Icon(
                  Icons.library_add,
                  // size: 40.0,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
              side: BorderSide(
                  color: CorporateColors.cafeteriaVeganGreen,
                  width: 2,
                  style: BorderStyle.solid)),
        );
      case SubjectListTileStatus.someLessonsSelected:
        return FlatButton(
          color: CorporateColors.cafeteriaVeganGreen.withOpacity(0.75),
          onPressed: () => _addAllLessonsFromSubject(context, state),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 4.0),
                child: Icon(
                  Icons.add_box,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
              side: BorderSide(
                  color: CorporateColors.cafeteriaVeganGreen,
                  width: 2,
                  style: BorderStyle.solid)),
        );
      case SubjectListTileStatus.allLessonsSelected:
        return FlatButton(
          color: Theme.of(context).primaryColor,
          onPressed: () => showAlertDialog(context, state),
          // onPressed: () => _switchSubjectState(context, state),
          // onPressed: () => _addAllLessonsFromSubject(context, state),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.check,
                color: CorporateColors.tinyCampusIconGrey.withOpacity(0.4),
              ),
            ],
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
              side: BorderSide(
                  color: CorporateColors.tinyCampusIconGrey,
                  width: 2,
                  style: BorderStyle.solid)),
        );
      case SubjectListTileStatus.noLessonsAvailable:
        return FlatButton(
          color: Colors.white,
          onPressed: null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.sentiment_neutral,
                color: CorporateColors.tinyCampusIconGrey.withOpacity(0.2),
              ),
            ],
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
              side: BorderSide(
                  color: CorporateColors.tinyCampusIconGrey.withOpacity(0.2),
                  width: 2,
                  style: BorderStyle.solid)),
        );
    }
  }

  void showAlertDialog(BuildContext context, OLessonSelectionReadyState state) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: I18nText('$_i18nKey.cancel'),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: I18nText(
        '$_i18nKey.remove',
        child: Text(
          "Entfernen",
          textAlign: TextAlign.start,
          style: TextStyle(color: CorporateColors.cafeteriaCautionRed),
        ),
      ),
      onPressed: () {
        _switchSubjectState(context, state);
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    var alert = AlertDialog(
      content: IntrinsicHeight(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // Icon(Icons.delete),
                I18nText(
                  '$_i18nKey.remove_module',
                  child: Text(
                    "Modul entfernen?",
                    textAlign: TextAlign.center,
                    style: CorporateTextStyles.dialogHeadlineStyle,
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 38.0),
              child: Text(widget.subject.name,
                  style: CorporateTextStyles.dialogBodyStyle),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                cancelButton,
                continueButton,
              ],
            )
          ],
        ),
      ),
    );

    showDialog(
      context: context,
      builder: (context) => alert,
    );
  }

  void _switchSubjectState(
      BuildContext context, OLessonSelectionReadyState state) {
    var lessonConfigBloc = BlocProvider.of<OLessonSelectionBloc>(context);
    var previousLessons = state.lessons;
    var lessonIdsArray = <int>[];
    if (widget.subject.lessons.isNotEmpty) {
      for (var lesson in widget.subject.lessons) {
        lessonIdsArray.add(lesson.id ?? -1);
      }
    }
    if (state.subjectHasLessonsInSelection(subject: widget.subject) == true) {
      //remove lessons
      lessonConfigBloc.add(
        RemoveMultipleOLessonsEvent(
          lessons: lessonIdsArray,
          previousLessonList: previousLessons,
        ),
      );
    } else {
      //add lessons
      lessonConfigBloc.add(
        AddMultipleOLessonsEvent(
          lessons: lessonIdsArray,
          previousLessonList: previousLessons,
        ),
      );
    }
  }

  void _addAllLessonsFromSubject(
      BuildContext context, OLessonSelectionReadyState state) {
    var lessonConfigBloc = BlocProvider.of<OLessonSelectionBloc>(context);
    var previousLessons = state.lessons;
    var lessonIdsArray = <int>[];
    if (widget.subject.lessons.isNotEmpty) {
      for (var lesson in widget.subject.lessons) {
        lessonIdsArray.add(lesson.id ?? -1);
      }
    }
    lessonConfigBloc.add(
      AddMultipleOLessonsEvent(
        lessons: lessonIdsArray,
        previousLessonList: previousLessons,
      ),
    );
  }
}

enum SubjectListTileStatus {
  noLessonsSelected,
  someLessonsSelected,
  allLessonsSelected,
  noLessonsAvailable,
}
