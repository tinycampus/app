/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_lesson_selection_bloc.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OLessonSelectionState extends Equatable {
  const OLessonSelectionState({required this.lessons});
  final List<int> lessons;

  @override
  List<Object> get props => [lessons];

  factory OLessonSelectionState.fromJson(Map<String, dynamic> json) =>
      _$OLessonSelectionStateFromJson(json);

  Map<String, dynamic> toJson() => _$OLessonSelectionStateToJson(this);

  bool isInSelection({int? lessonId}) {
    if (lessons.isEmpty) return false;
    if (lessons.contains(lessonId)) {
      return true;
    }
    return false;
  }

  bool subjectHasLessonsInSelection({required OSubject subject}) {
    var checkIfContained = false;
    if (lessons.isNotEmpty) {
      if (subject.lessons.isNotEmpty) {
        for (var subjectLesson in subject.lessons) {
          if (lessons.contains(subjectLesson.id)) {
            checkIfContained = true;
          }
        }
      }
    }
    return checkIfContained;
  }

  bool lessonsAreInSelection({required List<OLesson> lessons}) {
    var checkIfContained = false;
    if (lessons.isNotEmpty) {
      for (var subjectLesson in lessons) {
        if (this.lessons.contains(subjectLesson.id)) {
          checkIfContained = true;
        }
      }
    }
    return checkIfContained;
  }

  int getAmountOfLessonsFromSubject({required OSubject subject}) {
    var amountOfLessons = 0;
    if (lessons.isNotEmpty) {
      if (subject.lessons.isNotEmpty) {
        for (var subjectLesson in subject.lessons) {
          if (lessons.contains(subjectLesson.id)) {
            amountOfLessons++;
          }
        }
      }
    }
    return amountOfLessons;
  }

  int getAmountOfLessonsFromPool({required OPool pool}) {
    var distinctLessons = <int>[];
    if (lessons.isNotEmpty) {
      if (pool.subjects.isNotEmpty) {
        for (var subject in pool.subjects) {
          if (subject.lessons.isNotEmpty) {
            for (var subjectLesson in subject.lessons) {
              if (lessons.contains(subjectLesson.id)) {
                distinctLessons.add(subjectLesson.id!);
              }
            }
          }
        }
      }
    }
    return distinctLessons.length;
  }

  double getPercentageOfLessonsFromPool({required OPool pool}) {
    var selectedLessons = <int>[];
    // List<int> distinctLessonsInPool = new List<int>();
    var distinctLessonsInPool = getDistinctLessonsInPool(pool);
    if (lessons.isNotEmpty) {
      if (pool.subjects.isNotEmpty) {
        for (var subject in pool.subjects) {
          if (subject.lessons.isNotEmpty) {
            for (var subjectLesson in subject.lessons) {
              if (lessons.contains(subjectLesson.id)) {
                selectedLessons.add(subjectLesson.id!);
              }
            }
          }
        }
      }
    }
    selectedLessons = selectedLessons.toSet().toList();
    var result = selectedLessons.length / distinctLessonsInPool.length;
    return result * 100;
  }

  List<int> getDistinctLessonsInPool(OPool pool) {
    var distinctLessonsInPool = <int>[];
    if (pool.subjects.isNotEmpty) {
      for (var subject in pool.subjects) {
        if (subject.lessons.isNotEmpty) {
          for (var subjectLesson in subject.lessons) {
            if (lessons.contains(subjectLesson.id)) {
              distinctLessonsInPool.add(subjectLesson.id!);
            }
          }
        }
      }
    }
    distinctLessonsInPool = distinctLessonsInPool.toSet().toList();
    return distinctLessonsInPool;
  }

  int getDistinctSelectedSubjectsInPool({required OPool pool}) {
    var distinctSubjects = <int>[];

    if (lessons.isNotEmpty) {
      if (pool.subjects.isNotEmpty) {
        for (var subject in pool.subjects) {
          if (subject.lessons.isNotEmpty) {
            for (var subjectLesson in subject.lessons) {
              if (lessons.contains(subjectLesson.id)) {
                distinctSubjects.add(subject.id);
              }
            }
          }
        }
      }
    }
    return distinctSubjects.toSet().toList().length;
  }

  int getDistinctSelectedSubjectsInProgram({required OProgram program}) {
    var distinctSubjects = <int>[];

    if (lessons.isNotEmpty) {
      if (program.pools.isNotEmpty) {
        for (var pool in program.pools) {
          if (pool.subjects.isNotEmpty) {
            for (var subject in pool.subjects) {
              if (subject.lessons.isNotEmpty) {
                for (var subjectLesson in subject.lessons) {
                  if (lessons.contains(subjectLesson.id)) {
                    distinctSubjects.add(subject.id);
                  }
                }
              }
            }
          }
        }
      }
    }
    // remove duplicates, make a list, check the length
    return distinctSubjects.toSet().toList().length;
  }
}

class OLessonSelectionReadyState extends OLessonSelectionState {
  OLessonSelectionReadyState({required List<int> lessons})
      : super(lessons: lessons);
}
