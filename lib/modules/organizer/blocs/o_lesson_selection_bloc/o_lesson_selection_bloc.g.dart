// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_lesson_selection_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OLessonSelectionState _$OLessonSelectionStateFromJson(Map json) =>
    OLessonSelectionState(
      lessons: (json['lessons'] as List<dynamic>).map((e) => e as int).toList(),
    );

Map<String, dynamic> _$OLessonSelectionStateToJson(
        OLessonSelectionState instance) =>
    <String, dynamic>{
      'lessons': instance.lessons,
    };
