/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_view_mode_bloc.dart';

abstract class OViewModeState {
  int currentRelativeWeek = 0;
  CurrentOrganizerState currentState = CurrentOrganizerState.initialState;
  OViewModeState();
}

class ODailyViewState extends OViewModeState {
  ODailyViewState(this.weekday);
  final int weekday;
}

class OWeeklyViewState extends OViewModeState {
  OWeeklyViewState(this.weekday);
  final int weekday;
}
