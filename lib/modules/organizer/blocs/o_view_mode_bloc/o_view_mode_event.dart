/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_view_mode_bloc.dart';

abstract class OViewModeEvent {
  OViewModeState executeAction(OViewModeState state) =>
      OWeeklyViewState(1); // TODO NNBD MIGRATION
}

class OWeeklyViewEvent extends OViewModeEvent {
  @override
  OViewModeState executeAction(OViewModeState state) =>
      OWeeklyViewState(1); // TODO NNBD MIGRATION
}

class ODailyViewEvent extends OViewModeEvent {
  ODailyViewEvent(this.weekday);

  final int weekday;

  @override
  OViewModeState executeAction(OViewModeState state) =>
      ODailyViewState(weekday);
}

class OAddWeekEvent extends OViewModeEvent {
  @override
  OViewModeState executeAction(OViewModeState state) {
    state.currentRelativeWeek++;
    return state;
  }
}

class OSubtractWeekEvent extends OViewModeEvent {
  @override
  OViewModeState executeAction(OViewModeState state) {
    state.currentRelativeWeek--;
    return state;
  }
}

class OSetWeekEvent extends OViewModeEvent {
  OSetWeekEvent(this.relativeWeek);

  final int relativeWeek;

  @override
  OViewModeState executeAction(OViewModeState state) {
    state.currentRelativeWeek = relativeWeek;
    return state;
  }
}

class OSetState extends OViewModeEvent {
  OSetState(this.newState);

  final CurrentOrganizerState newState;

  @override
  OViewModeState executeAction(OViewModeState state) {
    state.currentState = newState;
    return state;
  }
}

class OToggleViewEvent extends OViewModeEvent {
  OToggleViewEvent(this.weekday);

  final int weekday;

  @override
  OViewModeState executeAction(OViewModeState state) {
    if (state is ODailyViewState) {
      return OWeeklyViewState(weekday);
    } else if (state is OWeeklyViewState) {
      return ODailyViewState(weekday);
    }
    return OWeeklyViewState(1); // TODO TEST NNBD MIGRATION
  }
}
