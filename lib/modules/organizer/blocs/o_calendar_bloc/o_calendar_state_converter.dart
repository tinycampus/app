/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_calendar_bloc.dart';

enum _OCalendarStateType { base, empty, ready }

class _OCalendarStateConverter
    extends OStateConverter<OCalendarState, _OCalendarStateType> {
  const _OCalendarStateConverter() : super();

  @override
  _OCalendarStateType fromTypeJsonValueInternal(dynamic jsonValue) =>
      _OCalendarStateType.values[jsonValue as int];

  @override
  int toTypeJsonValueInternal(OCalendarState object) {
    _OCalendarStateType type;

    switch (object.runtimeType) {
      case OCalendarEmptyState:
        type = _OCalendarStateType.empty;
        break;
      case OCalendarReadyState:
        type = _OCalendarStateType.ready;
        break;
      default:
        type = _OCalendarStateType.base;
    }

    return type.index;
  }

  @override
  OCalendarState fromJsonInternal(
      _OCalendarStateType type, Map<String, dynamic> json) {
    final base = OCalendarState.fromJson(json);

    switch (type) {
      case _OCalendarStateType.base:
        return base;
      case _OCalendarStateType.empty:
        return OCalendarEmptyState();
      case _OCalendarStateType.ready:
        return OCalendarReadyState(
          lastUpdated: base.lastUpdated,
          weekMap: base.weekMap,
          lessonIDs: base.lessonIDs,
        );
    }
  }

  @override
  Map<String, dynamic> toJsonInternal(OCalendarState object) => object.toJson();

  @override
  OCalendarState migrate(Map<String, dynamic> json) {
    final tmp = OCalendarState.fromJson(json);
    return OCalendarReadyState(
      lastUpdated: tmp.lastUpdated,
      weekMap: tmp.weekMap,
      lessonIDs: tmp.lessonIDs,
    );
  }
}
