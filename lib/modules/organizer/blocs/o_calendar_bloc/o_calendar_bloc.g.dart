// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_calendar_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OCalendarState _$OCalendarStateFromJson(Map json) => OCalendarState(
      lastUpdated: DateTime.parse(json['lastUpdated'] as String),
      weekMap: (json['weekMap'] as Map).map(
        (k, e) => MapEntry(int.parse(k as String),
            OWeek.fromJson(Map<String, dynamic>.from(e as Map))),
      ),
      lessonIDs:
          (json['lessonIDs'] as List<dynamic>).map((e) => e as int).toList(),
    );

Map<String, dynamic> _$OCalendarStateToJson(OCalendarState instance) =>
    <String, dynamic>{
      'lastUpdated': instance.lastUpdated.toIso8601String(),
      'weekMap':
          instance.weekMap.map((k, e) => MapEntry(k.toString(), e.toJson())),
      'lessonIDs': instance.lessonIDs,
    };
