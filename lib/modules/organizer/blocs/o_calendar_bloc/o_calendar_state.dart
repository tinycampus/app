/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_calendar_bloc.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OCalendarState extends Equatable {
  OCalendarState({
    required this.lastUpdated,
    required this.weekMap,
    required this.lessonIDs,
  });

  final DateTime lastUpdated;
  final Map<int, OWeek> weekMap;
  final List<int> lessonIDs;

  factory OCalendarState.fromJson(Map<String, dynamic> json) =>
      _$OCalendarStateFromJson(json);

  Map<String, dynamic> toJson() => _$OCalendarStateToJson(this);

  @override
  List<Object> get props => [
        lastUpdated,
        weekMap,
        lessonIDs,
      ];
}

class OCalendarEmptyState extends OCalendarState {
  OCalendarEmptyState()
      : super(
          lastUpdated: DateTime.fromMillisecondsSinceEpoch(0),
          weekMap: <int, OWeek>{},
          lessonIDs: <int>[],
        );
}

class OCalendarReadyState extends OCalendarState {
  OCalendarReadyState({
    required DateTime lastUpdated,
    required Map<int, OWeek> weekMap,
    required List<int> lessonIDs,
  }) : super(lastUpdated: lastUpdated, weekMap: weekMap, lessonIDs: lessonIDs);
}
