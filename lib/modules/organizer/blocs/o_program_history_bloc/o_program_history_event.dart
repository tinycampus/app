/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_program_history_bloc.dart';

abstract class OProgramHistoryEvent extends Equatable {
  const OProgramHistoryEvent();
}

class AddOProgramToHistoryEvent extends OProgramHistoryEvent {
  final OProgram oldProgram;
  final OProgram newerProgram;

  AddOProgramToHistoryEvent(this.oldProgram, this.newerProgram);

  @override
  List<Object> get props => [oldProgram, newerProgram];
}

class UserCheckedChangeEvent extends OProgramHistoryEvent {
  final String contextId;

  UserCheckedChangeEvent(this.contextId);

  @override
  List<Object> get props => [contextId];
}

class UserCheckedAllChangeForProgramEvent extends OProgramHistoryEvent {
  final String contextId;

  UserCheckedAllChangeForProgramEvent(this.contextId);

  @override
  List<Object> get props => [contextId];
}

class OProgramHistoryResetEvent extends OProgramHistoryEvent {
  const OProgramHistoryResetEvent();

  @override
  List<Object> get props => [];
}

class AddLastCheckedEvent extends OProgramHistoryEvent {
  @override
  List<Object> get props => [];
}

class ResetSpecificOProgramHistoryEvent extends OProgramHistoryEvent {
  final OProgram program;

  ResetSpecificOProgramHistoryEvent(this.program);

  @override
  List<Object> get props => [program];
}
