// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_color_mapping_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OColorMappingState _$OColorMappingStateFromJson(Map json) => OColorMappingState(
      previousColorList: (json['previousColorList'] as Map).map(
        (k, e) => MapEntry(int.parse(k as String), e as String),
      ),
    );

Map<String, dynamic> _$OColorMappingStateToJson(OColorMappingState instance) =>
    <String, dynamic>{
      'previousColorList':
          instance.previousColorList.map((k, e) => MapEntry(k.toString(), e)),
    };
