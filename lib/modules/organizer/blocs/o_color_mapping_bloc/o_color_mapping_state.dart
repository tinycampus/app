/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_color_mapping_bloc.dart';

@immutable
@JsonSerializable(anyMap: true, explicitToJson: true)
class OColorMappingState extends Equatable {
  OColorMappingState({
    required Map<int, String> previousColorList,
  }) : previousColorList = Map.from(previousColorList);

  final Map<int, String> previousColorList;

  @override
  List<Object> get props => [previousColorList];

  factory OColorMappingState.fromJson(Map<String, dynamic> json) =>
      _$OColorMappingStateFromJson(json);

  Map<String, dynamic> toJson() => _$OColorMappingStateToJson(this);
}

class OColorMappingReadyState extends OColorMappingState {
  OColorMappingReadyState(Map<int, String> previousColorList)
      : super(previousColorList: previousColorList);

  @override
  List<Object> get props => [previousColorList];

  Color getColor({int? subjectId}) {
    // if (previousColorList == null) return Colors.grey;
    if (subjectId == null) return Colors.grey;
    if (subjectId == 0) return Colors.grey;
    return (previousColorList.containsKey(subjectId)
        ? HexColor(previousColorList[subjectId] ?? "ff9e9e9e")
        : Colors.grey);
  }
}
