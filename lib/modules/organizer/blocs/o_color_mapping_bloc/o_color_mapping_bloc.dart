/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../bloc/local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../../utility/organizer_state_converter.dart';

part 'o_color_mapping_bloc.g.dart';
part 'o_color_mapping_event.dart';
part 'o_color_mapping_state.dart';
part 'o_color_mapping_state_converter.dart';

class OColorMappingBloc
    extends LocalUserHydBloc<OColorMappingEvent, OColorMappingState> {
  OColorMappingBloc() : super(OColorMappingReadyState(<int, String>{}));
  // @override
  // OColorMappingState get initialState => OColorMappingReadyState();

  @override
  Stream<OColorMappingState> mapEventToState(
    OColorMappingEvent event,
  ) async* {
    // TODO: implement mapEventToState
    yield OColorMappingReadyState(<int, String>{});
    if (event is MapMultiOSubjectToRandomColorEvent) {
      var listWithAddedRandomColors =
          _mapMultiOSubjectsToRandomColorAction(event);
      yield OColorMappingReadyState(
        listWithAddedRandomColors,
      );
    } else if (event is MapOSubjectToColorEvent) {
      var listWithAddedSubjectColor = _mapSingleOSubjectToColorAction(event);
      yield OColorMappingReadyState(
        listWithAddedSubjectColor,
      );
    } else if (event is OColorMappingResetEvent) {
      yield OColorMappingReadyState(
        <int, String>{},
      );
    }
  }

  Map<int, String> _mapSingleOSubjectToColorAction(
      MapOSubjectToColorEvent event) {
    var previousColorList = Map<int, String>.from(event.previousColorList);
    previousColorList[event.subject] = event.color;
    return previousColorList;
  }

  Map<int, String> _mapMultiOSubjectsToRandomColorAction(
      MapMultiOSubjectToRandomColorEvent event) {
    var previousColorList = Map<int, String>.from(event.previousColorList);

    for (var subject in event.subjects) {
      if (previousColorList[subject] == null) {
        var randomColor =
            Color((Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.5);
        previousColorList[subject] = randomColor.value.toRadixString(16);
      }
    }
    return previousColorList;
    // MapMultiOSubjectToRandomColorEvent
  }

  @override
  OColorMappingState fromJson(Map<String, dynamic> json) =>
      _OColorMappingStateConverter().fromJson(json);

  @override
  Map<String, dynamic> toJson(OColorMappingState state) =>
      _OColorMappingStateConverter().toJson(state);
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
