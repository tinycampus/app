/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_program_configuration_bloc.dart';

enum _OProgramConfigurationStateType { base, empty, ready }

class _OProgramConfigurationStateConverter extends OStateConverter<
    OProgramConfigurationState, _OProgramConfigurationStateType> {
  const _OProgramConfigurationStateConverter() : super();

  @override
  _OProgramConfigurationStateType fromTypeJsonValueInternal(
          dynamic jsonValue) =>
      _OProgramConfigurationStateType.values[jsonValue as int];

  @override
  int toTypeJsonValueInternal(OProgramConfigurationState object) {
    _OProgramConfigurationStateType type;

    switch (object.runtimeType) {
      case OProgramConfigurationReadyState:
        type = _OProgramConfigurationStateType.ready;
        break;
      case OProgramConfigurationEmptyState:
        type = _OProgramConfigurationStateType.empty;
        break;
      default:
        type = _OProgramConfigurationStateType.base;
    }

    return type.index;
  }

  @override
  OProgramConfigurationState fromJsonInternal(
      _OProgramConfigurationStateType type, Map<String, dynamic> json) {
    final base = OProgramConfigurationState.fromJson(json);

    switch (type) {
      case _OProgramConfigurationStateType.base:
        return base;
      case _OProgramConfigurationStateType.empty:
        return OProgramConfigurationEmptyState();
      case _OProgramConfigurationStateType.ready:
        return OProgramConfigurationReadyState(programs: base.programs);
    }
  }

  @override
  Map<String, dynamic> toJsonInternal(OProgramConfigurationState object) =>
      object.toJson();

  @override
  OProgramConfigurationState migrate(Map<String, dynamic> json) {
    final tmp = OProgramConfigurationState.fromJson(json);
    return OProgramConfigurationReadyState(programs: tmp.programs);
  }
}
