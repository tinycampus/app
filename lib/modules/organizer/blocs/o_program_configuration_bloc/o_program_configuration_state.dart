/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_program_configuration_bloc.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OProgramConfigurationState extends Equatable {
  const OProgramConfigurationState({List<OProgram>? programs})
      : programs = programs ?? const <OProgram>[];

  final List<OProgram> programs;

  factory OProgramConfigurationState.fromJson(Map<String, dynamic> json) =>
      _$OProgramConfigurationStateFromJson(json);

  Map<String, dynamic> toJson() => _$OProgramConfigurationStateToJson(this);

  @override
  List<Object> get props => [programs];
}

class OProgramConfigurationEmptyState extends OProgramConfigurationState {
  OProgramConfigurationEmptyState();

  @override
  List<Object> get props => [programs];
}

class OProgramConfigurationReadyState extends OProgramConfigurationState {
  OProgramConfigurationReadyState({List<OProgram>? programs})
      : super(programs: programs);

  @override
  List<Object> get props => [programs];
}
