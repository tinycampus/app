/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';

// TODO: Cleanup
class OCommonFunctions {
  static int forceInt(dynamic toIntValue) {
    if (toIntValue is String) {
      return int.parse(toIntValue);
    }
    if (toIntValue is int) {
      return toIntValue;
    } else {
      debugPrint("[SEVERE ERROR ORGANIZER DATA MANAGER]"
          " Could not determine an int value");
      return 0;
    }
  }

  static String forceString(dynamic toStringValue) {
    if (toStringValue is String) {
      return toStringValue;
    }
    if (toStringValue is int) {
      return toStringValue.toString();
    } else {
      debugPrint("[SEVERE ERROR ORGANIZER DATA MANAGER]"
          " Could not determine a string value");
      return "0";
    }
  }
}
