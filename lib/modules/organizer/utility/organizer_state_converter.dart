/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

/// Extend this class to provide custom converters for a specific [Type].
///
/// [T] is the bloc state you'd like to convert to and from.
///
/// [E] is the data type you'd like to use as json representation of [T]'s
/// different possible state classes.
abstract class OStateConverter<T, E>
    implements JsonConverter<T, Map<String, dynamic>> {
  const OStateConverter();

  @override
  T fromJson(Map<String, dynamic> json) {
    if (json.containsKey("type")) {
      final type = fromTypeJsonValueInternal(json["type"]);

      final result = fromJsonInternal(type, json["state"]);

      if (result == null) {
        throw FormatException();
      } else {
        return result;
      }
    } else {
      return migrate(json);
    }
  }

  @override
  Map<String, dynamic> toJson(T object) => {
        "type": toTypeJsonValueInternal(object),
        "state": toJsonInternal(object),
      };

  E fromTypeJsonValueInternal(dynamic jsonValue);

  dynamic toTypeJsonValueInternal(T object);

  T fromJsonInternal(E type, Map<String, dynamic> json);

  Map<String, dynamic> toJsonInternal(T object);

  T migrate(Map<String, dynamic> json);
}
