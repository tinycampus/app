/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

import '../../../common/constants/api_constants.dart';
import '../../../common/tc_utility_functions.dart';
import '../../../repositories/http/http_repository.dart';
import '../model/o_element.dart';
import '../model/o_resource.dart';
import '../model/o_room.dart';
import 'organizer_common_functions.dart';

class OrganizerInstanceFunctions {
  static Map<String, List<OElement>> cachedAffectedOElements =
      <String, List<OElement>>{};

  static Future<List<OElement>> getOrganizerInstances(
    List<int>? lessonIDs,
    DateTime? date,
    HttpRepository _httpRepo,
  ) async {
    if (lessonIDs == null) {
      return <OElement>[];
    }
    final lessonIDArray = (StringBuffer()..writeAll(lessonIDs, ",")).toString();
    debugPrint("[LOAD INSTANCES BY LESSON]"
        "Requesting INSTANCES from Organizer: $lessonIDArray");

    final requestParam = date != null
        ? 'date=${DateFormat('yyyy-MM-dd').format(date)}'
        : 'interval=term';

    final url = "$thmUrl/organizer/?option=com_organizer&view=instances&"
        "format=json&$requestParam&unitIDs=$lessonIDArray";
    debugPrint(url);

    if (cachedAffectedOElements[url] != null) {
      return cachedAffectedOElements[url]!;
    }

    final response = jsonDecode(await _httpRepo.read(url));
    debugPrint(response.runtimeType.toString());

    List<OElement> organizerElements;
    if (response is List) {
      organizerElements = parseInstances(response);
      if (date != null) {
        cachedAffectedOElements[url] = organizerElements;
      }
      return organizerElements;
    } else if (response is Map<String, dynamic>) {
      return getPastFutureInfo(response);
    } else {
      return []; // no entries found!
    }
  }

  static List<OElement> parseInstances(List<dynamic> list) {
    var organizerElements = <OElement>[];
    for (Map<String, dynamic> a in list) {
      var oElement = OElement();
      // NT = New terminology that was introduced in 2020
      oElement.id = OCommonFunctions.forceInt(a["instanceID"]);
      oElement.name = OCommonFunctions.forceString(a["name"]);
      oElement.fullname = OCommonFunctions.forceString(a["fullName"]);
      if (oElement.fullname == "") {
        oElement.fullname = OCommonFunctions.forceString(a["name"]);
      }
      oElement.comment = a["comment"];
      oElement.subjectId =
          OCommonFunctions.forceInt(a["eventID"]); // NT: event == subject
      oElement.lessonId =
          OCommonFunctions.forceInt(a["unitID"]); // NT: unit == lesson
      oElement.type = a["methodCode"] ?? "?"; // NT: method == type
      oElement.typeLong = a["method"] ?? "?";
      oElement.organization = OCommonFunctions.forceString(a["organization"]);
      oElement.organizationId = OCommonFunctions.forceInt(a["organizationID"]);
      oElement.abbr = a["code"]; // NT: code == abbr
      // OElements need to have an abbreviation, so let's try to give it
      // an abbr, even if it is not provided.
      if (oElement.abbr == "") {
        oElement.abbr = a["subjectNo"];
      }
      if (oElement.abbr == "") {
        oElement.abbr = a["name"];
      }
      oElement.date = DateTime.parse(a["date"]);
      var tempDate = DateTime.parse(a["date"]);
      oElement.start = tempDate.add(Duration(
        hours: int.parse((a["startTime"] as String?)?.substring(0, 2) ?? ""),
        minutes: int.parse((a["startTime"] as String?)?.substring(3, 5) ?? ""),
      ));
      oElement.end = tempDate.add(Duration(
        hours: int.parse((a["endTime"] as String?)?.substring(0, 2) ?? ""),
        minutes: int.parse((a["endTime"] as String?)?.substring(3, 5) ?? ""),
      ));

      oElement.resource = getResource(a["resources"]);
      organizerElements.add(oElement);
    }
    return organizerElements;
  }

  static List<OResource> getResource(Map<String, dynamic>? json) =>
      buildEntitiesFromMap(
        json,
        (e) => OResource(
          resourceID: OCommonFunctions.forceInt(e.key),
          code: e.value["code"],
          person: e.value["person"],
          role: e.value["role"],
          roleID: OCommonFunctions.forceInt(e.value["roleID"]),
          rooms: getRooms(e.value["rooms"]),
        ),
      );

  static List<ORoom> getRooms(Map<String, dynamic>? input) =>
      buildEntitiesFromMap(
        input,
        (e) => ORoom(
          id: OCommonFunctions.forceInt(e.key),
          name: e.value["room"],
        ),
      );

  static OElement generatePastFutureOElementPlaceholder({
    DateTime? past,
    DateTime? future,
  }) =>
      OElement(start: past, end: future, type: "NODATA");

  static List<OElement> getPastFutureInfo(Map<String, dynamic> json) {
    final jsonPastDate = json["pastDate"];
    debugPrint(jsonPastDate);

    final jsonFutureDate = json["futureDate"];
    debugPrint(jsonFutureDate);

    if (jsonPastDate == null && jsonFutureDate == null) {
      return [];
    }

    final past = DateTime.tryParse(jsonPastDate);
    final future = DateTime.tryParse(jsonFutureDate);

    return [
      generatePastFutureOElementPlaceholder(past: past, future: future),
    ];
  }
}
