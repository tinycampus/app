// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_department.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ODepartment _$ODepartmentFromJson(Map<String, dynamic> json) => ODepartment(
      id: OCommonFunctions.forceInt(json['id']),
      name: json['name'] as String?,
    );

Map<String, dynamic> _$ODepartmentToJson(ODepartment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
