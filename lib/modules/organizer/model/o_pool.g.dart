// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_pool.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OPool _$OPoolFromJson(Map json) => OPool(
      name: json['name'] as String? ?? "",
      id: json['id'] as int? ?? -1,
      subjects: (json['subjects'] as List<dynamic>?)
          ?.map((e) => OSubject.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$OPoolToJson(OPool instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'subjects': instance.subjects.map((e) => e.toJson()).toList(),
    };
