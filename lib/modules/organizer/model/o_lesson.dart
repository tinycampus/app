/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import '../utility/organizer_common_functions.dart';
import 'o_change_record.dart';

part 'o_lesson.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OLesson {
  OLesson({
    this.name,
    this.id,
    this.comment,
    this.subjectID,
    this.method,
    this.subjectName,
    this.fullname,
    this.enabled,
  });

  String? name;

  @JsonKey(fromJson: OCommonFunctions.forceInt)
  int? id;
  String? comment;

  @JsonKey(fromJson: OCommonFunctions.forceInt)
  int? subjectID;
  String? method;
  String? subjectName;
  String? fullname;
  bool? enabled;

  factory OLesson.fromJson(Map<String, dynamic> json) =>
      _$OLessonFromJson(json);

  Map<String, dynamic> toJson() => _$OLessonToJson(this);

  /// [other] is regarded as the most recent compared object
  OChangeRecord? identifyChanges(
      OLesson? other, List<OLesson>? siblings, List<OLesson>? otherSiblings) {
    var record = OChangeRecord();
    record.type = runtimeType.toString();
    record.oldRef = shallowCopy().toJson();
    record.type = runtimeType.toString();
    record.userChecked = false;
    record.contextId = toContextString();
    if (other == null) {
      record.newRef = shallowCopy().toJson();
      record.delta.add(ODelta.removed);
      // return null;
      return record;
    }
    record.newRef = other.shallowCopy().toJson();
    // check if name is the same
    if (name != other.name || fullname != other.fullname) {
      record.delta.add(ODelta.nameChanged);
    }
    if (comment != other.comment) {
      record.delta.add(ODelta.descriptionChanged);
    }
    // check if it was added
    if (siblings != null && otherSiblings != null) {
      var list = siblings.map((e) => e.id).toList();
      var otherList = otherSiblings.map((e) => e.id).toList();
      if (!list.contains(other.id)) {
        record.delta.add(ODelta.added);
      }
      if (!otherList.contains(id)) {
        record.delta.add(ODelta.removed);
      }
    }
    if (siblings == null && otherSiblings != null) {
      record.delta.add(ODelta.added);
    }
    // check if there are changes
    if (record.delta.isEmpty) {
      return null;
    }
    return record;
  }

  static OChangeRecord wasAdded(OLesson a) {
    // here, b is the "older reference", since we need to track backwards
    var record = OChangeRecord();
    record.contextId = a.toContextString();
    record.type = a.runtimeType.toString();
    record.oldRef = null;
    record.newRef = a.toJson();
    record.userChecked = false;
    record.delta = <ODelta>[];
    record.delta.add(ODelta.added);
    return record;
  }

  OLesson shallowCopy() => OLesson(
      id: id, name: name, comment: comment, fullname: fullname, method: method);

  String toContextString() => id.toString();
}
