// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_resource.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OResource _$OResourceFromJson(Map json) => OResource(
      resourceID: json['resourceID'] == null
          ? -1
          : OCommonFunctions.forceInt(json['resourceID']),
      code: json['code'] as String? ?? "",
      person: json['person'] as String? ?? "",
      role: json['role'] as String? ?? "",
      roleID: json['roleID'] == null
          ? -1
          : OCommonFunctions.forceInt(json['roleID']),
      rooms: (json['rooms'] as List<dynamic>?)
          ?.map((e) => ORoom.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$OResourceToJson(OResource instance) => <String, dynamic>{
      'resourceID': instance.resourceID,
      'code': instance.code,
      'person': instance.person,
      'role': instance.role,
      'roleID': instance.roleID,
      'rooms': instance.rooms.map((e) => e.toJson()).toList(),
    };
