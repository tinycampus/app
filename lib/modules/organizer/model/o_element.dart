/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import '../utility/organizer_common_functions.dart';
import 'o_resource.dart';

part 'o_element.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OElement {
  OElement({
    this.id,
    this.comment,
    this.date,
    this.start,
    this.end,
    this.subjectId,
    this.subjectNo,
    this.lessonId,
    this.abbr,
    this.name,
    this.fullname,
    this.type = "",
    this.typeLong = "",
    this.cyclic,
    List<OResource>? resources,
  }) : resource = resources ?? <OResource>[];

  int? id;
  String? comment;
  DateTime? date;
  DateTime? start;
  DateTime? end;
  int? subjectId;
  int? lessonId;
  String? subjectNo;
  String? abbr;
  String? fullname;
  String? name;
  String type;
  String typeLong;
  String? organization;

  @JsonKey(fromJson: OCommonFunctions.forceInt)
  int? organizationId;

  bool? cyclic;
  List<OResource> resource;

  factory OElement.fromJson(Map<String, dynamic> json) =>
      _$OElementFromJson(json);

  Map<String, dynamic> toJson() => _$OElementToJson(this);
}
