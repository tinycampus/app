/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../common/tc_theme.dart';
import 'bloc/category/category_bloc.dart';
import 'bloc/question/question_bloc.dart';
import 'widgets/add_question_widget.dart';
import 'widgets/question_widget.dart';

class QAndAQuestionScreenArguments {
  QAndAQuestionScreenArguments({
    this.showCategory = true,
    this.openAskQuestion = false,
    this.initialQuestionText = '',
  });

  final bool openAskQuestion;
  final bool showCategory;
  final String initialQuestionText;
}

///Screen which displays all of the Questions
class QAndAQuestionScreen extends StatelessWidget {
  final bool showCategory;
  final bool openAskQuestion;
  final String initialQuestionText;

  QAndAQuestionScreen({
    Key? key,
    this.showCategory = true,
    this.openAskQuestion = false,
    this.initialQuestionText = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _currentCat = BlocProvider.of<CategoryBloc>(context).state.current;
    BlocProvider.of<QuestionBloc>(context).add(
      LoadQuestionsEvent(_currentCat.id),
    );
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text(_currentCat.title),
      ),
      body: RefreshIndicator(
        strokeWidth: 3,
        color: CorporateColors.tinyCampusOrange,
        onRefresh: () async {
          BlocProvider.of<QuestionBloc>(context)
              .add(LoadQuestionsEvent(_currentCat.id));
        },
        child: BlocBuilder<QuestionBloc, QuestionState>(
          builder: (context, state) => ListView.builder(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: (state.allQuestions.length) + 1,
            itemBuilder: (context, index) {
              if (index == 0) {
                return AddQuestionWidget(
                  category: _currentCat,
                  openAskQuestion: openAskQuestion,
                  initialText: initialQuestionText,
                );
              }

              final _question = state.allQuestions[index - 1];
              return Padding(
                padding: const EdgeInsets.only(top: 12),
                child: QuestionWidget(
                  _question,
                  key: ValueKey(_question.id),
                  canNavigate: true,
                  showCategory: showCategory,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
