// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'question_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension LoadingQuestionsStateCopyWith on LoadingQuestionsState {
  LoadingQuestionsState copyWith({
    List<Question>? allQuestions,
    Question? currentQuestion,
  }) {
    return LoadingQuestionsState(
      allQuestions: allQuestions ?? this.allQuestions,
      currentQuestion: currentQuestion ?? this.currentQuestion,
    );
  }
}

extension QuestionsLoadedStateCopyWith on QuestionsLoadedState {
  QuestionsLoadedState copyWith({
    List<Question>? allQuestions,
    Question? currentQuestion,
  }) {
    return QuestionsLoadedState(
      allQuestions: allQuestions ?? this.allQuestions,
      currentQuestion: currentQuestion ?? this.currentQuestion,
    );
  }
}

extension CurrentQuestionLoadedStateCopyWith on CurrentQuestionLoadedState {
  CurrentQuestionLoadedState copyWith({
    Question? currentQuestion,
  }) {
    return CurrentQuestionLoadedState(
      currentQuestion: currentQuestion ?? this.currentQuestion,
    );
  }
}

extension QuestionToggledStateCopyWith on QuestionToggledState {
  QuestionToggledState copyWith({
    List<Question>? allQuestions,
    Question? currentQuestion,
  }) {
    return QuestionToggledState(
      allQuestions: allQuestions ?? this.allQuestions,
      currentQuestion: currentQuestion ?? this.currentQuestion,
    );
  }
}
