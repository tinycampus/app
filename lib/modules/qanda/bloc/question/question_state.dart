/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'question_bloc.dart';

///Abstract class which defines the basis of any Question State.
///Some getters are included to filter all questions based on whether the
///User is the Author and to find a single question by it's ID.
@immutable
abstract class QuestionState extends Equatable {
  final List<Question> allQuestions;
  final Question currentQuestion;
  List<Question> get myQuestions =>
      allQuestions.where((element) => element.canEdit == true).toList();

  Question? questionById(int id) =>
      allQuestions.firstWhereOrNull((q) => q.id == id);

  QuestionState({
    this.allQuestions = const [],
    Question? currentQuestion,
  }) : currentQuestion = currentQuestion ?? Question.placeholder();

  @override
  List<Object> get props => [allQuestions, currentQuestion];
}

///State which represents the absence of Questions
class EmptyQuestionState extends QuestionState {}

///State which represents that the loading of Questions has concluded
@CopyWith()
class LoadingQuestionsState extends QuestionState {
  LoadingQuestionsState({
    required List<Question> allQuestions,
    Question? currentQuestion,
  }) : super(allQuestions: allQuestions, currentQuestion: currentQuestion);
}

@CopyWith()
class QuestionsLoadedState extends QuestionState {
  QuestionsLoadedState(
      {required List<Question> allQuestions, Question? currentQuestion})
      : super(allQuestions: allQuestions, currentQuestion: currentQuestion);
}

@CopyWith()
class CurrentQuestionLoadedState extends QuestionState {
  CurrentQuestionLoadedState({required Question currentQuestion})
      : super(currentQuestion: currentQuestion);
}

@CopyWith()
class QuestionToggledState extends QuestionsLoadedState {
  QuestionToggledState(
      {required List<Question> allQuestions, Question? currentQuestion})
      : super(allQuestions: allQuestions, currentQuestion: currentQuestion);
}
