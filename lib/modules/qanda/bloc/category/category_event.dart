/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'category_bloc.dart';

@immutable
abstract class CategoryEvent {
  const CategoryEvent();

  Future<void> _performAction(CategoryBloc bloc);

  CategoryState _nextState(CategoryBloc bloc);
}

class LoadingCompleteEvent extends CategoryEvent {
  final CategoryState nextState;

  LoadingCompleteEvent(this.nextState);

  @override
  CategoryState _nextState(CategoryBloc bloc) => nextState;

  @override
  Future<void> _performAction(CategoryBloc bloc) async {}
}

class LoadCategoriesEvent extends CategoryEvent {
  @override
  Future<void> _performAction(CategoryBloc bloc) async {
    try {
      var _httpRepo = HttpRepository();
      var newReplies = 0;
      var categories = <Category>[];
      Map<String, dynamic> response =
          jsonDecode(await _httpRepo.read(api.questionsCategoriesOverviewUrl));
      newReplies = response["newReplies"] ?? 0;
      for (var c in response["categories"]) {
        categories.add(Category.fromJson(c));
      }
      bloc.add(
        LoadingCompleteEvent(
          CategoriesLoadedState(categories, bloc.state.current, newReplies),
        ),
      );
    } on Exception catch (e) {
      debugPrint("EXCEPTION CAUGHT IN LOAD CATEGORIES $e");
      bloc.add(LoadingCompleteEvent(bloc.state));
    }
  }

  @override
  CategoryState _nextState(CategoryBloc bloc) => LoadingCategoriesState(
        bloc.state.all,
        bloc.state.current,
        bloc.state.newReplies,
      );
}

class SubscribeCategoryEvent extends CategoryEvent {
  final int categoryID;

  SubscribeCategoryEvent({required this.categoryID});

  @override
  Future<void> _performAction(CategoryBloc bloc) async {
    try {
      var _httpRepo = HttpRepository();
      await _httpRepo.post("${api.questionsCategoriesUrl}/$categoryID");
      bloc.add(LoadCategoriesEvent());
    } on Exception catch (e) {
      debugPrint("EXCEPTION CAUGHT IN SUBSCRIBE CATEGORY $e");
    }
  }

  @override
  CategoryState _nextState(CategoryBloc bloc) => LoadingCategoriesState(
        bloc.state.all,
        bloc.state.current,
        bloc.state.newReplies,
      );
}

class UnsubscribeCategoryEvent extends CategoryEvent {
  final int categoryID;

  UnsubscribeCategoryEvent({required this.categoryID});

  @override
  Future<void> _performAction(CategoryBloc bloc) async {
    try {
      var _httpRepo = HttpRepository();
      await _httpRepo.delete("${api.questionsCategoriesUrl}/$categoryID");
      bloc.add(LoadCategoriesEvent());
    } on Exception catch (e) {
      debugPrint("EXPCETION CAUGHT IN UNSUBCRIBE CATEGORY $e");
    }
  }

  @override
  CategoryState _nextState(CategoryBloc bloc) => LoadingCategoriesState(
        bloc.state.all,
        bloc.state.current,
        bloc.state.newReplies,
      );
}

class ChangeCurrentCategoryEvent extends CategoryEvent {
  final Category current;

  ChangeCurrentCategoryEvent({required this.current});

  @override
  Future<void> _performAction(CategoryBloc bloc) async {
    bloc.add(LoadCategoriesEvent());
  }

  @override
  CategoryState _nextState(CategoryBloc bloc) => ChangeCurrentCategoryState(
        bloc.state.all,
        current,
        bloc.state.newReplies,
      );
}

class CategoryResetEvent extends CategoryEvent {
  const CategoryResetEvent();

  @override
  CategoryState _nextState(CategoryBloc bloc) => EmptyCategoryState();

  @override
  Future<void> _performAction(CategoryBloc bloc) async {}
}
