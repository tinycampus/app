/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'category_bloc.dart';

@immutable
class CategoryState {
  final Category current;
  final List<Category> all;
  final int newReplies;

  @mustCallSuper
  CategoryState({
    required this.current,
    required this.all,
    required this.newReplies,
  });

  List<Category> get subscribed =>
      all.where((element) => element.subscribed).toList();

  List<Category> get notSubscribed =>
      all.where((element) => !element.subscribed).toList();
}

class EmptyCategoryState extends CategoryState {
  EmptyCategoryState()
      : super(
          all: [],
          current: Category.placeholder(),
          newReplies: 0,
        );
}

class CategoriesLoadedState extends CategoryState {
  CategoriesLoadedState(List<Category> all, Category current, int newReplies)
      : super(all: all, current: current, newReplies: newReplies);
}

class ChangeCurrentCategoryState extends CategoryState {
  ChangeCurrentCategoryState(
      List<Category> all, Category current, int newReplies)
      : super(all: all, current: current, newReplies: newReplies);
}

class LoadingCategoriesState extends CategoryState {
  LoadingCategoriesState(List<Category> all, Category current, int newReplies)
      : super(all: all, current: current, newReplies: newReplies);

  final Widget loadingIndicator = Container(
    alignment: Alignment.center,
    padding: EdgeInsets.only(top: 20.0),
    child: Center(
      child: SizedBox(
        width: 33,
        height: 33,
        child: CircularProgressIndicator(),
      ),
    ),
  );
}
