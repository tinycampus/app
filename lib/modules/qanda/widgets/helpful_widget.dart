/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../../../common/tinycampus_icons.dart';
import '../bloc/answer/answer_bloc.dart';
import '../model/answer.dart';

///Widget which displays whether an answer is helpful. If the user is the author
///of the question it also displays the possibility to mark something as helpful
class HelpfulWidget extends StatelessWidget {
  final Answer answer;
  final bool canMarkAsHelpful;

  HelpfulWidget({
    Key? key,
    required this.answer,
    required this.canMarkAsHelpful,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => (!canMarkAsHelpful && !answer.isHelpful)
      ? Container()
      : Container(
          width: 42,
          margin: EdgeInsets.only(right: 16.0),
          child: IconButton(
            tooltip: FlutterI18n.translate(
                context,
                answer.isHelpful
                    ? 'modules.q_and_a.helpful.marked_helpful'
                    : 'modules.q_and_a.helpful.mark_as_helpful'),
            onPressed: canMarkAsHelpful
                ? answer.isHelpful
                    ? () {
                        BlocProvider.of<AnswerBloc>(context).add(
                          MarkAnswerNotHelpfulEvent(answer.id),
                        );
                      }
                    : () {
                        BlocProvider.of<AnswerBloc>(context).add(
                          MarkAnswerHelpfulEvent(answer.id),
                        );
                      }
                : null,
            icon: TweenAnimationBuilder<double>(
              duration: Duration(milliseconds: 800),
              curve: Curves.elasticOut,
              tween:
                  Tween<double>(begin: 0.0, end: answer.isHelpful ? 1.55 : 1.0),
              builder: (context, value, child) => Transform.scale(
                scale: value,
                child: Icon(
                  answer.isHelpful
                      ? TinyCampusIcons.star_100p
                      : TinyCampusIcons.star_0p,
                  color: answer.isHelpful
                      ? CorporateColors.tinyCampusOrange
                      : CorporateColors.tinyCampusIconGrey,
                ),
              ),
            ),
          ),
        );
}
