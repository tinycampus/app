/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../../../common/tinycampus_icons.dart';
import '../../../common/widgets/dialog/tc_dialog.dart';
import '../bloc/category/category_bloc.dart';
import '../model/category.dart';

///Header Widget which routes to the Category selection screen
class StartScreenCategoryWidget extends StatelessWidget {
  final Category _category;
  StartScreenCategoryWidget(
    this._category, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var onlyMain = _category.subcategories.isEmpty;
    var subcategory = onlyMain
        ? _category
        : _category.subcategories.firstWhere((element) => element.subscribed);

    return Card(
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0))),
      elevation: 1.0,
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: () {
          var currentCategory = subcategory;
          BlocProvider.of<CategoryBloc>(context)
              .add(ChangeCurrentCategoryEvent(current: currentCategory));
          Navigator.pushNamed(context, qaAllQuestions);
        },
        onLongPress: () {
          // TCDialog.showCustomDialog();
          TCDialog.showCustomDialog(
            context: context,
            onConfirm: () => _removeCategory(context, subcategory),
            headlineText: FlutterI18n.translate(
                context, 'modules.q_and_a.unsubscribe_category'),
            bodyText: FlutterI18n.translate(
                context, 'modules.q_and_a.unsubscribe_text',
                translationParams: <String, String>{
                  'module': subcategory.title
                }),
            functionActionText: FlutterI18n.translate(
              context,
              'common.actions.unsubscribe',
            ),
            functionActionColor: CorporateColors.cafeteriaCautionRed,
          );
          //   content: I18nText(
          //   'modules.q_and_a.unsubscribe_text',
          //   translationParams: <String, String>{'module':
          //   '${category.title}'},
          // ),
          // _showDeleteDialog(subcategory, context);
        },
        // TODO: This is costly, but sufficiant for now. find a better solution
        child: IntrinsicHeight(
          child: Row(
            children: <Widget>[
              Container(
                width: 110,
                constraints:
                    BoxConstraints(minHeight: 140, maxHeight: double.infinity),
                margin: EdgeInsets.only(right: 10),
                color: CorporateColors.passiveBackgroundLightBlue,
                child: subcategory.logo.isNotEmpty
                    ? Image.asset(
                        // TODO: Change to right path when clear which path
                        _category.logo,
                        fit: BoxFit.fill,
                      )
                    : Center(
                        child: Text(
                          subcategory.title
                              .replaceAll(" ", "")
                              .substring(0, min(subcategory.title.length, 3))
                              .toUpperCase(),
                          style: TextStyle(
                            color:
                                CorporateColors.tinyCampusBlue.withOpacity(0.2),
                            fontWeight: FontWeight.w400,
                            fontSize: 36,
                            letterSpacing: -1.0,
                          ),
                        ),
                      ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 8.0),
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            _category.title,
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                                ?.copyWith(
                                    fontSize: onlyMain ? 22 : 16,
                                    color: onlyMain
                                        ? CorporateColors.tinyCampusBlue
                                        : CorporateColors.tinyCampusIconGrey),
                          ),
                          !onlyMain
                              ? Text(
                                  subcategory.title,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline2
                                      ?.copyWith(
                                          fontSize: onlyMain ? 16 : 22,
                                          color: onlyMain
                                              ? CorporateColors
                                                  .tinyCampusIconGrey
                                              : CorporateColors.tinyCampusBlue),
                                )
                              : Container(),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            TinyCampusIcons.twentyfourHours,
                            size: 26,
                            color: CorporateColors.tinyCampusIconGrey
                                .withOpacity(0.6),
                          ),
                          subcategory.countActivities > 0
                              ? Container(
                                  margin:
                                      const EdgeInsets.only(right: 4, left: 12),
                                  child: Text(
                                    "+${subcategory.countActivities}",
                                    style: TextStyle(
                                      color: CorporateColors
                                          .qAndAActivityTextGreen,
                                      fontSize: 20,
                                    ),
                                  ),
                                )
                              : Container(
                                  margin: const EdgeInsets.only(left: 12),
                                ),
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: I18nPlural(
                                'modules.q_and_a.activity.times',
                                subcategory.countActivities,
                                child: Text(
                                  '',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      ?.copyWith(
                                          fontSize: 20,
                                          color: subcategory.countActivities > 0
                                              ? CorporateColors
                                                  .tinyCampusTextSoft
                                              : CorporateColors
                                                  .tinyCampusIconGrey
                                                  .withOpacity(0.5)),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _removeCategory(BuildContext context, Category category) {
    BlocProvider.of<CategoryBloc>(context)
        .add(UnsubscribeCategoryEvent(categoryID: category.id));
  }
}
