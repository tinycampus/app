/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../../../common/tinycampus_icons.dart';
import '../../../common/widgets/dialog/tc_dialog.dart';
import '../../../common/widgets/profile/svg_profile_widget.dart';
import '../../../common/widgets/reporting/popup_report_button.dart';
import '../../../common/widgets/reporting/reported_blur_wrapper.dart';
import '../../../common/widgets/tc_link_text.dart';
import '../../../reward/widgets/heart_widget.dart';
import '../api_helper.dart';
import '../bloc/category/category_bloc.dart';
import '../bloc/edit/edit_bloc.dart';
import '../bloc/question/question_bloc.dart';
import '../model/question.dart';
import 'edit_question_widget.dart';
import 'flair_widget.dart';
import 'helpful_answer_display.dart';

///Widget which displays a question
class QuestionWidget extends StatelessWidget {
  final Question? _question;
  final bool canNavigate;
  final bool showCategory;
  final bool showAnswers;
  final bool showViews;
  final bool showAuthor;
  final bool showHeart;
  final bool showWholeQuestion;
  final bool highlightAuthor;
  final int index;

  const QuestionWidget(
    this._question, {
    this.canNavigate = false,
    this.showCategory = true,
    this.showAuthor = true,
    this.showAnswers = true,
    this.showViews = true,
    this.showWholeQuestion = false,
    this.highlightAuthor = false,
    this.showHeart = true,
    this.index = 0,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final question = _question;
    final textController = TextEditingController(text: _question?.body);
    return BlocBuilder<EditBloc, EditState>(
      builder: (context, state) => ReportedBlurWrapper(
        shouldBlur: question?.isReported ?? false,
        child: question == null
            ? Container()
            : !(state.editing[index] ?? false)
                ? GestureDetector(
                    child: Material(
                      child: Card(
                        margin: EdgeInsets.zero,
                        shape: Border(),
                        elevation: 0.0,
                        child: InkWell(
                          onTap: canNavigate
                              ? () {
                                  BlocProvider.of<QuestionBloc>(context).add(
                                      UpdateSingleQuestionEvent(question.id));
                                  Navigator.of(context).pushNamed(
                                    qaViewQuestionRoute,
                                    arguments: question.id,
                                  );
                                }
                              : null,
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                buildQuestionHeader(context, question),
                                buildQuestionBody(context, question),
                                if (canNavigate &&
                                    question.helpfulAnswer != null)
                                  buildHelpfulAnswer(question),
                                buildQuestionFooter(context, question),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : EditQuestionWidget(
                    closeCallback: () {
                      BlocProvider.of<EditBloc>(context)
                          .add(CloseEditEvent(index));
                    },
                    textController: textController,
                    confirmCallback: () {
                      if (textController.text.trim().isNotEmpty) {
                        BlocProvider.of<QuestionBloc>(context).add(
                          EditQuestionEvent(
                              textController.text.trim(),
                              question.id,
                              BlocProvider.of<CategoryBloc>(context)
                                  .state
                                  .current
                                  .id),
                        );
                        BlocProvider.of<EditBloc>(context)
                            .add(CloseEditEvent(index));
                      }
                    },
                  ),
      ),
    );
  }

  Padding buildHelpfulAnswer(Question question) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: InlineAnswerDisplay(
          question.helpfulAnswer,
        ),
      );

  Padding buildQuestionBody(BuildContext context, Question question) => Padding(
        padding: EdgeInsets.only(
          top: canNavigate || question.author.flairs.isEmpty ? 2.0 : 6.0,
          bottom: canNavigate ? 8.0 : 12.0,
        ),
        child: TCLinkText(
          text: question.body,
          textAlign: TextAlign.start,
          softWrap: true,
          maxLines: showWholeQuestion ? null : 6,
          overflow:
              showWholeQuestion ? TextOverflow.fade : TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.bodyText1,
          linkStyle: Theme.of(context).textTheme.bodyText1?.copyWith(
                decoration: TextDecoration.underline,
              ),
        ),
      );

  SizedBox buildQuestionFooter(BuildContext context, Question question) =>
      SizedBox(
        height: 26.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                  "${timeago.format(
                    question.createdAt,
                    locale: FlutterI18n.currentLocale(context)?.languageCode ??
                        "de",
                  )} ${_edited(context, question)}",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2
                      ?.copyWith(color: Colors.grey)),
            ),
            if (showHeart)
              HeartWidget(
                likable: question,
                toggleLike: () => BlocProvider.of<QuestionBloc>(context)
                    .add(ToggleLikeEvent(question)),
                fontSize: 20,
              )
          ],
        ),
      );

  Row buildQuestionHeader(BuildContext context, Question question) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (showAuthor && highlightAuthor)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          question.category,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .headline4
                              ?.copyWith(
                                  color: CorporateColors.tinyCampusIconGrey),
                        ),
                      ),
                      _headerRightIcons(context, question),
                    ],
                  ),
                if (showCategory)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      if (!highlightAuthor)
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Text(
                                  question.category,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      ?.copyWith(
                                        color:
                                            CorporateColors.tinyCampusIconGrey,
                                      ),
                                ),
                              ),
                              // _headerRightIcons(context),
                            ],
                          ),
                        ),
                    ],
                  ),
                if (showAuthor)
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: canNavigate ? 4.0 : 12.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            SvgProfileWidget(
                                radius: highlightAuthor ? 20 : 10,
                                svgString: question.author.displayName),
                            Container(
                              width: 12,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          question.author.displayName,
                                          style: highlightAuthor
                                              ? Theme.of(context)
                                                  .textTheme
                                                  .headline3
                                              : Theme.of(context)
                                                  .textTheme
                                                  .headline4,
                                        ),
                                      ),
                                      // TODO: Use when requirement is clear
                                      // canNavigate
                                      //     ? Container()
                                      //     : _verifiedWidget(),
                                    ],
                                  ),
                                  canNavigate
                                      ? Container()
                                      : FlairWidget(
                                          flairs: question.author.flairs,
                                        ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
              ],
            ),
          ),
        ],
      );

  Row _headerRightIcons(BuildContext context, Question question) => Row(
        children: [
          if (showViews && question.views > 0) _viewCount(context, question),
          if (showAnswers && question.responses > 0)
            _answerCount(context, question),
          if (!question.canEdit)
            PopupReportButton(
              reportPostRoute: reportQuestionByIdUrl(question.id),
              userId: question.author.authorId,
            ),
          if (question.canEdit && !canNavigate) ...[
            PopupMenuButton<String>(
              child: Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Icon(
                  Icons.more_vert,
                  color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
                ),
              ),
              onSelected: (value) {
                var _questionBloc = BlocProvider.of<QuestionBloc>(context);
                switch (value) {
                  case "delete":
                    {
                      TCDialog.showCustomDialog(
                          context: context,
                          functionActionColor:
                              CorporateColors.cafeteriaCautionRed,
                          onConfirm: () {
                            _questionBloc.add(DeleteQuestionEvent(
                              question.id,
                              BlocProvider.of<CategoryBloc>(context)
                                  .state
                                  .current
                                  .id,
                            ));
                            Navigator.pop(context);
                          },
                          functionActionText: FlutterI18n.translate(
                              context, 'common.actions.delete'),
                          headlineText: FlutterI18n.translate(
                              context, 'modules.q_and_a.delete_question'),
                          bodyText: FlutterI18n.translate(
                              context, 'modules.q_and_a.delete_question_text'));
                      break;
                    }
                  case "edit":
                    {
                      BlocProvider.of<EditBloc>(context)
                          .add(OpenEditEvent(index));
                      break;
                    }
                }
              },
              itemBuilder: (context) => <PopupMenuEntry<String>>[
                PopupMenuItem<String>(
                  value: 'delete',
                  child: I18nText('common.actions.delete'),
                ),
                PopupMenuItem<String>(
                  value: 'edit',
                  child: I18nText('common.actions.edit'),
                ),
              ],
            ),
          ]
        ],
      );

  Widget _viewCount(BuildContext context, Question question) => Padding(
        padding: const EdgeInsets.only(right: 12.0),
        child: Row(
          children: [
            Text(
              question.views.toString(),
              style: TextStyle(
                color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
                fontSize: 16.0,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 4),
              child: Icon(
                Icons.remove_red_eye,
                color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
                size: 20,
              ),
            ),
          ],
        ),
      );

  Widget _answerCount(BuildContext context, Question question) => Container(
        margin: EdgeInsets.only(
          right: 3.0,
        ),
        child: Row(
          children: <Widget>[
            Text(
              question.responses.toString(),
              style: TextStyle(
                color: CorporateColors.qAndAActivityTextGreen,
                fontSize: 16.0,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(12, 4, 8, 4),
              child: Icon(
                TinyCampusIcons.message,
                color: CorporateColors.qAndAActivityTextGreen,
                size: 16,
              ),
            ),
          ],
        ),
      );

  String _edited(BuildContext context, Question question) => question.isEdited
      ? FlutterI18n.translate(context, "modules.q_and_a.edited")
      : "";
}
