/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../reward/model/likable.dart';
import 'answer.dart';
import 'author.dart';

part 'question.g.dart';

///Model of a question
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class Question extends Equatable implements Likable {
  final int id;
  final Author author;
  final String category;
  @JsonKey(name: 'text')
  final String body;
  final Answer? helpfulAnswer;

  final DateTime createdAt;
  final DateTime modifiedAt;

  @JsonKey(name: 'numberOfAnswers')
  final int responses;
  final int views;

  @override
  @JsonKey(name: 'liked')
  final bool isLiked;
  @override
  final int likes;
  final String likableType = 'question';

  @JsonKey(name: 'reported')
  final bool isReported;

  final bool canEdit;

  Question({
    required this.id,
    required this.author,
    required this.category,
    required this.body,
    required this.createdAt,
    required this.modifiedAt,
    required this.responses,
    required this.views,
    required this.isLiked,
    required this.likes,
    this.isReported = false,
    this.helpfulAnswer,
    this.canEdit = false,
  });

  factory Question.placeholder() => Question(
        id: -1,
        author: Author.placeholder(),
        category: 'category',
        body: 'body',
        createdAt: DateTime.now(),
        modifiedAt: DateTime.now(),
        responses: 0,
        views: 0,
        isLiked: false,
        likes: 0,
      );

  factory Question.fromJson(Map<String, dynamic> json) =>
      _$QuestionFromJson(json);

  Map<String, dynamic> toJson() => _$QuestionToJson(this);

  @override
  List<Object?> get props => [
        id,
        author,
        category,
        body,
        createdAt,
        modifiedAt,
        responses,
        views,
        isLiked,
        likes,
        isReported,
        helpfulAnswer,
        canEdit,
      ];

  bool get isEdited => createdAt.difference(modifiedAt).inSeconds < -3;
}
