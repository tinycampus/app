// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'author.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension AuthorCopyWith on Author {
  Author copyWith({
    int? authorId,
    String? displayName,
    List<Flair>? flairs,
  }) {
    return Author(
      authorId: authorId ?? this.authorId,
      displayName: displayName ?? this.displayName,
      flairs: flairs ?? this.flairs,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Author _$AuthorFromJson(Map json) => Author(
      authorId: json['authorId'] as int,
      displayName: json['displayName'] as String,
      flairs: (json['flairs'] as List<dynamic>)
          .map((e) => Flair.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$AuthorToJson(Author instance) => <String, dynamic>{
      'authorId': instance.authorId,
      'displayName': instance.displayName,
      'flairs': instance.flairs.map((e) => e.toJson()).toList(),
    };
