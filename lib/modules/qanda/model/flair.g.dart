// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flair.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension FlairCopyWith on Flair {
  Flair copyWith({
    int? id,
    String? title,
  }) {
    return Flair(
      id: id ?? this.id,
      title: title ?? this.title,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Flair _$FlairFromJson(Map json) => Flair(
      id: json['id'] as int,
      title: json['title'] as String,
    );

Map<String, dynamic> _$FlairToJson(Flair instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
    };
