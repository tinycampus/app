/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'flair.dart';

part 'author.g.dart';

@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class Author extends Equatable {
  final int authorId;

  final String displayName;

  final List<Flair> flairs;

  Author({
    required this.authorId,
    required this.displayName,
    required this.flairs,
  });

  factory Author.placeholder() => Author(
        authorId: -1,
        displayName: 'Max Mustermann',
        flairs: [],
      );

  factory Author.fromJson(Map<String, dynamic> json) => _$AuthorFromJson(json);

  Map<String, dynamic> toJson() => _$AuthorToJson(this);

  @override
  List<Object> get props => [
        authorId,
        displayName,
        flairs,
      ];
}
