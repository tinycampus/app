// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'question.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension QuestionCopyWith on Question {
  Question copyWith({
    Author? author,
    String? body,
    bool? canEdit,
    String? category,
    DateTime? createdAt,
    Answer? helpfulAnswer,
    int? id,
    bool? isLiked,
    bool? isReported,
    int? likes,
    DateTime? modifiedAt,
    int? responses,
    int? views,
  }) {
    return Question(
      author: author ?? this.author,
      body: body ?? this.body,
      canEdit: canEdit ?? this.canEdit,
      category: category ?? this.category,
      createdAt: createdAt ?? this.createdAt,
      helpfulAnswer: helpfulAnswer ?? this.helpfulAnswer,
      id: id ?? this.id,
      isLiked: isLiked ?? this.isLiked,
      isReported: isReported ?? this.isReported,
      likes: likes ?? this.likes,
      modifiedAt: modifiedAt ?? this.modifiedAt,
      responses: responses ?? this.responses,
      views: views ?? this.views,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Question _$QuestionFromJson(Map json) => Question(
      id: json['id'] as int,
      author: Author.fromJson(Map<String, dynamic>.from(json['author'] as Map)),
      category: json['category'] as String,
      body: json['text'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      modifiedAt: DateTime.parse(json['modifiedAt'] as String),
      responses: json['numberOfAnswers'] as int,
      views: json['views'] as int,
      isLiked: json['liked'] as bool,
      likes: json['likes'] as int,
      isReported: json['reported'] as bool? ?? false,
      helpfulAnswer: json['helpfulAnswer'] == null
          ? null
          : Answer.fromJson(
              Map<String, dynamic>.from(json['helpfulAnswer'] as Map)),
      canEdit: json['canEdit'] as bool? ?? false,
    );

Map<String, dynamic> _$QuestionToJson(Question instance) => <String, dynamic>{
      'id': instance.id,
      'author': instance.author.toJson(),
      'category': instance.category,
      'text': instance.body,
      'helpfulAnswer': instance.helpfulAnswer?.toJson(),
      'createdAt': instance.createdAt.toIso8601String(),
      'modifiedAt': instance.modifiedAt.toIso8601String(),
      'numberOfAnswers': instance.responses,
      'views': instance.views,
      'liked': instance.isLiked,
      'likes': instance.likes,
      'reported': instance.isReported,
      'canEdit': instance.canEdit,
    };
