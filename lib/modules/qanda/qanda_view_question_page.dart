/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../common/tc_theme.dart';
import 'bloc/answer/answer_bloc.dart';
import 'bloc/edit/edit_bloc.dart';
import 'bloc/question/question_bloc.dart';
import 'model/question.dart';
import 'widgets/add_answer_widget.dart';
import 'widgets/answer_widget.dart';
import 'widgets/first_answer_widget.dart';
import 'widgets/question_widget.dart';

///Page which displays a single question with all the answers that have been
///given so far.
class QAndAViewQuestionPage extends StatefulWidget {
  final int id;

  QAndAViewQuestionPage(
    this.id, {
    Key? key,
  }) : super(key: key);

  @override
  _QAndAViewQuestionPageState createState() => _QAndAViewQuestionPageState();
}

class _QAndAViewQuestionPageState extends State<QAndAViewQuestionPage> {
  Question? question;

  @override
  void initState() {
    super.initState();

    question =
        BlocProvider.of<QuestionBloc>(context).state.questionById(widget.id);

    if (question == null) {
      BlocProvider.of<QuestionBloc>(context).add(LoadQuestionsEvent(-1));
    }

    BlocProvider.of<AnswerBloc>(context).add(LoadAnswersEvent(widget.id));
  }

  @override
  Widget build(BuildContext context) {
    var element = <int, bool>{};
    return BlocConsumer<QuestionBloc, QuestionState>(
      listenWhen: (prev, next) => next is QuestionsLoadedState,
      listener: (context, state) => setState(
        () => question = state.questionById(widget.id),
      ),
      builder: (context, questionState) => Scaffold(
        appBar: AppBar(
          title: I18nText(
            'common.actions.back',
          ),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: RefreshIndicator(
          strokeWidth: 3,
          color: CorporateColors.tinyCampusOrange,
          onRefresh: () async {
            final q = question;
            if (q == null) {
              BlocProvider.of<QuestionBloc>(context).add(
                LoadQuestionsEvent(-1),
              );
            } else {
              BlocProvider.of<QuestionBloc>(context).add(
                UpdateSingleQuestionEvent(q.id),
              );
              BlocProvider.of<AnswerBloc>(context).add(
                LoadAnswersEvent(q.id),
              );
            }
          },
          child: BlocConsumer<AnswerBloc, AnswerState>(
            listener: (context, state) {
              if (question != null && state.questionId != question!.id) {
                BlocProvider.of<AnswerBloc>(context).add(
                  LoadAnswersEvent(widget.id),
                );
              }
            },
            builder: (context, answerState) {
              /// This is a result of a badly handled state.
              /// [wrongAnswers] should never be true.
              /// However, because old answers are accessed,
              /// this is required in order to not confuse users.
              /// TODO: Find a better solution
              if (question == null || answerState.questionId != question!.id) {
                return Center(child: CircularProgressIndicator());
              }

              var hasAnswers = answerState.answers.isNotEmpty;
              return ListView.separated(
                itemCount: _calcListLength(answerState),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  if (index == 0) {
                    element.putIfAbsent(index, () => false);
                    return Padding(
                      padding: const EdgeInsets.only(top: 12),
                      child: QuestionWidget(
                        questionState.questionById(widget.id),
                        showWholeQuestion: true,
                        highlightAuthor: true,
                        index: index,
                      ),
                    );
                  }
                  if (index == 1 && !hasAnswers) {
                    element.putIfAbsent(index, () => false);
                    if (_calcListLength(answerState) == index + 1) {
                      BlocProvider.of<EditBloc>(context)
                          .add(LoadEditableEvent(element));
                    }
                    return Center(
                        child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: FirstAnswerWidget(),
                    ));
                  }
                  if ((index == 1 && hasAnswers) ||
                      (index == 2 && !hasAnswers)) {
                    element.putIfAbsent(index, () => false);
                    if (_calcListLength(answerState) == index + 1) {
                      BlocProvider.of<EditBloc>(context)
                          .add(LoadEditableEvent(element));
                    }
                    return AddAnswerWidget(widget.id, index);
                  }
                  if (!hasAnswers) {
                    element.putIfAbsent(index, () => false);
                    if (_calcListLength(answerState) == index + 1) {
                      BlocProvider.of<EditBloc>(context)
                          .add(LoadEditableEvent(element));
                    }
                    return FirstAnswerWidget();
                  }

                  final _answer =
                      answerState.answers[index - (hasAnswers ? 2 : 3)];
                  element.putIfAbsent(index, () => false);
                  if (_calcListLength(answerState) == index + 1) {
                    BlocProvider.of<EditBloc>(context)
                        .add(LoadEditableEvent(element));
                  }
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 4.0),
                    child: AnswerWidget(
                      _answer,
                      index,
                      key: ValueKey(_answer.id),
                      canMarkAsHelpful:
                          questionState.questionById(widget.id)?.canEdit ??
                              false,
                    ),
                  );
                },
                separatorBuilder: (context, index) =>
                    const Divider(height: 2.0, color: Colors.transparent),
              );
            },
          ),
        ),
      ),
    );
  }

  int _calcListLength(AnswerState state) {
    var length = state.answers.length;
    return length == 0 ? 3 : length + 2;
  }
}
