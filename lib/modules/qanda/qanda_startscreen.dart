/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../common/constants/routing_constants.dart';
import '../../common/tc_theme.dart';
import 'bloc/category/category_bloc.dart';
import 'bloc/question/question_bloc.dart';
import 'widgets/all_questions_start_screen_header.dart';
import 'widgets/my_questions_start_screen_header.dart';
import 'widgets/startscreen_category_widget.dart';

///The first page in the Q and A module.
class QAndAStartScreen extends StatelessWidget {
  const QAndAStartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CategoryBloc>(context).add(LoadCategoriesEvent());
    BlocProvider.of<QuestionBloc>(context).add(LoadQuestionsEvent(-1));
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: I18nText(
          'modules.q_and_a.title',
        ),
      ),
      body: BlocBuilder<CategoryBloc, CategoryState>(builder: (context, state) {
        switch (state.runtimeType) {
          case LoadingCategoriesState:
            return (state as LoadingCategoriesState).loadingIndicator;
          case ChangeCurrentCategoryState:
          case CategoriesLoadedState:
            return ListView(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                MyQuestionsStartScreenHeader(newReplies: state.newReplies),
                AllQuestionsStartScreenHeader(),
                Container(
                  margin:
                      EdgeInsets.only(bottom: 8, top: 8, left: 16, right: 16),
                  child: I18nText(
                    'modules.q_and_a.added_categories',
                    child: Text(
                      '',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: CorporateColors.tinyCampusIconGrey,
                      ),
                    ),
                  ),
                ),
                Column(
                  children: state.subscribed
                      .map(StartScreenCategoryWidget.new)
                      .toList(),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, qaAddCategories);
                  },
                  child: Card(
                    margin:
                        EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(6.0))),
                    elevation: 1.0,
                    clipBehavior: Clip.antiAlias,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 10,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Color.fromARGB(255, 240, 240, 240),
                            foregroundColor: CorporateColors.tinyCampusIconGrey,
                            child: Icon(Icons.add),
                          ),
                          I18nText(
                            'modules.q_and_a.more_categories',
                            child: Text(
                              '',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w400,
                                color: CorporateColors.tinyCampusDarkBlue,
                              ),
                            ),
                          ),
                          I18nPlural(
                            'modules.q_and_a.x_categories.times',
                            state.notSubscribed.length,
                            child: Text(
                              '',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2
                                  ?.copyWith(
                                      color:
                                          CorporateColors.tinyCampusIconGrey),
                            ),
                          ),
                        ],
                      ),
                    ),
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ],
            );
        }
        return Container();
      }),
    );
  }
}
