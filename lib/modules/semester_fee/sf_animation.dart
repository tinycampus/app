/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SFAnimation {
  SFAnimation({
    required this.controller,
    double beginOpacity = 0.0,
    double endOpacity = 1.00,
    double startPercent = 0.0,
    double endPercent = 1.00,
  })  : opacity = Tween<double>(begin: beginOpacity, end: endOpacity).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        ),
        percent = Tween<double>(begin: startPercent, end: endPercent).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        ),
        scale = Tween<double>(begin: 0.97, end: 1.0).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        );

  final AnimationController controller;
  final Animation<double> opacity;
  final Animation<double> percent;
  final Animation<double> scale;
}
