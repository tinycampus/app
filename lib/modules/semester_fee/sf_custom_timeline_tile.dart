/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';
import 'package:timeline_tile/timeline_tile.dart';

import '../../common/tc_theme.dart';

enum CTTPosition {
  isStart,
  isMiddle,
  isEnd,
}

class CustomTimelineTile extends StatelessWidget {
  const CustomTimelineTile({
    Key? key,
    required this.date,
    this.timelineAlign = TimelineAlign.end,
    this.position = CTTPosition.isMiddle,
    this.isToday = false,
    this.customTextLabelColor,
    this.customWidget,
    this.labelI18nKey = "",
    this.hasIndicator = true,
  }) : super(key: key);

  final DateTime date;
  final TimelineAlign timelineAlign;
  final CTTPosition position;
  final Widget? customWidget;
  final String labelI18nKey;
  final bool isToday;
  final Color? customTextLabelColor;
  final bool hasIndicator;

  @override
  Widget build(BuildContext context) {
    final child = Container(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      margin: EdgeInsets.only(left: 24.0),
      child: customWidget ??
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                FlutterI18n.translate(context, labelI18nKey),
                style: Theme.of(context).textTheme.headline4?.copyWith(
                    color: customTextLabelColor ??
                        (isToday ? CorporateColors.tinyCampusOrange : null)),
              ),
              Text(
                DateFormat.yMMMMd(
                        FlutterI18n.currentLocale(context)?.languageCode ??
                            'de')
                    .format(date),
                style: Theme.of(context).textTheme.bodyText1?.copyWith(
                      fontSize: 20.0,
                    ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                      DateFormat.EEEE(
                        FlutterI18n.currentLocale(context)?.languageCode ??
                            'de',
                      ).format(date),
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(
                            fontSize: 16.0,
                            color: date.weekday >= 6
                                ? CorporateColors.tinyCampusIconGrey
                                : CorporateColors.tinyCampusIconGrey,
                          )),
                  if (position == CTTPosition.isStart && date.weekday >= 6)
                    Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: Icon(
                        Icons.warning_amber_rounded,
                        size: 24,
                        color:
                            CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
                      ),
                    ),
                ],
              ),
            ],
          ),
    );
    return TimelineTile(
      alignment: timelineAlign,
      isFirst: position == CTTPosition.isStart ? true : false,
      isLast: position == CTTPosition.isEnd ? true : false,
      hasIndicator: hasIndicator,
      indicatorStyle: IndicatorStyle(
        iconStyle: IconStyle(
            color: hasIndicator ? Colors.white : Colors.transparent,
            iconData: Icons.circle,
            fontSize: 20),
        color: !hasIndicator
            ? Colors.transparent
            : customTextLabelColor ??
                (isToday
                    ? CorporateColors.tinyCampusOrange
                    : CurrentTheme().tcBlue),
        width: 24,
      ),
      afterLineStyle:
          LineStyle(color: CorporateColors.tinyCampusBlue.withOpacity(0.3)),
      beforeLineStyle:
          LineStyle(color: CorporateColors.tinyCampusBlue.withOpacity(0.3)),
      startChild: timelineAlign == TimelineAlign.end
          ? child
          : timelineAlign == TimelineAlign.center
              ? child
              : null,
      endChild: timelineAlign == TimelineAlign.start
          ? child
          : timelineAlign == TimelineAlign.center
              ? child
              : null,
    );
  }
}
