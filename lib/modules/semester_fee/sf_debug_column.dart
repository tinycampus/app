/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';

import 'sf_interval.dart';

class DebugTextColumn extends StatelessWidget {
  const DebugTextColumn({
    Key? key,
    required this.state,
    required this.nearestInterval,
    required this.now,
  }) : super(key: key);

  final SFState state;
  final SFInterval nearestInterval;
  final DateTime now;

  @override
  Widget build(BuildContext context) {
    final formatter = DateFormat.yMEd(
      FlutterI18n.currentLocale(context)?.languageCode ?? 'de',
    );

    return Column(
      children: [
        Text(state.toString()),
        Text(nearestInterval.semester.toString()),
        Text("Now: ${formatter.format(now)}"),
        Text("StartWait: ${formatter.format(nearestInterval.startInterval)}"),
        Text("StartPay: ${formatter.format(nearestInterval.beginPayment)}"),
        Text("EndPay: ${formatter.format(nearestInterval.endPayment)}"),
      ],
    );
  }
}
