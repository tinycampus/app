/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../../common/tc_theme.dart';
import '../../../organizer/ui/organizer/o_drawer/i18n_common.dart';

class LastUpdate extends StatelessWidget {
  final DateTime lastUpdate;

  const LastUpdate({Key? key, required this.lastUpdate}) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.only(top: 24.0, bottom: 36.0),
        child: Center(
          child: Text(
            "${FlutterI18n.translate(context, 'modules.zs.updated')} "
            "${lastUpdate.day}"
            ".${lastUpdate.month}."
            "${lastUpdate.year}",
            style: TextStyle(color: CurrentTheme().textPassive, fontSize: 20),
          ),
        ),
      );
}
