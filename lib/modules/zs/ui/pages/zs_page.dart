/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../common/assets_adapter.dart';
import '../../../../common/constants/api_constants.dart';
import '../../../../common/tc_utility_functions.dart';
import '../../../../repositories/http/http_repository.dart';
import '../../model/zs_item.dart';
import '../widgets/last_update.dart';
import '../widgets/link_card.dart';
import '../widgets/link_tile.dart';

class ZsPage extends StatefulWidget {
  const ZsPage({Key? key}) : super(key: key);

  @override
  _ZsPageState createState() => _ZsPageState();
}

class _ZsPageState extends State<ZsPage> {
  List<ZsItem> items = [];
  DateTime lastUpdate = DateTime.utc(2021, 3, 12);

  @override
  void didChangeDependencies() {
    loadModuleData(FlutterI18n.currentLocale(context).toString());
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(FlutterI18n.translate(context, 'modules.zs.title')),
        ),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              AspectRatio(
                  aspectRatio: 1.3,
                  child: Image.asset(
                    AssetAdapter.module(ModuleModel.zs),
                    fit: BoxFit.fitHeight,
                  )),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
                child: LinkCard(
                  disclaimer: FlutterI18n.translate(
                    context,
                    'modules.zs.disclaimer',
                  ),
                  title: FlutterI18n.translate(
                    context,
                    'modules.zs.services',
                  ),
                  items: items.isNotEmpty
                      ? items.map((e) => LinkTile(item: e)).toList()
                      : [
                          Center(
                              child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: CircularProgressIndicator(),
                          ))
                        ],
                ),
              ),
              LastUpdate(lastUpdate: lastUpdate),
            ],
          ),
        ),
      );

  Future<void> loadModuleData(String language) async {
    final fileKey = '$language.zs.json';
    lastUpdate = await getLastUpdate(fileKey);
    items = await loadDataFromServer(fileKey);

    if (items.isNotEmpty) {
      saveToSP(items, fileKey);
      setLastUpdate(fileKey, DateTime.now());
    }

    if (items.isEmpty) items = await loadFromSP(fileKey);
    if (items.isEmpty) items = await loadFromAssets(fileKey);

    setState(() {});
  }

  Future<List<ZsItem>> loadDataFromServer(String key) async {
    var url = "$localizationUri$key";
    try {
      var res = await HttpRepository().get(
        url,
        secured: false,
        headers: {'Accept': 'application/json'},
      );

      return buildEntitiesFromList(
        jsonDecode(utf8.decode(res.bodyBytes)),
        ZsItem.fromJson,
      );
    } on Exception catch (e) {
      debugPrint(e.toString());
      debugPrint("Continuing with Data from local Files");
    }

    return [];
  }

  Future<void> saveToSP(List<ZsItem> items, String key) async {
    var sp = await SharedPreferences.getInstance();
    sp.setStringList(key, items.map((e) => jsonEncode(e.toJson())).toList());
  }

  Future<List<ZsItem>> loadFromSP(String key) async {
    var sp = await SharedPreferences.getInstance();
    List<String?> stringList;

    try {
      stringList = sp.getStringList(key) ?? [];
      debugPrint("Reading ZS-data from Shared Preferences");

      return stringList
          .where((e) => e != null)
          .map((e) => ZsItem.fromJson(jsonDecode(e!)))
          .toList();
    } on Exception catch (e) {
      debugPrint("Couldn't read Shared Preferences for $key");
      debugPrint(e.toString());
    }

    return [];
  }

  Future<List<ZsItem>> loadFromAssets(String key) async {
    debugPrint("Reading ZS-data from Asset Files");
    final localJson = jsonDecode(
      await DefaultAssetBundle.of(context).loadString(
        "assets/flutter_i18n/$key",
      ),
    );

    return buildEntitiesFromList(localJson, ZsItem.fromJson);
  }

  Future<void> setLastUpdate(String key, DateTime timestamp) async {
    var sp = await SharedPreferences.getInstance();
    final keyLastUpdated = '$key.lastUpdated';
    await sp.setString(keyLastUpdated, timestamp.toIso8601String());
  }

  Future<DateTime> getLastUpdate(String key) async {
    var sp = await SharedPreferences.getInstance();
    final keyLastUpdated = '$key.lastUpdated';
    var res = sp.getString(keyLastUpdated);
    return res != null ? DateTime.parse(res) : lastUpdate;
  }
}
