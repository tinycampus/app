// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'zs_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ZsItem _$ZsItemFromJson(Map<String, dynamic> json) => ZsItem(
      title: json['title'] as String? ?? '',
      subtitle: json['subtitle'] as String? ?? '',
      imageUrl: json['imageUrl'] as String? ?? '',
      url: json['url'] as String? ?? '',
    );

Map<String, dynamic> _$ZsItemToJson(ZsItem instance) => <String, dynamic>{
      'title': instance.title,
      'subtitle': instance.subtitle,
      'imageUrl': instance.imageUrl,
      'url': instance.url,
    };
