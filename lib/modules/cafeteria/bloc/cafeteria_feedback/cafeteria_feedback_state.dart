/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'cafeteria_feedback_bloc.dart';

/// A [CafeteriaFeedbackState] holds an [itemFeedback] and [commenting]
@CopyWith()
class CafeteriaFeedbackState extends Equatable {
  final CafeteriaItemFeedback itemFeedback;
  final bool commenting;

  CafeteriaFeedbackState(
      {required this.itemFeedback, required this.commenting});

  CafeteriaFeedbackState._fromState(CafeteriaFeedbackState state)
      : itemFeedback = state.itemFeedback,
        commenting = state.commenting;

  @override
  List<Object> get props => [itemFeedback];
}

/// A [CafeteriaFeedbackState] that is called when checking for persisted data
class InitialCafeteriaFeedbackState extends CafeteriaFeedbackState {
  InitialCafeteriaFeedbackState()
      : super(
          itemFeedback: CafeteriaItemFeedback.initialData(),
          commenting: false,
        );
}

/// A [CafeteriaFeedbackState] that is called when loading
/// [CafeteriaItemFeedback] from backend
class LoadingCafeteriaFeedbackState extends CafeteriaFeedbackState {
  LoadingCafeteriaFeedbackState._fromState(CafeteriaFeedbackState state)
      : super._fromState(state);
}

/// A [CafeteriaFeedbackState] that is called when [commenting] is changed
class CommentingCafeteriaFeedbackState extends CafeteriaFeedbackState {
  CommentingCafeteriaFeedbackState._fromState(CafeteriaFeedbackState state)
      : super._fromState(state);
}

/// A [CafeteriaFeedbackState] that is called when a comment is deleted
class DeleteCommentCafeteriaFeedbackState extends CafeteriaFeedbackState {
  DeleteCommentCafeteriaFeedbackState._fromState(CafeteriaFeedbackState state)
      : super._fromState(state);
}

/// A [CafeteriaFeedbackState] that is called when posting
/// a [CafeteriaItemComment]
class PostCommentState extends CafeteriaFeedbackState {
  PostCommentState._fromState(CafeteriaFeedbackState state)
      : super._fromState(state);
}

/// A [CafeteriaFeedbackState] that is called when changing the status of
/// a likable object in [CafeteriaItemComment]
class LikeChangeFeedbackState extends CafeteriaFeedbackState {
  LikeChangeFeedbackState._fromState(CafeteriaFeedbackState state)
      : super._fromState(state);
}

/// A [CafeteriaFeedbackState] that is called when no other action is required
class IdleCafeteriaFeedbackState extends CafeteriaFeedbackState {
  IdleCafeteriaFeedbackState._fromState(CafeteriaFeedbackState state)
      : super._fromState(state);
}

/// A [CafeteriaFeedbackState] that is called when communication with backend
/// fails. It takes a [message] which is listened to in [CafeteriaItemPage]
class CafeteriaFeedbackErrorState extends CafeteriaFeedbackState {
  final String message;

  CafeteriaFeedbackErrorState._fromState(
    CafeteriaFeedbackState state,
    this.message,
  ) : super._fromState(state);
}
