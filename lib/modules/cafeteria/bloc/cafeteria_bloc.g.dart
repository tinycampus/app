// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CafeteriaStateCopyWith on CafeteriaState {
  CafeteriaState copyWith({
    List<Cafeteria>? cafeteriaList,
  }) {
    return CafeteriaState(
      cafeteriaList: cafeteriaList ?? this.cafeteriaList,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CafeteriaState _$CafeteriaStateFromJson(Map json) => CafeteriaState(
      cafeteriaList: (json['cafeteriaList'] as List<dynamic>?)
          ?.map((e) => Cafeteria.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$CafeteriaStateToJson(CafeteriaState instance) =>
    <String, dynamic>{
      'cafeteriaList': instance.cafeteriaList.map((e) => e.toJson()).toList(),
    };
