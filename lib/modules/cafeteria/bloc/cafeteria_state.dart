/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'cafeteria_bloc.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
@CopyWith()

/// The State for [CafeteriaBloc] that holds a List of every [Cafeteria]
class CafeteriaState extends Equatable {
  final List<Cafeteria> cafeteriaList;

  CafeteriaState({List<Cafeteria>? cafeteriaList})
      : cafeteriaList = cafeteriaList ?? [];

  CafeteriaState._fromState(CafeteriaState state)
      : cafeteriaList = state.cafeteriaList;

  CafeteriaState._fromList(this.cafeteriaList);

  /// Creates a [CafeteriaState] from a Json.
  factory CafeteriaState.fromJson(Map<String, dynamic> json) =>
      _$CafeteriaStateFromJson(json);

  /// Maps a [CafeteriaState] to a Json.
  Map<String, dynamic> toJson() => _$CafeteriaStateToJson(this);

  @override
  String toString() => "$runtimeType: ${toJson()}";

  @override
  List<Object> get props => [cafeteriaList];
}

/// A [CafeteriaState] that is called when checking for
/// persisted [Cafeteria] data
class InitialCafeteriaState extends CafeteriaState {
  InitialCafeteriaState() : super(cafeteriaList: []);
  InitialCafeteriaState.fromState(CafeteriaState state)
      : super._fromState(state);
}

/// A [CafeteriaState] that is called when loading [Cafeteria] data
/// from external API
class LoadingCafeteriaState extends CafeteriaState {
  LoadingCafeteriaState();
  LoadingCafeteriaState.fromState(CafeteriaState state)
      : super._fromState(state);
}

/// A [CafeteriaState] that serves as the idle State to hold [Cafeteria] data
class LoadedCafeteriaState extends CafeteriaState {
  LoadedCafeteriaState(CafeteriaState loadedState)
      : super._fromState(loadedState);
}
