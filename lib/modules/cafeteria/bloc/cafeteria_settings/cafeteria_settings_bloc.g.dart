// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_settings_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CafeteriaSettingsStateCopyWith on CafeteriaSettingsState {
  CafeteriaSettingsState copyWith({
    List<String>? cafeteriaSelection,
    CafeteriaSettings? settings,
  }) {
    return CafeteriaSettingsState(
      cafeteriaSelection: cafeteriaSelection ?? this.cafeteriaSelection,
      settings: settings ?? this.settings,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CafeteriaSettingsState _$CafeteriaSettingsStateFromJson(Map json) =>
    CafeteriaSettingsState(
      cafeteriaSelection: (json['cafeteriaSelection'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      settings: json['settings'] == null
          ? null
          : CafeteriaSettings.fromJson(
              Map<String, dynamic>.from(json['settings'] as Map)),
    );

Map<String, dynamic> _$CafeteriaSettingsStateToJson(
        CafeteriaSettingsState instance) =>
    <String, dynamic>{
      'settings': instance.settings.toJson(),
      'cafeteriaSelection': instance.cafeteriaSelection,
    };
