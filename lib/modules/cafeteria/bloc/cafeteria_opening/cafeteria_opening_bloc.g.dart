// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_opening_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CafeteriaOpeningStateCopyWith on CafeteriaOpeningState {
  CafeteriaOpeningState copyWith({
    List<CafeteriaOpeningHours>? cafeteriaOpeningHours,
  }) {
    return CafeteriaOpeningState(
      cafeteriaOpeningHours:
          cafeteriaOpeningHours ?? this.cafeteriaOpeningHours,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CafeteriaOpeningState _$CafeteriaOpeningStateFromJson(Map json) =>
    CafeteriaOpeningState(
      cafeteriaOpeningHours: (json['cafeteriaOpeningHours'] as List<dynamic>?)
              ?.map((e) => CafeteriaOpeningHours.fromJson(
                  Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$CafeteriaOpeningStateToJson(
        CafeteriaOpeningState instance) =>
    <String, dynamic>{
      'cafeteriaOpeningHours':
          instance.cafeteriaOpeningHours.map((e) => e.toJson()).toList(),
    };
