// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CafeteriaItem _$CafeteriaItemFromJson(Map json) => CafeteriaItem(
      category: json['category'] as String,
      date: json['date'] as String,
      descriptionClean: json['description_clean'] as String,
      description: json['description'] as String,
      artikel: json['artikel'] as String,
      beschreibung: json['beschreibung'] as String,
      artikelClean: json['artikel_clean'] as String,
      beschreibungClean: json['beschreibung_clean'] as String,
      prices: Prices.fromJson(Map<String, dynamic>.from(json['prices'] as Map)),
      allergens: Map<String, String>.from(json['allergens'] as Map),
      additives: (json['additives'] as Map).map(
        (k, e) => MapEntry(int.parse(k as String), e as String),
      ),
      characteristics: (json['characteristics'] as List<dynamic>)
          .map((e) =>
              Characteristic.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      image: json['image'] as String,
    );

Map<String, dynamic> _$CafeteriaItemToJson(CafeteriaItem instance) =>
    <String, dynamic>{
      'category': instance.category,
      'date': instance.date,
      'artikel': instance.artikel,
      'artikel_clean': instance.artikelClean,
      'beschreibung': instance.beschreibung,
      'beschreibung_clean': instance.beschreibungClean,
      'description': instance.description,
      'description_clean': instance.descriptionClean,
      'allergens': instance.allergens,
      'additives': instance.additives.map((k, e) => MapEntry(k.toString(), e)),
      'prices': instance.prices.toJson(),
      'characteristics':
          instance.characteristics.map((e) => e.toJson()).toList(),
      'image': instance.image,
    };
