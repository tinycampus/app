/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../common/constants/api_constants.dart';
import 'characteristic.dart';
import 'prices.dart';

part 'cafeteria_item.g.dart';

/// A model for a [CafeteriaItem]
@immutable
@JsonSerializable(anyMap: true, explicitToJson: true)
class CafeteriaItem extends Equatable {
  /// The category of an item, e.g. "information" or "Ausgabe 1"
  final String category;

  /// The date of when the item is to be shown
  final String date;

  /// name of the item
  final String artikel;

  /// name of the item
  @JsonKey(name: "artikel_clean")
  final String artikelClean;

  /// description of the item
  final String beschreibung;

  /// description of the item
  @JsonKey(name: "beschreibung_clean")
  final String beschreibungClean;

  /// description of the item
  final String description;

  /// description of the item
  @JsonKey(name: "description_clean")
  final String descriptionClean;

  /// A [Map] of allergen ID and names
  final Map<String, String> allergens;

  /// A [Map] of additive ID and names
  final Map<int, String> additives;

  /// the Prices for staff, guest and student
  final Prices prices;

  /// A [List] of the various characteristics such as "vegan" or "garlic"
  final List<Characteristic> characteristics;

  /// image path
  final String image;

  ///Standard CafeteriaItem constructor
  CafeteriaItem({
    required this.category,
    required this.date,
    required this.descriptionClean,
    required this.description,
    required this.artikel,
    required this.beschreibung,
    required this.artikelClean,
    required this.beschreibungClean,
    required this.prices,
    required this.allergens,
    required this.additives,
    required this.characteristics,
    required this.image,
  });

  factory CafeteriaItem.fromJson(Map<String, dynamic> json) {
    if (json["additives"] is List) {
      json["additives"] = <String, dynamic>{};
    }
    if (json["allergens"] is List) {
      json["allergens"] = <String, dynamic>{};
    }
    return _$CafeteriaItemFromJson(json);
  }

  Map<String, dynamic> toJson() => _$CafeteriaItemToJson(this);

  String getUniqueId({required String cafeteriaName}) => "$cafeteriaName"
      "XXX"
      "$category"
      "XXX"
      "$descriptionClean";

  @override
  List<Object> get props => [
        category,
        date,
        artikel,
        artikelClean,
        beschreibung,
        beschreibungClean,
        description,
        descriptionClean,
        prices,
        additives,
        characteristics,
        image,
      ];

  CafeteriaItem.defaultItem()
      : category = "Ausgabe 1",
        date = "2020-03-18",
        artikel = "Rahmgeschnetzeltes vom Schwein mit Champignons "
            "(2,3,5,30,36,41,30a,30c)",
        artikelClean = "Rahmgeschnetzeltes vom Schwein mit Champignons",
        beschreibung =
            "und Spätzle (30,32,30a) #Fleisch \"Geprüfte Qualität - HESSEN\"",
        beschreibungClean =
            "und Spätzle #Fleisch \"Geprüfte Qualität - HESSEN\"",
        description = "Rahmgeschnetzeltes  vom Schwein mit Champignons "
            "(2,3,5,30,36,41,30a,30c)  und Spätzle (30,32,30a)  "
            "Fleisch \"Geprüfte Qualität - HESSEN\"",
        descriptionClean = "Rahmgeschnetzeltes  vom Schwein mit Champignons  "
            "und Spätzle  Fleisch \"Geprüfte Qualität - HESSEN\"",
        allergens = {
          "30": "Glutenhaltiges Getreide",
          "30a": "Weizen",
          "30c": "Gerste",
          "32": "Eier",
          "36": "Milch",
          "41": "Schwefeldioxid und Sulfite"
        },
        additives = {
          2: "Konservierungsstoff",
          3: "Antioxidationsmittel",
          5: "Geschwefelt"
        },
        prices = Prices("4,90", "5,40", "2,90"),
        characteristics = [
          Characteristic(
            abbreviation: "GQH",
            name: "Geprüfte Qualität Hessen",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/GQH.png",
          ),
          Characteristic(
            abbreviation: "S",
            name: "Schwein",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/S.png",
          ),
          Characteristic(
            abbreviation: "KNO",
            name: "Knoblauch",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/KNO.png",
          ),
          Characteristic(
            abbreviation: "EIG",
            name: "Eigenproduktion",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/EIG.png",
          )
        ],
        image =
            "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/2e9n2_8.jpg";

  CafeteriaItem.defaultVegetarianItem()
      : category = "Ausgabe 2",
        date = "2020-03-18",
        artikel = "Pizza Funghi mit Champignons und Tomaten",
        artikelClean = "Pizza Funghi <br>mit Champignons und Tomaten",
        beschreibung = "",
        beschreibungClean = "",
        description = "Pizza Funghi  mit Champignons und Tomaten (2,30,36,30a)",
        descriptionClean = "Pizza Funghi  mit Champignons und Tomaten",
        allergens = {
          "30": "Glutenhaltiges Getreide",
          "30a": "Weizen",
          "36": "Milch"
        },
        additives = {2: "Konservierungsstoff"},
        prices = Prices("4,60", "5,10", "2,60"),
        characteristics = [
          Characteristic(
            abbreviation: "V",
            name: "vegetarisch",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/V.png",
          ),
          Characteristic(
            abbreviation: "KNO",
            name: "Knoblauch",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/KNO.png",
          ),
          Characteristic(
            abbreviation: "EIG",
            name: "Eigenproduktion",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/EIG.png",
          )
        ],
        image =
            "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/cmc5652rc_2.jpg";

  CafeteriaItem.defaultVeganItem()
      : category = "Ausgabe 3",
        date = "2020-03-18",
        artikel = "Pasta (30,30a)",
        artikelClean = "Pasta",
        beschreibung = "mit Tomaten-Basilikum-Sauce (30,30a)",
        beschreibungClean = "mit Tomaten-Basilikum-Sauce",
        description = "Pasta (30,30a) mit Tomaten-Basilikum-Sauce (30,30a)",
        descriptionClean = "Pasta mit Tomaten-Basilikum-Sauce",
        allergens = {"30": "Glutenhaltiges Getreide", "30a": "Weizen"},
        additives = {},
        prices = Prices("4,60", "5,10", "2,60"),
        characteristics = [
          Characteristic(
            abbreviation: "VEG",
            name: "Vegan",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/VEG.png",
          ),
          Characteristic(
            abbreviation: "KNO",
            name: "Knoblauch",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/KNO.png",
          ),
          Characteristic(
            abbreviation: "EIG",
            name: "Eigenproduktion",
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/EIG.png",
          )
        ],
        image =
            "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/311n2r7n3_img_1336.jpg";
}
