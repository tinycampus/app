// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_item_rating.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CafeteriaItemRatingCopyWith on CafeteriaItemRating {
  CafeteriaItemRating copyWith({
    double? avgRating,
    int? ratings,
    int? userRating,
  }) {
    return CafeteriaItemRating(
      avgRating: avgRating ?? this.avgRating,
      ratings: ratings ?? this.ratings,
      userRating: userRating ?? this.userRating,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CafeteriaItemRating _$CafeteriaItemRatingFromJson(Map json) =>
    CafeteriaItemRating(
      avgRating: (json['avgRating'] as num).toDouble(),
      ratings: json['ratings'] as int,
      userRating: json['userRating'] as int,
    );

Map<String, dynamic> _$CafeteriaItemRatingToJson(
        CafeteriaItemRating instance) =>
    <String, dynamic>{
      'avgRating': instance.avgRating,
      'ratings': instance.ratings,
      'userRating': instance.userRating,
    };
