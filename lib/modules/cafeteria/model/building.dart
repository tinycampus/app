/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'building.g.dart';

/// A [Building] holds a [name], [locality], [postalCode] and [streetAdress]
/// of a [Cafeteria]
@immutable
@JsonSerializable(anyMap: true, explicitToJson: true)
class Building extends Equatable {
  /// name of the [Cafeteria]
  final String name;

  /// City of the [Cafeteria]
  final String locality;

  /// the postal code of the [Cafeteria]
  final String postalCode;

  /// the street address of the [Cafeteria]
  final String streetAdress;

  /// Default constructor
  Building({
    required this.name,
    required this.locality,
    required this.postalCode,
    required this.streetAdress,
  });

  /// Example constructor
  Building.example(this.name)
      : locality = "Beispielstadt",
        postalCode = "B-1337",
        streetAdress = "Beispielstraße";

  factory Building.fromJson(Map<String, dynamic> json) =>
      _$BuildingFromJson(json);

  Map<String, dynamic> toJson() => _$BuildingToJson(this);

  @override
  List<Object> get props => [name, locality, postalCode, streetAdress];
}
