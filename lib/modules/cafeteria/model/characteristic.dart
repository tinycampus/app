/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

part 'characteristic.g.dart';

/// A model for a characteristic
/// This can be e.g. "Vegan", "Knoblauch"(not localized, german) etc.
@JsonSerializable(anyMap: true, explicitToJson: true)
class Characteristic {
  /// abbreviation of the characteristic
  final String abbreviation;

  /// name of the characteristic
  final String name;

  /// image path
  final String image;

  /// Default constructor
  const Characteristic({
    required this.abbreviation,
    required this.name,
    required this.image,
  });

  factory Characteristic.fromJson(Map<String, dynamic> json) =>
      _$CharacteristicFromJson(json);

  Map<String, dynamic> toJson() => _$CharacteristicToJson(this);
}
