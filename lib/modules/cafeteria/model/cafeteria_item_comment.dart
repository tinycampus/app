/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cafeteria_item_comment.g.dart';

/// A model that holds the information of a comment on a [CafeteriaItem]
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class CafeteriaItemComment extends Equatable {
  /// The id of the author
  @JsonKey(defaultValue: -1)
  final int authorId;

  /// The author of the comment
  final String author;

  /// Timestamp of the comment's creation
  final String createdAt;

  /// the comment's ID
  final int id;

  /// True if the current user has liked the item
  final bool liked;

  /// The amount of likes the item has
  final int likes;

  /// Content of the comment
  final String text;

  /// If [User] can edit this
  @JsonKey(defaultValue: false)
  final bool canEdit;

  /// If this was reported to the backend
  @JsonKey(name: 'reported', defaultValue: false)
  final bool isReported;

  /// Default constructor
  CafeteriaItemComment({
    required this.authorId,
    required this.author,
    required this.createdAt,
    required this.id,
    required this.liked,
    required this.likes,
    required this.text,
    required this.canEdit,
    required this.isReported,
  });

  factory CafeteriaItemComment.fromJson(Map<String, dynamic> json) =>
      _$CafeteriaItemCommentFromJson(json);

  Map<String, dynamic> toJson() => _$CafeteriaItemCommentToJson(this);

  @override
  List<Object> get props => [
        authorId,
        author,
        createdAt,
        id,
        liked,
        likes,
        text,
        canEdit,
        isReported,
      ];
}
