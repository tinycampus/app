/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import '../ui/widgets/cafeteria_item_generator.dart';
import 'building.dart';
import 'cafeteria_item.dart';

part 'cafeteria.g.dart';

///A class which holds all relevant Information for a single cafeteria.
///It provides getters which help sorting the [items] list.
@immutable
@JsonSerializable(anyMap: true, explicitToJson: true)
class Cafeteria extends Equatable {
  ///Cafeteria constructor, which takes a building and a List of items
  ///each category is created dynamically based on the items.
  Cafeteria({
    required this.building,
    required this.items,
  })  : categoryNames = readCategories(items),
        categories = _matchItemCategories(items, readCategories(items));

  static final List<CafeteriaItem> exampleItems =
      CafeteriaItemGenerator.generateRandomItemList(input: 5);

  Cafeteria.example(String name)
      : building = Building.example(name),
        items = exampleItems,
        categoryNames = readCategories(exampleItems),
        categories =
            _matchItemCategories(exampleItems, readCategories(exampleItems));

  ///Field which holds information about the
  ///Building the Cafeteria is located in
  final Building building;

  ///A list of all Items (including Information-Items)
  ///available in this Cafeteria
  final List<CafeteriaItem> items;

  ///A list of all categories (mostly Food Serving Counters) of the Cafeteria
  final List<String> categoryNames;

  ///A map which holds the correlation between the items and the categories
  final Map<String, List<CafeteriaItem>> categories;

  ///getter to filter items based on the Weekday
  List<CafeteriaItem> weekday(String day) => _getCafeteriaItemsForWeekday(day);

  /// gets the current week number for a [DateTime]
  int weekNumber(DateTime date) {
    var dayOfYear = int.parse(DateFormat("D").format(date));
    return ((dayOfYear - date.weekday + 10) / 7).floor();
  }

  /// getter for all [CafeteriaItem]s for a given Weekday
  List<CafeteriaItem> _getCafeteriaItemsForWeekday(String day) {
    if (items.where((item) => item.date == day).toList().isNotEmpty) {
      return items.where((item) => item.date == day).toList();
    } else {
      return [];
    }
  }

  factory Cafeteria.fromJson(Map<String, dynamic> json) =>
      _$CafeteriaFromJson(json);

  Map<String, dynamic> toJson() => _$CafeteriaToJson(this);

  @override
  List<Object> get props => [building, items];

  @override
  String toString() => 'Cafeteria: ${toJson().toString()}';

  ///Returns a list of all [categories] contained in the [items] passed
  static List<String> readCategories(List<CafeteriaItem> items) {
    var categories = <String>[];
    for (var item in items) {
      if (!categories.contains(item.category)) categories.add(item.category);
    }
    return categories;
  }

  ///Returns a list of all [categories], but excludes certain
  ///categories provided by the list of strings [excludedCategories]
  static List<String> readCategoriesWithExclusion(
      List<CafeteriaItem> items, List<String> excludedCategories) {
    var categories = <String>[];
    for (var item in items) {
      for (var excludedCategory in excludedCategories) {
        if (item.category.toLowerCase() != excludedCategory.toLowerCase()) {
          if (!categories.contains(item.category)) {
            categories.add(item.category);
          }
        }
      }
    }
    return categories;
  }

  ///Returns a list of all [items] with the [category] "information"
  static List<CafeteriaItem> filterInformationItems(List<CafeteriaItem> items) {
    var informationItemList = <CafeteriaItem>[];
    for (var item in items) {
      if (item.category.toLowerCase().trim() == "information") {
        informationItemList.add(item);
      }
    }
    return informationItemList;
  }

  static Map<String, List<CafeteriaItem>> _matchItemCategories(
    List<CafeteriaItem> items,
    List<String> categoryNames,
  ) {
    var categories = <String, List<CafeteriaItem>>{};
    for (var category in categoryNames) {
      categories.putIfAbsent(category,
          () => items.where((item) => item.category == category).toList());
    }
    return categories;
  }
}
