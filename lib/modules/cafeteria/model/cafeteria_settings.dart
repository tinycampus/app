/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cafeteria_settings.g.dart';

///Settings for the CafeteriaModule
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class CafeteriaSettings extends Equatable {
  /// bool that remembers that the User has gone through the Wizard
  /// or skipped it
  /// will also be false if no [Cafeteria] has been selected
  final bool hasDoneWizard;

  /// an Enum to determine if the User wants to see prices for
  /// student, staff or guest
  final PriceClass priceClass;

  /// an Enum to determine if the User is vegan, vegetarian or none
  /// of the previous
  final CafeteriaFilter filter;
  final List<String> favorites;

  /// A [List] of allergens the User has decided to be warned about
  final List<Allergens> allergens;

  /// bool to switch to fallback API for debug purpose in case
  /// external API fails
  final bool showTestApi;

  /// A List of [dislikedIngredients] the [User] wants to avoid.
  /// List may contain: Fish, Poultry, Lamb, Beef, Pork,
  /// Alcohol, Gelatin, Spicy, Garlic
  final List<DislikedIngredients> dislikedIngredients;

  /// Default constructor
  CafeteriaSettings({
    this.hasDoneWizard = false,
    this.priceClass = PriceClass.student,
    this.favorites = const [],
    this.filter = CafeteriaFilter.none,
    this.allergens = const [],
    this.dislikedIngredients = const [],
    this.showTestApi = false,
  });

  /// Constructor that creates Settings from other Settings (redundant?)
  CafeteriaSettings.fromSettings(CafeteriaSettings settings)
      : hasDoneWizard = settings.hasDoneWizard,
        priceClass = settings.priceClass,
        favorites = settings.favorites,
        filter = settings.filter,
        allergens = settings.allergens,
        dislikedIngredients = settings.dislikedIngredients,
        showTestApi = settings.showTestApi;

  /// Initial constructor
  factory CafeteriaSettings.initialSettings() => CafeteriaSettings();

  factory CafeteriaSettings.fromJson(Map<String, dynamic> json) =>
      _$CafeteriaSettingsFromJson(json);

  Map<String, dynamic> toJson() => _$CafeteriaSettingsToJson(this);

  @override
  List<Object> get props => [
        hasDoneWizard,
        priceClass,
        favorites,
        filter,
        allergens,
        dislikedIngredients,
        showTestApi,
      ];
}

///Enum class for various filters
enum CafeteriaFilter { none, vegetarian, vegan }

/// Enum class for price classes
enum PriceClass { student, staff, guest }

/// Enum class for allergens
enum Allergens {
  gluten,
  crustacean,
  egg,
  fish,
  peanut,
  soy,
  milk,
  nuts,
  celery,
  mustard,
  sesame,
  sulfur,
  lupines,
  cthulu
}

/// Enum class for ingredients the User wants to avoid
enum DislikedIngredients {
  fish,
  poultry,
  lamb,
  beef,
  pork,
  alcohol,
  gelatin,
  spicy,
  garlic
}

/// A getter for localized Strings for each allergen
extension AllergenNames on Allergens {
  String get name {
    switch (this) {
      case Allergens.gluten:
        return 'modules.cafeteria.settings.allergens_list.gluten';
      case Allergens.crustacean:
        return 'modules.cafeteria.settings.allergens_list.crustacean';
      case Allergens.egg:
        return 'modules.cafeteria.settings.allergens_list.egg';
      case Allergens.fish:
        return 'modules.cafeteria.settings.allergens_list.fish';
      case Allergens.peanut:
        return 'modules.cafeteria.settings.allergens_list.peanut';
      case Allergens.soy:
        return 'modules.cafeteria.settings.allergens_list.soy';
      case Allergens.milk:
        return 'modules.cafeteria.settings.allergens_list.milk';
      case Allergens.nuts:
        return 'modules.cafeteria.settings.allergens_list.nuts';
      case Allergens.celery:
        return 'modules.cafeteria.settings.allergens_list.celery';
      case Allergens.mustard:
        return 'modules.cafeteria.settings.allergens_list.mustard';
      case Allergens.sesame:
        return 'modules.cafeteria.settings.allergens_list.sesame';
      case Allergens.sulfur:
        return 'modules.cafeteria.settings.allergens_list.sulfur';
      case Allergens.lupines:
        return 'modules.cafeteria.settings.allergens_list.lupines';
      case Allergens.cthulu:
        return 'modules.cafeteria.settings.allergens_list.cthulu';
      default:
        return "";
    }
  }

  /// A getter for IDs for each allergen
  String get id {
    switch (this) {
      case Allergens.gluten:
        return "30";
      case Allergens.crustacean:
        return "31";
      case Allergens.egg:
        return "32";
      case Allergens.fish:
        return "33";
      case Allergens.peanut:
        return "34";
      case Allergens.soy:
        return "35";
      case Allergens.milk:
        return "36";
      case Allergens.nuts:
        return "37";
      case Allergens.celery:
        return "38";
      case Allergens.mustard:
        return "39";
      case Allergens.sesame:
        return "40";
      case Allergens.sulfur:
        return "41";
      case Allergens.lupines:
        return "42";
      case Allergens.cthulu:
        return "43";
      default:
        return "";
    }
  }
}

/// A getter for localized Strings for price classes
extension PriceClassNames on PriceClass {
  String get name {
    switch (this) {
      case PriceClass.student:
        return 'modules.cafeteria.prices.students';
      case PriceClass.staff:
        return 'modules.cafeteria.prices.employees';
      case PriceClass.guest:
        return 'modules.cafeteria.prices.guests';
      default:
        return "";
    }
  }
}

/// A getter for localized Strings for filters
extension CafeteriaFilterNames on CafeteriaFilter {
  String get name {
    switch (this) {
      case CafeteriaFilter.none:
        return 'modules.cafeteria.settings.filters.no_filter';
      case CafeteriaFilter.vegetarian:
        return 'modules.cafeteria.settings.filters.vegetarian';
      case CafeteriaFilter.vegan:
        return 'modules.cafeteria.settings.filters.vegan';
      default:
        return "";
    }
  }
}

/// A getter for localized Strings for disliked ingredients
extension DislikedIngredientNames on DislikedIngredients {
  String get name {
    switch (this) {
      case DislikedIngredients.fish:
        return 'modules.cafeteria.settings.disliked_ingredients.fish';
      case DislikedIngredients.poultry:
        return 'modules.cafeteria.settings.disliked_ingredients.poultry';
      case DislikedIngredients.lamb:
        return 'modules.cafeteria.settings.disliked_ingredients.lamb';
      case DislikedIngredients.beef:
        return 'modules.cafeteria.settings.disliked_ingredients.beef';
      case DislikedIngredients.pork:
        return 'modules.cafeteria.settings.disliked_ingredients.pork';
      case DislikedIngredients.alcohol:
        return 'modules.cafeteria.settings.disliked_ingredients.alcohol';
      case DislikedIngredients.gelatin:
        return 'modules.cafeteria.settings.disliked_ingredients.gelatin';
      case DislikedIngredients.spicy:
        return 'modules.cafeteria.settings.disliked_ingredients.spicy';
      case DislikedIngredients.garlic:
        return 'modules.cafeteria.settings.disliked_ingredients.garlic';
      default:
        return "";
    }
  }

  /// A getter for abbreviations for ingredients
  String get abbreviation {
    switch (this) {
      case DislikedIngredients.fish:
        return "F";
      case DislikedIngredients.poultry:
        return "G";
      case DislikedIngredients.lamb:
        return "L";
      case DislikedIngredients.beef:
        return "R";
      case DislikedIngredients.pork:
        return "S";
      case DislikedIngredients.alcohol:
        return "A";
      case DislikedIngredients.gelatin:
        return "GL";
      case DislikedIngredients.spicy:
        return "HOT";
      case DislikedIngredients.garlic:
        return "KNO";
      default:
        return "";
    }
  }
}
