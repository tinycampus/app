// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_item_comment.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CafeteriaItemCommentCopyWith on CafeteriaItemComment {
  CafeteriaItemComment copyWith({
    String? author,
    int? authorId,
    bool? canEdit,
    String? createdAt,
    int? id,
    bool? isReported,
    bool? liked,
    int? likes,
    String? text,
  }) {
    return CafeteriaItemComment(
      author: author ?? this.author,
      authorId: authorId ?? this.authorId,
      canEdit: canEdit ?? this.canEdit,
      createdAt: createdAt ?? this.createdAt,
      id: id ?? this.id,
      isReported: isReported ?? this.isReported,
      liked: liked ?? this.liked,
      likes: likes ?? this.likes,
      text: text ?? this.text,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CafeteriaItemComment _$CafeteriaItemCommentFromJson(Map json) =>
    CafeteriaItemComment(
      authorId: json['authorId'] as int? ?? -1,
      author: json['author'] as String,
      createdAt: json['createdAt'] as String,
      id: json['id'] as int,
      liked: json['liked'] as bool,
      likes: json['likes'] as int,
      text: json['text'] as String,
      canEdit: json['canEdit'] as bool? ?? false,
      isReported: json['reported'] as bool? ?? false,
    );

Map<String, dynamic> _$CafeteriaItemCommentToJson(
        CafeteriaItemComment instance) =>
    <String, dynamic>{
      'authorId': instance.authorId,
      'author': instance.author,
      'createdAt': instance.createdAt,
      'id': instance.id,
      'liked': instance.liked,
      'likes': instance.likes,
      'text': instance.text,
      'canEdit': instance.canEdit,
      'reported': instance.isReported,
    };
