// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cafeteria _$CafeteriaFromJson(Map json) => Cafeteria(
      building:
          Building.fromJson(Map<String, dynamic>.from(json['building'] as Map)),
      items: (json['items'] as List<dynamic>)
          .map((e) =>
              CafeteriaItem.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$CafeteriaToJson(Cafeteria instance) => <String, dynamic>{
      'building': instance.building.toJson(),
      'items': instance.items.map((e) => e.toJson()).toList(),
    };
