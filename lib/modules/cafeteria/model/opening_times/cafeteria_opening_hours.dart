/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cafeteria_opening_hours.g.dart';

/// A model for opening times for a [Cafeteria]
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class CafeteriaOpeningHours {
  /// The id for the [Cafeteria]
  @JsonKey(defaultValue: 0)
  final int cafeteriaId;

  /// the name of the [Cafeteria]
  @JsonKey(defaultValue: "")
  final String name;

  /// The eventual news to be displayed about the [Cafeteria]
  @JsonKey(defaultValue: "")
  final String news;

  /// The description for the [Cafeteria]
  @JsonKey(defaultValue: "")
  final String description;

  /// The image path for the [Cafeteria] image
  @JsonKey(defaultValue: "")
  final String image;

  /// The latitude for the [Cafeteria] coordinates
  @JsonKey(defaultValue: "")
  final String latitude;

  /// The longitude for the [Cafeteria] coordinates
  @JsonKey(defaultValue: "")
  final String longitude;

  /// a [Map] that, for each weekday, holds a [List] of [Map]
  /// The [List] holds pairs of "opens:" and "closes:" with their respective
  /// time
  @JsonKey(defaultValue: {})
  final Map<String, List<Map<String, String>>> openingHours;

  /// a [Map] that, for each weekday, holds a [List] of [Map]
  /// The [List] holds pairs of "opens:" and "closes:" with their respective
  /// time
  @JsonKey(defaultValue: {})
  final Map<String, List<Map<String, String>>> eatingHours;

  /// A [Map] that holds days in which this [Cafeteria] is closed
  @JsonKey(defaultValue: [])
  final List<Map<String, String>> closures;

  /// Default constructor
  CafeteriaOpeningHours({
    required this.cafeteriaId,
    required this.name,
    required this.news,
    required this.description,
    required this.image,
    required this.latitude,
    required this.longitude,
    required this.openingHours,
    required this.eatingHours,
    required this.closures,
  });

  /// Fallback empty constructor
  CafeteriaOpeningHours.empty()
      : cafeteriaId = 0,
        name = "",
        news = "",
        description = "",
        image = "",
        latitude = "",
        longitude = "",
        openingHours = {},
        eatingHours = {},
        closures = [];

  factory CafeteriaOpeningHours.fromJson(Map<String, dynamic> json) =>
      _$CafeteriaOpeningHoursFromJson(json);

  Map<String, dynamic> toJson() => _$CafeteriaOpeningHoursToJson(this);
}
