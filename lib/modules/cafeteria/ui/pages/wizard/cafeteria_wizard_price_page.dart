/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../model/cafeteria_settings.dart';
import '../../widgets/settings_widget.dart';
import '../../widgets/wizard_example_card.dart';

/// A wizard page that lets the user chose which price class is to be displayed
/// e.g. "Student", "Staff" and "Guest"
class CafeteriaWizardPricePage extends StatefulWidget {
  final ValueChanged<CafeteriaSettings> onSettingsChanged;
  final CafeteriaSettings settings;

  const CafeteriaWizardPricePage({
    Key? key,
    required this.onSettingsChanged,
    required this.settings,
  }) : super(key: key);

  @override
  _CafeteriaWizardPricePageState createState() =>
      _CafeteriaWizardPricePageState();
}

class _CafeteriaWizardPricePageState extends State<CafeteriaWizardPricePage> {
  @override
  Widget build(BuildContext context) => ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ConstrainedBox(
                  child: WizardExampleCard(
                    category: Category.price,
                    settings: widget.settings,
                  ),
                  constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width / 2 - 10,
                    maxHeight: MediaQuery.of(context).size.width / 2 - 10,
                  ),
                ),
              ],
            ),
          ),
          SettingsWidget(
            settings: widget.settings,
            title: 'modules.cafeteria.wizard.price_category',
            type: SettingsType.price,
            onSettingsChanged: (_settings) =>
                widget.onSettingsChanged(_settings),
            children: PriceClass.values.toList(),
          ),
        ],
      );
}
