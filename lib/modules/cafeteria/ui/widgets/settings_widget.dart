/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../model/cafeteria.dart';
import '../../model/cafeteria_settings.dart';

/// A Widget that lets the user select their [CafeteriaSettings]
/// according to [SettingsType]
class SettingsWidget extends StatefulWidget {
  final CafeteriaSettings settings;
  final String title;
  final SettingsType type;
  final List<dynamic> children;
  final ValueChanged<dynamic> onSettingsChanged;

  SettingsWidget({
    Key? key,
    required this.title,
    required this.type,
    required this.onSettingsChanged,
    required this.children,
    required CafeteriaSettings settings,
  })  : settings = CafeteriaSettings.fromSettings(settings),
        super(key: key);

  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends State<SettingsWidget> {
  int _num = 0;

  @override
  Widget build(BuildContext context) {
    Widget Function(String, int) buildMethod;
    List<String> childNames;
    switch (widget.type) {
      case SettingsType.cafeteria:
        buildMethod = _buildCafeteriaSelectElement;

        childNames =
            widget.children.map((e) => (e as Cafeteria).building.name).toList();
        break;
      case SettingsType.price:
        buildMethod = _buildPriceElement;
        childNames = PriceClass.values
            .map((e) => FlutterI18n.translate(context, e.name))
            .toList();
        break;
      case SettingsType.filter:
        buildMethod = _buildFilterElement;
        childNames = CafeteriaFilter.values
            .map((e) => FlutterI18n.translate(context, e.name))
            .toList();
        break;
      case SettingsType.allergens:
        buildMethod = _buildAllergensElement;
        childNames = Allergens.values
            .map((e) => FlutterI18n.translate(context, e.name))
            .toList();
        break;
      case SettingsType.disliked:
        buildMethod = _buildDislikedIngredientsElement;
        childNames = DislikedIngredients.values
            .map((e) => FlutterI18n.translate(context, e.name))
            .toList();
        break;
    }

    ///We return a Form Field in hopes to later use a Form and Validation
    return FormField(
      builder: (context) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Container(
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 20,
                    ),
                    child: I18nText(
                      widget.title,
                      child: Text(
                        widget.title,
                        style: TextStyle(
                          color: CorporateColors.tinyCampusBlue,
                          fontSize: 18,
                        ),
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ),
                ],
              ),
              for (int i = 0; i < widget.children.length; i++)
                buildMethod(
                  childNames[i],
                  i,
                ),
            ],
          ),
        ),
      ),
    );
  }

  /// A Widget that lets the user select diet options
  Widget _buildFilterElement(String name, int index) =>
      RadioListTile<CafeteriaFilter>(
        value: widget.children[index],
        controlAffinity: ListTileControlAffinity.trailing,
        groupValue: BlocProvider.of<CafeteriaSettingsBloc>(context)
            .state
            .settings
            .filter,
        onChanged: (value) =>
            widget.onSettingsChanged(widget.settings.copyWith(filter: value)),
        title: Text(name),
      );

  /// A Widget that lets the user select price options
  Widget _buildPriceElement(String name, int index) =>
      RadioListTile<PriceClass>(
        value: widget.children[index],
        controlAffinity: ListTileControlAffinity.trailing,
        groupValue: BlocProvider.of<CafeteriaSettingsBloc>(context)
            .state
            .settings
            .priceClass,
        onChanged: (value) {
          widget.onSettingsChanged(widget.settings.copyWith(priceClass: value));
        },
        title: Text(name),
      );

  /// A Widget that lets the user select allergen options
  Widget _buildAllergensElement(String name, int index) {
    var _value = BlocProvider.of<CafeteriaSettingsBloc>(context)
        .state
        .settings
        .allergens
        .contains(widget.children[index]);
    return CheckboxListTile(
      value: _value,
      onChanged: (checked) {
        if (widget.children[index] == Allergens.cthulu) _num++;

        if (_num == 8) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text('Praise!')));
        }

        final newAllergens = widget.settings.allergens.toList();

        if (checked ?? false) {
          newAllergens.add(widget.children[index]);
        } else {
          newAllergens.remove(widget.children[index]);
        }

        widget.onSettingsChanged(
          widget.settings.copyWith(allergens: newAllergens),
        );
      },
      title: Text(name),
    );
  }

  /// A Widget that lets the user select ingredients to avoid
  Widget _buildDislikedIngredientsElement(String name, int index) {
    var _value = BlocProvider.of<CafeteriaSettingsBloc>(context)
        .state
        .settings
        .dislikedIngredients
        .contains(widget.children[index]);
    return CheckboxListTile(
      value: _value,
      onChanged: (checked) {
        final newDisliked = widget.settings.dislikedIngredients.toList();

        if (checked ?? false) {
          newDisliked.add(widget.children[index]);
        } else {
          newDisliked.remove(widget.children[index]);
        }

        widget.onSettingsChanged(
          widget.settings.copyWith(dislikedIngredients: newDisliked),
        );
      },
      title: Text(name),
    );
  }

  /// A helper function to check if a [cafeteriaList] contains a [cafeteria]
  bool _containsChild(List<String> cafeteriaList, String cafeteria) {
    var contains = false;
    for (var element in cafeteriaList) {
      if (element == cafeteria) {
        contains = true;
      }
    }
    return contains;
  }

  /// A function that returns a [CheckboxListTile] for each [Cafeteria]
  Widget _buildCafeteriaSelectElement(String name, int index) {
    final cafeteriaName = (widget.children[index] as Cafeteria).building.name;
    var _value = _containsChild(
      BlocProvider.of<CafeteriaSettingsBloc>(context).state.cafeteriaSelection,
      cafeteriaName,
    );
    var _tempList = [
      ...BlocProvider.of<CafeteriaSettingsBloc>(context)
          .state
          .cafeteriaSelection,
    ];

    return CheckboxListTile(
      value: _value,
      onChanged: (checked) {
        debugPrint('$index');

        if (checked ?? false) {
          _tempList.add(cafeteriaName);
        } else {
          _tempList.removeWhere(
            (cafeteria) => cafeteria == cafeteriaName,
          );
        }

        widget.onSettingsChanged(_tempList);
      },
      title: Text(name),
    );
  }
}

/// An enum class for the different types of settings
enum SettingsType { cafeteria, price, filter, allergens, disliked }
