/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../model/cafeteria_item.dart';
import '../../model/cafeteria_settings.dart';

/// A Widget that contains the prices for a [CafeteriaItem] for the
/// price classes of student, staff and guest
class CafeteriaItemPriceRow extends StatelessWidget {
  final CafeteriaItem item;
  final CafeteriaSettings settings;

  const CafeteriaItemPriceRow({
    Key? key,
    required this.item,
    required this.settings,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                Text(
                  _formatPrice(item.prices.student),
                  style: TextStyle(
                    color: settings.priceClass == PriceClass.student
                        ? Colors.black
                        : Colors.grey,
                    fontWeight: FontWeight.w300,
                    fontSize: 24.0,
                  ),
                ),
                I18nText(
                  'modules.cafeteria.items.students',
                  child: Text(
                    'Studierende',
                    style: TextStyle(
                      color: CorporateColors.tinyCampusDarkBlue,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                Text(
                  _formatPrice(item.prices.staff),
                  style: TextStyle(
                    color: settings.priceClass == PriceClass.staff
                        ? Colors.black
                        : Colors.grey,
                    fontWeight: FontWeight.w300,
                    fontSize: 24.0,
                  ),
                ),
                I18nText(
                  'modules.cafeteria.items.employees',
                  child: Text(
                    'Mitarbeiter*innen',
                    style: TextStyle(
                      color: CorporateColors.tinyCampusDarkBlue,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                Text(
                  _formatPrice(item.prices.guest),
                  style: TextStyle(
                    color: settings.priceClass == PriceClass.guest
                        ? Colors.black
                        : Colors.grey,
                    fontWeight: FontWeight.w300,
                    fontSize: 24.0,
                  ),
                ),
                I18nText(
                  'modules.cafeteria.items.guests',
                  child: Text(
                    'Gäste',
                    style: TextStyle(
                      color: CorporateColors.tinyCampusDarkBlue,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );

  String _formatPrice(String rawPrice) =>
      rawPrice == 'k.A.' ? rawPrice : '$rawPrice €';
}
