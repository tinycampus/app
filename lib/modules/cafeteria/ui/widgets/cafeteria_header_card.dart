/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../bloc/cafeteria_opening/cafeteria_opening_bloc.dart';
import '../../model/cafeteria_item.dart';
import 'details/detail_opening_row.dart';
import 'header_card/header_information_block.dart';
import 'header_card/header_opening_widget.dart';

///A widget which displays basic information about a Cafeteria
class CafeteriaHeaderCard extends StatelessWidget {
  /// name of the [Cafeteria]
  final String name;

  /// a [List] of [informationItems] that will be displayed below
  /// the [CafeteriaHeaderCard]
  final List<CafeteriaItem> informationItems;

  /// a [List] of [eatingHours] that contains opening and closing times for
  /// when food is being sold in the [Cafeteria]
  final List<Map<String, String>> eatingHours;

  /// a [List] of [closures] that contains days in which
  /// the [Cafeteria] is not open
  final List<Map<String, String>> closures;
  final bool isToday;
  final int weekDay;

  const CafeteriaHeaderCard({
    Key? key,
    required this.name,
    required this.informationItems,
    this.eatingHours = const [],
    this.closures = const [],
    this.isToday = false,
    required this.weekDay,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaOpeningBloc, CafeteriaOpeningState>(
        buildWhen: (prevState, currState) =>
            currState.runtimeType != prevState.runtimeType,
        builder: (context, state) => Card(
          elevation: 3,
          margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 12.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(12),
              onTap: () {
                Navigator.of(context).pushNamed(
                  cafeteriaDetailRoute,
                  arguments: name,
                );
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: [
                        I18nText(
                          'modules.cafeteria.header.disclaimer',
                          child: Text('',
                              style: TextStyle(
                                  color: CorporateColors.tinyCampusIconGrey,
                                  fontSize: 12.0)),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Text(
                                name,
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.headline2,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 16.0),
                          child: OpeningWidget(
                            isOpen: _isOpen(closures, eatingHours,
                                isToday: isToday),
                            isLoaded: state.runtimeType == LoadedOpeningState,
                            eatingHours: eatingHours,
                            nearestClosingTime: _getNearestClosingTime(
                              eatingHours,
                              context,
                            ),
                            nearestOpeningTime: _getNearestOpeningTime(
                              eatingHours,
                              context,
                            ),
                          ),
                        ),
                        if (state.runtimeType == LoadedOpeningState)
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 5,
                              horizontal: 15,
                            ),
                            child: DetailOpeningRow(
                              cafeteriaName: name,
                              weekDay: _getWeekDay(weekDay),
                              showBorderTimes: true,
                              isToday: isToday,
                            ),
                          ),
                      ],
                    ),
                    CafeteriaHeaderInformationBlock(
                      informations: informationItems,
                      name: name,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );

  /// Determines whether the [Cafeteria] is currently open
  bool _isOpen(
    List<Map<String, String>> closures,
    List<Map<String, String>> eatingHours, {
    bool isToday = false,
  }) {
    try {
      if (eatingHours.isEmpty) return false;
      if (!isToday) return false;

      var now = DateTime.now();

      for (var closure in closures) {
        var from = closure['from'] ?? '';
        var to = closure['to'] ?? '';

        if (DateFormat('yyyy-MM-dd').parse(from).difference(now).inDays <= 0 &&
            DateFormat('yyyy-MM-dd').parse(to).difference(now).inDays >= 0) {
          return false;
        }
      }

      for (var time in eatingHours) {
        var open =
            TimeOfDay.fromDateTime(DateFormat.Hm().parse(time['open'] ?? ''));
        var close =
            TimeOfDay.fromDateTime(DateFormat.Hm().parse(time['close'] ?? ''));
        if ((toDouble(open) - toDouble(TimeOfDay.fromDateTime(now))) <= 0 &&
            (toDouble(close) - toDouble(TimeOfDay.fromDateTime(now))) >= 0) {
          return true;
        }
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
    }

    return false;
  }

  String _getWeekDay(int day) {
    switch (day) {
      case 1:
        return 'monday';
      case 2:
        return 'tuesday';
      case 3:
        return 'wednesday';
      case 4:
        return 'thursday';
      case 5:
        return 'friday';
      case 6:
        return 'saturday';
      case 7:
        return 'sunday';
      default:
        return '';
    }
  }

  /// a function that returns the amount of time that has passed in
  /// a day as double
  double toDouble(TimeOfDay myTime) => myTime.hour + myTime.minute / 60.0;

  /// a function that returns the nearest closing time for the day
  String _getNearestClosingTime(
    List<Map<String, String>> eatingHours,
    BuildContext context,
  ) {
    final now = toDouble(TimeOfDay.fromDateTime(DateTime.now()));
    var result = '';
    var diff = 25.0;

    for (var time in eatingHours) {
      var close =
          TimeOfDay.fromDateTime(DateFormat.Hm().parse(time['close'] ?? ''));

      if ((toDouble(close) - now).abs() <= diff) {
        diff = (toDouble(close) - now).abs();
        result = DateFormat.jm(
          FlutterI18n.currentLocale(context)?.languageCode ?? 'de',
        ).format(DateFormat.Hm().parse(time['close'] ?? ''));
      }
    }

    return result;
  }

  /// a function that returns the nearest opening time for the day.
  /// If there are multiple opening times, it will return either
  /// the first one that is in the future or if there isn't one in the
  /// future, it will return the earliest opening time for that day
  String _getNearestOpeningTime(
    List<Map<String, String>> eatingHours,
    BuildContext context,
  ) {
    if (eatingHours.isEmpty) {
      return FlutterI18n.translate(context, 'modules.cafeteria.header.unknown');
    }

    final now = toDouble(TimeOfDay.fromDateTime(DateTime.now()));
    final lang = FlutterI18n.currentLocale(context)?.languageCode ?? 'de';
    var result = DateFormat.jm(lang)
        .format(DateFormat.Hm().parse(eatingHours[0]['open'] ?? ''));

    for (var time in eatingHours) {
      var open =
          TimeOfDay.fromDateTime(DateFormat.Hm().parse(time['open'] ?? ''));

      if ((toDouble(open) - now) > 0) {
        result = DateFormat.jm(lang)
            .format(DateFormat.Hm().parse(time['open'] ?? ''));
      }
    }

    return result;
  }
}
