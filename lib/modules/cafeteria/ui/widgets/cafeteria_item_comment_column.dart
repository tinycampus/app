/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';
import '../../model/cafeteria_item.dart';
import 'cafeteria_item_comment_entry.dart';

/// A Widget that contains all [CafeteriaItemCommentEntry]s for
/// a [CafeteriaItem]
class CafeteriaCommentColumn extends StatelessWidget {
  const CafeteriaCommentColumn({
    Key? key,
    required this.name,
    required this.item,
  }) : super(key: key);

  final String name;
  final CafeteriaItem item;

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaFeedbackBloc, CafeteriaFeedbackState>(
        builder: (context, state) {
          if (state.itemFeedback.comments.isEmpty) {
            return Container(height: 1.0);
          }

          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              children: <Widget>[
                ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  // Add one entry, as items and separators are swapped
                  itemCount: state.itemFeedback.comments.length + 1,
                  separatorBuilder: (context, index) =>
                      CafeteriaItemCommentEntry(
                    cafeteriaName: name,
                    item: item,
                    comment: state.itemFeedback.comments[index],
                    itemId: item.getUniqueId(cafeteriaName: name),
                  ),
                  itemBuilder: (_, __) => Container(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                  ),
                ),
              ],
            ),
          );
        },
      );
}
