/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../../common/tc_theme.dart';
import '../../../../common/tinycampus_icons.dart';
import '../../../../common/widgets/dialog/tc_dialog.dart';
import '../../../../common/widgets/profile/svg_profile_widget.dart';
import '../../../../common/widgets/reporting/popup_report_button.dart';
import '../../../../common/widgets/reporting/reported_blur_wrapper.dart';
import '../../api_helper.dart';
import '../../bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';
import '../../model/cafeteria_item.dart';
import '../../model/cafeteria_item_comment.dart';

/// A Widget that takes [CafeteriaItemComment] data, the [itemId] of the
/// [CafeteriaItem] it belongs to, the [item] itself and the [cafeteriaName]
/// of the [Cafeteria] the item is served at
/// It displays the [CafeteriaItemComment] along with its likable component
class CafeteriaItemCommentEntry extends StatelessWidget {
  final String itemId;
  final CafeteriaItemComment _comment;
  final CafeteriaItem item;
  final String cafeteriaName;

  const CafeteriaItemCommentEntry({
    Key? key,
    required CafeteriaItemComment comment,
    required this.itemId,
    required this.item,
    required this.cafeteriaName,
  })  : _comment = comment,
        super(key: key);

  @override
  Widget build(BuildContext context) => ReportedBlurWrapper(
        shouldBlur: _comment.isReported,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      SvgProfileWidget(radius: 16, svgString: _comment.author),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 12.0),
                          child: Text(
                            _comment.author,
                            textAlign: TextAlign.start,
                            style: Theme.of(context).textTheme.headline3,
                          ),
                        ),
                      ),
                      if (_comment.canEdit)
                        GestureDetector(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Icon(
                                TinyCampusIcons.trash,
                                color: CorporateColors.tinyCampusIconGrey
                                    .withOpacity(0.5),
                                size: 20,
                              ),
                            ),
                            onTap: () {
                              TCDialog.showCustomDialog(
                                  context: context,
                                  functionActionColor:
                                      CorporateColors.cafeteriaCautionRed,
                                  onConfirm: () {
                                    BlocProvider.of<CafeteriaFeedbackBloc>(
                                            context)
                                        .add(
                                      DeleteCommentEvent(
                                        concId: item.getUniqueId(
                                            cafeteriaName: cafeteriaName),
                                        id: _comment.id,
                                      ),
                                    );
                                  },
                                  functionActionText: FlutterI18n.translate(
                                      context, 'common.actions.delete'),
                                  headlineText: FlutterI18n.translate(
                                      context,
                                      'modules.cafeteria.'
                                      'items.feedback.delete_comment_headline'),
                                  bodyText: FlutterI18n.translate(
                                      context,
                                      'modules.cafeteria.'
                                      'items.feedback.'
                                      'delete_comment_description'));
                            }),
                      if (!_comment.canEdit) ...[
                        PopupReportButton(
                          reportPostRoute: reportCommentByIdUrl(_comment.id),
                          userId: _comment.authorId,
                        ),
                      ],
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 5,
                      bottom: 5,
                      right: 8,
                    ),
                    child: Text(
                      _comment.text,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          timeago.format(
                            DateTime.parse(_comment.createdAt),
                            locale: FlutterI18n.currentLocale(context)
                                    ?.languageCode ??
                                'de',
                          ),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              ?.copyWith(
                                  color: CorporateColors.tinyCampusIconGrey),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text(
                              '${_comment.likes}',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.grey,
                              ),
                            ),
                            IconButton(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 4),
                              icon: Icon(
                                _comment.liked
                                    ? TinyCampusIcons.heart
                                    : TinyCampusIcons.heartOutline,
                                size: _comment.liked ? 24 : 20,
                              ),
                              color: _comment.liked
                                  ? Colors.red
                                  : CorporateColors.tinyCampusIconGrey,
                              onPressed: () {
                                BlocProvider.of<CafeteriaFeedbackBloc>(context)
                                    .add(
                                  LikeChangeEvent(
                                    concId: item.getUniqueId(
                                        cafeteriaName: cafeteriaName),
                                    currentlyLiked: _comment.liked,
                                    id: _comment.id,
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}
