/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:catcher/catcher.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'bloc/tc_bloc_observer/tc_bloc_observer.dart';
import 'bloc/tc_bloc_provider.dart';
import 'common/asset_cacher.dart';
import 'common/catcher_config.dart' as catcher_config;
import 'common/constants/api_constants.dart';
import 'common/constants/localization_constants.dart' show supportedLocales;
import 'common/constants/routing_constants.dart';
import 'common/tc_theme.dart';
import 'localization/fallback_loaders.dart';
import 'localization/tc_localization_delegate.dart';
import 'navigation/router.dart' as router;
import 'notifications/status/status_notification_widget.dart';
import 'repositories/tc_repository_provider.dart';

/// Set global stuff, like i18n and orientation etc. .
Future<void> main() async {
  /// Wait for Flutter to initialize before executing platform calls.
  WidgetsFlutterBinding.ensureInitialized();

  /// Only allow portrait mode.
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  /// TODO: [1.a] Test how users react to this change
  // SystemChrome.setEnabledSystemUIOverlays([]);

  /// TODO: [1.b] Re-enable if users do not like the new approach
  /// Make design somewhat cleaner.
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle());

  /// Load initial Theme
  CurrentTheme();

  /// Check if a forcedLocale is set. If not
  final forcedLocale = await getForcedLocale();

  /// Let equatable override toString-methods globally.
  EquatableConfig.stringify = kDebugMode;

  /// Choose [TranslationLoader] based on execution mode.
  TranslationLoader translationLoader = kDebugMode
      ? FileTranslationLoaderJsonDecode.debug(forcedLocale: forcedLocale)
      : LocalFallbackNetworkLoader(
          baseUri: localizationUri, forcedLocale: forcedLocale);

  final translationDelegate = TcI18nDelegate(
    translationLoader: translationLoader,
    missingTranslationHandler: (key, locale) {
      try {
        throw LocalizationException(key, locale?.languageCode);
      } on LocalizationException catch (error, stackTrace) {
        // TODO: Time threshold!
        // Beware! In release, this could happen 1000 times in a second
        Catcher.reportCheckedError(error, stackTrace);
        debugPrint(error.toString());
      }
    },
  );

  // Needs only to  be set once
  timeago.setLocaleMessages('de', timeago.DeMessages());
  timeago.setLocaleMessages('en', timeago.EnMessages());

  // Load .env file with Sentry DSNs for Catcher
  await dotenv.load(fileName: 'assets/env/.env');

  /// Install our own [BlocObserver] for logging.
  Bloc.observer = TCBlocObserver();

  Catcher(
    navigatorKey: GlobalKey<NavigatorState>(),
    rootWidget: TcApp(translationDelegate: translationDelegate),
    debugConfig: catcher_config.getDebugConfig(),
    profileConfig: catcher_config.getProfileConfig(),
    releaseConfig: catcher_config.getReleaseConfig(),
  );
}

/// The root widget of our app.
/// This provides the global app state via [Bloc]- and [RepositoryProvider]s,
/// This also sets the global app theme via [TcTheme] and provides localization
/// for the app via [FlutterI18n]. Navigation is handled by the [router].
class TcApp extends StatelessWidget {
  final TcI18nDelegate translationDelegate;

  const TcApp({Key? key, required this.translationDelegate}) : super(key: key);

  @override
  Widget build(BuildContext context) => AssetCacher(
        child: TcTheme(
          childBuilder: (context, theme) => TCRepositoryProvider(
            child: TCBLoCProvider(
              child: MaterialApp(
                navigatorKey: Catcher.navigatorKey,
                title: 'tinyCampus',
                theme: theme,
                builder: (_, widget) {
                  Catcher.addDefaultErrorWidget(showStacktrace: true);
                  return Scaffold(
                    /// TODO: [1.a] Test how users react to this change
                    // resizeToAvoidBottomInset: true,
                    body: Stack(
                      children: [
                        if (widget != null) widget, // this is the main app
                        StatusNotificationWidget(),
                      ],
                    ),
                  );
                },
                initialRoute: mainRoute,
                onGenerateRoute: router.generateRoute,
                localizationsDelegates: [
                  translationDelegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                supportedLocales: supportedLocales,
              ),
            ),
          ),
        ),
      );
}
