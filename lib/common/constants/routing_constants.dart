/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

// Main route
const String mainRoute = '/';

// Login routes
const String loginRoute = '/login';
const String loginGreeterRoute = '/login/greeter';

// Bouncy Ball route
const String offlineGame = '/egg/offline_game';

// Home routes
const String homeTabRoute = '/home';
const String homeSettingsRoute = '/home/settings';

// Timeline routes
const String timelineTabRoute = '/timeline';
const String timelineSettingsRoute = '/timeline/settings';

// Profile routes
const String profileTabRoute = '/profile';
const String profileHeartsRoute = '/profile/hearts';

// Settings routes
const String settingsTabRoute = '/settings';
const String settingsLanguageRoute = '/settings/language';
const String settingsCreditsRoute = '/settings/credits';
const String settingsBrandInfoRoute = '/settings/brandInfo';
const String settingsDataDeletionRoute = '/settings/dataDeletion';
// TODO This route is currently unused
const String settingsDataUsageLiabilityRoute = '/settings/dataUsageLiability';

// Coming Soon route
const String comingSoonRoute = '/coming_soon';

// vCard routes
const String vCardModuleRoute = '/vcard';
const String vCardEditRoute = '/vcard/edit';
const String vCardQRCodeRoute = '/vcard/qrcode';
const String vCardContactScreenRoute = 'vcard/contact/screen';
const String vCardContactNoteRoute = 'vcard/contact/note';

// Q & A routes
const String qaModuleRoute = '/qanda';
const String qaAllQuestions = '/qanda/qanda_page';
const String qaAddCategories = '/qanda/qanda_categories';
const String qaAddSubcategories = '/qanda/qanda_subcategories';
const String qaViewQuestionRoute = '/qanda/question_view';
const String qaMyQuestionsRoute = '/qanda/my_questions';

// Organizer routes
const String orgaModuleRoute = '/organizer';
const String orgaConfigurationRoute = '/organizer/configuration';
const String orgaSelectProgramRoute = '/organizer/selectProgram';
const String orgaPoolConfigRoute = '/organizer/poolConfig';
const String orgaProgramHistoryRoute = '/organizer/programHistory';
const String orgaSubjectConfigRoute = '/organizer/subjectConfig';
const String orgaSearchModulesRoute = '/organizer/searchModules';
const String orgaSelectedModulesRoute = '/organizer/selectedModules';
const String orgaMultipleModulesExplanation =
    '/organizer/multiModulesExplanation';

// Cafeteria routes
const String cafeteriaModuleRoute = '/cafeteria';
const String cafeteriaSelectionRoute = '/cafeteria/selection';
const String cafeteriaOverviewRoute = '/cafeteria/overview';
const String cafeteriaDetailRoute = '/cafeteria/details';
const String cafeteriaSettingsRoute = '/cafeteria/settings';
const String cafeteriaWizardRoute = '/cafeteria/wizard';
const String cafeteriaItemRoute = '/cafeteria/item';

// Bulletin Board route
const String bulletinBoardRoute = '/bulletinboard';

// Semester Fee route
const String semesterFeeRoute = '/semesterfee';

// Events route
const String eventsRoute = '/events';

// Feedback route
const String feedbackRoute = '/feedback';

// Code of Conduct route
const String cocRoute = '/coc';

// Important Links route
const String importantLinksRoute = '/importantLinks';

// Practice Phase routes
const String practicePhaseRoute = '/pp';
const String ppAboutRoute = '$practicePhaseRoute/about';
const String ppEntryRoute = '$practicePhaseRoute/entry';
const String ppOverviewRoute = '$practicePhaseRoute/overview';
const String ppQuizRoute = '$practicePhaseRoute/quiz';
const String ppResultRoute = '$practicePhaseRoute/result';
const String ppSelectionRoute = '$practicePhaseRoute/selection';
const String ppSendDataRoute = '$practicePhaseRoute/sendData';
const String ppVerifyDataRoute = '$practicePhaseRoute/verifyData';
const String ppUnitRoute = '$practicePhaseRoute/unit';

// Zentrale Studienberatung route
const String zsRoute = '/zs';

// Hochschulsport route
const String sportRoute = '/sport';

/// bouncy ball
const String bbRoute = "/bb";
