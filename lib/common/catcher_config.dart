/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:catcher/catcher.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:sentry/sentry.dart';

import 'custom_preference_report_mode.dart';

final String _debugDsn = dotenv.env['SENTRY_DSN_DEBUG'] ?? '';
final String _releaseDsn = dotenv.env['SENTRY_DSN_RELEASE'] ?? '';

final ConsoleHandler _defaultConsoleHandler = ConsoleHandler(
  enableDeviceParameters: false,
  enableApplicationParameters: false,
);

SentryHandler _getSentryHandler(String dsn, {bool printLogs = false}) =>
    SentryHandler(SentryClient(SentryOptions(dsn: dsn)), printLogs: printLogs);

CatcherOptions getDebugConfig() => CatcherOptions(
      CustomPreferenceReportMode(),
      [
        _defaultConsoleHandler,
        if (_debugDsn.isNotEmpty) _getSentryHandler(_debugDsn),
      ],
    );

CatcherOptions getProfileConfig() => getDebugConfig();

CatcherOptions getReleaseConfig() => CatcherOptions(
      CustomPreferenceReportMode(),
      [
        if (_releaseDsn.isNotEmpty) _getSentryHandler(_releaseDsn),
      ],
    );
