/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../modules/organizer/ui/organizer/organizer_constants.dart';
import '../../tc_theme.dart';

class ReportedBlurWrapper extends StatefulWidget {
  final bool shouldBlur;
  final Widget child;

  const ReportedBlurWrapper({
    Key? key,
    required this.shouldBlur,
    required this.child,
  }) : super(key: key);

  @override
  _ReportedBlurWrapperState createState() => _ReportedBlurWrapperState();
}

class _ReportedBlurWrapperState extends State<ReportedBlurWrapper> {
  late bool isBlurred;

  @override
  void initState() {
    super.initState();
    isBlurred = widget.shouldBlur;
  }

  @override
  Widget build(BuildContext context) => !isBlurred
      ? widget.child
      : Stack(
          fit: StackFit.passthrough,
          alignment: Alignment.center,
          children: [
            widget.child,
            if (isBlurred) ...[
              Positioned.fill(
                child: ClipRect(
                  child: Container(
                    alignment: Alignment.center,
                    color: Theme.of(context).backgroundColor,
                  ),
                ),
              ),
              // TODO: maybe optimize later
              // Positioned.fill(
              //   child: ClipRect(
              //     child: BackdropFilter(
              //       filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
              //       child: Container(
              //         alignment: Alignment.center,
              //         color: Colors.white54,
              //       ),
              //     ),
              //   ),
              // ),
              Positioned.fill(
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () => setState(() => isBlurred = false),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            FlutterI18n.translate(
                              context,
                              'common.report.overlay.headline',
                            ),
                            textAlign: TextAlign.center,
                            style:
                                Theme.of(context).textTheme.headline2?.copyWith(
                                      fontWeight: FontWeight.w700,
                                    ),
                          ),
                          Text(
                            FlutterI18n.translate(
                              context,
                              'common.report.overlay.body',
                            ),
                            textAlign: TextAlign.center,
                            style:
                                Theme.of(context).textTheme.headline4?.copyWith(
                                      fontWeight: FontWeight.w400,
                                      color: CorporateColors.tinyCampusIconGrey,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ],
        );
}
