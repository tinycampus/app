/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';

class SvgProfileWidget extends StatelessWidget {
  const SvgProfileWidget({
    Key? key,
    this.radius = 12,
    required this.svgString,
  }) : super(key: key);

  final double radius;
  final String svgString;

  @override
  Widget build(BuildContext context) => ClipOval(
        clipBehavior: Clip.antiAlias,
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: radius,
          child: Transform.scale(
            scale: 1.15,
            child: SvgPicture.string(
              Jdenticon.toSvg(
                svgString,
                size: radius.toInt() * 2,
              ),
              fit: BoxFit.fitHeight,
              semanticsLabel: FlutterI18n.translate(
                  context,
                  'profile.'
                  'semantics.avatar'),
            ),
          ),
        ),
      );
}
