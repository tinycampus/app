/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import '../tc_theme.dart';

class TCPressableWrapper extends StatelessWidget {
  const TCPressableWrapper({
    Key? key,
    required this.widget,
    this.margin = const EdgeInsets.symmetric(vertical: 4.0, horizontal: 12.0),
    this.elevation = 4.0,
    this.borderRadius = 8.0,
  }) : super(key: key);

  final Widget widget;
  final EdgeInsets margin;
  final double elevation;
  final double borderRadius;

  @override
  Widget build(BuildContext context) => Container(
        margin: margin,
        child: Material(
          clipBehavior: Clip.antiAlias,
          borderRadius: BorderRadius.circular(borderRadius),
          color: CurrentTheme().themeData.primaryColor,
          elevation: elevation,
          shadowColor: CurrentTheme().textPassive.withOpacity(0.4),
          child: widget,
        ),
      );
}

class TCButton extends StatelessWidget {
  const TCButton({
    Key? key,
    this.onPressedCallback,
    this.buttonLabel = "Okay",
    this.foregroundColor,
    this.customFontStyle,
    this.backgroundColor,
    this.borderRadius = 8,
    this.borderThickness,
    this.elevation = 0,
    this.borderColor,
    this.alternativeWidget,
    this.customPadding,
    this.customHeight,
    this.noAnimationDuration = false,
    this.useAutoSizeText = false,
    this.buttonStyle = TCButtonStyle.filled,
  }) : super(key: key);

  final VoidCallback? onPressedCallback;
  final String buttonLabel;
  final Color? foregroundColor;
  final TextStyle? customFontStyle;
  final Color? backgroundColor;
  final double borderRadius;
  final double? borderThickness;
  final Color? borderColor;
  final double elevation;
  final Widget? alternativeWidget;
  final EdgeInsets? customPadding;
  final double? customHeight;
  final bool noAnimationDuration;
  final bool useAutoSizeText;
  final TCButtonStyle buttonStyle;

  @override
  Widget build(BuildContext context) {
    Color backgroundColor;
    double borderThickness;
    Color borderColor;
    Color foregroundColor;
    switch (buttonStyle) {
      case TCButtonStyle.filled:
        backgroundColor = this.backgroundColor ??
            CurrentTheme().themeData.colorScheme.secondary;
        borderThickness = this.borderThickness ?? 0.0;
        borderColor = this.borderColor ?? Colors.transparent;
        foregroundColor =
            this.foregroundColor ?? CurrentTheme().themeData.primaryColor;
        break;
      case TCButtonStyle.outline:
        backgroundColor =
            this.backgroundColor ?? CurrentTheme().themeData.primaryColor;
        borderThickness = this.borderThickness ?? 2.0;
        borderColor =
            this.borderColor ?? CurrentTheme().themeData.colorScheme.secondary;
        foregroundColor = this.foregroundColor ??
            CurrentTheme().themeData.colorScheme.secondary;
        break;
    }
    return Row(
      children: <Widget>[
        Expanded(
          child: RaisedButton(
            padding: customPadding,
            animationDuration: noAnimationDuration ? Duration.zero : null,
            elevation: elevation,
            disabledColor: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
            child: Container(
              height: customHeight,
              padding: customPadding ?? EdgeInsets.all(16.0),
              child: alternativeWidget ??
                  Center(
                    child: useAutoSizeText
                        ? AutoSizeText(
                            buttonLabel,
                            minFontSize: 8,
                            style: customFontStyle ??
                                Theme.of(context).textTheme.headline3?.copyWith(
                                      color: foregroundColor,
                                      letterSpacing: -0.4,
                                    ),
                            textAlign: TextAlign.center,
                          )
                        : Text(
                            buttonLabel,
                            style: customFontStyle ??
                                Theme.of(context).textTheme.headline3?.copyWith(
                                      color: foregroundColor,
                                      letterSpacing: -0.4,
                                    ),
                            textAlign: TextAlign.center,
                          ),
                  ),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              side: BorderSide(
                color: borderColor,
                width: borderThickness,
              ),
            ),
            color: this.backgroundColor ?? backgroundColor,
            onPressed: onPressedCallback,
          ),
        ),
      ],
    );
  }
}

enum TCButtonStyle {
  filled,
  outline,
}
