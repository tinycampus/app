/*
 * Copyright 2020-2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../modules/organizer/ui/organizer/o_drawer/i18n_common.dart';
import '../tc_theme.dart';

// TODO: Cleanup
class TcPopupWidgets {
  static PopupMenuItem<T> buildTextPopupItem<T>({
    required String title,
    required T value,
    String? subtitle,
    TextStyle? titleStyle,
    TextStyle? subtitleStyle,
    int titleMaxLines = 2,
    int subtitleMaxLines = 2,
  }) =>
      PopupMenuItem<T>(
        value: value,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              textAlign: TextAlign.start,
              style: titleStyle ?? CurrentTheme().themeData.textTheme.bodyText1,
              maxLines: titleMaxLines,
              overflow: TextOverflow.ellipsis,
            ),
            if (subtitle != null)
              Text(
                subtitle,
                style: subtitleStyle ??
                    CurrentTheme()
                        .themeData
                        .textTheme
                        .bodyText2
                        ?.copyWith(color: CurrentTheme().textPassive),
                maxLines: subtitleMaxLines,
                overflow: TextOverflow.ellipsis,
              ),
          ],
        ),
      );

  static PopupMenuItem<T> buildCheckedPopupItem<T>(
    ValueNotifier<bool> valueNotifier,
    T resultValue,
    String label,
  ) =>
      PopupMenuItem<T>(
        value: resultValue,
        child: ValueListenableBuilder<bool>(
          valueListenable: valueNotifier,
          builder: (context, value, child) => Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      label,
                      textAlign: TextAlign.start,
                      style: CurrentTheme().themeData.textTheme.bodyText1,
                    ),
                    Text(
                      FlutterI18n.translate(
                        context,
                        valueNotifier.value
                            ? 'popup.active'
                            : 'popup.deactivated',
                      ),
                      style: CurrentTheme()
                          .themeData
                          .textTheme
                          .bodyText2
                          ?.copyWith(
                              color: valueNotifier.value
                                  ? CurrentTheme()
                                      .themeData
                                      .colorScheme
                                      .secondary
                                  : CurrentTheme().textPassive),
                    ),
                  ],
                ),
              ),
              AbsorbPointer(
                child: Checkbox(
                  value: valueNotifier.value,
                  onChanged: (x) {},
                ),
              )
            ],
          ),
        ),
      );
}
