/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:dynamic_themes/dynamic_themes.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'assets_adapter.dart';
import 'themes/dark_theme.dart' as dark;
import 'themes/light_theme.dart' as light;

part 'themes/current_theme.dart';

// Todo: make colors right! See bottom for old colors!

class TcTheme extends StatelessWidget {
  final ThemedWidgetBuilder childBuilder;
  final themeCollection = ThemeCollection(
    themes: {
      TCThemes.light.index: light.theme,
      TCThemes.dark.index: dark.theme,
    },
    fallbackTheme: ThemeData.light(),
  );

  TcTheme({Key? key, required this.childBuilder}) : super(key: key);

  @override
  Widget build(BuildContext context) => DynamicTheme(
        themeCollection: themeCollection,
        defaultThemeId: TCThemes.light.index,
        builder: childBuilder,
      );
  // static CurrentTheme get currentTheme => CurrentTheme();
}

enum TCThemes {
  light,
  dark,
}

class CorporateTextStyles {
  /// TextStyles for Organizer
  static const TextStyle organizerHeadline = TextStyle(
    fontSize: 24,
    color: CorporateColors.tinyCampusBlue,
    fontWeight: FontWeight.w400,
    letterSpacing: -1.0,
  );
  static const TextStyle organizerDailyType = TextStyle(
    fontSize: 32,
    color: CorporateColors.tinyCampusBlue,
    fontWeight: FontWeight.w200,
    letterSpacing: -1.5,
  );
  static const TextStyle dialogHeadlineStyle = TextStyle(
    fontSize: 26.0,
    fontWeight: FontWeight.w600,
    letterSpacing: -1.0,
  );
  static const TextStyle dialogBodyStyle = TextStyle(
    fontSize: 18.0,
    letterSpacing: -0.6,
  );

  /// TextStyles for QandA
  static const TextStyle qandaPassiveHeadline = TextStyle(
    color: CorporateColors.passiveFontColor,
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
  );
  static const TextStyle qandaAuthorHeadline = TextStyle(
    color: CorporateColors.tinyCampusBlue,
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
    letterSpacing: -0.5,
  );
  static const TextStyle qandaCategoryHeadline = TextStyle(
    color: CorporateColors.tinyCampusBlue,
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
    letterSpacing: -0.5,
  );
}

// From common/colors.dart

// TODO: Cleanup
class CorporateColors {
  static const Color kimGrey = Color.fromRGBO(75, 93, 103, 1);
  static const Color kimRed = Color.fromRGBO(186, 24, 70, 1);
  static const Color kimYellow = Color.fromRGBO(251, 186, 0, 1);
  static const Color kimPink = Color.fromRGBO(220, 139, 163, 1);

  static const Color tinyCampusOrange = Color.fromRGBO(252, 90, 73, 1);
  static const Color tinyCampusBlue = Color.fromRGBO(1, 68, 87, 1);
  static const Color tinyCampusLightBlue = Color.fromRGBO(28, 141, 179, 1);
  static const Color tinyCampusDarkBlue = Color.fromRGBO(1, 68, 87, 1);
  static const Color tinyCampusIconGrey = Color.fromRGBO(128, 128, 128, 1);
  static const Color tinyCampusPassiveIconGrey =
      Color.fromRGBO(247, 247, 247, 1);
  static const Color tinyCampusPassiveIconGreyDM =
      Color.fromRGBO(31, 63, 70, 1);
  static const Color tinyCampusGreen = Color.fromARGB(255, 56, 123, 44);
  static const Color tinyCampusTextStandard = Color.fromRGBO(0, 25, 32, 1);
  static const Color tinyCampusTextSoft = Color.fromRGBO(0, 25, 32, 1);
  static const Color cafeteriaVeganGreen = Color.fromRGBO(96, 128, 76, 1);
  static const Color cafeteriaCautionRed = Color.fromRGBO(205, 89, 81, 1);
  static const Color ppAnswerWrongRed = Color.fromRGBO(214, 74, 93, 1);
  static const Color ppAnswerWrongBackgroundRed =
      Color.fromRGBO(240, 232, 233, 1);
  static const Color ppAnswerCorrectGreen = Color.fromRGBO(109, 158, 31, 1);
  // TODO: Experiment with the darker color and collect feedback.
  // static const Color ppAnswerCorrectGreen = Color.fromRGBO(128, 186, 37, 1);
  static const Color cafeteriaCautionYellow = Color.fromRGBO(196, 135, 35, 1);
  static const Color cafeteriaOpeningBarGrey = Color.fromRGBO(217, 217, 217, 1);
  static const Color cafeteriaLightGreen = Color.fromRGBO(193, 211, 193, 1);
  static const Color cafeteriaDarkGreen = Color.fromRGBO(71, 161, 55, 1);
  static const Color qAndAActivityTextGreen = Color.fromARGB(255, 16, 176, 92);

  static const Color thmGreen = Color.fromRGBO(128, 186, 36, 1);
  static const Color thmGrey = Color.fromRGBO(74, 92, 102, 1);
  static const Color thmGreyText = Color.fromRGBO(57, 74, 89, 1);
  static const Color thmLinkColor = Color.fromRGBO(156, 19, 46, 1);
  static const Color thmRed = Color.fromRGBO(156, 19, 46, 1);
  static const Color thmYellow = Color.fromRGBO(244, 170, 0, 1);
  static const Color thmCyan = Color.fromRGBO(0, 186, 288, 1);
  static const Color thmBlue = Color.fromRGBO(0, 40, 120, 1);

  static const Color passiveBackgroundLight = Color.fromRGBO(243, 243, 243, 1);
  static const Color passiveBackgroundDark = Color.fromRGBO(200, 200, 200, 1);
  static const Color passiveBackgroundLightBlue =
      Color.fromRGBO(215, 226, 229, 1);
  static const Color passiveFontColor = Color.fromRGBO(121, 121, 121, 1);

  static const Color darkModeBackground = Color.fromRGBO(0, 24, 30, 1);
  static const Color darkModeContent = Color.fromRGBO(0, 37, 46, 1);

  static const Color accentColor = Color.fromARGB(255, 1, 68, 87);
  static const Color snackbarBackground = Color.fromRGBO(81, 81, 81, 1);

  static Color nameToColor(String name) {
    assert(name.length > 1);
    final hash = name.hashCode & 0xffff;
    final hue = (360.0 * hash / (1 << 15)) % 360.0;
    return HSVColor.fromAHSV(1.0, hue, 0.4, 0.90).toColor();
  }
}

Color shadeColor(Color color) {
  var red = color.red;
  var green = color.green;
  var blue = color.blue;

  red = (red <= 0x7f) ? _leftShift(red) : red;
  green = (green <= 0x7f) ? _leftShift(green) : green;
  blue = (blue <= 0x7f) ? _leftShift(blue) : blue;

  red = (red <= 0xff) ? red : 0xff;
  green = (green <= 0xff) ? green : 0xff;
  blue = (blue <= 0xff) ? blue : 0xff;

  return Color.fromARGB(0xff, red, green, blue);
}

// https://stackoverflow.com/a/56329800
// License: CC BY-SA 4.0
// Author: Filip Veličković, edited by Gerard
MaterialColor createMaterialColor(Color color) {
  final strengths = <double>[.05];
  final swatch = <int, Color>{};
  final r = color.red, g = color.green, b = color.blue;

  for (var i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }

  for (final strength in strengths) {
    final ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  }
  return MaterialColor(color.value, swatch);
}

int _leftShift(int val) {
  while (val < 0x7f) {
    val = val << 1;
  }
  return val;
}

// From common/corporate_theme.dart

/*
import 'package:flutter/material.dart';

const Color _Primary = Color.fromRGBO(0x19, 0x8c, 0xb2, 1);
const Color _Secondary = Color.fromRGBO(0xff, 0x70, 0x43, 1);
const Color _Black1 = Color.fromRGBO(0x3e, 0x3e, 0x3e, 1);
const Color _Black2 = Color.fromRGBO(0x4c, 0x3d, 0x3b, 1);
const Color _White = Color.fromRGBO(0xf7, 0xf0, 0xef, 1);

const Map<int, Color> _map2 = const <int, Color>{
  50: Colors.white,
  100: const Color.fromRGBO(0xff, 0xe2, 0xd9, 1),
  200: const Color.fromRGBO(0xff, 0xc6, 0xb4, 1),
  300: const Color.fromRGBO(0xff, 0xa9, 0x8e, 1),
  400: const Color.fromRGBO(0xff, 0x8d, 0x69, 1),
  500: _Secondary,
  600: const Color.fromRGBO(0xcc, 0x5a, 0x36, 1),
  700: const Color.fromRGBO(0x99, 0x43, 0x28, 1),
  800: const Color.fromRGBO(0x66, 0x2d, 0x1b, 1),
  900: const Color.fromRGBO(0x33, 0x16, 0x0d, 1),
};

const Map<int, Color> _map1 = const <int, Color>{
  50: Colors.white,
  100: const Color.fromRGBO(0xd1, 0xe8, 0xf0, 1),
  200: const Color.fromRGBO(0xa3, 0xd1, 0xe0, 1),
  300: const Color.fromRGBO(0x75, 0xba, 0xd1, 1),
  400: const Color.fromRGBO(0x47, 0xa3, 0xc1, 1),
  500: _Primary,
  600: const Color.fromRGBO(0x14, 0x70, 0x8e, 1),
  700: const Color.fromRGBO(0x0f, 0x54, 0x6b, 1),
  800: const Color.fromRGBO(0x0a, 0x38, 0x47, 1),
  900: const Color.fromRGBO(0x05, 0x1c, 0x24, 1),
};


MaterialColor _secondarySwatch = MaterialColor(_Secondary.value, _map2);
MaterialColor _primarySwatch = MaterialColor(_Primary.value, _map1);

final ThemeData corporateTheme = ThemeData(
    brightness: Brightness.light,
    primaryColor: _Primary,
    accentColor: _Secondary,
    primarySwatch: _primarySwatch,
    dividerColor: _Black2,
);
 */
