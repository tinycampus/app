/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

import 'tc_theme.dart';

class TinyCampusMarkdownStylesheet extends MarkdownStyleSheet {
  TinyCampusMarkdownStylesheet(BuildContext context)
      : super(
          h1: Theme.of(context)
              .textTheme
              .headline3
              ?.copyWith(color: CurrentTheme().textSoftLightBlue, height: 2.2),
          h2: TextStyle(
            color: Theme.of(context).textTheme.headline2?.color,
            fontSize: 28.0,
            letterSpacing: -1.0,
            fontWeight: FontWeight.w400,
            height: 1.4,
          ),
          p: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w400,
          ),
          listBullet: TextStyle(
            color: CorporateColors.tinyCampusOrange,
            fontSize: 14.0,
            letterSpacing: -1.0,
          ),
          a: CurrentTheme().themeData.textTheme.bodyText1?.copyWith(
                color: CurrentTheme().tcBlueFont,
                fontSize: 16.0,
                fontWeight: FontWeight.w700,
              ),
          blockSpacing: 10,
          listIndent: 16,
        );
}
