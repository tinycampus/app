/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/utils/message_printer.dart';

/// Translation delegate that manage the new locale received from the framework
class TcI18nDelegate extends LocalizationsDelegate<FlutterI18n> {
  late FlutterI18n _translationObject;
  late Locale currentLocale;

  TcI18nDelegate({
    required TranslationLoader translationLoader,
    required MissingTranslationHandler missingTranslationHandler,
  }) {
    _translationObject = FlutterI18n(
      translationLoader,
      '.',
      missingTranslationHandler: missingTranslationHandler,
    );
  }

  @override
  bool isSupported(final Locale locale) => true;

  @override
  Future<FlutterI18n> load(final Locale locale) async {
    MessagePrinter.info("New locale: $locale");
    final translationLoader = _translationObject.translationLoader;
    if (translationLoader != null) {
      if (translationLoader.locale != locale ||
          _translationObject.decodedMap == null ||
          _translationObject.decodedMap!.isEmpty) {
        translationLoader.locale = currentLocale = locale;
        await _translationObject.load();
      }
    }
    return _translationObject;
  }

  // TODO: When changing the theme, this gets triggered for no
  // reason. Please examine.
  // old method
  // return currentLocale == null || currentLocale == old.currentLocale;
  @override
  bool shouldReload(final TcI18nDelegate old) => false;
}
