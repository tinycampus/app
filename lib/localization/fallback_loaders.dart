/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui show Locale, window;

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/loaders/file_translation_loader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../common/constants/localization_constants.dart';
import '../repositories/http/http_repository.dart';

export 'localization_exceptions.dart';

// ui.window.locale.languageCode is used to get initial lang code
Future<ui.Locale> getForcedLocale() async =>
    await SharedPreferences.getInstance().then(
      (value) => ui.Locale(
        value.getString(overrideLanguage) ?? ui.window.locale.languageCode,
      ),
    );

extension FileTranslationLoaderJsonDecode on FileTranslationLoader {
  /// Debug wrapper for [FileTranslationLoader] class
  static FileTranslationLoader debug({
    String fallbackFile = "en",
    String basePath = "assets/flutter_i18n",
    bool useCountryCode = false,
    ui.Locale? forcedLocale,
  }) =>
      FileTranslationLoader(
        fallbackFile: fallbackFile,
        basePath: basePath,
        useCountryCode: useCountryCode,
        forcedLocale: forcedLocale,
        decodeStrategies: decodeStrategies,
      );
}

class LocalFallbackNetworkLoader extends FileTranslationLoader {
  AssetBundle networkAssetBundle;
  final Uri baseUri;

  LocalFallbackNetworkLoader({
    required this.baseUri,
    ui.Locale? forcedLocale,
    String fallbackFile = "en",
    bool useCountryCode = false,
    String basePath = "assets/flutter_i18n",
  })  : networkAssetBundle = FallbackAssetBundle(baseUri, basePath),
        super(
          fallbackFile: fallbackFile,
          useCountryCode: useCountryCode,
          forcedLocale: forcedLocale,
          basePath: basePath,
          decodeStrategies: decodeStrategies,
        );

  @override
  Future<String> loadString(final String fileName, final String extension) =>
      networkAssetBundle.loadString(
          '$fileName${kProfileMode ? '.profile' : ''}${'.$extension'}');
}

class FallbackAssetBundle extends NetworkAssetBundle {
  FallbackAssetBundle(Uri baseUrl, String fallbackPath)
      : _fallbackPath = fallbackPath,
        _baseUrl = baseUrl,
        _httpClient = HttpRepository(),
        super(baseUrl);

  final String _fallbackPath;
  final Uri _baseUrl;
  final HttpRepository _httpClient;

  Uri _urlFromKey(String key) => _baseUrl.resolve(key);

  @override
  Future<ByteData> load(String key) async {
    try {
      final response = await _httpClient
          .readBytes(_urlFromKey(key), secured: false)
          .timeout(const Duration(milliseconds: 1500));
      writeJson(key, response);
      // debugPrint(String.fromCharCodes(bytes));
      return response.buffer.asByteData();
    } on Exception catch (e) {
      debugPrint(e.toString());
      return readTempJson(key);
    }
  }

  Future<String> get _tempPath async {
    final directory = await getTemporaryDirectory();

    return directory.path;
  }

  Future<File> _tempFile(String filename) async {
    final path = await _tempPath;
    return File('$path/$filename');
  }

  Future<File> writeJson(String filename, Uint8List content) async {
    final file = await _tempFile(filename);

    // Write the file.
    return file.writeAsBytes(content);
  }

  Future<ByteData> readTempJson(String key) async {
    final file = await _tempFile(key);
    if (file.existsSync()) {
      return (await file.readAsBytes()).buffer.asByteData();
    }
    // Fallback if there is no internet connection or cached json
    return rootBundle.load('$_fallbackPath/$key');
  }
}
