/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../bloc/authentication/auth_bloc.dart';
import '../../bloc/user/user_bloc.dart';
import '../../common/constants/api_constants.dart';
import '../../common/constants/routing_constants.dart';
import '../../common/styled_print.dart';
import '../../common/tc_theme.dart';
import '../../common/widgets/dialog/tc_dialog.dart';
import '../../repositories/http/http_repository.dart';
import '../../repositories/token/token_repository.dart';
import '../app_start/landing_page.dart';

class AccountSettingsWidget extends StatefulWidget {
  const AccountSettingsWidget({
    Key? key,
  }) : super(key: key);

  @override
  _AccountSettingsWidgetState createState() => _AccountSettingsWidgetState();
}

class _AccountSettingsWidgetState extends State<AccountSettingsWidget>
    with SingleTickerProviderStateMixin {
  String token = "";
  ValueNotifier<bool> tokenFound = ValueNotifier<bool>(false);
  ValueNotifier<bool> clickedOnButton = ValueNotifier<bool>(false);
  ValueNotifier<bool> copiedRecovery = ValueNotifier<bool>(false);
  ValueNotifier<bool> verificationChecked = ValueNotifier<bool>(false);
  ValueNotifier<bool> verified = ValueNotifier<bool>(false);
  ValueNotifier<bool> deleteUserContent = ValueNotifier<bool>(false);
  ValueNotifier<bool> deleteButtonEnabled = ValueNotifier<bool>(false);

  @override
  void initState() {
    super.initState();
    _getToken();
    _checkVerification(context);
  }

  void _checkVerification(BuildContext context) {
    verified.value = userIsVerified(context);
  }

  Future<void> _getToken() async {
    token = await TokenRepository().recoveryCode ?? '';

    if (token.isEmpty) {
      eprint("Recovery-Token not found", caller: AccountSettingsWidget);
    } else {
      sprint("recovery-token: $token");
      tokenFound.value = true;
    }
  }

  @override
  Widget build(BuildContext context) => Column(
        children: [
          BlocBuilder<UserBloc, UserState>(
            builder: (context, userState) => ListView.separated(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              separatorBuilder: (context, index) =>
                  Divider(indent: 16, endIndent: 16),
              itemCount: accountSettingListTiles(context, userState).length,
              itemBuilder: (context, index) =>
                  accountSettingListTiles(context, userState)[index],
            ),
          ),
          Container(height: 16),
        ],
      );

  List<Widget> accountSettingListTiles(
      BuildContext context, UserState userState) {
    final emailSent = userState.user.emailSent;
    verified.value = userState.user.verified;
    return [
      if (!kReleaseMode)
        TextButton(
            child: Text("Remove Token Received Dates"),
            onPressed: () {
              TokenRepository().removeDatesReceived();
            }),
      if (!kReleaseMode) accountVerifiedTile(),
      if (!verified.value && !emailSent) verifiedAccountListTile(),
      if (!verified.value && emailSent) checkVerificationState(context),
      if (!verified.value) recoveryWidgetTile(),
      if (verified.value) verifiedTile(context),
      logoutTile(context),
      deleteAccountTile(),
    ];
  }

  Widget verifiedTile(BuildContext context) => ListTile(
        title: Text(
            FlutterI18n.translate(context, "settings.account.verified.title")),
        subtitle: Text(FlutterI18n.translate(
            context, "settings.account.verified.description")),
        trailing: Icon(Icons.check),
      );

  Widget checkVerificationState(BuildContext context) => ListTile(
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(FlutterI18n.translate(
                      context, "login.verification.check_status")),
                  Text(
                    FlutterI18n.translate(
                        context, "login.verification.check_emails"),
                    style: CurrentTheme()
                        .themeData
                        .textTheme
                        .caption
                        ?.copyWith(fontSize: 14),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 120,
              width: 120,
              child: FlareActor(
                "assets/flare/tc_logo_compound.flr",
                isPaused: false,
                animation: CurrentTheme().theme == TCThemes.light
                    ? "success_register_mail"
                    : "success_register_mail_DM",
                // fit: BoxFit.contain,
                shouldClip: true,
              ),
            ),
          ],
        ),
        // subtitle: Text(
        //     FlutterI18n.translate(context, "
        // login.verification.check_emails")),
        // trailing: Icon(
        //   Icons.mail,
        //   color: CurrentTheme().themeData.accentColor,
        // ),
        // trailing: Container(
        //   height: 40,
        //   width: 40,
        //   child: FlareActor(
        //     "assets/flare/tc_logo_compound.flr",
        //     isPaused: false,
        //     animation: "success_register_mail",
        //     fit: BoxFit.contain,
        //     shouldClip: false,
        //   ),
        // ),
        // BlocConsumer<UserBloc, UserState>(
        //   listener: (context, state) {
        //     if (state is UserLoggedInState) {
        //       if (state.user.verified == true) {
        //         setState(() {
        //           verified.value = true;
        //         });
        //       }
        //     }
        //   },
        //   builder: (context, state) => AnimatedOpacity(
        //       duration: Duration(milliseconds: 60),
        //       opacity: (state is UserLoadingState) ? 1.0 : 0.0,
        //       child: Container(
        //           height: 20,
        //           width: 20,
        //           child: CircularProgressIndicator(
        //             strokeWidth: 2,
        //           ))),
        // ),
        onTap: () {
          BlocProvider.of<UserBloc>(context).add(UserFetchEvent());
        },
      );

  ListTile logoutTile(BuildContext context) => ListTile(
        title: Text(FlutterI18n.translate(context, "common.actions.logout")),
        trailing: Icon(Icons.logout),
        onTap: () {
          TCDialog.showCustomDialog(
            context: context,
            functionActionText:
                FlutterI18n.translate(context, 'common.actions.logout_caps'),
            bodyText: FlutterI18n.translate(context, 'common.messages.logout'),
            headlineText:
                FlutterI18n.translate(context, 'common.actions.logout_caps'),
            popAfterFunction: false,
            alternativeWidget: SizedBox(
              height: 210,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 32.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        FlutterI18n.translate(
                            context, 'common.messages.logout'),
                        textAlign: TextAlign.start,
                      ),
                      ValueListenableBuilder<bool>(
                        valueListenable: deleteUserContent,
                        builder: (context, _, child) => CheckboxListTile(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 16, horizontal: 0),
                          value: deleteUserContent.value,
                          title: Text(
                            FlutterI18n.translate(
                                context, 'common.messages.delete_data'),
                          ),
                          subtitle: Text(
                            FlutterI18n.translate(
                                context, 'common.messages.delete_data_explain'),
                          ),
                          onChanged: (value) {
                            deleteUserContent.value = !deleteUserContent.value;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            onConfirm: () {
              RepositoryProvider.of<AuthBloc>(context).add(AuthLogoutEvent(
                  deleteUserData: deleteUserContent.value,
                  deleteSharedPrefs: deleteUserContent.value));
              Navigator.pushNamedAndRemoveUntil(
                context,
                loginRoute,
                (route) => false,
              );
              // });
            },
          );
        },
      );

  ListTile changePasswordTile() => ListTile(
        title: Text(
            FlutterI18n.translate(context, "settings.account.change_password")),
        subtitle: Text("NOT IMPLEMENTED YET"),
      );

  Widget deleteAccountTile() => BlocBuilder<UserBloc, UserState>(
        builder: (context, state) => ListTile(
          title: I18nText(
            'settings.account.delete_account',
          ),
          trailing: Icon(Icons.delete_forever_outlined),
          onTap: () {
            TCDialog.showCustomDialog(
              context: context,
              functionActionColor: CorporateColors.cafeteriaCautionRed,
              functionActionText:
                  FlutterI18n.translate(context, 'common.actions.delete'),
              bodyText: FlutterI18n.translate(
                  context, 'settings.account.delete_account_body'),
              headlineText: FlutterI18n.translate(
                  context, 'settings.account.delete_account'),
              popAfterFunction: false,
              alternativeWidget: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 32.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      I18nText('settings.account.delete_account_body'),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          state.user.displayName,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      ValueListenableBuilder<bool>(
                        valueListenable: deleteButtonEnabled,
                        builder: (context, _, child) => TextField(
                          onChanged: (text) => deleteButtonEnabled.value =
                              text.trim() == state.user.displayName,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              onConfirmEnabled: deleteButtonEnabled,
              onConfirm: () async {
                if (!deleteButtonEnabled.value) return;

                deleteButtonEnabled.value = false;

                final navigator = Navigator.of(context);
                final messenger = ScaffoldMessenger.of(context);
                final authBloc = RepositoryProvider.of<AuthBloc>(context);

                try {
                  // Request account deletion
                  await HttpRepository().post(
                    '$authDeleteUrl?name=${state.user.displayName}',
                  );
                } on Exception {
                  navigator.pop();

                  messenger.showSnackBar(
                    SnackBar(
                      duration: Duration(seconds: 5),
                      content: I18nText(
                        'settings.account.delete_account_error',
                      ),
                    ),
                  );

                  return;
                }

                // Logout and delete local data
                authBloc.add(
                  AuthLogoutEvent(
                    deleteUserData: true,
                    deleteSharedPrefs: true,
                  ),
                );

                // Return to login screen
                navigator.pushNamedAndRemoveUntil(loginRoute, (route) => false);
              },
              onCancel: () {
                deleteButtonEnabled.value = false;
              },
            );
          },
        ),
      );

  Widget accountVerifiedTile() => BlocBuilder<UserBloc, UserState>(
        builder: (context, state) => ListTile(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("E-Mail Sent:",
                  style: CurrentTheme().themeData.textTheme.caption),
              Text(state.user.emailSent.toString()),
              Text("Username:",
                  style: CurrentTheme().themeData.textTheme.caption),
              Text(state.user.userName),
              Text("Display Name:",
                  style: CurrentTheme().themeData.textTheme.caption),
              Text(state.user.displayName),
              Text("Verified:",
                  style: CurrentTheme().themeData.textTheme.caption),
              Text(state.user.verified.toString()),
            ],
          ),
        ),
      );

  Widget verifiedAccountListTile() => BlocBuilder<UserBloc, UserState>(
        builder: (context, state) => ListTile(
          title: Row(
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(FlutterI18n.translate(
                  context,
                  verified.value
                      ? 'settings.account.status.account_verified'
                      : 'settings.account.status.account_verify')),
              if (!verified.value)
                Container(
                  height: 10,
                  width: 10,
                  margin: const EdgeInsets.only(left: 4),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: CorporateColors.ppAnswerWrongRed),
                ),
            ],
          ),
          subtitle: verified.value
              ? null
              : Text(FlutterI18n.translate(
                  context, "settings.account.set_email_password")),
          trailing: verified.value ? null : Icon(Icons.chevron_right_rounded),
          onTap: verified.value
              ? null
              : () {
                  Navigator.pushNamed(context, loginRoute,
                      arguments: LandingPageArguments(
                          tasks: AvailableTasks.upgradeOnly));
                },
        ),
      );

  bool userIsVerified(BuildContext context) =>
      BlocProvider.of<UserBloc>(context).state.user.verified;

  ValueListenableBuilder<bool> recoveryWidgetTile() =>
      ValueListenableBuilder<bool>(
        valueListenable: tokenFound,
        builder: (context, tokenFoundValue, child) =>
            ValueListenableBuilder<bool>(
          valueListenable: clickedOnButton,
          builder: (context, clickedOnButtonValue, child) =>
              ValueListenableBuilder<bool>(
            valueListenable: copiedRecovery,
            builder: (context, copiedRecoveryValue, child) => !tokenFound.value
                ? ListTile(
                    title: Text(FlutterI18n.translate(
                        context, "settings.account.search_recovery_code")),
                  )
                : ListTile(
                    onTap: () {
                      if (!clickedOnButton.value) {
                        clickedOnButton.value = true;
                      } else {
                        Clipboard.setData(ClipboardData(text: token));
                        if (!copiedRecovery.value) {
                          copiedRecovery.value = true;
                          Future.delayed(Duration(milliseconds: 1200), () {
                            if (mounted) {
                              copiedRecovery.value = false;
                            }
                          });
                        }
                      }
                    },
                    title: Text(clickedOnButtonValue && tokenFoundValue
                        ? token
                        : FlutterI18n.translate(
                            context, "settings.account.show_recovery")),
                    subtitle: subtitleWidget(
                        clickedOnButtonValue: clickedOnButtonValue,
                        tokenFoundValue: tokenFoundValue),
                    trailing: !(clickedOnButtonValue && tokenFoundValue)
                        ? null
                        : AnimatedSwitcher(
                            duration: Duration(milliseconds: 260),
                            layoutBuilder: (currentChild, previousChildren) {
                              var children = previousChildren;
                              if (currentChild != null) {
                                children = children.toList()..add(currentChild);
                              }
                              return Stack(
                                  children: children,
                                  alignment: Alignment.centerLeft);
                            },
                            transitionBuilder: (child, animation) =>
                                FadeTransition(
                                    child: Container(child: child),
                                    opacity: Tween<double>(begin: 0.0, end: 1.0)
                                        .animate(
                                      CurvedAnimation(
                                        parent: animation,
                                        curve: Interval(0.5, 1.0),
                                      ),
                                    )),
                            child: copiedRecoveryValue
                                ? Icon(
                                    Icons.check,
                                    key: Key("copiedRecoveryValue_icon_true"),
                                  )
                                : Icon(
                                    Icons.copy_outlined,
                                    key: Key("copiedRecoveryValue_icon_false"),
                                  ),
                          ),
                  ),
          ),
        ),
      );

  Widget subtitleWidget({
    required bool clickedOnButtonValue,
    required bool tokenFoundValue,
  }) =>
      Align(
        alignment: Alignment.centerLeft,
        child: AnimatedSwitcher(
          duration: Duration(milliseconds: 260),
          layoutBuilder: (currentChild, previousChildren) {
            var children = previousChildren;
            if (currentChild != null) {
              children = children.toList()..add(currentChild);
            }
            return Stack(children: children, alignment: Alignment.centerLeft);
          },
          transitionBuilder: (child, animation) => FadeTransition(
              child: Container(child: child),
              opacity: Tween<double>(begin: 0.0, end: 1.0).animate(
                CurvedAnimation(
                  parent: animation,
                  curve: Interval(0.5, 1.0),
                ),
              )),
          child: !(clickedOnButtonValue && tokenFoundValue)
              ? Text(
                  FlutterI18n.translate(
                      context, "settings.account.press_to_show"),
                  key: Key("cbdks_01"),
                )
              : copiedRecovery.value
                  ? Text(
                      FlutterI18n.translate(
                          context, "settings.account.copy_code"),
                      key: Key("cbdks_02"),
                      style: TextStyle(color: CurrentTheme().tcBlueFont),
                    )
                  : Text(
                      FlutterI18n.translate(
                          context, "settings.account.press_again"),
                      key: Key("cbdks_03")),
        ),
      );
}
