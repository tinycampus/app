/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../common/tc_theme.dart';

//World-Widget on Settings_Language_Page and Settings_Page
class LanguageWidget extends StatelessWidget {
  final String languageText;
  final bool isLoading;

  LanguageWidget(
    this.languageText, {
    Key? key,
    this.isLoading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SizedBox(
        width: 60,
        height: 80,
        child: CircleAvatar(
          radius: 85,
          child: AnimatedSwitcher(
            duration: Duration(milliseconds: 160),
            child: isLoading
                ? CircularProgressIndicator()
                : Stack(
                    children: <Widget>[
                      Material(
                        shape: CircleBorder(),
                        child: CircleAvatar(
                          child: Image.asset(
                            'assets/images/settings_language/world.png',
                          ),
                          radius: 85,
                        ),
                      ),
                      Center(
                        child: Text(
                          languageText,
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              ?.copyWith(color: CurrentTheme().tcBlue),
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      );
}
