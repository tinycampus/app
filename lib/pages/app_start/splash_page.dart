/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../bloc/authentication/auth_bloc.dart';
import '../../bloc/user/user_bloc.dart';
import '../../common/constants/routing_constants.dart';
import '../../common/styled_print.dart';
import '../../common/tc_theme.dart';
import '../../common/widgets/tc_button.dart';
import '../../modules/organizer/ui/organizer/organizer_constants.dart';
import '../../navigation/bottom_navigation/bottom_navigation.dart';
import '../../navigation/bottom_navigation/tab_helper.dart';
import '../../repositories/token/token_repository.dart';
import '../app_start/landing_page.dart';
import 'splash_page_nav_obstacle.dart';

/// SplashPage to be shown after OS-SplashPage to allow [AuthBloc] to load data.
class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    with SingleTickerProviderStateMixin {
  /// Notifier for when an obstacle changes. Listener have to be disposed!
  final _navNotifier = ValueNotifier<NavObstacle>(NavObstacle());
  final showFallbackWidget = ValueNotifier<bool>(false);
  final accessToken = ValueNotifier<String>("");
  final refreshToken = ValueNotifier<String>("");
  final isAccessTokenExpired = ValueNotifier<bool>(true);
  final navigationIntentFired = ValueNotifier<bool>(false);
  final navigationDestination = ValueNotifier<String>("");
  final accessTokenReceivedDate =
      ValueNotifier<DateTime>(DateTime.fromMillisecondsSinceEpoch(0));
  final refreshTokenReceivedDate =
      ValueNotifier<DateTime>(DateTime.fromMillisecondsSinceEpoch(0));
  final accessTokenExpirationDate =
      ValueNotifier<DateTime>(DateTime.fromMillisecondsSinceEpoch(0));
  final refreshTokenExpirationDate =
      ValueNotifier<DateTime>(DateTime.fromMillisecondsSinceEpoch(0));
  late LongAnimationHelper refreshHelper;

  @override
  void initState() {
    super.initState();
    _navNotifier.addListener(
      () => NavObstacle.navigateConditionally(_navNotifier.value),
    );
    refreshHelper = LongAnimationHelper(this);
    initValues();
    readToken();
    initFallbackWidgetTimer();
  }

  void initFallbackWidgetTimer() {
    Future.delayed(Duration(seconds: kReleaseMode ? 6 : 2), () {
      if (mounted) {
        showFallbackWidget.value = true;
      }
    });
  }

  void initValues() {
    accessToken.value = "";
    refreshToken.value = "";
    accessTokenReceivedDate.value = DateTime.fromMillisecondsSinceEpoch(0);
    refreshTokenReceivedDate.value = DateTime.fromMillisecondsSinceEpoch(0);
    accessTokenExpirationDate.value = DateTime.fromMillisecondsSinceEpoch(0);
    refreshTokenExpirationDate.value = DateTime.fromMillisecondsSinceEpoch(0);
  }

  Future<void> readTokenDates(TokenMeta tokenMeta) async {
    accessTokenReceivedDate.value = tokenMeta.accessTokenReceivedDate;
    refreshTokenReceivedDate.value = tokenMeta.refreshTokenReceivedDate;
    accessTokenExpirationDate.value = tokenMeta.accessTokenExpirationDate;
    refreshTokenExpirationDate.value = tokenMeta.refreshTokenExpirationDate;

    /// Check if access token is expired
    isAccessTokenExpired.value = tokenMeta.isAccessTokenExpired;
  }

  Future<void> readToken() async {
    navigationIntentFired.value = false;
    navigationDestination.value = "";

    try {
      final authBloc = RepositoryProvider.of<AuthBloc>(context);
      final token = await TokenRepository().getTokenObject();
      var tokenMeta = await TokenRepository().tokenMeta;
      if (token == null) {
        initValues();
        navigateToLogin();
        return;
      }

      if (tokenMeta.isRefreshTokenExpired) {
        authBloc.add(
            AuthLogoutEvent(deleteUserData: false, deleteSharedPrefs: false));
        navigationDestination.value = "Login - Refresh Expired";
        debugPrint(
            "[SPLASH] Refresh Token expired OR Expiration Date not found.");
        navigateToLogin();
        return;
      }

      accessToken.value = token.accessToken;
      refreshToken.value = token.refreshToken;

      try {
        await readTokenDates(tokenMeta);

        if (!tokenMeta.isAccessTokenExpired) {
          navigateToHome();
        } else {
          /// If the access token is expired, await is tolerable in this case
          try {
            await TokenRepository().refreshAccessToken();
          } on Exception catch (e) {
            eprint(e, caller: SplashPage);
          }
          tokenMeta = await TokenRepository().tokenMeta;
          await readTokenDates(tokenMeta);
          if (!tokenMeta.isAccessTokenExpired) {
            navigateToHome();
          } else {
            navigateToLogin();
          }
        }
      } on Exception catch (e) {
        eprint(e, caller: SplashPage);
        navigateToLogin();
      }
    } on Exception catch (e) {
      eprint(e, caller: SplashPage);
      navigateToLogin();
    }
  }

  void navigateToLogin({bool forceNavigation = kReleaseMode}) {
    /// [TODO] How can we use custom transitions WITH route-string constants?
    /// Use this if the methods fails:
    // Navigator.pushNamedAndRemoveUntil(
    //   context,
    //   loginRoute,
    //   (route) => false,
    // );
    navigationIntentFired.value = true;
    navigationDestination.value = "Login";
    if (forceNavigation) {
      Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(
            pageBuilder: (c, a1, a2) => LandingPage(),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 160),
          ),
          ModalRoute.withName(loginRoute));
    }
  }

  void navigateToHome({bool forceNavigation = kReleaseMode}) {
    /// [TODO] How can we use custom transitions WITH route-string constants?
    navigationIntentFired.value = true;
    navigationDestination.value = "Home";
    if (forceNavigation) {
      Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(
            pageBuilder: (c, a1, a2) => BottomNavigation(
              tab: TabItem.home,
            ),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 160),
          ),
          ModalRoute.withName(homeTabRoute));
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Container(
          color: Theme.of(context).primaryColor,
          alignment: Alignment.center,
          child: Stack(
            children: [
              Positioned.fill(
                child: Row(
                  children: [
                    Spacer(),
                    Expanded(
                      child: Center(child: CircularProgressIndicator()),
                    ),
                    Spacer(),
                  ],
                ),
              ),
              if (!kReleaseMode) buildTokenDebug(),
              if (!kReleaseMode) buildDebugBottomButtons(context),
              buildTimedEscapeButtons(context),
            ],
          ),
        ),
      );

  Positioned buildTimedEscapeButtons(BuildContext context) => Positioned(
        bottom: 36,
        left: 64,
        right: 64,
        child: ValueListenableBuilder<bool>(
          valueListenable: showFallbackWidget,
          builder: (context, value, child) => AnimatedSwitcher(
            duration: Duration(milliseconds: 240),
            child: !value
                ? Container()
                : Wrap(
                    children: [
                      TCButton(
                        buttonLabel: FlutterI18n.translate(
                            context, "common.actions.logout"),
                        onPressedCallback: () {
                          BlocProvider.of<AuthBloc>(context)
                              .add(AuthLogoutEvent());
                          navigateToLogin(forceNavigation: true);
                        },
                      ),
                    ],
                  ),
          ),
        ),
      );

  Positioned buildDebugBottomButtons(BuildContext context) => Positioned(
        bottom: 140,
        left: 0,
        right: 0,
        child: Wrap(
          children: [
            FlatButton(
              child: Text(
                "Refresh Access Token",
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    ?.copyWith(color: CorporateColors.tinyCampusOrange),
              ),
              onPressed: () async {
                navigationIntentFired.value = false;
                try {
                  await TokenRepository().refreshAccessToken();
                } on Exception catch (e) {
                  eprint(e);
                }
                readToken();
              },
            ),
            FlatButton(
              child: Text(
                "Login-Seite",
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    ?.copyWith(color: CorporateColors.tinyCampusOrange),
              ),
              onPressed: () => navigateToLogin(forceNavigation: true),
            ),
            FlatButton(
              child: Text(
                "Home Screen",
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    ?.copyWith(color: CorporateColors.tinyCampusOrange),
              ),
              onPressed: () => navigateToHome(forceNavigation: true),
            ),
            FlatButton(
              child: Text(
                "Delete Token",
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    ?.copyWith(color: CorporateColors.tinyCampusOrange),
              ),
              onPressed: () async {
                await TokenRepository().deleteToken();
                setState(() {});
              },
            ),
            FlatButton(
              child: Text(
                "Remove First Run Flag",
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    ?.copyWith(color: CorporateColors.tinyCampusOrange),
              ),
              onPressed: () async {
                await TokenRepository().deleteToken();
                readToken();
                setState(() {});
              },
            ),
          ],
        ),
      );

  AnimatedBuilder buildTokenDebug() => AnimatedBuilder(
        animation: Listenable.merge([
          refreshHelper.loop,
          accessToken,
          refreshToken,
          accessTokenReceivedDate,
          refreshTokenReceivedDate,
          accessTokenExpirationDate,
          refreshTokenExpirationDate,
          navigationIntentFired,
          navigationDestination,
          isAccessTokenExpired,
        ]),
        builder: (context, child) {
          final descriptionStyle = TextStyle(
            color: CurrentTheme().textPassive,
          );
          final headlineStyle = CurrentTheme().themeData.textTheme.headline2;
          return Positioned(
              top: 40,
              left: 0,
              right: 0,
              child: Column(
                children: [
                  Text(DateTime.now().toString()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                            Text("Access Token", style: headlineStyle),
                            Text(accessToken.value),
                            Text("Received", style: descriptionStyle),
                            Text(DateFormat("yy-MMMM-dd HH:mm:ss")
                                .format(accessTokenReceivedDate.value)),
                            Text("Expires", style: descriptionStyle),
                            Text(DateFormat("yy-MMMM-dd HH:mm:ss")
                                .format(accessTokenExpirationDate.value)),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Text("Refresh Token", style: headlineStyle),
                            Text(refreshToken.value),
                            Text("Received", style: descriptionStyle),
                            Text(DateFormat("yy-MMMM-dd HH:mm:ss")
                                .format(refreshTokenReceivedDate.value)),
                            Text("Expires", style: descriptionStyle),
                            Text(DateFormat("yy MMMM-dd HH:mm:ss")
                                .format(refreshTokenExpirationDate.value)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Text("Access Token expired: ${isAccessTokenExpired.value}"),
                  BlocBuilder<UserBloc, UserState>(
                    builder: (context, state1) =>
                        Text(state1.runtimeType.toString()),
                  ),
                  BlocBuilder<AuthBloc, AuthState>(
                    builder: (context, state2) =>
                        Text(state2.runtimeType.toString()),
                  ),
                  Text("Navigation fired: ${navigationIntentFired.value}"),
                  Text(
                      "Navigation destination: ${navigationDestination.value}"),
                ],
              ));
        },
      );

  @override
  void dispose() {
    _navNotifier.dispose();
    refreshHelper.dispose();
    super.dispose();
  }
}

class LongAnimationHelper {
  late Animation<double> gameLoop;
  late AnimationController loop;

  LongAnimationHelper(TickerProvider tickerProvider) {
    loop = AnimationController(
        duration: const Duration(days: 365), vsync: tickerProvider);
    gameLoop = CurvedAnimation(parent: loop, curve: Curves.easeOut);
    loop
      ..addListener(
        () {
          // do fancy stuff
        },
      )
      ..forward();
  }

  void dispose() {
    loop.stop();
    loop.dispose();
  }
}
