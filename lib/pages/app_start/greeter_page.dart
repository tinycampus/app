/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../bloc/user/user_bloc.dart';
import '../../common/tc_theme.dart';
import 'greeter_extension.dart';

/// GreeterPage shown after a successful login or registration.
class GreeterPage extends StatelessWidget {
  const GreeterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocBuilder<UserBloc, UserState>(
        builder: (context, state) => Scaffold(
          backgroundColor: Theme.of(context).primaryColor,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // Has to have same styling, except color, as greeter text
                // at `widgets/login_header.dart` for HeroAnimation to work!
                Hero(
                  tag: DateTime.now().greetingKey,
                  child: Text(
                    FlutterI18n.translate(context, DateTime.now().greetingKey),
                    style: state.user.displayName.isNotEmpty
                        ? Theme.of(context).textTheme.headline1?.copyWith(
                            color: Theme.of(context).colorScheme.secondary)
                        : Theme.of(context).textTheme.headline1?.copyWith(
                            color: CorporateColors.tinyCampusTextStandard),
                  ),
                ),
                if (state.user.displayName.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      state.user.displayName,
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          ?.copyWith(color: CurrentTheme().textSoftLightBlue),
                      textAlign: TextAlign.center,
                    ),
                  ),
              ],
            ),
          ),
        ),
      );
}
