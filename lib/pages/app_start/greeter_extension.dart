/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

/// An extension to easily calculate the appropriate greeting
/// depending on the current time.
extension GreetingExtension on DateTime {
  /// Returns a localization key to look up the calculated greeting.
  String get greetingKey {
    var baseKey = 'login.greetings';
    if ([4, 5, 6, 7, 8, 9, 10, 11].contains(hour)) {
      return '$baseKey.morning';
    } else if ([12, 13, 14, 15, 16, 17].contains(hour)) {
      return '$baseKey.day';
    } else {
      return '$baseKey.evening';
    }
  }
}
