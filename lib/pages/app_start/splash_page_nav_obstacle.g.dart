// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'splash_page_nav_obstacle.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension NavObstacleCopyWith on NavObstacle {
  NavObstacle copyWith({
    void Function()? decision,
    bool? dialog,
    bool? snack,
  }) {
    return NavObstacle(
      decision: decision ?? this.decision,
      dialog: dialog ?? this.dialog,
      snack: snack ?? this.snack,
    );
  }
}
