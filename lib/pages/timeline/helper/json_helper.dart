/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import '../../../common/constants/api_constants.dart';

/// Sammlung von Helfer-Funktionen um das Interagieren
/// mit dem Server zu erleichtern.

/// Generiert aus [page] Parameter für die Requests
String _pagingSettings(int page) {
  if (page < 0) {
    throw FormatException('page darf nicht kleiner als 1 sein', page);
  }
  return '?page=$page';
}

/// Erzeugt einen String um Timeline-Einträge abzufragen (GET-Request).
///
/// [page] und `size` sind  optional und bestimmen welche Seite und
/// wie viele Einträge pro Seite angefordert werden
String getTLEntries({int page = 0}) => '$timelineUrl${_pagingSettings(page)}';

/// Erzeugt einen String zurück der user-spezifisch
/// die favorisierten Einträge abfragt (GET-Request)
///
/// [page] und `limit` sind optional
String getFavedTLEntries({int page = 0}) =>
    '$timelineUrl/faved${_pagingSettings(page)}';

/// Erzeugt einen String um den Favoriten-Status
/// eines Eintrages zu togglen. (POST- bzw. DELETE-Request)
String toggleFavedTLEntry({required int entryID}) =>
    '$timelineUrl/faved/$entryID';

/// Erzeugt einen String um alle von einem [user]
/// versteckten Einträge abzufragen (GET-Request).
String getHiddenTLEntries() => '$timelineUrl/hidden';

/// Erzeugt einen String um den Versteckt-Status eines Eintrages zu togglen.
/// (POST- bzw. DELETE-Request)
String postHideTLEntry({required int entryID}) =>
    '$timelineUrl/hidden/$entryID';

String deleteAllHiddenStates() => '$timelineUrl/hidden';

String getPinnedTLEntries() => '$timelineUrl/pinned';

String getUnpinnedTLEntries() => '$timelineUrl/pinned/unpin';

String togglePinnedTLEntry({required int entryID}) =>
    '$timelineUrl/pinned/unpin/$entryID';

String toggleTLSourceSubscription(
        {required int sourceID, required String subscriptionType}) =>
    '$timelineUrl/sources/$sourceID/$subscriptionType';

String getTLSources() => '$timelineUrl/sources/settings';
