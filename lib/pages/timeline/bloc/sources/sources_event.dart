/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'sources_bloc.dart';

/// Archetype for all source events.
@Immutable()
abstract class SourceEvent extends Equatable {
  @override
  bool get stringify => true;

  @override
  List<Object> get props => [];
}

/// User wants to subscribe to a specific [source]
@Immutable()
class SourceEventSubscribe extends SourceEvent {
  /// Source that is handled.
  final TLSource source;

  /// [source] marks the Source that the user wants to subscribe to.
  SourceEventSubscribe({required this.source});

  @override
  List<Object> get props => [source];
}

/// User wants to unsubscribe from a specific [source]
class SourceEventUnsubscribe extends SourceEvent {
  /// Source that is handled.
  final TLSource source;

  /// [source] marks the Source that the user wants to unsubscribe from.
  SourceEventUnsubscribe({required this.source});

  @override
  List<Object> get props => [source];
}

/// Causes the [SourceBloc] to query the server for a list
/// of sources that the user is subscribed to.
class SourceEventGetSubscriptions extends SourceEvent {}

/// Causes the [SourceBloc] to query the server for a list
/// of all sources that are available to the user.
class SourceEventGetAllSubscriptions extends SourceEvent {}

/// Causes the [SourceBloc] to query the server for a list
/// of the default subscriptions.
class SourceEventGetDefaultSubscriptions extends SourceEvent {}

/// Causes the [SourceBloc] to query the server to restore all hidden entries.
class SourceEventRestoreHiddenEntries extends SourceEvent {}
