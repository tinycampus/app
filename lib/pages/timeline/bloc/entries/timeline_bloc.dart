/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../helper/helper.dart';
import '../../model/timeline_entry.dart';
import 'timeline_state_exceptions.dart';

part 'timeline_bloc.g.dart';
part 'timeline_event.dart';
part 'timeline_state.dart';

///Bloc-Klasse für die Kommunikation mit dem Server um [TLEntry]s zu generieren
///
/// [_last] Gibt an, ob noch weitere Einträge auf dem Server vorliegen oder
/// ob bereits alle abgerufen wurden.
///
/// `httpClient` wird für die direkte Kommunikation über das Internet verwendet
class TimelineBloc extends HydratedBloc<TEvent, TState> {
  /// Wrapper for [HttpRepository]. Contains a few qol-methods
  /// to parse the server response.
  final BlocHttpHelper thh;
  final Future<SharedPreferences> _sp;

  /// Timeline Bloc
  /// [sharedPreferences] is mainly used to make unit tests easier.
  TimelineBloc({
    required this.thh,
  })  : _sp = SharedPreferences.getInstance(),
        super(TStateUninitialized());

  ///[transform] wurde angepasst, damit die Anwendung 500 ms auf eine Antwort
  ///des Server wartet und nicht direkt ein Fehler erzeugt wird,
  ///weil der Server zu langsam ist.
  @override
  Stream<Transition<TEvent, TState>> transformEvents(
    Stream<TEvent> events,
    Stream<Transition<TEvent, TState>> Function(TEvent event) next,
  ) =>
      super.transformEvents(
        events.debounce(
          (event) {
            if (event is TEventReset) {
              return Stream.periodic(Duration(milliseconds: 0));
            }
            return Stream.periodic(Duration(milliseconds: 500));
          },
        ),
        next,
      );

  @override
  Stream<TState> mapEventToState(TEvent event) async* {
    yield await event._interaction(this);
  }

  @override
  TState fromJson(Map<String, dynamic> json) {
    try {
      return TStateLoaded.fromJson(json);
    } on Exception catch (_) {
      clear();
      return TStateUninitialized();
    }
  }

  @override
  Map<String, dynamic> toJson(TState state) {
    if (state is TStateLoaded) {
      try {
        return state.toJson();
      } on Exception catch (_) {
        return TStateUninitialized().toJson();
      }
    }
    return TStateUninitialized().toJson();
  }
}
