/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

class TimelineStateException implements Exception {
  final String message;

  @pragma('vm:entry-point')
  const TimelineStateException([this.message = ""]);

  @override
  String toString() =>
      'TimelineStateException${(message.isEmpty ? '' : ': $message')}';
}

class WrongJSONVersionException implements Exception {
  final String expectedVersionString;
  final String attemptedVersion;

  final String state;

  @pragma('vm:entry-point')
  const WrongJSONVersionException({
    required this.attemptedVersion,
    required this.expectedVersionString,
    required this.state,
  });

  @override
  String toString() => 'WrongJSONVersionException: expected json implementing '
      'version $expectedVersionString but found version $attemptedVersion '
      'while creating $state';
}
