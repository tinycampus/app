/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of '../timeline_page.dart';

typedef Visibility = bool Function();

const double _scale = 32.0;

final _pinnedImage = _buildImage(
  'assets/images/tinycampus/timeline_pinned_xxxhdpi.png',
);

final _recentImage = _buildImage(
  'assets/images/tinycampus/timeline_recent_xxxhdpi.png',
);

final _datedImage = _buildImage(
  'assets/images/tinycampus/timeline_dated_xxxhdpi.png',
);

Image _buildImage(String asset) => Image(
      image: ExactAssetImage(
        asset,
        scale: _scale,
      ),
      loadingBuilder: (context, child, loadingProgress) {
        if (loadingProgress == null) return child;
        return Center(
          child: CircularProgressIndicator(
            value: loadingProgress.expectedTotalBytes != null
                ? loadingProgress.cumulativeBytesLoaded /
                    loadingProgress.expectedTotalBytes!
                : null,
          ),
        );
      },
    );
