/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import '../../../common/assets_adapter.dart';

part 'timeline_source.g.dart';

/// Timeline source which contains all relevant information
@CopyWith()
@Immutable()
@JsonSerializable()
class TLSource extends Equatable {
  /// Name of the source
  final String name;

  /// Source ID
  final int id;

  /// Description of source
  final String description;

  /// Subscription state
  final bool subscribed;

  /// Source logo
  @JsonKey(fromJson: _logoFromJson, toJson: _logoToJson)
  final AssetImage logo;

  /// Creates a new source object for the timeline
  TLSource({
    required this.name,
    required this.id,
    required this.description,
    required this.subscribed,
    required this.logo,
  });

  /// Creates a new source object form a json object.
  factory TLSource.fromJson(Map<String, dynamic> json) =>
      _$TLSourceFromJson(json);

  Map<String, dynamic> toJson() => _$TLSourceToJson(this);

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [name, id, description, subscribed];

  static AssetImage _logoFromJson(String path) => path.isEmpty
      ? AssetImage(AssetAdapter.tinyCampusLogoWithPadding())
      : AssetImage('assets/images/$path');

  static String _logoToJson(AssetImage image) => image.assetName.substring(14);
}
