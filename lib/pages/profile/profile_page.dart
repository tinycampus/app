/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../bloc/authentication/auth_bloc.dart';
import '../../bloc/user/user_bloc.dart';
import '../../common/constants/routing_constants.dart';
import '../../common/tc_theme.dart';
import '../../common/widgets/dialog/tc_dialog.dart';
import 'profile_menu_choice.dart';
import 'widgets/badge_achievements.dart';
import 'widgets/exp_profile_picture.dart';

/// A Page to display your Profile.
class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final double _width = window.physicalSize.width / window.devicePixelRatio;
  ValueNotifier<bool> deleteUserContent = ValueNotifier<bool>(false);

  @override
  void initState() {
    BlocProvider.of<UserBloc>(context).add(UserFetchEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) => BlocBuilder<UserBloc, UserState>(
        builder: (context, state) => Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: PreferredSize(
            preferredSize: Size(_width, 10),
            child: AppBar(
              elevation: 0,
            ),
          ),
          body: NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overscroll) {
              overscroll.disallowGlow();
              return false;
            },
            child: SingleChildScrollView(
              child: Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  Positioned(
                    left: -(_width * .3 / 2),
                    top: -(_width / 1.4),
                    child: Container(
                      width: _width * 1.3,
                      height: _width * 1.3,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Theme.of(context).primaryColor,
                          boxShadow: [
                            BoxShadow(color: Colors.grey, blurRadius: 5)
                          ]),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          ExpProfilePicture(
                            user: state.user,
                            hearts: state.user.totalHearts,
                          ),
                          _buildUserName(state.user.displayName),
                          Align(
                            alignment: Alignment.center,
                            child: I18nText(
                              'profile.level',
                              translationParams: {
                                'level': '${state.user.level}'
                              },
                              child: Text(
                                'Level',
                                style: Theme.of(context).textTheme.headline3,
                              ),
                            ),
                          ),
                          Divider(
                            color: Colors.transparent,
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: PopupMenuButton<Choice>(
                      color: Theme.of(context).primaryColor,
                      itemBuilder: (context) => choices
                          .map((choice) => PopupMenuItem<Choice>(
                                value: choice,
                                child: ListTile(
                                  title: Text(choice.title),
                                  trailing: Icon(choice.icon),
                                  onTap: () => TCDialog.showCustomDialog(
                                    context: context,
                                    functionActionText: FlutterI18n.translate(
                                        context, 'common.actions.logout_caps'),
                                    bodyText: FlutterI18n.translate(
                                        context, 'common.messages.logout'),
                                    headlineText: FlutterI18n.translate(
                                        context, 'common.actions.logout_caps'),
                                    popAfterFunction: false,
                                    alternativeWidget: SizedBox(
                                      height: 210,
                                      child: SingleChildScrollView(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 32.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                FlutterI18n.translate(context,
                                                    'common.messages.logout'),
                                                textAlign: TextAlign.start,
                                              ),
                                              ValueListenableBuilder(
                                                valueListenable:
                                                    deleteUserContent,
                                                builder: (context, _, child) =>
                                                    CheckboxListTile(
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          vertical: 16,
                                                          horizontal: 0),
                                                  value:
                                                      deleteUserContent.value,
                                                  title: Text(
                                                    FlutterI18n.translate(
                                                      context,
                                                      'common.messages'
                                                      '.delete_data',
                                                    ),
                                                  ),
                                                  subtitle: Text(
                                                    FlutterI18n.translate(
                                                      context,
                                                      'common.messages'
                                                      '.delete_data_explain',
                                                    ),
                                                  ),
                                                  onChanged: (value) {
                                                    deleteUserContent.value =
                                                        !deleteUserContent
                                                            .value;
                                                  },
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    onConfirm: () {
                                      RepositoryProvider.of<AuthBloc>(context)
                                          .add(AuthLogoutEvent(
                                              deleteUserData:
                                                  deleteUserContent.value,
                                              deleteSharedPrefs:
                                                  deleteUserContent.value));
                                      Navigator.pushNamedAndRemoveUntil(
                                        context,
                                        loginRoute,
                                        (route) => false,
                                      );
                                      // });
                                    },
                                  ),
                                ),
                              ))
                          .toList(),
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      SizedBox(
                        height: _width * 0.6,
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20.0),
                        child: BadgeAchievements(user: state.user),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      );

  Column _buildUserName(String userName) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 280,
            child: Text(
              userName,
              style: userName.length >= 25
                  ? Theme.of(context)
                      .textTheme
                      .headline3
                      ?.copyWith(color: CorporateColors.tinyCampusTextSoft)
                  : Theme.of(context).textTheme.headline2?.copyWith(
                        color: CurrentTheme().textSoftLightBlue,
                      ),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      );
}
