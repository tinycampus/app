/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../../../bloc/user/model/badge_model.dart';
import '../../../common/tc_theme.dart';

class BadgeProgressBar extends StatelessWidget {
  final Badge badge;
  final int _neededConditions;
  final int _currentConditions;

  BadgeProgressBar({Key? key, required this.badge})
      : _neededConditions = badge.conditions[0].needed,
        _currentConditions =
            min(badge.conditions[0].current, badge.conditions[0].needed),
        super(key: key);

  @override
  Widget build(BuildContext context) =>
      Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: <Widget>[
        SizedBox(
          width: 60,
          child: Text(
            '$_currentConditions/$_neededConditions',
            style: Theme.of(context)
                .textTheme
                .bodyText1
                ?.copyWith(color: CorporateColors.tinyCampusIconGrey),
          ),
        ),
        Expanded(
          flex: 10,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                _currentConditions == 0
                    ? Colors.white
                    : CorporateColors.tinyCampusBlue,
                Colors.white
              ]),
              border: Border.all(),
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: LinearPercentIndicator(
              lineHeight: 15,
              animationDuration: 900,
              curve: Curves.easeInOut,
              animation: true,
              percent: _currentConditions / _neededConditions,
              progressColor: CorporateColors.tinyCampusBlue,
              linearStrokeCap: LinearStrokeCap.roundAll,
              backgroundColor: Colors.white,
            ),
          ),
        ),
      ]);
}
