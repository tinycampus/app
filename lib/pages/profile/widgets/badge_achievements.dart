/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../bloc/user/model/badge_model.dart';
import '../../../bloc/user/user_bloc.dart';
import '../../../pages/profile/widgets/badge_unlocked_achievements.dart';
import '../../../pages/profile/widgets/badge_widget.dart';

/// A Showcase based on the users chosen badges
class BadgeAchievements extends StatelessWidget {
  const BadgeAchievements({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) => BlocBuilder<UserBloc, UserState>(
        builder: (context, state) => Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            I18nText(
              'badges.showcase.title',
              child: Text(
                'Achievements',
                style: Theme.of(context).textTheme.headline2,
                textAlign: TextAlign.center,
              ),
            ),
            const Divider(
              color: Colors.transparent,
            ),
            BadgeUnlockedAchievements(
              unlockedBadges: _getUnlockedBadges(),
            ),
            const Divider(
              color: Colors.transparent,
            ),
            ..._getLockedBadgeWidgets(),
          ],
        ),
      );

  /// Returns all unlocked [Badge]s, sorted by most recent
  List<Badge> _getUnlockedBadges() => user.achievedBadges
    ..sort((a, b) => b.unlockDate!.compareTo(a.unlockDate!));

  /// Returns all locked [Badge]s as [BadgeWidget]s
  List<BadgeWidget> _getLockedBadgeWidgets() =>
      user.lockedBadges.map((b) => BadgeWidget(badge: b)).toList();
}
