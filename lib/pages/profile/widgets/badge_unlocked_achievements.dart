/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/material.dart';

import '../../../bloc/user/model/badge_model.dart';
import '../controller/badge_loop_controller.dart';
import '../profile_badge_card.dart';

const _constraints = BoxConstraints(minHeight: 100, maxHeight: 100);

class BadgeUnlockedAchievements extends StatelessWidget {
  BadgeUnlockedAchievements({
    Key? key,
    required this.unlockedBadges,
  }) : super(key: key);

  final List<Badge> unlockedBadges;

  @override
  Widget build(BuildContext context) => Container(
        alignment: Alignment.center,
        constraints: _constraints,
        child: ListView.separated(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          shrinkWrap: (unlockedBadges.length <= 3),
          // Make the content be the separator so we have
          // padding items at start and end
          itemBuilder: (_, index) => Container(width: 20),
          separatorBuilder: (_, index) => (index < unlockedBadges.length)
              ? _BadgeUnlockedDisplay(badge: unlockedBadges[index])
              : _BadgeUnlockedDisplay.placeHolder(),
          // Because we use the separators to be the content
          // this has to have an extra item
          itemCount: max(unlockedBadges.length, 3) + 1,
        ),
      );
}

class _BadgeUnlockedDisplay extends StatelessWidget {
  final Badge badge;
  final bool interactive;

  final FlareControls _ctrl = BadgeLoopController();

  _BadgeUnlockedDisplay({
    Key? key,
    required this.badge,
    this.interactive = true,
  }) : super(key: key);

  _BadgeUnlockedDisplay.placeHolder()
      : badge = Badge.placeholder(),
        interactive = false;

  @override
  Widget build(BuildContext context) => RawMaterialButton(
        constraints: _constraints.copyWith(minWidth: 80, maxWidth: 80),
        shape: CircleBorder(),
        fillColor: Theme.of(context).primaryColor,
        elevation: 3,
        onPressed: () {
          if (interactive) {
            Navigator.of(context).push(
              PageRouteBuilder(
                opaque: false,
                transitionDuration: Duration(milliseconds: 0),
                pageBuilder: (_, __, ___) => ProfileBadgeCard(badge),
              ),
            );
          }
        },
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: FlareActor(
            "assets/flare/${badge.asset}",
            alignment: Alignment.center,
            fit: BoxFit.scaleDown,
            antialias: false,
            animation: badge.animationLevel,
            controller: _ctrl,
            callback: _ctrl.onCompleted,
            isPaused: false,
          ),
        ),
      );
}
