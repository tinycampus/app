/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'module.g.dart';

/// A model for available modules in the app.
@JsonSerializable(anyMap: true, explicitToJson: true)
class Module extends Equatable {
  /// The ID of this, specified in `module_list.dart`.
  /// May not be 0 so the server can represent a [enabled]-value of false.
  final int id;

  /// The name of this, most often used as prefix for their routes.
  final String name;

  /// TODO: use this for semantics!
  /// The description of this, used as a text in `HomeSettingsPage`
  final String description;

  /// Path to the asset image file.
  final String pathToImage;

  /// Path used by the apps navigation, see `routing_constants.dart`.
  final String onTapPath;

  /// Defines visibility in HomePage.
  /// Server-side false is represented by a negative id.
  final bool enabled;

  /// TODO: find a way to make this const!
  Module({
    required this.id,
    required this.name,
    required this.description,
    required this.pathToImage,
    required this.onTapPath,
    this.enabled = true,
  }) : assert(id != 0) {
    if (id == 0) {
      throw ArgumentError("Parameter id was 0.");
    }
  }

  Module._toggledFrom(Module module)
      : id = module.id,
        name = module.name,
        description = module.description,
        pathToImage = module.pathToImage,
        onTapPath = module.onTapPath,
        enabled = !module.enabled;

  // TODO: find better naming for this!
  /// Toggles [enabled] for visibility on `HomePage` etc.
  Module toToggled() => Module._toggledFrom(this);

  /// Creates this from a JSON (see [JsonSerializable]).
  factory Module.fromJson(Map<String, dynamic> json) => _$ModuleFromJson(json);

  /// Serializes this to a JSON (see [JsonSerializable]).
  Map<String, dynamic> toJson() => _$ModuleToJson(this);

  @override
  String toString() => name;

  /// A [List] that defines on which properties this is compared
  /// to other instances (see [Equatable]).
  @override
  List<Object> get props => [
        id,
        name,
        description,
        pathToImage,
        onTapPath,
        enabled,
      ];
}
