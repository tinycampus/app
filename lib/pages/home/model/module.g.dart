// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'module.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Module _$ModuleFromJson(Map json) => Module(
      id: json['id'] as int,
      name: json['name'] as String,
      description: json['description'] as String,
      pathToImage: json['pathToImage'] as String,
      onTapPath: json['onTapPath'] as String,
      enabled: json['enabled'] as bool? ?? true,
    );

Map<String, dynamic> _$ModuleToJson(Module instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'pathToImage': instance.pathToImage,
      'onTapPath': instance.onTapPath,
      'enabled': instance.enabled,
    };
