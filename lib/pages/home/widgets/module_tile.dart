/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../bloc/home_bloc.dart';
import '../model/module.dart';

/// A [CheckboxListTile] used `HomeSettingsPage`.
class ModuleTile extends StatelessWidget {
  /// [Module] to be displayed.
  final Module module;

  /// [index] of [module] in [HomeState.modules].
  final int index;

  const ModuleTile({
    Key? key,
    required this.module,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        color: Theme.of(context).primaryColor,
        child: CheckboxListTile(
          activeColor: CurrentTheme().tcDarkBlue,
          secondary: AspectRatio(
            aspectRatio: 1,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(module.pathToImage),
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
          key: Key(module.name),
          value: module.enabled,
          onChanged: (enable) => BlocProvider.of<HomeBloc>(context).add(
            enable ?? false ? HomeEnableEvent(index) : HomeDisableEvent(index),
          ),
          title: I18nText(
            module.name,
            child: Text(
              '',
              style: Theme.of(context).textTheme.headline3,
            ),
          ),
          subtitle: I18nText(
            module.description,
            child: Text(
              '',
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          isThreeLine: true,
        ),
      );
}
