/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../common/constants/routing_constants.dart';

/// A [Card] used as footer and shortcut to `HomeSettingsPage`
/// used in `HomePage`.
class HomeFooter extends StatelessWidget {
  const HomeFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => FractionallySizedBox(
        widthFactor: .5,
        child: AspectRatio(
          aspectRatio: 1,
          child: Card(
            color: Theme.of(context).primaryColor,
            margin: EdgeInsets.all(7.5),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(7.5),
            ),
            elevation: 7,
            child: InkWell(
              onTap: () => Navigator.pushNamed(
                context,
                homeSettingsRoute,
              ),
              child: Container(
                height: 100,
                constraints: BoxConstraints.expand(),
                child: Icon(
                  Icons.add,
                  // TODO: use themed colors?
                  color: Colors.grey[300],
                  size: 75,
                ),
              ),
            ),
          ),
        ),
      );
}
