/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'bloc/home_bloc.dart';
import 'widgets/module_tile.dart';

/// A settings page of the TC-App Home Page showing available Modules
/// and allowing those to be reordered, enabled and disabled.
///
/// Uses [HomeBloc] for business logic
/// and updates when [HomeState] changes.
class HomeSettingsPage extends StatelessWidget {
  const HomeSettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: I18nText('home.settings.title'),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) => ReorderableListView(
            header: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 32.0),
              margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
              child: I18nText(
                'home.settings.modify_arrangement',
                child: Text(
                  '',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w400),
                ),
              ),
              color: Theme.of(context).primaryColor,
            ),
            children: state.modules
                .map((module) => ModuleTile(
                      key: Key(module.name),
                      module: module,
                      index: state.modules.indexOf(module),
                    ))
                .toList(),
            onReorder: (oldIndex, newIndex) =>
                BlocProvider.of<HomeBloc>(context).add(
              HomeReorderEvent(
                oldIndex,
                (newIndex > oldIndex) ? newIndex - 1 : newIndex,
              ),
            ),
          ),
        ),
      );
}
