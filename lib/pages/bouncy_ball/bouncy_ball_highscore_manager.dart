/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'dart:convert';

import '../../common/constants/api_constants.dart';
import '../../repositories/http/http_repository.dart';
import 'model/highscores.dart';

class BouncyBallHighScoreManager {
  static final BouncyBallHighScoreManager _instance =
      BouncyBallHighScoreManager._internal();

  factory BouncyBallHighScoreManager() => _instance;

  HighScores? tables;
  bool updated = false;

  BouncyBallHighScoreManager._internal() {
    tables = null;
    updated = false;
  }

  Future<HighScores> updateTable() async {
    var res = await HttpRepository().get("$bbUrl/highscore");
    final utf8Res = utf8.decode(res.bodyBytes);
    final json = jsonDecode(utf8Res);
    tables = HighScores.fromJson(json);
    updated = true;
    return Future.value(tables);
  }

  Future<HighScores> sendNewHighScore(String name, int score) async {
    var map = {'name': name, 'score': score};
    await HttpRepository().post(
      "$bbUrl/highscore",
      body: jsonEncode(map),
    );
    return updateTable();
  }
}
