/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:vector_math/vector_math.dart';

import '../../common/styled_print.dart';

enum EType {
  fly,
  upper,
  lower,
  bouncer,
  liftBouncer,
  dropBouncer,
  heavy,
  player,
  particle,
}

class GameObject {
  Vector2 pos = Vector2(300, 50);
  Vector2 vel = Vector2(0, 0);
  double hitBox = 40;

  // double hitBox = Random().nextDouble() * 80.0 + 40;
  // Sphere hitBox = Sphere.centerRadius(Vector3(0, 0, 0), 140);
  double alive = 190;
  List<GameObject> particles = <GameObject>[];
  List<GameObject> frontParticles = <GameObject>[];
  final maxParticles = 12;
  double maxParticleVelocity = 5;
  List<GameObject> touches = <GameObject>[];
  int id = Random().nextInt(999999999);
  bool marked = false;
  bool jumped = false;
  ValueNotifier<bool> jumping = ValueNotifier<bool>(false);
  ValueNotifier<bool> doubleJumped = ValueNotifier<bool>(false);
  static const double floor = 50;
  static const double defaultBegin = 360;
  static const double randomSpeedConst = 10.0;
  Vector2 jumpAcceleration = Vector2(0, 8);
  EType type = EType.upper;

  void createRandomParticles() {
    for (var i = 0; i < maxParticles; i++) {
      particles.add(GameObject.particle(pos, vel));
    }
  }

  GameObject.random()
      : pos = Vector2(150, Random().nextBool() ? 80 : 160),
        vel = Vector2(
          (Random().nextDouble() - 0.5) * randomSpeedConst,
          (Random().nextDouble() - 0.5) * randomSpeedConst,
        );

  GameObject.particle(Vector2 originalPos, Vector2 originalVel)
      : pos = Vector2(originalPos.x, originalPos.y),
        vel = Vector2(
              (Random().nextDouble()) - 0.5,
              (Random().nextDouble()) - 0.5,
            ) +
            (originalVel / 6),
        hitBox = 5,
        type = EType.particle {
    vel = vel * maxParticleVelocity;
  }

  GameObject.player()
      : pos = Vector2(80, 50),
        vel = Vector2(0, 0),
        hitBox = 40,
        type = EType.player {
    // createRandomParticles();
  }

  GameObject.fly()
      : pos = Vector2(500, 250),
        vel = Vector2(-5, 0),
        type = EType.upper {
    // createRandomParticles();
  }

  GameObject.upper()
      : pos = Vector2(500, 150),
        vel = Vector2(-5, 0),
        type = EType.upper {
    // createRandomParticles();
  }

  GameObject.upper2()
      : pos = Vector2(500, 250),
        vel = Vector2(-5, 0),
        type = EType.upper {
    // createRandomParticles();
  }

  GameObject.lower()
      : pos = Vector2(500, 50),
        vel = Vector2(-5, 0),
        type = EType.lower {
    // createRandomParticles();
  }

  GameObject.bouncer()
      : pos = Vector2(500, 250),
        vel = Vector2(-5, 0),
        type = EType.bouncer {
    // createRandomParticles();
  }

  GameObject.liftBouncer()
      : pos = Vector2(500, 250),
        vel = Vector2(-5, 14),
        type = EType.liftBouncer {
    // createRandomParticles();
  }

  GameObject.dropBouncer()
      : pos = Vector2(500, 250),
        vel = Vector2(-5, -10),
        type = EType.dropBouncer {
    // createRandomParticles();
  }
  GameObject.heavy()
      : pos = Vector2(500, 250),
        vel = Vector2(-5, -10),
        type = EType.heavy {
    // createRandomParticles();
  }

  GameObject();

  void checkCollision(GameObject b) {
    if (this != b) {
      if (pos.distanceTo(b.pos) < hitBox || b.pos.distanceTo(pos) < b.hitBox) {
        // sprint("collision");
        addCollision(b);
      }
    }
  }

  void addCollision(GameObject b) {
    if (!touches.contains(b)) {
      touches.add(b);
    }
    if (!b.touches.contains(this)) {
      touches.add(this);
    }
  }

  void requestJump() {
    jumped = true;
  }

  void jump(double delta) {
    if (!jumping.value) {
      sprint("jumped");
      jumping.value = true;
      vel -= vel;
      vel += jumpAcceleration;
      // vel+=Vector2(1,0);
    } else if (!doubleJumped.value && jumping.value) {
      sprint("doubleJumped.value");
      doubleJumped.value = true;
      vel -= vel;
      vel += jumpAcceleration;
    }
    //  else if (doubleJumped.value){
    //   vel = Vector2(0,-5);
    // }
  }

  void multiplyVel(double val) {
    vel *= val;
  }

  void addVel(Vector2 val) {
    vel += val;
  }

  void playerLanding(double delta) {
    for (var i = 0; i < maxParticles * 5; i++) {
      if (vel.y > 2.5) {
        particles.add(GameObject.particle(pos, vel.clone()..y = 0)
          ..multiplyVel(0.8)
          ..alive = 10 + Random().nextInt(30).toDouble()
          ..addVel(Vector2(0, Random().nextDouble() * 3.5)));
      } else {
        particles.add(GameObject.particle(pos, vel.clone()..y = 0)
          ..multiplyVel(0.8)
          ..alive = 10 + Random().nextInt(30).toDouble()
          ..addVel(Vector2(0, Random().nextDouble() * 0.5)));
      }
    }
    // vel+=Vector2(-2,0);
    for (var i = 0; i < maxParticles; i++) {
      frontParticles.add(GameObject.particle(pos, vel)
        ..multiplyVel(0.8)
        ..alive = 10 + Random().nextInt(30).toDouble()
        ..addVel(Vector2(0, Random().nextDouble() * 0.3)));
    }
  }

  void positionUpdate(double delta) {
    pos.add(vel * delta);
    switch (type) {
      case EType.fly:
      case EType.upper:
      case EType.lower:
        break;
      case EType.player:
        // sprint(vel.y);
        vel.y -= 0.27 * delta;
        if (pos.y <= floor) {
          vel.y = -(vel.y / 4);
          if (jumping.value) {
            playerLanding(delta);
          }
          pos.y = floor;
          jumping.value = false;
          jumped = false;
          doubleJumped.value = false;
          if (vel.y < 1 && !jumping.value) {
            vel.y = 0;
          }
        }
        break;
      case EType.particle:
        // sprint(vel.y);
        vel.y -= 0.1 * delta;
        if (pos.y <= floor) {
          vel.y = -vel.y * 0.7;
          pos.y = floor;
        }
        break;
      case EType.bouncer:
      case EType.liftBouncer:
      case EType.dropBouncer:
      case EType.heavy:
        applyGravity(delta);
        if (pos.y <= floor) {
          playerLanding(delta);
          bounce();
          pos.y = floor;
          jumping.value = false;
          jumped = false;
          doubleJumped.value = false;
        }
        // TODO: think about better atomization
        break;
    }
  }

  void bounce() {
    vel.y = -vel.y * (type == EType.heavy ? 0.5 : 1);
  }

  void applyGravity(double delta) {
    vel.y -= 0.3 * delta * (type == EType.heavy ? 1 : 1); // gravity
  }

  void particleUpdate(List<GameObject> particles, int d) {
    final toBeDeleted = <GameObject>[];
    // player.update(elapsedTime);
    // if (player.touches.isNotEmpty) {
    //   loop.stop();
    // }
    for (var i = 0; i < particles.length; i++) {
      particles[i].update(d);
      if (particles[i].marked) toBeDeleted.add(particles[i]);
    }
    for (var item in toBeDeleted) {
      particles.remove(item);
    }
  }

  void update(int d) {
    final delta = (d / 17600);
    particleUpdate(particles, d);
    particleUpdate(frontParticles, d);
    // for (var particle in particles) {
    //   particle.update(d);
    // }
    if (jumped) {
      jumped = false;
      jump(delta);
    }
    positionUpdate(delta);

    switch (type) {
      case EType.fly:
      case EType.upper:
      case EType.lower:
      case EType.bouncer:
      case EType.liftBouncer:
      case EType.particle:
      case EType.dropBouncer:
      case EType.heavy:
        // if (type == EType.particle) {
        //   alive--;
        //   if (alive < 8){
        //     alive = 0;
        //   }
        // }
        // alive--;
        reduceLife(delta);
        // hitBox += 1.2;
        if (touches.isNotEmpty) {
          // hitBox = Random().nextDouble() * 45;
          hitBox += 1.2;
        }
        if (hitBox <= 0) marked = true;
        if (alive <= 0) marked = true;
        break;
      case EType.player:
        break;
    }
  }

  void reduceLife(double delta) {
    alive -= delta;
  }
}
