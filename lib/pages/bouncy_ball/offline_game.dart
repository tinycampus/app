/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/styled_print.dart';
import '../../common/tc_theme.dart';
import '../../common/widgets/tc_button.dart';
import 'bouncy_ball_highscore_manager.dart';
import 'game_object.dart';
import 'model/highscores.dart';
import 'model/score.dart';
import 'offline_game_painter.dart';
import 'widgets/highscore_table.dart';

class OfflineGameWidget extends StatefulWidget {
  OfflineGameWidget({Key? key}) : super(key: key);

  @override
  _OfflineGameWidgetState createState() => _OfflineGameWidgetState();
}

class _OfflineGameWidgetState extends State<OfflineGameWidget>
    with TickerProviderStateMixin {
  int amountTicks = 0;
  late AnimationController animIntroController;
  late AnimationController animRotationController;
  ValueNotifier<bool> gameEnded = ValueNotifier<bool>(false);
  ValueNotifier<bool> sendScoreBlocked = ValueNotifier<bool>(false);
  ValueNotifier<bool> hasRankings = ValueNotifier<bool>(false);
  ValueNotifier<int> personalHighscore = ValueNotifier<int>(0);
  ValueNotifier<HighScores?> tables = ValueNotifier<HighScores?>(null);
  ValueNotifier<PossibleGameEndStates> state =
      ValueNotifier<PossibleGameEndStates>(PossibleGameEndStates.nothingNew);
  TextEditingController controller = TextEditingController();
  Animation<double>? gameLoop;
  AnimationController? loop;
  bool showPersonal = true;
  bool showDebug = false;
  List<GameObject> units = <GameObject>[];
  late GameObject player;
  late Painter painter;
  late ui.Image imageRef;
  Duration buttonTransition = Duration(milliseconds: 260);
  Duration? time;
  Stopwatch? watch;
  Duration highScore = Duration.zero;
  double? timeFactor;
  double? timeFactorLimit;
  Duration lastDeltaTime = Duration.zero;
  Duration deltaTime = Duration.zero;
  int spawnCap = 830000;
  int spawnInterval = 1600000;
  int spawnCounter = 0;

  @override
  void initState() {
    super.initState();
    initPainter();
    initAnimations();
    initGameWorld();
    initPersonalHighscore();
  }

  void restartGame() {
    units.clear();
    initAnimations();
    initGameWorld();
    setState(() {});
  }

  Future<void> loadData() async {
    try {
      final data =
          await rootBundle.load('assets/images/praxis_phase/wrong.png');

      imageRef = await loadImage(Uint8List.view(data.buffer));
    } on Exception catch (e) {
      eprint(e.toString());
    }
  }

  final String highscoreKey = "bouncy_ball_personal_highscore";
  Future<void> setHighscore() async {
    sprint(getCurrentScore());
    final sp = await SharedPreferences.getInstance();
    if (sp.containsKey(highscoreKey)) {
      if (sp.get(highscoreKey) is! int) {
        sp.remove(highscoreKey);
      }
      if ((sp.getInt(highscoreKey) ?? 0) < getCurrentScore()) {
        sp.setInt(highscoreKey, getCurrentScore());
        personalHighscore.value = getCurrentScore();
        setState(() {});
      }
    } else {
      sp.setInt(highscoreKey, getCurrentScore());
      personalHighscore.value = getCurrentScore();
      setState(() {});
    }
  }

  Future<void> initPersonalHighscore() async {
    final sp = await SharedPreferences.getInstance();
    var val = 0;
    if (sp.containsKey(highscoreKey)) {
      if (sp.get(highscoreKey) is! int) {
        sp.remove(highscoreKey);
      } else {
        try {
          val = sp.getInt(highscoreKey) ?? 0;
          setState(() {});
        } on Exception catch (e) {
          eprint(e);
        }
      }
    }
    personalHighscore.value = val;

    if (!BouncyBallHighScoreManager().updated) {
      tables.value = await BouncyBallHighScoreManager().updateTable();
    } else {
      tables.value = BouncyBallHighScoreManager().tables;
    }
    personalHighscore.value = tables.value?.personal.score ?? val;
    if (personalHighscore.value == -1 && val != -1) {
      personalHighscore.value = val;
    }
  }

  Future<ui.Image> loadImage(List<int> img) async {
    final completer = Completer<ui.Image>();

    ui.decodeImageFromList(img as Uint8List, (img) {
      // TODO: this cast seems dangerous, test
      setState(() {
        // isImageloaded = true;
      });
      return completer.complete(img);
    });
    return completer.future;
  }

  void initGameWorld() {
    sprint("Init Game World");
    hasRankings.value = false;
    state.value = PossibleGameEndStates.gameRunning;
    buttonTransition = Duration(milliseconds: 520);
  }

  void initPainter() {
    painter = Painter(
      null,
      units,
      // imageRef,
    );
  }

  @override
  void dispose() {
    loop?.dispose();
    super.dispose();
  }

  void resetAnimations() {}
  void cyclingAnimationListener(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      loop?.reverse();
    } else if (status == AnimationStatus.dismissed) {
      loop?.forward();
    }
  }

  void collisionDetection() {
    for (var i = 0; i < units.length; i++) {
      units[i].touches.clear();
      player.touches.clear();
    }
    // final touches = <GameObject>[];
    for (var i = 0; i < units.length; i++) {
      // units[i].update(elapsedTime);
      // if (units[i].marked) toBeDeleted.add(units[i]);
      final a = units[i];
      player.checkCollision(a);
      // for (var j = 0; j < units.length; j++) {
      //   final b = units[j];
      //   a.checkCollision(b);
      // }
    }
    // for (var item in toBeDeleted) {
    //   // units.remove(item);
    // }
  }

  void initAnimations() {
    player = GameObject.player();
    units.add(player);
    time = Duration();
    watch = Stopwatch();
    highScore = Duration.zero;
    spawnCounter = 0;
    spawnInterval = 1600000;
    spawnCap = 830000;
    timeFactor = 1.0;
    timeFactorLimit = 1.6;
    loop = loop ??
        AnimationController(duration: const Duration(days: 365), vsync: this);
    gameLoop = CurvedAnimation(parent: loop!, curve: Curves.easeOut);
    lastDeltaTime = Duration.zero;
    deltaTime = Duration.zero;
    loop
      ?..addStatusListener(cyclingAnimationListener)
      ..addListener(() {
        final updateTime = (calcAndReturnDeltaTime()).clamp(0, 64000);
        update(updateTime);
      });
    watch?.start();
    gameEnded.value = false;
    loop?.forward();
  }

  void update(int elapsedTime) {
    // spawnCounter = spawnCounter + deltaTime;
    highScore = highScore + Duration(microseconds: elapsedTime ~/ 1000);
    if ((timeFactor ?? 0) < (timeFactorLimit ?? 0)) {
      timeFactor =
          (1 + (highScore.inMilliseconds / 300)).clamp(1.0, timeFactorLimit!);
    }
    elapsedTime = (elapsedTime * timeFactor!).toInt();
    // sprint(timeFactor);
    spawnLoop(elapsedTime);
    final toBeDeleted = <GameObject>[];
    player.update(elapsedTime);
    if (player.touches.isNotEmpty) {
      // GAME HAS ENDED
      units.clear();
      loop?.stop();
      endState();
      setHighscore();
      gameEnded.value = true;
    }
    for (var i = 0; i < units.length; i++) {
      units[i].update(elapsedTime);
      units[i].touches.clear();

      if (units[i].marked) toBeDeleted.add(units[i]);
    }
    for (var item in toBeDeleted) {
      units.remove(item);
    }
    collisionDetection();
  }

  int calcAndReturnDeltaTime() {
    deltaTime = watch?.elapsed ?? Duration.zero;
    watch
      ?..reset()
      ..start();
    return deltaTime.inMicroseconds;
  }

  void spawnLoop(int elapsedTime) {
    spawnCounter += elapsedTime;
    if (spawnCounter > spawnInterval) {
      if (spawnInterval > spawnCap) {
        spawnInterval = spawnInterval ~/ 1.012;
        // spawnInterval = spawnInterval - 3200000~/32;
      }
      spawnCounter -= spawnCounter;
      switch (Random().nextInt(8)) {
        case 0:
          units.add(GameObject.liftBouncer());
          units.add(GameObject.lower());
          break;
        case 1:
          units.add(GameObject.liftBouncer());
          units.add(GameObject.upper());
          break;
        case 2:
          units.add(GameObject.upper());
          units.add(GameObject.lower());
          break;
        case 3:
          units.add(GameObject.lower());
          break;
        case 4:
          units.add(GameObject.bouncer());
          break;
        case 5:
          units.add(GameObject.dropBouncer());
          units.add(GameObject.lower());
          break;
        case 6:
          units.add(GameObject.upper());
          units.add(GameObject.upper2());
          break;
        case 7:
          units.add(GameObject.upper());
          units.add(GameObject.heavy());
          break;
        default:
          break;
      }
    }
  }

  @override
  Widget build(BuildContext context) => Container(
        color: Color(0xffE8AE2F),
        child: Column(
          children: [
            Expanded(
              child: Stack(
                alignment: Alignment.bottomCenter,
                clipBehavior: Clip.none,
                children: [
                  bgImageScrolling(),
                  bgCloudsScrolling(),
                  bottomImageScrolling(),
                  Positioned(left: 0, top: 48, child: BackButton()),
                  renderScore(),
                  Positioned(
                      bottom: 0,
                      left: 0,
                      child: AnimatedBuilder(
                        animation: loop ?? ValueNotifier<bool>(false),
                        builder: (context, child) => CustomPaint(
                          painter: Painter(
                            null,
                            units,
                            // imageRef,
                          ),
                        ),
                      )),
                ],
              ),
            ),
            // ValueListenableBuilder(
            //     valueListenable: state,
            //     builder: (context, value, child) =>
            //         Text(state.value.toString())),
            jumpButtonWidget(),
            if (showDebug) showDebugOptions(),
          ],
        ),
      );

  Widget bottomImageScrolling() => Positioned(
        left: 0,
        right: 0,
        child: AnimatedBuilder(
          animation: loop ?? ValueNotifier<bool>(false),
          builder: (context, child) => SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            child: Container(
              height: 30,
              width: 500,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    "assets/images/bouncy_ball/sand.png",
                  ),
                  repeat: ImageRepeat.repeat,
                  alignment: FractionalOffset(
                      -highScore.inMicroseconds.toDouble() / 200, 0),
                ),
              ),
              clipBehavior: Clip.none,
            ),
          ),
        ),
      );

  Widget bgImageScrolling() => Positioned(
        left: 0,
        right: 0,
        child: AnimatedBuilder(
          animation: loop ?? ValueNotifier<bool>(false),
          builder: (context, child) => SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 2000),
              child: Container(
                height: 900,
                width: 2600,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage(
                    "assets/images/bouncy_ball/bgbeach.png",
                  ),
                  fit: BoxFit.contain,
                  repeat: ImageRepeat.repeat,
                  alignment: FractionalOffset(
                      -highScore.inMicroseconds.toDouble() / 22450, 0),
                )),
                clipBehavior: Clip.none,
              ),
            ),
          ),
        ),
      );
  Widget bgCloudsScrolling() => Positioned(
        left: 0,
        right: 0,
        bottom: 150,
        child: AnimatedBuilder(
          animation: loop ?? ValueNotifier<bool>(false),
          builder: (context, child) => SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            child: Container(
              height: 50,
              width: 2500,
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: AssetImage(
                  "assets/images/bouncy_ball/clouds.png",
                ),
                repeat: ImageRepeat.repeat,
                alignment: FractionalOffset(
                    -highScore.inMicroseconds.toDouble() / 17000, 0),
              )),
              clipBehavior: Clip.none,
            ),
          ),
        ),
      );

  Row showDebugOptions() => Row(
        children: [
          Expanded(
            child: TCButton(
              buttonLabel: "reset",
              onPressedCallback: () {
                initGameWorld();
                initAnimations();
                units.clear();
                setState(() {});
              },
            ),
          ),
          Expanded(
            child: TCButton(
              buttonLabel: "clear",
              onPressedCallback: () {
                loop?.stop();
                units.clear();
                setState(() {});
              },
            ),
          ),
          Expanded(
            child: TCButton(
              buttonLabel: "stop",
              onPressedCallback: () {
                loop?.stop();
                watch?.stop();
                setState(() {});
              },
            ),
          ),
          Expanded(
            child: TCButton(
              buttonLabel: "resume",
              onPressedCallback: () {
                loop?.forward();
                watch?.start();
                setState(() {});
              },
            ),
          ),
        ],
      );

  String getCurrentScoreString() => getCurrentScore().toString();
  int getCurrentScore() =>
      (((highScore.inMicroseconds / 500).floor() + 1) * 10);

  Positioned renderScore() => Positioned(
      top: 0,
      left: 0,
      right: 0,
      child: RepaintBoundary(
        child: GestureDetector(
          onTap: () {
            if (!kReleaseMode) {
              setState(() {
                // showDebug = !showDebug;
              });
            }
          },
          child: ValueListenableBuilder(
            valueListenable: gameEnded,
            builder: (context, value, child) => Stack(
              children: [
                Column(
                  children: [
                    Container(height: 42),
                    Text(
                      "SCORE",
                      style: TextStyle(letterSpacing: 5.0),
                    ),
                    TweenAnimationBuilder<int>(
                      duration: Duration(milliseconds: 900),
                      tween: IntTween(begin: 0, end: personalHighscore.value),
                      builder: (context, tweenValue, child) => Text(
                        FlutterI18n.translate(
                            context, "modules.bouncy_ball.personal_best",
                            translationParams: {"points": "$tweenValue"}),
                        style: TextStyle(
                            letterSpacing: 0.0,
                            fontWeight: FontWeight.w700,
                            color: CorporateColors.tinyCampusBlue),
                      ),
                      child: Container(),
                    ),
                    AnimatedBuilder(
                      animation: loop ?? ValueNotifier<bool>(false),
                      builder: (context, child) => Column(
                        children: [
                          Text(
                            getCurrentScoreString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 70, fontWeight: FontWeight.w900),
                          ),
                        ],
                        // Text(player.pos.toString()),
                        // Text(player.vel.toString()),
                        // Text(player.jumping.toString()),
                        // Text(player.jumped.toString()),
                        // Text(player.doubleJumped.toString()),
                        // Text(spawnInterval.toString()),
                        // Text(spawnCounter.toString()),
                        // Text("TimeFactor: $timeFactor"),
                      ),
                    ),
                  ],
                ),
                ValueListenableBuilder(
                  valueListenable: state,
                  builder: (context, value, child) => AnimatedContainer(
                    curve: Curves.easeInOut,
                    duration: buttonTransition,
                    // duration: Duration(milliseconds: 76),
                    margin: const EdgeInsets.only(top: 20),
                    child: ValueListenableBuilder(
                      valueListenable: tables,
                      builder: (context2, value2, child2) => upperWidgetState(),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ));

  Widget jumpButtonWidget() => SafeArea(
        bottom: true,
        child: AnimatedSize(
            alignment: Alignment.bottomCenter,
            curve: Curves.easeOut,
            duration: buttonTransition,
            child: lowerWidgetState()),
      );

  Widget bounceButton() => Row(children: [
        Expanded(
            child: GestureDetector(
          onTapDown: (a) {
            player.requestJump();
          },
          child: AbsorbPointer(
            child: AnimatedBuilder(
              animation:
                  Listenable.merge([player.jumping, player.doubleJumped]),
              builder: (context, child) => AnimatedContainer(
                duration: buttonTransition,
                curve: Curves.easeInOut,
                height: gameEnded.value ? 0 : null,
                color: const Color(0xffE8AE2F),
                key: const Key("hi_hi"),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TCButton(
                    customHeight: 80,
                    key: const Key("hi_hi2"),
                    buttonLabel: player.doubleJumped.value
                        ? ""
                        : player.jumping.value
                            ? "Double Bounce"
                            : "Bounce",
                    backgroundColor: player.doubleJumped.value
                        ? CurrentTheme().themeData.backgroundColor
                        : CurrentTheme().themeData.colorScheme.secondary,
                    noAnimationDuration: false,
                    onPressedCallback: player.doubleJumped.value ? null : () {},
                  ),
                ),
              ),
            ),
          ),
        )),
      ]);

  Widget determineButton() {
    switch (state.value) {
      case PossibleGameEndStates.gameRunning:
        return Container(key: Key("bounceButton"), child: bounceButton());
      case PossibleGameEndStates.nothingNew:
        return Container(
            key: Key("nothingNewButtons"), child: nothingNewButtons());
      case PossibleGameEndStates.sendScore:
        return Container(key: Key("sendScoreButton"), child: sendScoreButton());
      case PossibleGameEndStates.showHighScores:
        return Container(
            key: Key("nothingNewButtons"), child: nothingNewButtons());
      // TODO: Handle this case.
      default:
        return Container();
    }
  }

  Widget sendScoreButton() => ValueListenableBuilder(
        valueListenable: tables,
        builder: (context, value, child) => value == null
            ? Container()
            : Container(
                color: const Color(0xffE8AE2F),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AnimatedBuilder(
                    animation: Listenable.merge([controller, sendScoreBlocked]),
                    builder: (context, child) => TCButton(
                      customHeight: 80,
                      key: const Key("send_2"),
                      buttonLabel: FlutterI18n.translate(
                        context,
                        "common.actions.send",
                      ),
                      backgroundColor:
                          CurrentTheme().themeData.colorScheme.secondary,
                      onPressedCallback: controller.text.isEmpty ||
                              sendScoreBlocked.value
                          ? null
                          : () async {
                              sendScoreBlocked.value = true;
                              try {
                                tables.value =
                                    await BouncyBallHighScoreManager()
                                        .sendNewHighScore(
                                            controller.text, getCurrentScore());
                                sendScoreBlocked.value = false;
                                showPersonal = true;
                                state.value =
                                    PossibleGameEndStates.showHighScores;
                              } on Exception catch (e) {
                                eprint("Could not send highscore, try again");
                                eprint(e);
                                sendScoreBlocked.value = false;
                              }
                            },
                    ),
                  ),
                ),
              ),
      );

  Widget nothingNewButtons() => Column(
        children: [
          Container(
            color: const Color(0xffE8AE2F),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ValueListenableBuilder(
                valueListenable: state,
                builder: (context, value, child) => TCButton(
                  customHeight: 80,
                  key: const Key("scores"),
                  buttonLabel: FlutterI18n.translate(
                      context,
                      value == PossibleGameEndStates.showHighScores
                          ? "modules.bouncy_ball.close_highscore"
                          : "modules.bouncy_ball.show_highscore"),
                  foregroundColor:
                      CurrentTheme().themeData.colorScheme.secondary,
                  borderColor: CurrentTheme().themeData.colorScheme.secondary,
                  borderThickness: 3,
                  backgroundColor: Theme.of(context).primaryColor,
                  onPressedCallback: value ==
                          PossibleGameEndStates.showHighScores
                      ? () {
                          state.value = PossibleGameEndStates.nothingNew;
                        }
                      : () {
                          showPersonal = true;
                          state.value = PossibleGameEndStates.showHighScores;
                        },
                ),
              ),
            ),
          ),
          Container(
            color: const Color(0xffE8AE2F),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TCButton(
                customHeight: 80,
                key: const Key("restart"),
                buttonLabel: FlutterI18n.translate(
                  context,
                  "modules.bouncy_ball.new_game",
                ),
                backgroundColor: CurrentTheme().themeData.colorScheme.secondary,
                onPressedCallback: () {
                  initAnimations();
                  initGameWorld();
                  setState(() {});
                },
              ),
            ),
          ),
        ],
      );

  Widget lowerWidgetState() => AnimatedBuilder(
        animation: Listenable.merge([gameEnded, state]),
        builder: (context, child) => Column(
          children: [
            AnimatedSwitcher(
              duration: buttonTransition,
              child: determineButton(),
              layoutBuilder: (currentChild, previousChildren) => Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  ...previousChildren,
                  currentChild ?? Container(),
                ],
              ),
              transitionBuilder: (child, animation) => FadeTransition(
                  child: Container(child: child),
                  opacity: Tween<double>(begin: 0.0, end: 1.0).animate(
                    CurvedAnimation(
                      parent: animation,
                      curve: Interval(0.7, 1.0),
                    ),
                  )),
            ),
          ],
        ),
      );

  Widget upperWidgetState() {
    switch (state.value) {
      case PossibleGameEndStates.nothingNew:
        return Container(
          margin: EdgeInsets.only(top: 160),
          child: Row(
            children: [
              Expanded(
                child: ValueListenableBuilder(
                  valueListenable: hasRankings,
                  builder: (context, value, child) => hasRankings.value
                      ? Container()
                      : Text(
                          FlutterI18n.translate(
                              context, "modules.bouncy_ball.did_not_make_it"),
                          style: Theme.of(context).textTheme.headline3,
                          textAlign: TextAlign.center,
                        ),
                ),
              ),
            ],
          ),
        );
      case PossibleGameEndStates.sendScore:
        return HighScoreTable(
          sendScore: true,
          showPersonal: true,
          controller: controller,
          close: () {
            // initAnimations();
            // initGameWorld();
            // setState(() {});
            state.value = PossibleGameEndStates.nothingNew;
          },
          tables: tables.value,
          globalPosition: isInTable(
              getCurrentScore(), tables.value?.global ?? [],
              modifier: 1),
          recentPosition: isInTable(
              getCurrentScore(), tables.value?.recent ?? [],
              modifier: 1),
          points: getCurrentScore(),
          personalHighscore: personalHighscore.value,
        );
      case PossibleGameEndStates.showHighScores:
        return HighScoreTable(
          sendScore: false,
          showPersonal: showPersonal,
          controller: controller,
          close: () {
            // initAnimations();
            // initGameWorld();
            // setState(() {});
            state.value = PossibleGameEndStates.nothingNew;
          },
          tables: tables.value,
          globalPosition: isInTable(
            getCurrentScore(),
            tables.value?.global ?? [],
          ),
          recentPosition: isInTable(
            getCurrentScore(),
            tables.value?.recent ?? [],
          ),
          points: getCurrentScore(),
          personalHighscore: personalHighscore.value,
        );
      default:
        return Container();
    }
  }

  void endState() {
    final currentScore = getCurrentScore();
    final newPersonalBest = isNewPB(currentScore);
    if (newPersonalBest ||
        isInTable(currentScore, tables.value?.recent) != -1 ||
        isInTable(currentScore, tables.value?.global) != -1) {
      if (newPersonalBest) {
        personalHighscore.value = currentScore;
      }
      hasRankings.value = true;
      showPersonal = true;
      state.value = PossibleGameEndStates.sendScore;
      return;
    }
    state.value = PossibleGameEndStates.nothingNew;
  }

  bool isNewPB(int points) => points > personalHighscore.value;

  int isInTable(int points, List<Score>? table, {int modifier = 0}) {
    if (table == null) return -1;
    var place =
        (table.indexWhere((element) => element.score < points)) + modifier;

    if (place == -1 + modifier) {
      place = -1;
    }
    if (table.length < 10 && place == -1) {
      place = table.length + modifier;
    }
    return place;
  }

  /*int _hasRank(List<Score> table) =>
      (table.indexWhere((element) => element.isSelf) + 1);*/
  // Widget renderPlayer({int milDelay, double opacity}) => AnimatedPositioned(
  //       duration:
  //           milDelay == null ? Duration.zero : Duration(milliseconds:
  // milDelay),
  //       left: player.pos.x - player.hitBox / 2,
  //       bottom: player.pos.y - player.hitBox / 2,
  //       height: player.hitBox,
  //       width: player.hitBox,
  //       child: RepaintBoundary(
  //         child: Transform(
  //           origin: Offset(player.hitBox / 2, player.hitBox / 2),
  //           transform: Matrix4.skewY(-player.vel.g / 15),
  //           child: Opacity(
  //             opacity: opacity ?? 1.0,
  //             child: AnimatedContainer(
  //               duration: Duration(milliseconds: 76),
  //               decoration: BoxDecoration(
  //                 color: player.touches.isNotEmpty ? Colors.red :
  // Colors.cyan,
  //                 shape: BoxShape.circle,
  //                 border: Border.all(
  //                   width: 4,
  //                   color: Colors.black,
  //                   style: BorderStyle.solid,
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ),
  //       ),
  //     );
}

enum PossibleGameEndStates {
  gameRunning,
  nothingNew,
  sendScore,
  showHighScores,
}
