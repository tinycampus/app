/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';

class HighScoreTextField extends StatefulWidget {
  final TextEditingController controller;

  HighScoreTextField({Key? key, required this.controller}) : super(key: key);

  @override
  _HighScoreTextFieldState createState() => _HighScoreTextFieldState();
}

class _HighScoreTextFieldState extends State<HighScoreTextField> {
  String _text = "";

  String letterRegexp = '[a-zA-ZäÄöÖüÜß]';

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      if (widget.controller.text.length > 5) {
        widget.controller.text = _text;
        widget.controller.selection = TextSelection.fromPosition(
            TextPosition(offset: widget.controller.text.length));
      } else {
        _text = widget.controller.text;
      }
    });
  }

  @override
  Widget build(BuildContext context) => Container(
        color: CurrentTheme().passiveIconColor,
        child: TextFormField(
          controller: widget.controller,
          textAlign: TextAlign.center,
          textCapitalization: TextCapitalization.characters,
          style: CurrentTheme().sportTextLabel,
          decoration: InputDecoration(
            counterText: "",
            errorStyle: TextStyle(
              fontSize: 18,
              color: Theme.of(context).textTheme.caption?.color,
            ),
            hintText: FlutterI18n.translate(
              context,
              "modules.bouncy_ball.your_initials",
            ),
            border: InputBorder.none,
          ),
          inputFormatters: [
            FilteringTextInputFormatter.allow(
              RegExp(letterRegexp),
            ),
          ],
          onChanged: (value) {
            if (value.length > 5) {
              widget.controller.text = value.substring(0, min(5, value.length));
            }
          },
        ),
      );
}
