// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'conditions_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Conditions _$ConditionsFromJson(Map json) => Conditions(
      current: json['current'] as int,
      needed: json['needed'] as int,
    );

Map<String, dynamic> _$ConditionsToJson(Conditions instance) =>
    <String, dynamic>{
      'current': instance.current,
      'needed': instance.needed,
    };
