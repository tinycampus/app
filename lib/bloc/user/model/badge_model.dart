/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'conditions_model.dart';

part 'badge_model.g.dart';

/// Model of the Badge
@JsonSerializable(anyMap: true, explicitToJson: true)
class Badge extends Equatable {
  /// Id for this specific Badge
  final int badgeId;

  /// Path to Flare asset
  final String asset;

  /// Level of animation
  final String animationLevel;

  /// Title of the Badge
  final String title;

  /// Text specifying achievement unlock requirements
  final String description;

  /// Category Badge belongs to
  final String category;

  /// Date if it's unlocked, null if it's still locked
  final DateTime? unlockDate;

  /// Condition to achieve this Badge
  final List<Conditions> conditions;

  /// Constructor for this
  const Badge({
    required this.badgeId,
    required this.asset,
    required this.animationLevel,
    required this.title,
    required this.description,
    required this.category,
    required this.unlockDate,
    required this.conditions,
  });

  const Badge.placeholder()
      : badgeId = -1,
        asset = "badge_placeholder.flr",
        animationLevel = "level1",
        title = "Verdiene weitere Badges",
        description = "Siehe nach, welche Aufgaben du noch erledigen kannst.",
        category = 'Placeholder',
        unlockDate = null,
        conditions = const [];

  factory Badge.fromJson(Map<String, dynamic> json) => _$BadgeFromJson(json);

  Map<String, dynamic> toJson() => _$BadgeToJson(this);

  @override
  List<Object?> get props => [
        badgeId,
        asset,
        animationLevel,
        title,
        description,
        category,
        unlockDate,
        conditions
      ];
}
