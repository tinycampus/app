// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map json) => User(
      displayName: json['displayName'] as String,
      userName: json['userName'] as String,
      level: json['level'] as int,
      totalHearts: json['totalHearts'] as int,
      hearts: Map<String, int>.from(json['hearts'] as Map),
      badges: (json['badges'] as List<dynamic>)
          .map((e) => Badge.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      verified: json['verified'] as bool,
      emailSent: json['emailSent'] as bool,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'displayName': instance.displayName,
      'userName': instance.userName,
      'level': instance.level,
      'totalHearts': instance.totalHearts,
      'hearts': instance.hearts,
      'badges': instance.badges.map((e) => e.toJson()).toList(),
      'verified': instance.verified,
      'emailSent': instance.emailSent,
    };
