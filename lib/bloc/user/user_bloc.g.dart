// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserState _$UserStateFromJson(Map json) => UserState(
      User.fromJson(Map<String, dynamic>.from(json['user'] as Map)),
      $enumDecodeNullable(_$UserErrorEnumMap, json['errorType']) ??
          UserError.none,
      json['errorMsg'] as String? ?? "",
    );

Map<String, dynamic> _$UserStateToJson(UserState instance) => <String, dynamic>{
      'user': instance.user.toJson(),
      'errorType': _$UserErrorEnumMap[instance.errorType],
      'errorMsg': instance.errorMsg,
    };

const _$UserErrorEnumMap = {
  UserError.none: 'none',
  UserError.post: 'post',
  UserError.fetch: 'fetch',
  UserError.read: 'read',
  UserError.write: 'write',
  UserError.offline: 'offline',
};
