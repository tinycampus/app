/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:developer';

import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';

import '../local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../local_user_hyd_bloc/local_user_hyd_storage.dart';
import '../perm_hyd_bloc/perm_hyd_bloc.dart';
import '../perm_hyd_bloc/perm_hyd_storage.dart';
import 'tc_bloc_debug_decision.dart';
import 'tc_bloc_helper.dart';

/// Class for global [Bloc] debugging prints.
class TCBlocObserver extends BlocObserver {
  final TCBlocHelper _helper = TCBlocHelper();

  TCBlocObserver() {
    _initStorages();
    TCBlocDebugDecision.loadDecisionMap();
  }

  /// Provide a shorthand to initialize [HydratedBloc]
  /// and [PermHydBloc]s [Storage]s.
  Future<void> _initStorages() => Future.wait([
        () async {
          HydratedBloc.storage = await HydratedStorage.build(
            storageDirectory: await getTemporaryDirectory(),
          );
        }(),
        () async {
          PermHydBloc.storage = await PermHydStorage.build();
        }(),
        () async {
          LocalUserHydBloc.storage = await LocalUserHydStorage.build();
        }(),
      ]);

  /// Provide debugPrints on every [BlocBase] instantiation.
  @override
  void onCreate(BlocBase bloc) {
    super.onCreate(bloc);
    bloc.debugIfAllowed(
      BlocAction.create,
      "${bloc.toString()}: ${bloc.state.toString()}",
    );
  }

  /// Provide debugPrints on every [Bloc]s [event].
  @override
  void onEvent(Bloc bloc, Object? event) {
    super.onEvent(bloc, event);
    bloc.debugIfAllowed(
      BlocAction.event,
      "${bloc.toString()}: ${event.toString()}",
    );
  }

  /// Provide debugPrints on every [Bloc]s [Change].
  ///
  // Only print this for real [Cubit]s.
  // [Bloc]s will be handled in [onTransition].
  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);

    if (bloc is! Bloc) {
      bloc.debugIfAllowed(
        BlocAction.change,
        "${bloc.toString()}: {\n"
        "\tCurrent: \t${change.currentState}\n"
        "\tNext: \t${change.nextState}}\n"
        "}",
      );

      if (bloc is HydratedMixin ||
          bloc is PermHydMixin ||
          bloc is LocalUserHydMixin) {
        bloc.debugIfAllowed(
          BlocAction.persistenceWriting,
          '${bloc.toString()}: { '
          '${(bloc as HydratedMixin).toJson(change.nextState)?.toString()}'
          ' }',
        );
      }
    }

    _helper.markBlocForReset(bloc);
  }

  /// Provide debugPrints on every [Bloc]s [Transition].
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);

    bloc.debugIfAllowed(
      BlocAction.transition,
      "${bloc.toString()}: {\n"
      "\tCurrent: \t${transition.currentState}\n"
      "\tEvent: \t\t${transition.event}\n"
      "\tNext: \t\t${transition.nextState}\n"
      "}",
    );

    if (bloc is HydratedBloc) {
      bloc.debugIfAllowed(
        BlocAction.persistenceWriting,
        '${bloc.toString()}: { '
        '${bloc.toJson(transition.nextState)?.toString()}'
        ' }',
      );
    }
  }

  /// Provide debugPrints on every [Cubit]s [error]
  /// and throw the error for [Catcher].
  @override
  void onError(BlocBase bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);

    log(
      "${bloc.toString()}: ${error.runtimeType}",
      time: DateTime.now(),
      name: "[${DateTime.now().toString()} "
          "| TCBlocDelegate "
          "| ${BlocAction.error.asString()}]",
      error: error,
      stackTrace: stacktrace,
    );

    throw BlocUnhandledErrorException(bloc, error, stacktrace);
  }

  /// Provide debugPrints on every [Cubit]s closing.
  @override
  void onClose(BlocBase bloc) {
    super.onClose(bloc);

    bloc.debugIfAllowed(
      BlocAction.close,
      "${bloc.toString()}: ${bloc.state.toString()}",
    );
  }

  /// Provide a shorthand to clear [PermHydStorage] and [HydratedStorage].
  ///
  /// To prevent [Storage]s to remain closed after this,
  /// re-instantiate [TCBlocObserver] and publish in [Bloc.observer].
  Future<void> clearStorages() async => await Future.wait([
        HydratedBloc.storage.clear(),
        PermHydBloc.storage.clear(),
      ]).then((_) {
        _helper.resetCubits();
        _helper.resetHydratedCubits();
        _helper.resetPermHydCubits();
      }).then((_) => Bloc.observer = TCBlocObserver());

  /// Provide a shorthand to clear [LocalUserHydStorage].
  ///
  /// To prevent storages to remain closed after this,
  /// re-instantiate [TCBlocObserver] and publish in [Bloc.observer].
  Future<void> clearLocalUserStorage() async =>
      await Future.wait([LocalUserHydBloc.storage.clear()])
          .then((_) => _helper.resetLocalUserHydCubits())
          .then((_) => Bloc.observer = TCBlocObserver());
}
