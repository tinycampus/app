/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

import '../../modules/business_cards/v_card/v_card_bloc.dart';
import '../../modules/cafeteria/bloc/cafeteria_bloc.dart';
import '../../modules/cafeteria/bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';
import '../../modules/cafeteria/bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../modules/important_links/bloc/bookmark_links/bookmark_links_bloc.dart';
import '../../modules/organizer/blocs/o_calendar_bloc/o_calendar_bloc.dart';
import '../../modules/organizer/blocs/o_color_mapping_bloc/o_color_mapping_bloc.dart';
import '../../modules/organizer/blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../modules/organizer/blocs/o_program_configuration_bloc/o_program_configuration_bloc.dart';
import '../../modules/organizer/blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../modules/organizer/blocs/o_settings_bloc/o_settings_bloc.dart';
import '../../modules/practice_phase/bloc/pp_model/pp_model_bloc.dart';
import '../../modules/practice_phase/bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../../modules/qanda/bloc/category/category_bloc.dart';
import '../../modules/sport/bloc/settings/exercise_settings_bloc.dart';
import '../../pages/home/bloc/home_bloc.dart';
import '../../pages/timeline/bloc/entries/timeline_bloc.dart';
import '../local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../perm_hyd_bloc/perm_hyd_bloc.dart';

/// WARNING! The Code below implements a "quick and dirty" fix for global cubit
/// states for resetting when logging the user out. For a List of global cubits
/// please see [TCBLoCProvider].

const Map<Type, dynamic> _kResolverMap = {
  // Basic Blocs:
  CafeteriaFeedbackBloc: CafeteriaFeedbackResetEvent(),
  CategoryBloc: CategoryResetEvent(),

  // Hydrated Blocs:
  TimelineBloc: TimelineResetEvent(),
  ExerciseSettingsBloc: ExerciseSettingsResetEvent(),

  // PermHyd Blocs:
  HomeBloc: HomeResetEvent(),
  PpModelBloc: PpModelResetEvent(),
  PpUserProgressBloc: UpbResetEvent(),
  VCardBloc: VCardResetEvent(),

  // LocalUserHyd Blocs:
  OProgramConfigurationBloc: OProgramConfigurationResetEvent(),
  OLessonSelectionBloc: OLessonSelectionResetEvent(),
  OColorMappingBloc: OColorMappingResetEvent(),
  OCalendarBloc: OCalendarResetEvent(),
  OProgramHistoryBloc: OProgramHistoryResetEvent(),
  OSettingsBloc: OSettingsResetEvent(),
  CafeteriaBloc: CafeteriaResetEvent(),
  CafeteriaSettingsBloc: CafeteriaSettingsResetEvent(),
  BookmarkLinksBloc: BookmarkLinksResetEvent(),
};

class TCBlocHelper {
  final Set<BlocBase> _blocs = {};
  final Set<HydratedMixin> _hydBlocs = {};
  final Set<PermHydMixin> _permHydBlocs = {};
  final Set<LocalUserHydMixin> _localUserHydBlocs = {};

  void markBlocForReset(BlocBase bloc) {
    if (bloc is LocalUserHydMixin) {
      _localUserHydBlocs.add(bloc);
    } else if (bloc is PermHydMixin) {
      _permHydBlocs.add(bloc);
    } else if (bloc is HydratedMixin) {
      _hydBlocs.add(bloc);
    } else {
      _blocs.add(bloc);
    }
  }

  void resetCubits() => _blocs.forEach(_resetCubit);

  void resetHydratedCubits() => _hydBlocs.forEach(_resetCubit);

  void resetPermHydCubits() => _permHydBlocs.forEach(_resetCubit);

  void resetLocalUserHydCubits() => _localUserHydBlocs.forEach(_resetCubit);

  void _resetCubit(BlocBase bloc) {
    if (_kResolverMap.containsKey(bloc.runtimeType)) {
      bloc is Bloc
          ? bloc.add(_kResolverMap[bloc.runtimeType])
          : (_kResolverMap[bloc.runtimeType] as VoidCallback)();
    }
  }
}
