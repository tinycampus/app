/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'auth_bloc.dart';

/// Abstract class which provides a private [performAction] Method
/// for all subclasses.
abstract class AuthEvent extends Equatable {
  AuthState _getNextState(AuthBloc bloc);

  Future<void> _performAction(AuthBloc bloc);

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [];
}

/// Subclass of [AuthEvent], defines an app start. Stars loading the necessary
/// information from the AuthBloc and returns an [AuthLoadingState]
class AppStartedEvent extends AuthEvent {
  @override
  AuthLoadingState _getNextState(AuthBloc bloc) => AuthLoadingState();

  @override
  Future<void> _performAction(AuthBloc bloc) async {
    var hasToken = await bloc._tokenRepo.hasToken;
    if (hasToken) {
      try {
        bloc._tokenRepo.refreshAccessToken();
        bloc.add(AuthLoggedInEvent());
      } on OfflineException catch (_) {
        // TODO: offline state?
        bloc.add(AuthLoggedInEvent());
      }
    } else {
      bloc.add(AuthLoggedOutEvent());
    }
  }
}

/// Subclass of [AuthEvent], defines a register event.
/// information from the AuthBloc and returns an [AuthLoadingState].
/// A [_displayName] must be provided.
/// An appropriate Bloc hast to be provided for the [_getNextState] method.
class AuthRegisterEvent extends AuthEvent {
  final String _displayName;
  final String username;
  final String password;
  final bool shouldUpgrade;

  AuthRegisterEvent(
    this._displayName, {
    this.shouldUpgrade = false,
    this.username = "",
    this.password = "",
  });

  @override
  AuthLoadingState _getNextState(AuthBloc bloc) => AuthLoadingState();

  @override
  Future<void> _performAction(AuthBloc bloc) async {
    final Map<String, dynamic> result = jsonDecode((await bloc._httpRepo
            .post(
              authRegisterUrl,
              headers: {
                HttpHeaders.contentTypeHeader: "application/json;charset=utf-8"
              },
              body: jsonEncode(<String, String>{'displayName': _displayName}),
              secured: false,
            )
            .timeout(Duration(seconds: 8)))
        .body);

    final recoveryCode = result["recoveryCode"];

    await bloc._tokenRepo.saveRecovery(
      recoveryCode: recoveryCode,
    );
    sprint(result.toString(), caller: AuthRegisterEvent);

    if (shouldUpgrade) {
      bloc.add(
        AuthTokenEvent(
          recoveryCode: recoveryCode,
          username: username,
          password: password,
        ),
      );
    }
  }

  @override
  List<Object> get props => [_displayName, username, password, shouldUpgrade];
}

class AuthTokenEvent extends AuthEvent {
  final String recoveryCode;
  final String username;
  final String password;

  AuthTokenEvent({
    required this.recoveryCode,
    this.username = '',
    this.password = '',
  });

  @override
  AuthLoadingState _getNextState(AuthBloc bloc) => AuthLoadingState();

  @override
  Future<void> _performAction(AuthBloc bloc) async {
    try {
      var token = await bloc._tokenRepo.recovery(recoveryCode);

      await bloc._tokenRepo.persistToken(token);
      bloc._tokenRepo
        ..persistDateReceivedRefreshToken()
        ..persistDateReceivedAccessToken();
      sprint(token);
      if (username.isNotEmpty && password.isNotEmpty) {
        bloc.add(AuthUpgradeEvent(username, password));
      } else {
        bloc.add(AuthLoggedInEvent());
      }
    } on Exception catch (e) {
      bloc.add(AuthErrorEvent._persistTokenFailed(e.toString()));
    }
  }

  @override
  List<Object> get props => [username, password];
}

class AuthRecoverEvent extends AuthEvent {
  final String _recoveryCode;

  AuthRecoverEvent(this._recoveryCode);

  @override
  AuthLoadingState _getNextState(AuthBloc bloc) => AuthLoadingState();

  @override
  Future<void> _performAction(AuthBloc bloc) async {
    try {
      var token = await bloc._tokenRepo.recovery(_recoveryCode);
      await bloc._tokenRepo.persistToken(token);
      await bloc._tokenRepo.saveRecovery(
        recoveryCode: _recoveryCode,
      );
      await bloc._tokenRepo.persistDateReceivedAccessToken();
      await bloc._tokenRepo.persistDateReceivedRefreshToken();
      bloc.add(AuthLoggedInEvent());
    } on BadRequestException catch (e) {
      bloc.add(AuthErrorEvent._credentialsIncorrect(e.toString()));
    } on Exception catch (e) {
      bloc.add(AuthErrorEvent._persistTokenFailed(e.toString()));
    }
  }

  @override
  List<Object> get props => [_recoveryCode];
}

class AuthUpgradeEvent extends AuthEvent {
  final String _email;
  final String _password;

  AuthUpgradeEvent(this._email, this._password);

  @override
  AuthLoadingState _getNextState(AuthBloc bloc) => AuthLoadingState();

  @override
  Future<void> _performAction(AuthBloc bloc) async {
    try {
      final response = await bloc._httpRepo
          .post(
            authUpgradeUrl,
            body: jsonEncode({'email': _email, 'password': _password}),
            secured: true,
          )
          .timeout(Duration(seconds: 8));
      sprint(response.toString(), style: PrintStyle.attention);
      bloc.add(AuthLoggedInEvent());
    } on BadRequestException catch (e) {
      if (e.message.contains("email_already_in_use")) {
        bloc.add(AuthErrorEvent._emailAlreadyInUse(e.toString()));
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
      bloc.add(AuthErrorEvent._upgradeUser(e.toString()));
    }
  }

  @override
  List<Object> get props => [_email, _password];
}

/// Subclass of [AuthEvent], capable of starting the login process.
/// A [_username] and [_password] must be provided.
/// Changes the State to [AuthLoadingState]
/// An appropriate Bloc hast to be provided for the [_getNextState] method.
class AuthLoginEvent extends AuthEvent {
  final String _username;
  final String _password;

  AuthLoginEvent(this._username, this._password);

  @override
  AuthLoadingState _getNextState(AuthBloc bloc) => AuthLoadingState();

  @override
  Future<void> _performAction(AuthBloc bloc) async {
    final token = await bloc._tokenRepo.authenticate(
      username: _username,
      password: _password,
    );
    await bloc._tokenRepo.persistToken(token);
    await bloc._tokenRepo.persistDateReceivedRefreshToken();
    await bloc._tokenRepo.persistDateReceivedAccessToken();
    bloc.add(AuthLoggedInEvent());
    sprint(token);
  }

  @override
  List<Object> get props => [_username, _password];
}

class AuthResetPasswordEvent extends AuthEvent {
  final String _username;

  AuthResetPasswordEvent(this._username);

  @override
  AuthLoadingState _getNextState(AuthBloc bloc) => AuthLoadingState();

  @override
  Future<void> _performAction(AuthBloc bloc) async {
    var url = "$authResetUrl${"/$_username"}";
    url = Uri.encodeFull(url);
    final result = await bloc._httpRepo
        .post(url, secured: false)
        .timeout(Duration(seconds: 8));
    sprint(result.statusCode.toString());
    bloc.add(AuthSuccessfulResetPwEmailSentEvent());
  }

  @override
  List<Object> get props => [_username];
}

class AuthSuccessfulResetPwEmailSentEvent extends AuthEvent {
  AuthSuccessfulResetPwEmailSentEvent();

  @override
  AuthResetEmailSentState _getNextState(AuthBloc bloc) =>
      AuthResetEmailSentState();

  @override
  Future<void> _performAction(AuthBloc bloc) async {}

  @override
  List<Object> get props => [];
}

/// Subclass of [AuthEvent] which tells the [UserBloc] to fetch the user data.
/// Changes the State to [AuthenticatedState]
/// An appropriate Bloc hast to be provided for the [_getNextState] method.
class AuthLoggedInEvent extends AuthEvent {
  @override
  AuthenticatedState _getNextState(AuthBloc bloc) => AuthenticatedState();

  @override
  Future<void> _performAction(AuthBloc bloc) async =>
      bloc._userBloc.add(UserFetchEvent());
}

/// Subclass of the [AuthEvent] capable of starting the logout process.
/// Changes the State to [AuthLoadingState].
/// An appropriate Bloc hast to be provided for the [_getNextState] method.
class AuthLogoutEvent extends AuthEvent {
  final bool deleteSecure;
  final bool deleteBlocs;
  final bool deleteUserData;
  final bool deleteSharedPrefs;

  final TCBlocObserver observer = Bloc.observer as TCBlocObserver;

  AuthLogoutEvent({
    this.deleteSecure = true,
    this.deleteBlocs = true,
    this.deleteUserData = false,
    this.deleteSharedPrefs = false,
  });

  @override
  UnauthenticatedState _getNextState(AuthBloc bloc) =>
      UnauthenticatedState.fromState(bloc.state);

  @override
  Future<void> _performAction(AuthBloc bloc) async {
    try {
      await Future.wait<void>([
        if (deleteSecure) FlutterSecureStorage().deleteAll(),
        if (deleteBlocs) observer.clearStorages(),
        if (deleteUserData) observer.clearLocalUserStorage(),
        if (deleteSharedPrefs)
          SharedPreferences.getInstance().then((prefs) => prefs.clear())
      ]);

      bloc.add(AuthLoggedOutEvent());
    } on Exception catch (e) {
      bloc.add(AuthErrorEvent._resettingStorageFailed(e.toString()));
    }
  }

  @override
  List<Object> get props => [
        deleteSecure,
        deleteBlocs,
        deleteUserData,
        deleteSharedPrefs,
      ];
}

///Subclass of the [AuthEvent]
/// Changes the State to [UnauthenticatedState]
/// An appropriate Bloc hast to be provided for the [_getNextState] method.
class AuthLoggedOutEvent extends AuthEvent {
  @override
  UnauthenticatedState _getNextState(AuthBloc bloc) =>
      UnauthenticatedState.fromState(bloc.state);

  @override
  Future<void> _performAction(AuthBloc bloc) async =>
      bloc._userBloc.add(UserLogoutEvent());
}

/// Subclass of the [AuthEvent] which is used for error-handling.
/// Changes the State to [AuthErrorState].
/// An appropriate Bloc hast to be provided for the [_getNextState] method.
class AuthErrorEvent extends AuthEvent {
  final AuthError _errorType;
  final String _errorMsg;

  AuthErrorEvent(this._errorType, this._errorMsg);

  AuthErrorEvent._unknown(this._errorMsg) : _errorType = AuthError.unknown;

  AuthErrorEvent._hasNoToken(this._errorMsg)
      : _errorType = AuthError.hasNoToken;

  AuthErrorEvent._persistTokenFailed(this._errorMsg)
      : _errorType = AuthError.persistTokenFailed;

  AuthErrorEvent._resettingStorageFailed(this._errorMsg)
      : _errorType = AuthError.resettingStorageFailed;

  AuthErrorEvent._registerFailed(this._errorMsg)
      : _errorType = AuthError.registerFailed;

  AuthErrorEvent._registerExistingUser(this._errorMsg)
      : _errorType = AuthError.registerExistingUser;

  AuthErrorEvent._upgradeUser(this._errorMsg)
      : _errorType = AuthError.upgradeUser;

  AuthErrorEvent._authenticateFailed(this._errorMsg)
      : _errorType = AuthError.authenticateFailed;

  AuthErrorEvent._credentialsIncorrect(this._errorMsg)
      : _errorType = AuthError.credentialsIncorrect;

  AuthErrorEvent._connectivity(this._errorMsg)
      : _errorType = AuthError.connectivity;

  AuthErrorEvent._messageFormat(this._errorMsg)
      : _errorType = AuthError.messageFormat;

  AuthErrorEvent._emailNotVerified(this._errorMsg)
      : _errorType = AuthError.emailNotVerified;

  // AuthErrorEvent._emailNotFound(this._errorMsg)
  //     : _errorType = AuthError.emailNotFound;

  AuthErrorEvent._emailAlreadyInUse(this._errorMsg)
      : _errorType = AuthError.emailAlreadyInUse;

  @override
  AuthErrorState _getNextState(AuthBloc bloc) =>
      _errorType == AuthError.connectivity
          ? AuthOfflineState(_errorType, _errorMsg)
          : AuthErrorState(_errorType, _errorMsg);

  @override
  Future<void> _performAction(AuthBloc bloc) async {
    debugPrint(toString());
    if (_errorType == AuthError.refreshExpired) {
      Future(
        () => bloc.add(AuthLogoutEvent()),
      );
    }
  }

  @override
  List<Object> get props => [_errorType, _errorMsg];
}
