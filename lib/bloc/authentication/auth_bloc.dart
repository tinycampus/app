/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/constants/api_constants.dart';
import '../../common/styled_print.dart';
import '../../repositories/http/http_repository.dart';
import '../../repositories/token/token_repository.dart';
import '../tc_bloc_observer/tc_bloc_observer.dart';
import '../user/user_bloc.dart';

part 'auth_event.dart';
part 'auth_state.dart';

/// [AuthBloc] initializes and  governs the access to the Token- and
/// Http-Repositories and provides these for the [UserBloc]
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final TokenRepository _tokenRepo;
  final HttpRepository _httpRepo;
  final UserBloc _userBloc;

  /// The constructor initializes all necessary Fields in the AuthBloc
  /// and furthermore sets the [Token]s  in the http-Repository
  /// Starts the App in an [AuthUninitializedState]
  /// A Future is used to provide an [AppStartedEvent] to this [Bloc]
  AuthBloc()
      : _tokenRepo = TokenRepository(),
        _httpRepo = HttpRepository(),
        _userBloc = UserBloc(),
        super(AuthUninitializedState()) {
    _httpRepo.setAuthFailedCallback(
      (t, e) => add(AuthErrorEvent(t, e.toString())),
    );
    Future(() => add(AppStartedEvent()));
  }

  /// Lets each [AuthEvent.performAction] method handle the business logic
  /// and returns a specific [AuthState] based on the Event
  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    sprint(event.runtimeType.toString(), style: PrintStyle.bloc);
    yield event._getNextState(this);
    _runCriticalActions(event);
  }

  /// Runs the [AuthEvent._performAction],
  /// which may cause [Exception]s to be thrown.
  Future<void> _runCriticalActions(AuthEvent event) async {
    try {
      await event._performAction(this);
    } on FormatException catch (e) {
      add(AuthErrorEvent._messageFormat(e.toString()));
      rethrow;
    } on SocketException catch (e) {
      add(AuthErrorEvent._connectivity(e.toString()));
      rethrow;
    } on TimeoutException catch (e) {
      add(AuthErrorEvent._connectivity(e.toString()));
      rethrow;
    } on PlatformException catch (e) {
      switch (event.runtimeType) {
        case AuthLoginEvent:
          add(AuthErrorEvent._persistTokenFailed(e.toString()));
          break;
        case AuthLogoutEvent:
          add(AuthErrorEvent._resettingStorageFailed(e.toString()));
          break;
        default:
          add(AuthErrorEvent._unknown(e.toString()));
          rethrow;
      }
    } on RefreshTokenExpiredException catch (e) {
      switch (event.runtimeType) {
        case AppStartedEvent:
          add(AuthErrorEvent._authenticateFailed(e.toString()));
          break;
        default:
          add(AuthErrorEvent._unknown(e.toString()));
          rethrow;
      }
    } on IncorrectCredentialsException catch (e) {
      switch (event.runtimeType) {
        case AuthLoginEvent:
          add(AuthErrorEvent._credentialsIncorrect(e.toString()));
          break;
        default:
          add(AuthErrorEvent._unknown(e.toString()));
          rethrow;
      }
    } on BadRequestException catch (e) {
      switch (event.runtimeType) {
        case AuthRegisterEvent:
          if (e.message.contains("already exists")) {
            add(AuthErrorEvent._registerExistingUser(e.toString()));
          } else {
            add(AuthErrorEvent._registerFailed(e.toString()));
          }
          break;
        case AuthResetPasswordEvent:
          // add(AuthErrorEvent._emailNotFound(e.toString()));
          AuthErrorEvent._unknown(e.toString());
          break;
        default:
          add(AuthErrorEvent._unknown(e.toString()));
          rethrow;
      }
    } on UnauthorizedException catch (e) {
      switch (event.runtimeType) {
        case AuthLoginEvent:
          if (e.message.contains("email_not_verified")) {
            add(AuthErrorEvent._emailNotVerified(e.toString()));
          } else {
            add(AuthErrorEvent._unknown(e.toString()));
          }
      }
    } on Exception catch (e) {
      switch (event.runtimeType) {
        case AppStartedEvent:
          add(AuthErrorEvent._hasNoToken(e.toString()));
          break;
        case AuthRegisterEvent:
          add(AuthErrorEvent._registerFailed(e.toString()));
          break;
        case AuthLoginEvent:
          add(AuthErrorEvent._authenticateFailed(e.toString()));
          break;
        case AuthLogoutEvent:
          add(AuthErrorEvent._resettingStorageFailed(e.toString()));
          break;
        default:
          add(AuthErrorEvent._unknown(e.toString()));
          rethrow;
      }
    }
  }

  /// returns the runtime type of the BLoC as a string
  @override
  String toString() => runtimeType.toString();
}
