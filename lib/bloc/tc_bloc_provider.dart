/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../modules/business_cards/v_card/v_card_bloc.dart';
import '../modules/cafeteria/bloc/cafeteria_bloc.dart';
import '../modules/cafeteria/bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';
import '../modules/cafeteria/bloc/cafeteria_opening/cafeteria_opening_bloc.dart';
import '../modules/cafeteria/bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../modules/important_links/bloc/bookmark_links/bookmark_links_bloc.dart';
import '../modules/organizer/blocs/o_calendar_bloc/o_calendar_bloc.dart';
import '../modules/organizer/blocs/o_color_mapping_bloc/o_color_mapping_bloc.dart';
import '../modules/organizer/blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../modules/organizer/blocs/o_loading_bloc/o_loading_bloc.dart';
import '../modules/organizer/blocs/o_program_configuration_bloc/o_program_configuration_bloc.dart';
import '../modules/organizer/blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../modules/organizer/blocs/o_settings_bloc/o_settings_bloc.dart';
import '../modules/practice_phase/bloc/pp_model/pp_model_bloc.dart';
import '../modules/practice_phase/bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../modules/qanda/bloc/answer/answer_bloc.dart';
import '../modules/qanda/bloc/category/category_bloc.dart';
import '../modules/qanda/bloc/edit/edit_bloc.dart';
import '../modules/qanda/bloc/question/question_bloc.dart';
import '../modules/sport/bloc/settings/exercise_settings_bloc.dart';
import '../pages/home/bloc/home_bloc.dart';
import '../pages/timeline/bloc/entries/timeline_bloc.dart';
import '../pages/timeline/helper/http_helper.dart';
import '../repositories/http/http_repository.dart';
import 'authentication/auth_bloc.dart';
import 'user/user_bloc.dart';

class TCBLoCProvider extends StatelessWidget {
  final Widget child;

  const TCBLoCProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) => MultiBlocProvider(
        providers: [
          // Basic Blocs:
          BlocProvider<AuthBloc>(
            create: (_) => AuthBloc(),
          ),
          BlocProvider<QuestionBloc>(
            create: (_) => QuestionBloc(),
          ),
          BlocProvider<AnswerBloc>(
            create: (_) => AnswerBloc(),
          ),
          BlocProvider<EditBloc>(
            create: (_) => EditBloc(),
          ),
          BlocProvider<OLoadingBloc>(
            create: (_) => OLoadingBloc(),
          ),
          BlocProvider<CafeteriaFeedbackBloc>(
            create: (_) => CafeteriaFeedbackBloc(),
          ),
          BlocProvider<CategoryBloc>(
            create: (_) => CategoryBloc(),
          ),

          // Hydrated Blocs:
          BlocProvider<TimelineBloc>(
            create: (context) => TimelineBloc(
              thh: BlocHttpHelper(
                httpRepo: RepositoryProvider.of<HttpRepository>(context),
              ),
            ),
          ),

          // PermHydrated Blocs:
          BlocProvider<UserBloc>(
            create: (_) => UserBloc(),
          ),
          BlocProvider<HomeBloc>(
            create: (_) => HomeBloc(),
          ),
          BlocProvider<PpModelBloc>(
            create: (_) => PpModelBloc(),
          ),
          BlocProvider<PpUserProgressBloc>(
            create: (_) => PpUserProgressBloc(),
          ),
          BlocProvider<CafeteriaOpeningBloc>(
            create: (_) => CafeteriaOpeningBloc(),
          ),
          BlocProvider<VCardBloc>(
            create: (_) => VCardBloc(),
          ),

          // LocalUser Blocs:
          BlocProvider<OProgramConfigurationBloc>(
            create: (_) => OProgramConfigurationBloc(),
          ),
          BlocProvider<OLessonSelectionBloc>(
            create: (_) => OLessonSelectionBloc(),
          ),
          BlocProvider<OColorMappingBloc>(
            create: (_) => OColorMappingBloc(),
          ),
          BlocProvider<OCalendarBloc>(
            create: (_) => OCalendarBloc(),
          ),
          BlocProvider<OProgramHistoryBloc>(
            create: (_) => OProgramHistoryBloc(),
          ),
          BlocProvider<OSettingsBloc>(
            create: (_) => OSettingsBloc(),
          ),
          BlocProvider<CafeteriaBloc>(
            create: (_) => CafeteriaBloc(),
          ),
          BlocProvider<CafeteriaSettingsBloc>(
            create: (_) => CafeteriaSettingsBloc(),
          ),
          BlocProvider<BookmarkLinksBloc>(
            create: (_) => BookmarkLinksBloc(),
          ),
          BlocProvider<ExerciseSettingsBloc>(
              create: (context) => ExerciseSettingsBloc()),
        ],
        child: child,
      );
}
