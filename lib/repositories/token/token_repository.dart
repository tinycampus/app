/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../common/constants/api_constants.dart';
import '../../common/styled_print.dart';
import '../http/http_repository.dart';

part 'token_model.dart';
part 'token_repository.g.dart';

// FIXME
final _client = kReleaseMode ? dotenv.env['OAUTH2_CLIENT_ID'] : "client";
final _secret = kReleaseMode ? dotenv.env['OAUTH2_CLIENT_SECRET'] : "secret";

const _printPrefix = "TokenRepository._fetchNewToken";

const tokenKey = "token";
const dateReceivedAccessTokenKey = "date_received_access_token";
const dateReceivedRefreshTokenKey = "date_received_refresh_token";

/// Codec used to en- and decode from String to String
final Codec<String, String> stringToBase64 = utf8.fuse(base64);

/// Repository used to read, write and delete the Authentication Tokens
/// from the local storage.
class TokenRepository {
  /// Singleton instance to be retrieved.
  static final TokenRepository _singleton = TokenRepository._internal();

  /// Factory constructor to return singleton instance.
  factory TokenRepository() => _singleton;

  /// Keychain to persist token and login info to.
  final FlutterSecureStorage _storage;

  /// HttpRepository to wrap every call.
  static final HttpRepository _httpRepo = HttpRepository();

  /// Actual constructor to initiate this.
  TokenRepository._internal() : _storage = FlutterSecureStorage();

  /// Primitive Mutex for locking [refreshAccessToken] to one execution.
  static final ValueNotifier<bool> _isRefreshing = ValueNotifier<bool>(false);

  Future<void> saveRecovery({required String recoveryCode}) =>
      _storage.write(key: 'recovery', value: recoveryCode);

  Future<Token> recovery(String recoveryCode) =>
      _fetchFirstToken(authRecoveryUrl, recoveryCode);

  Future<String?> get recoveryCode => _storage.read(key: "recovery");

  Future<Token> _fetchFirstToken(String url, String recovery) async {
    try {
      final response = await _httpRepo.post(
        '$url/$recovery',
        headers: {
          HttpHeaders.contentTypeHeader: "application/json;charset=utf-8",
        },
        secured: false,
      );
      return Token.fromJson(jsonDecode(response.body));
    } on Exception catch (e) {
      debugPrint("$_printPrefix: encountered Exception: ${e.toString()}");
      rethrow;
    }
  }

  /// Authentication method which uses [username] and [password]
  /// to fetch an appropriate token from the Backend
  Future<Token> authenticate({
    required String username,
    required String password,
  }) =>
      _fetchNewToken(
        authFetchTokenUrl,
        {
          "grant_type": "password",
          "username": username,
          "password": password,
        },
      );

  ///Deletes the current [Token] from the local storage
  Future<void> deleteToken() async {
    eprint("Deleting Token", caller: TokenRepository);
    await _storage.delete(key: tokenKey);
    await _storage.delete(key: dateReceivedAccessTokenKey);
    await _storage.delete(key: dateReceivedRefreshTokenKey);
  }

  /// Saves the provided [Token] to the local Storage
  Future<void> persistToken(Token token) =>
      _storage.write(key: tokenKey, value: jsonEncode(token.toJson()));

  /// Saves the [DateTime] when the last [accessToken] was received
  Future<void> persistDateReceivedAccessToken() => _storage.write(
      key: dateReceivedAccessTokenKey, value: DateTime.now().toIso8601String());

  /// Reads the [DateTime] when the last [accessToken] was received
  Future<String?> readDateReceivedAccessToken() => _storage.read(
        key: dateReceivedAccessTokenKey,
      );

  /// Saves the [DateTime] when the last [refreshToken] was received
  Future<void> persistDateReceivedRefreshToken() => _storage.write(
      key: dateReceivedRefreshTokenKey,
      value: DateTime.now().toIso8601String());

  /// Reads the [DateTime] when the last [refreshToken] was received
  Future<String?> readDateReceivedRefreshToken() => _storage.read(
        key: dateReceivedRefreshTokenKey,
      );

  /// Removes the [DateTime] when the last [accessToken] was received
  Future<void> removeDateReceivedRefreshToken() async =>
      await _storage.delete(key: dateReceivedRefreshTokenKey);

  /// Removes the [DateTime] when the last [refreshToken] was received
  Future<void> removeDateReceivedAccessToken() async =>
      await _storage.delete(key: dateReceivedAccessTokenKey);

  /// Removes both received [DateTime] for Access and Refresh token.
  Future<void> removeDatesReceived() async {
    await removeDateReceivedRefreshToken();
    await removeDateReceivedAccessToken();
  }

  /// Returns a boolean indication whether or not the Repository already
  /// has a token or not.
  Future<bool> get hasToken async => (await _readToken()) != null;

  /// Returns the [accessToken] from the local storage
  Future<String> getToken() async => (await _readToken())?.accessToken ?? '';

  Future<Token?> getTokenObject() => _readToken();

  Future<Token?> _readToken() async {
    final json = await _storage.read(key: tokenKey);
    return (json != null) ? Token.fromJson(jsonDecode(json)) : null;
  }

  /// refreshes the current [Token] using the provided [Token.refreshToken]
  /// and saves it to the local storage
  Future<void> refreshAccessToken() async {
    if (_isRefreshing.value) {
      sprint("Already Refreshing", style: PrintStyle.attention);
      final c = Completer<void>();
      final f = c.future;
      _isRefreshing.addListener(c.complete);
      await f;
      _isRefreshing.removeListener(c.complete);
      sprint("Skip Refreshing", style: PrintStyle.attention);
    } else {
      try {
        _isRefreshing.value = true;
        sprint("Refresh Starting", style: PrintStyle.attention);
        final oldToken = await _readToken();
        if (oldToken == null) {
          eprint("Old access and refresh token was not found",
              caller: TokenRepository);
          throw Exception("Old access and refresh token was not found");
        } else {
          Token newToken;
          try {
            newToken = await _fetchNewToken(
              authRefreshTokenUrl,
              {
                "grant_type": "refresh_token",
                "refresh_token": oldToken.refreshToken,
              },
            );
            await persistDateReceivedAccessToken();
            await persistToken(newToken);
          } on RefreshTokenExpiredException {
            eprint("[DELETE TOKEN]", caller: TokenRepository);
            await deleteToken();
            rethrow;
          }
        }
      } on Exception catch (_) {
        rethrow;
      } finally {
        sprint("Refresh finished", style: PrintStyle.attention);
        _isRefreshing.value = false;
      }
    }
  }

  Future<Token> _fetchNewToken(String url, Map<String, String> body) async {
    try {
      final response = await _httpRepo.post(
        url,
        headers: {
          HttpHeaders.authorizationHeader:
              "Basic ${stringToBase64.encode("$_client:$_secret")}",
        },
        body: body,
        secured: false,
      );
      return Token.fromJson(jsonDecode(response.body));
    } on BadRequestException catch (e, s) {
      debugPrint("$_printPrefix: exception encountered: ${e.toString()} \n"
          " ${s.toString()}");
      throw _differentiateGrantType(body);
    } on Exception catch (e, s) {
      debugPrint("$_printPrefix: exception encountered: ${e.toString()} \n"
          " ${s.toString()}");
      rethrow;
    }
  }

  Exception _differentiateGrantType(Map<String, String> body) {
    const grantType = "grant_type";
    const userName = "username";
    const password = "password";
    const refreshToken = "refresh_token";
    String grantInformation;
    Exception result;

    switch (body[grantType]) {
      case password:
        // TODO: provide message
        result = IncorrectCredentialsException('');
        grantInformation = '"$userName": ${body[userName]}, '
            '"$password": ${body[password]}';
        break;
      case refreshToken:
        // TODO: provide message
        result = RefreshTokenExpiredException('');
        grantInformation = '"$refreshToken": ${body[refreshToken]}';
        break;
      default:
        throw ArgumentError("$_printPrefix: "
            "Unsupported $grantType: ${body[grantType]}");
    }

    debugPrint('$_printPrefix: '
        '"grant_type": ${body["grant_type"]}, $grantInformation');

    return result;
  }

  Future<TokenMeta> get tokenMeta async {
    final tokenMeta = TokenMeta();
    await tokenMeta.loadFromStorage();
    return tokenMeta;
  }
}

class TokenMeta {
  DateTime accessTokenReceivedDate = DateTime.fromMillisecondsSinceEpoch(0);
  DateTime accessTokenExpirationDate = DateTime.fromMillisecondsSinceEpoch(0);
  DateTime refreshTokenReceivedDate = DateTime.fromMillisecondsSinceEpoch(0);
  DateTime refreshTokenExpirationDate = DateTime.fromMillisecondsSinceEpoch(0);
  Future<void> loadFromStorage() async {
    try {
      final receivedAccessToken =
          await TokenRepository().readDateReceivedAccessToken();
      if (receivedAccessToken != null) {
        accessTokenReceivedDate = DateTime.parse(receivedAccessToken);
      }
      final receivedRefreshToken =
          await TokenRepository().readDateReceivedRefreshToken();
      if (receivedRefreshToken != null) {
        refreshTokenReceivedDate = DateTime.parse(receivedRefreshToken);
      }
    } on Exception catch (e) {
      eprint(e);
    }
    accessTokenExpirationDate = accessTokenReceivedDate.add(Duration(days: 28));
    refreshTokenExpirationDate =
        refreshTokenReceivedDate.add(Duration(days: 365));
  }

  bool get isAccessTokenExpired =>
      DateTime.now().isAfter(accessTokenExpirationDate);

  bool get isRefreshTokenExpired =>
      DateTime.now().isAfter(refreshTokenExpirationDate);
}
