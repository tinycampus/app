/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'http_repository.dart';

/// An exception cause by an error in [HttpRepository].
/// Thrown when no subclass matches.
class HttpRepoException implements ClientException {
  @override
  final String message;

  /// The URL of the HTTP request or response that failed.
  @override
  final Uri? uri;

  /// The Standard [Exception] thrown by [HttpRepository].
  HttpRepoException(this.message, [this.uri]);

  @override
  String toString() => "${runtimeType.toString()}: "
      "{ message: $message, uri: ${uri.toString()} }";
}

/// Thrown when login fails with bad credentials.
class IncorrectCredentialsException extends HttpRepoException {
  IncorrectCredentialsException(String message, [Uri? uri])
      : super(message, uri);
}

/// Thrown when registration fails.
class RegistrationFailedException extends HttpRepoException {
  RegistrationFailedException(String message, [Uri? uri]) : super(message, uri);
}

/// Thrown when registration fails with user already exists.
class RegistrationUserAlreadyExistsException extends HttpRepoException {
  RegistrationUserAlreadyExistsException(String message, [Uri? uri])
      : super(message, uri);
}

/// Thrown when authentication fails with expired refresh token.
class RefreshTokenExpiredException extends HttpRepoException {
  RefreshTokenExpiredException(String message, [Uri? uri])
      : super(message, uri);
}

/// Thrown when [Response.statusCode] is 400.
class BadRequestException extends HttpRepoException {
  BadRequestException(String message, [Uri? uri]) : super(message, uri);
}

/// Thrown when [Response.statusCode] is 401.
class UnauthorizedException extends HttpRepoException {
  UnauthorizedException(String message, [Uri? uri]) : super(message, uri);
}

/// Thrown when [Response.statusCode] is 403.
class ForbiddenException extends HttpRepoException {
  ForbiddenException(String message, [Uri? uri]) : super(message, uri);
}

/// Thrown when [Response.statusCode] is 404.
class NotFoundException extends HttpRepoException {
  NotFoundException(String message, [Uri? uri]) : super(message, uri);
}

/// Thrown when [Response.statusCode] is 405.
class MethodNotAllowedException extends HttpRepoException {
  MethodNotAllowedException(String message, [Uri? uri]) : super(message, uri);
}

/// Thrown when [Response.statusCode] is 429
class TooManyRequestsException extends HttpRepoException {
  TooManyRequestsException(String message, [Uri? uri]) : super(message, uri);
}

/// Thrown when [Response.statusCode] is 500.
class InternalServerErrorException extends HttpRepoException {
  InternalServerErrorException(String message, [Uri? uri])
      : super(message, uri);
}

/// Thrown when [Response.statusCode] is 501.
class NotImplementedException extends HttpRepoException {
  NotImplementedException(String message, [Uri? uri]) : super(message, uri);
}

/// Thrown when trying to send a [Request] while offline.
class OfflineException extends HttpRepoException {
  OfflineException(String message, [Uri? uri]) : super(message, uri);
}
