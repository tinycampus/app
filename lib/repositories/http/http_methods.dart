/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'http_repository.dart';

/// Base for [HttpRepository] to separate its own logic
/// from the abstraction of [Client]-methods.
///
/// Can't use [extension] here because [Mockito]
/// can only override instance methods.
abstract class _HttpRepoBase {
  /// Interface for implementing the fundamental business logic.
  Future<Response> _retry(_HttpRequest request);

  /// Sends an HTTP HEAD request with the given headers to the given URL, which
  /// can be a [Uri] or a [String].
  Future<Response> head(
    Object url, {
    Map<String, String>? headers,
    bool secured = true,
  }) =>
      _retry(
        _HttpRequest(
          secured: secured,
          method: 'HEAD',
          url: url,
          headers: headers,
        ),
      );

  /// Sends an HTTP GET request with the given headers to the given URL, which
  /// can be a [Uri] or a [String].
  Future<Response> get(
    Object url, {
    Map<String, String>? headers,
    bool secured = true,
  }) async =>
      await _retry(
        _HttpRequest(
          secured: secured,
          method: 'GET',
          url: url,
          headers: headers,
        ),
      );

  /// Sends an HTTP POST request with the given headers and body to the given
  /// URL, which can be a [Uri] or a [String].
  ///
  /// [body] sets the body of the request. It can be a [String], a [List<int>]
  /// or a [Map<String, String>]. If it's a String, it's encoded using
  /// [encoding] and used as the body of the request. The content-type of the
  /// request will default to "text/plain".
  ///
  /// If [body] is a List, it's used as a list of bytes for the body of the
  /// request.
  ///
  /// If [body] is a Map, it's encoded as form fields using [encoding]. The
  /// content-type of the request will be set to
  /// `"application/x-www-form-urlencoded"`; this cannot be overridden.
  ///
  /// [encoding] defaults to [utf8].
  Future<Response> post(
    Object url, {
    Map<String, String>? headers,
    Object body = '',
    Encoding? encoding,
    bool secured = true,
  }) =>
      _retry(
        _HttpRequest(
          secured: secured,
          method: 'POST',
          url: url,
          headers: headers,
          body: body,
          encoding: encoding,
        ),
      );

  /// Sends an HTTP PUT request with the given headers and body to the given
  /// URL, which can be a [Uri] or a [String].
  ///
  /// [body] sets the body of the request. It can be a [String], a [List<int>]
  /// or a [Map<String, String>]. If it's a String, it's encoded using
  /// [encoding] and used as the body of the request. The content-type of the
  /// request will default to "text/plain".
  ///
  /// If [body] is a List, it's used as a list of bytes for the body of the
  /// request.
  ///
  /// If [body] is a Map, it's encoded as form fields using [encoding]. The
  /// content-type of the request will be set to
  /// `"application/x-www-form-urlencoded"`; this cannot be overridden.
  ///
  /// [encoding] defaults to [utf8].
  Future<Response> put(
    Object url, {
    Map<String, String>? headers,
    Object body = '',
    Encoding? encoding,
    bool secured = true,
  }) =>
      _retry(
        _HttpRequest(
          secured: secured,
          method: 'PUT',
          url: url,
          headers: headers,
          body: body,
          encoding: encoding,
        ),
      );

  /// Sends an HTTP PATCH request with the given headers and body to the given
  /// URL, which can be a [Uri] or a [String].
  ///
  /// [body] sets the body of the request. It can be a [String], a [List<int>]
  /// or a [Map<String, String>]. If it's a String, it's encoded using
  /// [encoding] and used as the body of the request. The content-type of the
  /// request will default to "text/plain".
  ///
  /// If [body] is a List, it's used as a list of bytes for the body of the
  /// request.
  ///
  /// If [body] is a Map, it's encoded as form fields using [encoding]. The
  /// content-type of the request will be set to
  /// `"application/x-www-form-urlencoded"`; this cannot be overridden.
  ///
  /// [encoding] defaults to [utf8].
  Future<Response> patch(
    Object url, {
    Map<String, String>? headers,
    Object body = '',
    Encoding? encoding,
    bool secured = true,
  }) =>
      _retry(
        _HttpRequest(
          secured: secured,
          method: 'PATCH',
          url: url,
          headers: headers,
          body: body,
          encoding: encoding,
        ),
      );

  /// Sends an HTTP DELETE request with the given headers to the given URL,
  /// which can be a [Uri] or a [String].
  Future<Response> delete(
    Object url, {
    Map<String, String>? headers,
    bool secured = true,
  }) =>
      _retry(
        _HttpRequest(
          secured: secured,
          method: 'DELETE',
          url: url,
          headers: headers,
        ),
      );

  /// Sends an HTTP GET request with the given headers to the given URL, which
  /// can be a [Uri] or a [String], and returns a Future that completes to the
  /// body of the response as a String.
  ///
  /// The Future will emit a [ClientException] if the response doesn't have a
  /// success status code.
  Future<String> read(
    Object url, {
    Map<String, String>? headers,
    bool secured = true,
  }) async =>
      (await get(url, headers: headers, secured: secured)).body;

  /// Sends an HTTP GET request with the given headers to the given URL, which
  /// can be a [Uri] or a [String], and returns a Future that completes to the
  /// body of the response as a list of bytes.
  ///
  /// The Future will emit a [ClientException] if the response doesn't have a
  /// success status code.
  Future<Uint8List> readBytes(
    Object url, {
    Map<String, String>? headers,
    bool secured = true,
  }) async =>
      (await get(url, headers: headers, secured: secured)).bodyBytes;
}
