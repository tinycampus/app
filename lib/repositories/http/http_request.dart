/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'http_repository.dart';

/// Abstraction class to translate between [HttpRepository] and [Client].
class _HttpRequest {
  final bool secured;
  final String method;

  /// This may only ever be [String] or [Uri]!
  final Object url;
  final Map<String, String> headers;

  /// This may only ever be [String], [List<int>] or [Map<String, String>]!
  final Object? body;
  final Encoding? encoding;

  // TODO: This should set the [late] field [header] via _injectHeaders.
  // That would speed up repeated calls to [_buildRequest].
  _HttpRequest({
    required this.secured,
    required this.method,
    required this.url,
    Map<String, String>? headers,
    this.body,
    this.encoding,
  }) : headers = headers ?? {};

  // TODO: should set this in constructor.
  Future<void> _injectHeaders() async {
    headers[HttpHeaders.authorizationHeader] =
        "Bearer ${await HttpRepository._tokenRepo.getToken()}";
    headers[HttpHeaders.contentTypeHeader] ??= "application/json;charset=utf-8";
    headers[HttpHeaders.acceptHeader] ??= "application/json;charset=utf-8";
  }

  /// Builds a non-streaming [Request].
  /// Has to build a response for ever call to [Client.send].
  Future<Request> _buildRequest() async {
    var request = Request(method, _fromUriOrString(url));

    if (secured) await _injectHeaders();

    request.headers.addAll(headers);
    if (encoding != null) request.encoding = encoding!;
    final localBody = body;
    if (localBody != null) {
      if (localBody is String) {
        request.body = localBody;
      } else if (localBody is List) {
        request.bodyBytes = localBody.cast<int>();
      } else if (localBody is Map) {
        request.bodyFields = localBody.cast<String, String>();
      } else {
        throw ArgumentError('Invalid request body "$localBody".');
      }
    }

    return request;
  }

  Uri _fromUriOrString(dynamic uri) =>
      uri is String ? Uri.parse(uri) : uri as Uri;
}
